# Known issues #

## Current ##
**OSX High Sierra**  
Sometimes a surface isn't drawn in Drive Mode, even after switching scenes and returning. Can't move around the map--nothing.
Seems to happen when launched from a fullscreen terminal (Terminal 2.8).

**OSX High Sierra, Ubutnu16.0.4 in VM Fusion in OS High Sierra**
There are sometimes gaps between tiles that are NOT due to rounding error. (Perhaps a grouting line needs to be drawn?)

**Ubuntu**
Meta key plus certain other keys is captured by the OS first.

**OSX High Sierra**
Edges in grid mode are sometimes missing for tiles nearish the clipping boundary (Sphere inside, for example).


## Partially mitigated ##
**ALL**
There is rounding error at the vertices on the clipping boundary.  Would be better to clip the mesh, and then build the tiles.
Mitigation: we clip several (currently 10) pixels outside the box to lessen the effect.


## Probably not a Klein Drive issue ##
**Ubuntu16.0.4 in VM Fusion in OSX High Sierra**  
Frame rate is 2.5-3 times higher INSIDE than outside the VM. (Is FLTK using the hardware correctly?)

**OSX High Sierra**  
It's many times faster to draw a rectangle than to copy a rectangle from an offscreen buffer. (Is FLTK using the hardware correctly?)


## Presumed fixed, but difficult to test ##
**OSX High Sierra**  
Sometimes when driving near (into?) a singularity (eg a crushed edge) the car vanishes into thin air.  Quiting to menu, and returning to drive mode gets it back. 
Fix: Check and correct NAN values in car's internal state.

