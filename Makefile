# ######################
# 
#  Klein Drive Makefile
#
# ######################

# ******************************************************************************
# 
# Libraries, paths and flags
#
# ******************************************************************************

ifeq (${INCLUDE_PATH},)
  INCLUDE_PATH = /usr/local/include
endif

# Libraries
# ======== 

# FLTK
FLTK_PATH = ${INCLUDE_PATH}/
FLTK_LDSTATIC = $(shell fltk-config --use-gl --use-images --ldstaticflags )
#FLTK_CXX_FLAGS += $(shell fltk-config --cxx --std=c++11) # Commented below, too

# Eigen
EIGEN_PATH = ${INCLUDE_PATH}/eigen3

# Libpng
LIBPNG_LDSTATIC +=  -static-libstdc++ -L/usr/local/Cellar/libpng/1.6.34/lib


# Klein Drive paths
# =================

#Klein Drive Source Directory
KLEINDRIVE_SRC_PATH = .

# Final binary name (in build director)
BIN = kleindrive

# Build Director
BUILD_DIR = ./build

# Documentation
DOCS_DATA   = ./docs
DOCS_TARGET = ./Documentation


# CXX
# ===

CXX = clang++

CXX_VERSION = c++14
#CXX_VERSION = c++1z

CXX_OPTIMIZATION = O2

#DEBUG_FLAGS_GENERIC = -Wall -Wextra -Wpedantic -Wconversion -Wshadow -Wfatal-errors
#DEBUG_FLAGS_SPECIAL = -Werror

# List ofSource Files
# ################## #

CPP = main.cpp
CPP += $(wildcard src/camera/*.cpp) 
CPP += $(wildcard src/cameraoperator/*.cpp)
CPP += $(wildcard src/collections/*.cpp)
CPP += $(wildcard src/collections/cameraoperators/*.cpp)
CPP += $(wildcard src/collections/fonts/*.cpp)
CPP += $(wildcard src/collections/welcomescene/*.cpp)
CPP += $(wildcard src/collections/scenes/*.cpp)
CPP += $(wildcard src/collections/scenes/stdscenes/*.cpp)
CPP += $(wildcard src/features/domainmap/*.cpp)
CPP += $(wildcard src/frontend/*.cpp)
CPP += $(wildcard src/graphics/*.cpp)
CPP += $(wildcard src/gui/*.cpp)
CPP += $(wildcard src/physics/*.cpp)
CPP += $(wildcard src/scene/*.cpp)
CPP += $(wildcard src/scenecollection/*.cpp)
#CPP += $(wildcard src/string/*.cpp)   #This folder is purely template files.
CPP += $(wildcard src/surface/*.cpp)
CPP += $(wildcard src/system/*.cpp)
CPP += $(wildcard src/types/*.cpp)



# *****************************************************************************
#
# make command
#
#******************************************************************************

CXX_FLAGS = -std=$(CXX_VERSION) -$(CXX_OPTIMIZATION) 
CXX_FLAGS += -I$(KLEINDRIVE_SRC_PATH)  

CXX_FLAGS += -I$(FLTK_PATH) 
CXX_FLAGS += -I$(EIGEN_PATH)
#CXX_FLAGS += $(FLTK_CXX_FLAGS)

CXX_FLAGS += $(DEBUG_FLAGS_GENERIC)
CXX_FLAGS += $(DEBUG_FLAGS_SPECIAL)

LDSTATIC = $(FLTK_LDSTATIC)
#LDSTATIC += $(LIBPNG_LDSTATIC)


# All .o files go to build dir.
OBJ = $(CPP:%.cpp=$(BUILD_DIR)/%.o)

# Gcc/Clang will create these .d files containing dependencies.
DEP = $(OBJ:%.o=%.d)

# Default target named after the binary.
$(BIN) : $(BUILD_DIR)/$(BIN)

# Actual target of the binary - depends on all .o files.
$(BUILD_DIR)/$(BIN) : $(OBJ)
	# Create build directories - same structure as sources.
	mkdir -p $(@D)
	# Just link all the object files.
	$(CXX) $(CXX_FLAGS) $^ -o  $@  $(LDSTATIC) 

# Include all .d files
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered
# by calling `-include $(DEP)`.
$(BUILD_DIR)/%.o : %.cpp
	mkdir -p $(@D)
# The -MMD flags additionaly creates a .d file with
# the same name as the .o file
	$(CXX) $(CXX_FLAGS) -MMD -c $< -o  $@ 



# *****************************************************************************
# 
# make commands
# 
# *****************************************************************************

.PHONY : OSX
OSX:
	make INCLUDE_PATH="/usr/local/include" 

.PHONY : Ubuntu
Ubuntu:
	make INCLUDE_PATH="/usr/include"

.PHONY : run
run :
	$(BUILD_DIR)/$(BIN)

.PHONY : docs
docs :
	doxygen $(DOCS_DATA)/Doxyfile


.PHONY : clean
clean :
	# This should remove all generated files.
	-rm $(BUILD_DIR)/$(BIN) $(OBJ) $(DEP)	



.PHONY : clean_docs
clean_docs :
	-rm -rv $(DOCS_TARGET)/*

.PHONY : cppcheck
cppcheck :
	cppcheck --std=c++14 --language=c++ --enable=all -I/Users/Cam/klein/ . 2> ./logs/cppcheck.txt

