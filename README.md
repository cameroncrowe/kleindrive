Klein Drive (Alpha)
===================

Klein Drive is by Cameron Crowe (copyright 2018).

If you have comments, questions, or use Klein Drive in your classroom, I would love to hear from you: [cameroncrowe1@gmail.com](mailto:cameroncrowe1@gmail.com).

Introduction
------------

Klein Drive allows users to navigate around surfaces in 3D space, and develop a feeling for their geometry and curvature.

Klein Drive is currently capable of rendering surfaces obtained from a rectangle by gluing opposing edges (with or without twists) and pinching edges to a point.  This includes Klein Bottle (after which Klein Drive is named), the Mobius Strip, the projective plane (in the form of the Cross Cap, Boy Surface and Roman Surface), the sphere, the torus and others.

In Preview Mode, a user can select among surfaces, and examine them from a distance.  In Drive Mode, the user is placed on a surface and left to move around. Movement on the surface are performed as if one were actually on the surface--moving forward takes the user along a geodesic.

In Navigation mode, Klein Drive displays an overview map, which details users recent movements and coordinate frame. The overview map also details how the surface is constructed from the rectangle.  Users can select from three different views: a generic view (with lines painted on the surface to aid orientation), grid view, and (Gaussian) curvature view.
d

Getting Klein Drive
-------------------

Klein drive has been tested on the following systems. General instructions below.

### Ubutnu (16.0.4) ###

To install all packages and compile Klein Drive to `~/kleindrive`, copy/paste the following code into your terminal.

```
cd ~
sudo apt install git clang libfltk1.3-dev libjpeg62-dev libxinerama-dev libxft-dev libeigen3-dev
git clone https://gitlab.com/cameroncrowe/kleindrive.git 
cd kleindrive
make Ubuntu
```

To run Klein Drive copy/paste the following code into your terminal.

```
cd ~/kleindrive
./build/kleindrive
```

### MacOS High Sierra ###

Run the following commands in a terminal (you may copy/paste them if you wish).

1.  Install [Homebrew](https://brew.sh/)  
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
2.  Install Packages:    
    You may need to run `brew` as root (that is, you may need to replace `brew` with `sudo brew`), and brew may ask you to install XCode Command Line Tools, which you can do by running `xcode-select --install`.
Git
```
brew install git
```
FLTK (Brew may give you further instructions)
```
brew install fltk
```
Eigen3
```
brew install eigen3
```

3. Compile Klein Drive:    
```
cd ~
git clone https://gitlab.com/cameroncrowe/kleindrive.git
cd kleindrive
make OSX
```

4. Run Klein Drive:     
   The klein drive executable is located in the build folder (./kleindrive/build) and may be run with. 
``` 
cd ~/kleindrive
./build/kleindrive
```

### General Instructions ###

1.  Install the following packages:
  * [FLTK v1.3.4](https://fltk.org)
  * [Eigen 3](https://eigen.tuxfamily.org/)

2.  Clone the Klein Drive repository into a directory:
```
git clone https://gitlab.com/cameroncrowe/kleindrive.git <some-directory>
```

3.  Update the make file to include the paths to FLTK and Eigen.  You may also change to your choice of C++14 compilers, but Klein Drive has only been tested with Clang.

4.  Run `make` to compile Klein Drive.  The executable will be located `./build/kleindrive`.
