//==-------------------------------------------------------------*- C++ -*-===//
//
// src/camera/AffineFrame.h - AffineFrame struct definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the AffineFrame struct definitions.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_AFFINEFRAME_H
#define KLEIN_DRIVE_AFFINEFRAME_H

#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Represents a position and orientation in 3-space.
struct AffineFrame final
{
public:
  /// \name Position component.
  /// @{

  /// Position of frame in 3-space.

  Vec3 position;

  /// @} //

  /// \name Orientation components.
  /// @{

  /// Right pointing normal vector.
  Vec3 right;

  /// Down pointing normal vector.
  Vec3 down;

  /// Forward pointing normal vector.
  Vec3 forward;

  /// @}
};

} // namespace kleindrive

#endif // KLEIN_DRIVE_AFFINEFRAME_H
