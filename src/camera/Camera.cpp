//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/Camera.cpp - Camera class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementation of the Camera class.
///
//===----------------------------------------------------------------------===//

#include "src/camera/Camera.h"
#include "src/camera/AffineFrame.h"
#include "src/camera/ZoomPowerSlider.h"
#include "src/camera/NeckTurnFollower.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <iostream>

namespace kleindrive {

namespace {

const phys_t PI = 4 * atan(1);

}

Camera::Camera(phys_t apertureRadius, ZoomPowerSlider zoomPowerSlider)
  : apertureRadius_{ apertureRadius }
  , zoomPowerSlider_{ zoomPowerSlider }
  , horizontalNeckTurnFollower_(0)
  , verticalNeckTurnFollower_(0)
{}

// Be sure to turn off all the Camera properties that need it!
void
Camera::turnOff()
{
  horizontalNeckTurnFollower_.turnOff();
  verticalNeckTurnFollower_.turnOff();
  zoomPowerSlider_.turnOff();
}

// Be sure to reset all the Camera properties that need it!
void
Camera::reset()
{
  horizontalNeckTurnFollower_.reset();
  verticalNeckTurnFollower_.reset();
  zoomPowerSlider_.reset();
}

CameraShotData
Camera::computeCameraShotData(AffineFrame apertureFrame, GameState* gameState)
{
  // Slide the zoom slider.
  Sliding sliding = Sliding::Still;
  if (gameState->camera.isZoomingIn)
    sliding = Sliding::Up;
  if (gameState->camera.isZoomingOut)
    sliding = Sliding::Down;
  if (gameState->camera.isResettingZoom)
    sliding = Sliding::Reset;

  // An increase in zoomPower by one doubles the zoomFactor.
  static const phys_t zoomBase = 2;

  const phys_t zoomPower =
    zoomPowerSlider_.moveSlider(sliding, gameState->time);
  const phys_t zoomFactor = pow(zoomBase, zoomPower);
  const phys_t focalLength = apertureRadius_ * zoomFactor;

  // For finding where we are:
//  std::cout << "==== Camera.cpp =======\n"
//            << " Zoom Power: " << zoomPower << "\n\n";

  // Compute CameraShotData.

  CameraShotData cameraShotData;

  cameraShotData.forwardNormal = apertureFrame.forward;
  cameraShotData.apertureRadius = apertureRadius_;
  cameraShotData.focalLength = focalLength;
  cameraShotData.focalPoint =
    apertureFrame.position - focalLength * apertureFrame.forward;

  cameraShotData.worldToAffineScreen =
    computeWorldToAffineScreen(focalLength, apertureFrame, gameState);

  return cameraShotData;
}

// We break our affine transformation down into several affine transformations.
Aff3
Camera::computeWorldToAffineScreen(phys_t focalLength,
                                   const AffineFrame& apertureFrame,
                                   GameState* gameState)
{
  Aff3 worldToCamera = computeWorldToCamera(focalLength, apertureFrame);
  Aff3 rotateCamera = computeCameraRotation(gameState);
  Aff3 cameraToAffineScreen =
    computeCameraToAffineScreen(focalLength, gameState);

  auto at = cameraToAffineScreen * rotateCamera * worldToCamera;
  return at;
}

// Returns the affine coordinate change
//
//   (x_world, y_world, z_world) --> (x_camera, y_camera, z_camera)
//
Aff3
Camera::computeWorldToCamera(phys_t focalLength,
                             const AffineFrame& apertureFrame)
{
  Aff3 cameraToWorld;

  cameraToWorld.translation() =
    apertureFrame.position - focalLength * apertureFrame.forward;

  cameraToWorld.linear().col(0) = apertureFrame.right;
  cameraToWorld.linear().col(1) = apertureFrame.down;
  cameraToWorld.linear().col(2) = apertureFrame.forward;

  // The linear part of cameraToWorld is orthonormal, so it's transpose is its
  // inverse.

  Aff3 worldToCamera; // Inverse of cameraToWorld.

  worldToCamera.linear() = cameraToWorld.linear().transpose();

  worldToCamera.translation() =
    -worldToCamera.linear() * cameraToWorld.translation();

  return worldToCamera;
}

// Rotate the head left/right or up/down in accordance with the game state.
Aff3
Camera::computeCameraRotation(GameState* gameState)
{
  
  phys_t a = 0;  // Target horizontal deflection.
  phys_t b = 0;  // Vertical deflection.

  // If the gameState wants us to turn, we turn.
  if (gameState->camera.isHeadTemporarilyTurned) {
    gameState->camera.isHeadTemporarilyTurned = false;
    a += gameState->camera.horizontalTurn;
    b -= gameState->camera.verticalTurn;
  }

  // Change the neck turn targets to those we've picked, then smoothly move
  // torward them.
  a = horizontalNeckTurnFollower_.moveFollower(a, gameState->time);
  b = verticalNeckTurnFollower_.moveFollower(b, gameState->time);
  Aff3 at;

  at.translation() << 0,
                      0,
                      0;

  Mat33 horiz;
        horiz << cos(a), 0,    -sin(a),
                 0,      1,    0,
                 sin(a), 0,    cos(a);

  Mat33 vert;
        vert  << 1,      0,        0,
                 0,      cos(b),  -sin(b),
                 0,      sin(b),   cos(b);

  at.linear() = vert * horiz;
  return at;
}

// Returns the affine coordinate change taking
//
//   (x_camera, y_camera, z_camera ) --> z_camera * (x_screen, y_screen, 1)
//
// This latter vector may, of course, also be expressed
//
//   (x_screen * z_camera, y_screen * z_camera, z_camera)
//
Aff3
Camera::computeCameraToAffineScreen(phys_t focalLength, GameState* gameState)
{
  const phys_t W = static_cast<phys_t>(gameState->animatedBox.w);  // Width of box.
  const phys_t H = static_cast<phys_t>(gameState->animatedBox.h);  // Height of box.
  const phys_t R = sqrt(W*W+H*H)/2;                                // Radius of screen.
  const phys_t r = apertureRadius_;                            // Radius of aperture.
  const phys_t f = focalLength;                                // Focal length.
	
  const phys_t S = (f * R) / r;                                // Scale factor

  const phys_t X = W/2 + gameState->animatedBox.x;             // Center of box.
  const phys_t Y = H/2 + gameState->animatedBox.y; 

  // One checks the desired transformation is
  //
  //   S, 0, X, 0
  //   0, S, Y, 0
  //   0, 0, 1, 0
  //   0, 0, 0, 1
  //
  Aff3 cameraToAffineScreen;
  cameraToAffineScreen.linear() << S, 0, X,
                                   0, S, Y,
                                   0, 0, 1;

  cameraToAffineScreen.translation() << 0,
                                        0,
                                        0;

  return cameraToAffineScreen;
}

} // namespace kleindrive
