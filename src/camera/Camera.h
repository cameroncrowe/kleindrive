//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/Camera.h - Camera class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Camera class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_CAMERA_H
#define KLEIN_DRIVE_CAMERA_H

#include "src/camera/AffineFrame.h"
#include "src/camera/CameraShotData.h"
#include "src/camera/ZoomPowerSlider.h"
#include "src/camera/NeckTurnFollower.h"
#include "src/gamestate/GameState.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Represents a camera in 3-space.
class Camera
{
public:
  /// Constructs a Camera.
  ///
  /// By default the neck has no horiztonal or later movemement, and the
  /// zoomPower is fixed at 0, meaning the radius of the aperture and the focal
  /// length are equal.
  Camera(phys_t apertureRadius = 1,
         ZoomPowerSlider zoomPowerSlider = ZoomPowerSlider(0));

  /// Called when the camera needs to be reset next time it is used.
  ///
  /// This keeps it from updating over a very long time step.
  void turnOff();

  /// This resets the Camera to it's default state.
  void reset();

  /// Returns all camera-related data needed to render a frame.
  CameraShotData computeCameraShotData(AffineFrame apertureFrame,
                                       GameState* gameState);

private:
  /// Returns the affine transformation that maps world coordinates to
  /// coordinates convenient for z-buffering.
  ///
  /// This transformation maps
  /// > x_world \n y_world \n z_world
  /// to
  /// > x_screen * z_camera \n y_screen * z_camera \n z_camera
  Aff3 computeWorldToAffineScreen(phys_t focalLength,
                                  const AffineFrame& apertureFrame,
                                  GameState* gameState);

  /// Returns the affine transformation taking world coordinates to camera
  /// coordinates.
  ///
  /// This transformation maps
  /// > x_world \n y_world \n z_world
  /// to
  /// > x_camera \n y_camera \n z_camera
  Aff3 computeWorldToCamera(phys_t focalLength,
                            const AffineFrame& apertureFrame);

  /// Returns the affine transformation that rotates the camera in accordance
  /// with the system state.
  Aff3 computeCameraRotation(GameState* gameState);

  /// Returns the affine transformation of 3-space convenient for z-buffering.
  ///
  /// This affine transformation maps
  /// > x_camera \n y_camera \n z_camera
  /// to
  /// > x_screen*z_camera \n y_screen*z_camera \n z_camera
  Aff3 computeCameraToAffineScreen(phys_t focalLength, GameState* gameState);

  /// The radius of the camera's aperture.
  phys_t apertureRadius_;

  /// Slider controlling the zoom power  Note this is not the same as
  /// magnification.  Zoom controls the focal length to aperture raadius ratio.
  ZoomPowerSlider zoomPowerSlider_;

  NeckTurnFollower horizontalNeckTurnFollower_;
  NeckTurnFollower verticalNeckTurnFollower_;
};
} // namespace kleindrive

#endif // KLEIN_DRIVE_CAMERA_H
