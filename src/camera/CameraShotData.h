//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/CameraShotData.h - CameraShotData class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the decalaration of the CameraShotData class.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_CAMERASHOTDATA_H
#define KLEIN_DRIVE_CAMERASHOTDATA_H

#include "src/camera/AffineFrame.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains all Camera data needed by the renderer.
struct CameraShotData final
{
public:
  /// How wide is the camera lense.
  phys_t apertureRadius;

  /// How far is the aperture from the focal point.
  phys_t focalLength;

  /// Points from the focal point in the direction of the center of the
  /// aperture.
  Vec3 forwardNormal;

  /// It is what it is.
  Vec3 focalPoint;

  /// The affine coordinate change from affine coordinates to "Affine screen"
  /// coordinates.
  ///
  /// See Camera for details.
  Aff3 worldToAffineScreen;
};

} // namespace kleindrive

#endif // KLEIN_DRIVE_CAMERASHOTDATA_H
