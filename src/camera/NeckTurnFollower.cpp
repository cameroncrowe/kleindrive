//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/NeckTurnFollower.cpp - NeckTurnFollower class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the NeckTurnFollower implementation.
///
//===----------------------------------------------------------------------===//

#include "src/camera/NeckTurnFollower.h"
#include "src/physics/DampenedFollower.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

const phys_t NeckTurnFollower::springConstant_ = 5;

const phys_t NeckTurnFollower::timeStep_ = 0.001;

NeckTurnFollower::NeckTurnFollower(phys_t defaultNeckTurn)
  : DampenedFollower{ defaultNeckTurn, DampenedFollower::Specs{ springConstant_, timeStep_ } }
{}

} // namespace kleindrive
