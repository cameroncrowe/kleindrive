//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/NeckTurnFollower.h - NeckTurnFollower class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the NeckTurnFollower definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_NECKTURNFOLLOWER_H
#define KLEIN_DRIVE_NECKTURNFOLLOWER_H

#include "src/physics/DampenedFollower.h"
#include "src/types/ValueTypes.h"
#include <math.h>

namespace kleindrive {

/// Represents a slider controlling neck movements.
class NeckTurnFollower : public DampenedFollower
{
public:
  /// Constructs a neck turn slider with given maximum defelction and default
  /// position.
  ///
  /// Typically the default position should let the user see some bit of the
  /// road.  This seems to mean looking downward slightly.
  NeckTurnFollower(phys_t defaultNeckTurn);

private:
  /// Controls how fast the neck catches up to what it's trying to do.
  static const phys_t springConstant_;

  /// The time granularity the neck turn.
  static const phys_t timeStep_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_NECKTURNFOLLOWER_H
