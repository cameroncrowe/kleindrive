//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/ZoomPowerSlider.cpp - ZoomPowerSlider class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementation of the ZoomPowerSlider
///
//===----------------------------------------------------------------------===//

#include "src/camera/ZoomPowerSlider.h"
#include "src/physics/DampenedSlider.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

const phys_t ZoomPowerSlider::slideSpeed_ = 1;

const phys_t ZoomPowerSlider::springConstant_ = 3;

const phys_t ZoomPowerSlider::timeStep_ = 0.001;

ZoomPowerSlider::ZoomPowerSlider(phys_t fixedZoomPower)
  : DampenedSlider{ DampenedSlider::Specs{ fixedZoomPower,
                                           fixedZoomPower,
                                           fixedZoomPower,
                                           slideSpeed_,
                                           springConstant_,
                                           timeStep_ } }
{}

ZoomPowerSlider::ZoomPowerSlider(phys_t minZoomPower,
                                 phys_t maxZoomPower,
                                 phys_t defaultZoomPower)
  : DampenedSlider{ DampenedSlider::Specs{ minZoomPower,
                                           maxZoomPower,
                                           defaultZoomPower,
                                           slideSpeed_,
                                           springConstant_,
                                           timeStep_ } }
{}

} // namespace kleindrive
