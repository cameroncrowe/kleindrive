//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/camera/ZoomPowerSlider.h - ZoomPowerSlider class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the decalaration of the ZoomPowerSlider
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_ZOOMPOWERSLIDER_H
#define KLEIN_DRIVE_ZOOMPOWERSLIDER_H

#include "src/physics/DampenedSlider.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Represents a slider controlling zoom power.
///
/// The zoom power controls the aperture radius to focal length ratio.
class ZoomPowerSlider : public DampenedSlider
{
public:
  /// Constructs the zoom slider with a fixed zoom power.
  explicit ZoomPowerSlider(phys_t fixedZoomPower);

  /// Constructs a slider with min, max and default zoom power.
  ZoomPowerSlider(phys_t minZoomPower,
                  phys_t maxZoomPower,
                  phys_t defaultZoomPower);

private:
  /// The speed at which the zoom slider slides.
  static const phys_t slideSpeed_;

  /// The constant controlling how fast a the zoomer catches up with the slider.
  static const phys_t springConstant_;

  /// The time granularity of the slider.
  static const phys_t timeStep_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_ZOOMPOWERSLIDER_H
