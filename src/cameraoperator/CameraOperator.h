//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/cameraoperator/CameraOperator.h - CameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CameraOperator class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CAMERAOPERATOR_H
#define KLEINDRIVE_CAMERAOPERATOR_H

#include "src/camera/Camera.h"
#include "src/gamestate/GameState.h"
#include "src/surface/TiledSurface.h"

namespace kleindrive {

/// Represents a camera operator who controls the movement of the camera.
class CameraOperator
{
public:
  /// Returns the current camera position given the input.
  /// pointCamera reactivates the cameraOperator if necessary.
  virtual CameraShotData pointCamera(const TiledSurface& tiledSurface,
                                     GameState* gameState) = 0;

  virtual ~CameraOperator() = default;

  /// This is called by the scene if the camera has been dormant.
  ///
  /// Typically used to reset the internal state of some object, so that it
  /// doesn't try to catch up over a long period.(eg the Car class, or the
  /// DampenedSlider class.
  virtual void turnOff() {}

  /// Reset to original state.
  virtual void reset() {}
};

} // namespace kleindrive

#endif // KLEINDRIVE_CAMERAOPERATOR_H
