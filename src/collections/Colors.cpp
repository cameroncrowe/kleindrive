//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/Colors.h - Color definitions file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains color definitions.
///
//===----------------------------------------------------------------------===//

#include "src/collections/Colors.h"
#include "src/graphics/Color.h"
#include "src/types/ValueTypes.h"
#include <limits>

namespace kleindrive {

/// Colors used in Klein Drive.	
namespace colors { 



namespace {

// Converts unsigned chars rbg channels to a Color.
[[maybe_unused]] Color
rgb2Color(unsigned char R, unsigned char G, unsigned char B) {
  return {R/255., G/255., B/255.};
}

/// Converts a hex rgb color to a Color.
[[maybe_unused]] Color hex2Color(int hex)
{
  phys_t R = ((hex >> 16) & 0xFF) / 255.;
  phys_t G = ((hex >> 8)  & 0xFF) / 255.;
  phys_t B = ( hex        & 0xFF) / 255.;
  return {R,G,B};
}


/// Generates the invisible color.
Color
invisibleColor()
{
  Color color = { 0, 0, 0};
  color.isVisible = false;
  return color;
}

} // namespace

const Color Black      = {0, 0, 0};
const Color Yellow     = {1, 1, 0};

const Color Blue       = hex2Color(0x00919C);
const Color Brown      = hex2Color(0xE38E50);
const Color Green      = hex2Color(0xCED75C);
const Color Purple     = hex2Color(0x805462);
const Color Red        = hex2Color(0xE43E22);
const Color White      = hex2Color(0xFFF2EC);

const Color HardPurple = affineCombination(Purple, Green, 0.7);
const Color HardRed    = affineCombination(Red,    Green, 0.3);
const Color HardWhite  = affineCombination(White,  Green, 0.7);

const Color WarmPurple = affineCombination(Purple, Yellow, 0.7);
const Color WarmRed    = affineCombination(Red,    Yellow, 0.7);
const Color WarmWhite  = affineCombination(White,  Yellow, 0.7);

const Color DarkRed    = affineCombination(Red, Black, 0.8);


const Color Invisible = invisibleColor();

} // namespace colors

} // namespace kleindrive
