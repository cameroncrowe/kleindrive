//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/Colors.h - Color definitions file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains color definitions.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_COLORS_H
#define KLEINDRIVE_COLORS_H

#include "src/graphics/Color.h"
#include "src/types/ValueTypes.h"
#include <limits>

namespace kleindrive {

/// Colors used in Klein Drive.
namespace colors {

/// This is for tiles only.  Tiles of this color are not drawn.
extern const Color Invisible;

extern const Color Black;
extern const Color White;

extern const Color Blue;
extern const Color Brown;
extern const Color Green;
extern const Color Purple;
extern const Color Red;
extern const Color Yellow;

extern const Color HardPurple;
extern const Color HardWhite;
extern const Color HardRed;

extern const Color WarmRed;
extern const Color WarmPurple;
extern const Color WarmWhite;

extern const Color DarkRed;


} // namespace colors

} // namespace kleindrive

#endif // KLEINDRIVE_COLORS_H
