//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/CarCameraOperator.cpp
//
//  CarCameraOperator class implentation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CarCameraOperator class implementation.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/camera/Camera.h"
#include "src/camera/CameraShotData.h"
#include "src/gamestate/GameState.h"
#include "src/physics/Car.h"
#include "src/surface/TiledSurface.h"

namespace kleindrive {
namespace cameraoperators {
namespace {

const phys_t PI = 4 * atan(1);

} // namespace

CarCameraOperator::CarCameraOperator(Car car,
                                     phys_t apertureRadius,
                                     phys_t cameraHeight,
                                     phys_t defaultZoomPower)
  : CameraOperator()
  , car_{ car }
  , camera_{ apertureRadius, ZoomPowerSlider(-3, 3, defaultZoomPower) }
  , cameraHeight_{ cameraHeight }
{}

void
CarCameraOperator::turnOff() 
{
  car_.turnOff();
  camera_.turnOff();
}

void
CarCameraOperator::reset() 
{
  car_.reset();
  camera_.reset();
}

CameraShotData
CarCameraOperator::pointCamera(const TiledSurface& tiledSurface,
                               GameState* gameState)
{
  // Drive the car.
  const auto carState = car_.driveCar(tiledSurface, gameState);

  // Update position in gameState.
  gameState->car.position = carState.position;
  gameState->car.heading = carState.heading;

  // This is useful to find out where you are when you're trying to figure out
  // where to start a scene.
  /*
  std::cout << "\n================="
            << "\nCAR CAMERA";
  std::cout << "\nposition " << carState.position.transpose();
  std::cout << "\nheading  " << carState.heading << "\n";
  */
  // Contruct an affine Frame from the car's position and heading.

  const phys_t u = carState.position(0);
  const phys_t v = carState.position(1);
  const phys_t t = gameState->time;
  
  // Turn the head up and down using the mouse.
  //const phys_t xMouse = gameState->mouse.x;
  const phys_t yMouse = gameState->mouse.y;
  //const phys_t xScreen = gameState->animatedBox.x;
  const phys_t yScreen = gameState->animatedBox.y;
  //const phys_t wScreen= std::max(gameState->animatedBox.w,1);
  const phys_t hScreen = std::max(gameState->animatedBox.h,1);

  // (xRaw, yRaw) is an affine change of the mouse from a point on the the
  // AnimatedBox to a point on the square
  //
  //   [-1,1]x[-1,1]
  //
  // with x, and y pointing right and up, respectively.

  //const phys_t xRaw = (xMouse - xScreen - wScreen / 2) / (wScreen / 2);
  const phys_t yRaw =
    (hScreen - (yMouse - yScreen) - hScreen / 2) / (hScreen / 2);

  //const phys_t A = 1;  // Horizontal constant;
  const phys_t B = 1.2;  // Vertical constant;

  //const phys_t x = 0;
  //((xRaw > 0) ? 1 : -1) *
  //  pow(std::abs(xRaw), A); // Make a nice big fat low movement zone in the
                            // middle, exaggerate as you go out.
  const phys_t y =
    ((yRaw > 0) ? 1 : -1) *
    pow(std::abs(yRaw), B); // Make a nice big fat low movement zone in the
                            // middle, exaggerate as you go out.

  //const phys_t maxHorizontalTurn = PI / 4;
  const phys_t maxVerticalTurn = PI / 4;

  //const phys_t horizontalTurn = maxHorizontalTurn * x;
  const phys_t verticalTurn = maxVerticalTurn * y;

  gameState->camera.isHeadTemporarilyTurned = true;
  //gameState->camera.horizontalTurn =  horizontalTurn;
  gameState->camera.verticalTurn = verticalTurn;

    const Vec2 carForward = Vec2(cos(carState.heading), sin(carState.heading));

  AffineFrame apertureFrame;

  // Apply chain rule to get forward vector.
  apertureFrame.forward = (tiledSurface.f_u(u, v, t) * carForward(0) +
                           tiledSurface.f_v(u, v, t) * carForward(1))
                            .normalized();

  apertureFrame.down = -tiledSurface.f_n(u, v, t);

  apertureFrame.right = apertureFrame.down.cross(apertureFrame.forward);

  // Position the correct height above the surface.
  apertureFrame.position = tiledSurface.f(u, v, t) +
                           -cameraHeight_ * apertureFrame.down -
                           cameraBack_ * apertureFrame.forward;

  // ZoomLens can build a camera from a frame.
  return camera_.computeCameraShotData(apertureFrame, gameState);
}

} // namespace cameraoperators

} // namespace kleindrive
