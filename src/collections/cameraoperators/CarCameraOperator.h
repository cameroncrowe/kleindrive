//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/CarCameraOperator.h
//  CarCameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CarCameraOperator class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CARCAMERAOPERATOR_H
#define KLEINDRIVE_CARCAMERAOPERATOR_H

#include "src/camera/Camera.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/gamestate/GameState.h"
#include "src/physics/Car.h"
#include "src/physics/DampenedSlider.h"
#include "src/surface/TiledSurface.h"
#include <iostream>

namespace kleindrive {

namespace cameraoperators {

/// Represents a camera operator that moves around via car.
class CarCameraOperator : public CameraOperator
{
public:
  /// Sets the car according to the given initial data.
  CarCameraOperator(Car car,
                    phys_t apertureRadius,
                    phys_t cameraHeight,
                    phys_t defaultZoomPower = -1);

  /// Turn off the parts of the car camera that shouldn't be left idling.
  virtual void turnOff() final override;

  /// Reset the car camera to it's original state.
  virtual void reset() final override;

protected:
  /// A car.
  Car car_;

  /// A camera.
  Camera camera_;

  /// How high does the camera sit from off the surface.
  phys_t cameraHeight_;

  /// How far back does the camera sit form the position of the car on the
  /// surface.  This should be far enough back that we can see the edge of the
  /// surface when we are right up against it.
  phys_t cameraBack_;

  /// How fast can we go before the our neck turns back to the default.
  phys_t maxSpeedNeckStaysTurned_;

private:
  /// Use the system state to move the camera around the surface as if on a car.
  virtual CameraShotData pointCamera(const TiledSurface& tiledSurface,
                                     GameState* gameState) final override;
};

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_CARCAMERAOPERATOR_H
