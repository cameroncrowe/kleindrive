//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/CylindricalCameraOperator.cpp
//
//  CylindricalCameraOperator class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the CylindricalCamera class impementation.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/collections/cameraoperators/CylindricalCameraOperator.h"
#include "src/camera/AffineFrame.h"
#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"

namespace kleindrive {

namespace cameraoperators {

namespace {

const phys_t PI = 4 * atan(1);

} // namespace

CylindricalCameraOperator::CylindricalCameraOperator(Camera camera,
                                                     phys_t radius,
                                                     phys_t minHeight,
                                                     phys_t maxHeight)
  : MouseCameraOperator(camera, -PI, PI, minHeight, maxHeight)
  , radius_{ radius }
  , centerHeight_{ (maxHeight + minHeight) / 2 }
{}

AffineFrame
CylindricalCameraOperator::computeAffineFrame(phys_t x, phys_t y, GameState*)
{
  static const phys_t horizontalWrapFactor = 1.1;
  static const phys_t verticalOvershootFactor = 1.1;

  x *= horizontalWrapFactor;
  y *= verticalOvershootFactor;

  AffineFrame apertureFrame;

  apertureFrame.position << radius_ * Vec3(cos(x), sin(x), y);
  apertureFrame.forward << -cos(x), -sin(x), centerHeight_ - y;
  apertureFrame.right << -sin(x), cos(x), 0;
  apertureFrame.down = apertureFrame.forward.cross(apertureFrame.right);

  return apertureFrame;
}

} // cameraoperators

} // namespace kleindrive
