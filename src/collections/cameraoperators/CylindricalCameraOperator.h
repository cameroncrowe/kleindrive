//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/CylindricalCameraOperator.h
//
//  CylindricalCameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CylindricalCamera class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CYLINDRICALCAMERAOPERATOR_H
#define KLEINDRIVE_CYLINDRICALCAMERAOPERATOR_H

#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"

namespace kleindrive {

namespace cameraoperators {

/// Represents a camera operator that moves around via car.
class CylindricalCameraOperator : public MouseCameraOperator
{
public:
  /// Sets the car according to the given initial data.
  CylindricalCameraOperator(Camera camera,
                            phys_t radius,
                            phys_t minHeight,
                            phys_t maxHeight);

private:
  /// Computes the affine frame based on a cylindrical coordinate system.
  virtual AffineFrame computeAffineFrame(phys_t x,
                                         phys_t y,
                                         GameState*) final override;

  /// The radius of the cylinder of the coordinate system.
  phys_t radius_;

  /// The center of the mid of the cylindar of our coordinate system.
  phys_t centerHeight_;
};

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_CYLINDRICALCAMERAOPERATOR_H
