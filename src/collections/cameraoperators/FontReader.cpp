//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/FontReader.cpp
//
//  FontReader class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the FontReader class definition.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/FontReader.h"
#include "src/camera/Camera.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/gamestate/GameState.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

namespace cameraoperators {

/// Sets the car according to the given initial data.
FontReader::FontReader(phys_t distanceFromSurface,
                       phys_t scrollSpeed,
                       Camera camera)
  : CameraOperator()
  , camera_{ camera }
  , distanceFromSurface_{ distanceFromSurface }
  , scrollSpeed_{ scrollSpeed }
{}

void
FontReader::turnOff() 
{
  camera_.turnOff();
}

void
FontReader::reset() 
{
  camera_.reset();
}

CameraShotData
FontReader::pointCamera(const TiledSurface& tiledSurface, GameState* gameState)
{

  // Scrolling along facing the strip.
  static const phys_t uCenter = tiledSurface.uMin() + tiledSurface.uRange() / 2;
  static const phys_t vMin = tiledSurface.vMin();
  static const phys_t vRange = tiledSurface.vRange();

  const phys_t v = std::fmod(vMin + scrollSpeed_ * gameState->time, 2 * vRange);

  const phys_t u = uCenter;
  const phys_t t = gameState->time;

  AffineFrame apertureFrame;

  apertureFrame.position =
    tiledSurface.f(u, v, t) + distanceFromSurface_ * tiledSurface.f_n(u, v, t);

  apertureFrame.forward = -tiledSurface.f_n(u, v, t);
  apertureFrame.right = tiledSurface.f_v(u, v, t).normalized();
  apertureFrame.down = apertureFrame.forward.cross(apertureFrame.right);

  return camera_.computeCameraShotData(apertureFrame, gameState);
}

} // cameraoperators

} // namespace kleindrive
