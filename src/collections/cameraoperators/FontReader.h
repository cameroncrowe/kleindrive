//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/FontReader.h
//
//  FontReader class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the FontReader class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_FONTREADER_H
#define KLEINDRIVE_FONTREADER_H

#include "src/camera/Camera.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/gamestate/GameState.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

namespace cameraoperators {

/// Represents a camera operator that moves around via car.
class FontReader : public CameraOperator
{
public:
  /// Sets the car according to the given initial data.
  FontReader(phys_t distanceFromSurface, phys_t scrollSpeed, Camera camera);

  /// Turns off the camera operator when not in use.
  virtual void turnOff() final override;

  /// Resets the font reader to its original state.
  virtual void reset() final override;

private:
  /// Use the system state to move the camera around the surface as if on a car.
  virtual CameraShotData pointCamera(const TiledSurface& tiledSurface,
                                     GameState* gameState) final override;

  /// A camera.
  Camera camera_;

  /// How far does the camera stand away from the surface.
  phys_t distanceFromSurface_;

  /// How fast does the camera pan down the surface.
  phys_t scrollSpeed_;

};

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_FONTREADER_H
