//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/LookoutCameraOperator.cpp
//
//  LookoutCameraOperator class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the LookoutCameraOperator class implementation.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/camera/AffineFrame.h"
#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"
#include <iostream>

namespace kleindrive {

namespace cameraoperators {

LookoutCameraOperator::LookoutCameraOperator(Camera camera,
                                             AffineFrame affineFrame,
                                             phys_t minHorizontalDeflection,
                                             phys_t maxHorizontalDeflection,
                                             phys_t minVerticalDeflection,
                                             phys_t maxVerticalDeflection)
  : MouseCameraOperator(camera,
                        minHorizontalDeflection,
                        maxHorizontalDeflection,
                        minVerticalDeflection,
                        maxVerticalDeflection)
  , affineFrame_{ affineFrame }
{}

AffineFrame
LookoutCameraOperator::computeAffineFrame(phys_t x,
                                          phys_t y,
                                          [[maybe_unused]]GameState* gameState)
{
  // I don't see a perfect way of meshing with the camera's is headturned state,
  // so we turn the camera's frame ourselves.

  Mat33 horiz;
        horiz << cos(x), 0,    sin(x),
                 0,      1,    0,
                -sin(x), 0,    cos(x);

  Mat33 vert;
        vert  << 1,      0,        0,
                 0,      cos(y),  -sin(y),
                 0,      sin(y),   cos(y);

  const Mat33 total = vert * horiz;

  Mat33 frame;
        frame.col(0) = affineFrame_.right;
        frame.col(1) = affineFrame_.down;
        frame.col(2) = affineFrame_.forward;

  const Mat33 rotatedFrame = frame * total;

  AffineFrame rotatedAffineFrame;

  rotatedAffineFrame.position = affineFrame_.position;
  rotatedAffineFrame.right    = rotatedFrame.col(0);
  rotatedAffineFrame.down     = rotatedFrame.col(1);
  rotatedAffineFrame.forward  = rotatedFrame.col(2);

  return rotatedAffineFrame;
}

} // cameraoperators

} // namespace kleindrive
