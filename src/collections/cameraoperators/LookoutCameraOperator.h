//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/LookoutCameraOperator.h
//
//  LookoutCameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the LookoutCameraOperator class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_LOOKOUTCAMERAOPERATOR
#define KLEINDRIVE_LOOKOUTCAMERAOPERATOR

#include "src/camera/AffineFrame.h"
#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"

namespace kleindrive {

namespace cameraoperators {

/// Sits at a given position and moves the camera around.
class LookoutCameraOperator : public MouseCameraOperator
{
public:
  /// Constructs a camera with default position affineFrame, that can move left,
  /// right, up and down within the given parameters.
  LookoutCameraOperator(Camera camera,
                        AffineFrame affineFrame,
                        phys_t minHorizontalDeflection,
                        phys_t maxHorizontalDeflection,
                        phys_t minVerticalDeflection,
                        phys_t maxVerticalDeflection);

private:
  // Simply enable the camera to move itself around within the bounds, and
  // return the fixed affine frame.
  virtual AffineFrame computeAffineFrame(phys_t x,
                                         phys_t y,
                                         GameState* gameState) final override;

  /// The default affine frame position for the camera.
  AffineFrame affineFrame_;

}; // class LookoutCameraOperator

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_LOOKOUTCAMERAOPERATOR
