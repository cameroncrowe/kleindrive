//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/MouseCameraOperator.cpp
//
//  MouseCameraOperator class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the MouseCamera class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/MouseCameraOperator.h"
#include "src/camera/Camera.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/gamestate/GameState.h"
#include "src/physics/DampenedSlider.h"
#include "src/surface/TiledSurface.h"

namespace kleindrive {

namespace cameraoperators {

MouseCameraOperator::MouseCameraOperator(Camera camera,
                                         phys_t xMin,
                                         phys_t xMax,
                                         phys_t yMin,
                                         phys_t yMax)
  : CameraOperator()
  , camera_{ camera }
  , xMin_{ xMin }
  , xRange_{ xMax - xMin }
  , yMin_{ yMin }
  , yRange_{ yMax - yMin }
{}

void
MouseCameraOperator::turnOff()
{
  camera_.turnOff();
}

void
MouseCameraOperator::reset() 
{
  camera_.reset();
}

CameraShotData
MouseCameraOperator::pointCamera(const TiledSurface&, GameState* gameState)
{
  // Compute the mouse position relative to the animated window.  Note that
  // if the mouse is outside the animation box, then the mouse position will
  // be outside the bounds.  Deal with it.
  const phys_t X = gameState->mouse.x - gameState->animatedBox.x,
               Y = gameState->mouse.y - gameState->animatedBox.y,
               W = gameState->animatedBox.w, H = gameState->animatedBox.h;

  const phys_t x = (X * xRange_ / W + xMin_);
  const phys_t y = ((H - Y) * yRange_ / H + yMin_);

  return camera_.computeCameraShotData(computeAffineFrame(x, y, gameState),
                                       gameState);
}

} // cameraoperators

} // namespace kleindrive
