//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/MouseCameraOperator.h
//
//  MouseCameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MouseCameraOperator class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MOUSECAMERAOPERATOR_H
#define KLEINDRIVE_MOUSECAMERAOPERATOR_H

#include "src/camera/Camera.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/gamestate/GameState.h"
#include "src/physics/DampenedSlider.h"
#include "src/surface/TiledSurface.h"

namespace kleindrive {

namespace cameraoperators {

/// Represents a camera operator who positions the camera relative to the mouse
/// position.
class MouseCameraOperator : public CameraOperator
{
public:
  /// Sets the car according to the given initial data.
  MouseCameraOperator(Camera camera,
                      phys_t xMin,
                      phys_t xMax,
                      phys_t yMin,
                      phys_t yMax);

  /// How does the mouse input translate to an affine frame defining the
  /// camera's position.  x and y will be within the parameters in the
  /// constructor, unless the mouse is outside the animatedBox window.
  virtual AffineFrame computeAffineFrame(phys_t x,
                                         phys_t y,
                                         GameState* gameState) = 0;

private:
  /// Turns off the camera operator.  Can be overwritten, but the camera still
  /// needs to be turned off.
  virtual void turnOff() override final;

  /// Resets the camera operator to its initial state.  Can be overwritten, but
  /// the camera still needs to be reset.
  virtual void reset() override final;

  /// Compute an affine based on mouse data and the virtual computeAffineFrame
  /// function.
  CameraShotData pointCamera(const TiledSurface&,
                             GameState* gameState) final override;

  /// The camera.
  Camera camera_;

  /// Min x values that will be passed to computeAffineFrame.
  phys_t xMin_;

  /// Range of x values passed to computeAffineFrame.
  phys_t xRange_;

  /// Min y value passed to computeAffineFrame.
  phys_t yMin_;

  /// Range of y values passed to computeAffineFrame.
  phys_t yRange_;

};

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_MOUSECAMERAOPERATOR
