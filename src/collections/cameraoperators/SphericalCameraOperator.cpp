//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/SphericalCameraOperator.cpp
//
//  SphericalCameraOperator class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SphericalCamera class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/camera/AffineFrame.h"
#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"

namespace kleindrive {

namespace cameraoperators {

namespace {

// Pi!
static const phys_t PI = 4 * atan(1);

}

SphericalCameraOperator::SphericalCameraOperator(Camera camera, phys_t radius)
  : MouseCameraOperator(camera, -PI, PI, -PI / 2, PI / 2)
  , radius_{ radius }
{}

AffineFrame
SphericalCameraOperator::computeAffineFrame(phys_t x,
                                            phys_t y,
                                            GameState*)
{
  static const phys_t horizontalWrapFactor = 1.1;
  static const phys_t verticalWrapFactor = 1.1;

  x *= -horizontalWrapFactor;
  y *= -verticalWrapFactor;

  AffineFrame apertureFrame;

  apertureFrame.position << radius_ *
                              Vec3(cos(x) * cos(y), sin(x) * cos(y), sin(y));

  apertureFrame.forward << -cos(x) * cos(y), -sin(x) * cos(y), -sin(y);

  apertureFrame.right << -sin(x), cos(x), 0;

  apertureFrame.down << cos(x) * sin(y), sin(x) * sin(y), -cos(y);

  return apertureFrame;
}

} // cameraoperators

} // namespace kleindrive
