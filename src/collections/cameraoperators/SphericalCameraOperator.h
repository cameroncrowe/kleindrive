//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/cameraoperators/SphericalCameraOperator.h
//
//  SphericalCameraOperator class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SphericalCamera class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_SPHERICALCAMERAOPERATOR_H
#define KLEINDRIVE_SPHERICALCAMERAOPERATOR_H

#include "src/camera/AffineFrame.h"
#include "src/camera/Camera.h"
#include "src/collections/cameraoperators/MouseCameraOperator.h"

namespace kleindrive {

namespace cameraoperators {

/// Represents a camera operator that moves around via car.
class SphericalCameraOperator : public MouseCameraOperator
{

public:
  /// Sets the car according to the given initial data.
  SphericalCameraOperator(Camera camera, phys_t radius);

private:
  /// Computes the affine frame of the camera based on a spherical coordinate
  /// system.
  virtual AffineFrame computeAffineFrame(phys_t x,
                                         phys_t y,
                                         GameState*) final override;

  /// How far from the origin is the camera.
  phys_t radius_;
};

} // cameraoperators

} // namespace kleindrive

#endif // KLEINDRIVE_SPHERICALCAMERAOPERATOR_H
