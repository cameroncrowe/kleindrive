//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/Code8x8ishFont.h - The Code8x8ishTypeface defintiion file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the Code8x8ishFont definition.
///
///
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KLEINDRIVEFONTS_H
#define KLEINDRIVE_KLEINDRIVEFONTS_H

#include "src/string/KDFont.h"

namespace kleindrive {

namespace fonts {

/// A font whose typeface is similar to Code8x8.
///
/// This font is intended to be used with the font strips in graphics, eg the
/// logo.
KDFont<8>
code8x8ishFont(int kerning = 1);

/// The code8x8ishFont, but with more rows, and extra symbols.
///
/// This font is intended to be used for the text on the menu and menu context
/// pages.
KDFont<12>
code8x8ishFontSpecial(int kerning = 1);

/// A font for symbols and pictures to print on surfaces
KDFont<100>
surfaceSymbols();

} // namespace fonts

} // namespace kleindrive

#endif // KLEINDRIVE_KLEINDRIVEFONTS
