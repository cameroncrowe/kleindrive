//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/fonts/SufaceSybols.cpp
//
// The SurfaceSymbols implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SurfaceSymbols implementation File.
///
//===----------------------------------------------------------------------===//

#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/string/KDFont.h"

namespace kleindrive {

namespace fonts {

// Put symbols in here that are going to be used on the font strips.
KDFont<100>
surfaceSymbols()
{

  KDFont<100> font(1);


  // 19 x 36 Mobius band
  font.add('M',{		
  "                                   ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  " 0  0                              ",
  "                                   ",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                            0  0  0",
  "                                   ",
  });

  // 64 x 31 earth for Spherical Waves
  font.add('E',{		
  "                                                                ",
  "                                                                ",
  "                      00000                                     ",
  "              00000000000000   0000                             ",
  "               0 0000000000    000      0     00                ",
  "        000000000  00000000            0    00000   0           ",
  "  00000 0000000000   00000       00   0000000000000000000       ",
  " 000000000000000000  000  00    00000000000000000000000000000   ",
  " 000000000000000000  00   0    000000000000000000000000000000   ",
  " 0000000000000  000  0       0 0000000000000000000000000000     ",
  " 000  00000000000000        00 00000000000000000000000   00     ",
  " 0     0000000000000        000000000000000000000000000         ",
  "       0000000000000         000000000000000000000000000        ",
  "       0000000000           0 000000000000000000000000 0        ",
  "       000000000           0000   000000000000000000 0 0        ",
  "       00000000            00000    00000000000000000 0         ",
  "        0000 00            0000 000000000000000000000          0",
  "         000 000          00000000000000000000000000            ",
  "          0000            000000000000000  000 0000  0          ",
  "             00000         0000000000000    00  000   0         ",
  "              000000        000000000000     0  00 00           ",
  "              0000000          00000000          0000  00       ",
  "              0000000000       0000000            00    00      ",
  "              0000000000        000000                00        ",
  "               00000000         00000 0              00000      ",
  "                0000000         00000 0            0000000      ",
  "                000000          0000               00000000     ",
  "                00000            00                0000000      ",
  "                0000                                   000   00 ",
  "                000                                     0   00  ",
  "                000                                             ",
  "                00                                              ",
  "                 0                                              ",
  "                                                                ",
  "                                                                ",
  });

  return font;
}

} // namespace fonts

} // namespace kleindrive
