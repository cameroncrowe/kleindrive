//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/KDSceneCollection.cpp
//
//  Klein Drive scene collection implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of Klein Drive's scene collection
///
//===----------------------------------------------------------------------===//

#include "src/collections/scenes/KDSceneCollection.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include <memory>
#include <vector>

namespace kleindrive {

namespace scenes {

// Standard surfaces:
//
// These have lines that help you orient yourself.
//
// Mobius strip

std::unique_ptr<Scene> stdBumps();

std::unique_ptr<Scene> stdMobiusStrip();

std::unique_ptr<Scene> stdTorus(bool isCarInside);
std::unique_ptr<Scene> stdHornTorus();

std::unique_ptr<Scene> stdEnneperSurface();

std::unique_ptr<Scene> stdRomanSurface();
std::unique_ptr<Scene> stdCrossCap();
std::unique_ptr<Scene> stdBoysSurface();

std::unique_ptr<Scene> stdKleinBottle();

std::unique_ptr<Scene> stdSphere(bool isCarInside);

std::unique_ptr<Scene> stdParaboloid(const bool isCarInside);
std::unique_ptr<Scene> stdCone(const bool isCarInside);
std::unique_ptr<Scene> stdHyperboloidSingleSheet(const bool isCarInside);
std::unique_ptr<Scene> stdHyperboloidDoubleSheet(const bool isCarInside);
std::unique_ptr<Scene> stdSaddle();
/*


// Minimal Surfaces
std::unique_ptr<Scene> stdHelicoid();
std::unique_ptr<Scene> stdCatenoid();
std::unique_ptr<Scene> stdCatalansMinimalSurface();

// Clasical Surfaces
std::unique_ptr<Scene> stdDinisSurface();

// Design Surfaces:
//
// These have interesting patterns drawn on them.
//
std::unique_ptr<Scene> designerMobiusStrip();
std::unique_ptr<Scene> designerToroidalTwill();
std::unique_ptr<Scene> designerCartesianSphere();
std::unique_ptr<Scene> designerPolarSphere();

// Waves:
//
// These have waves moving around.
//
std::unique_ptr<Scene> wavesSquare();
std::unique_ptr<Scene> wavesSpherical();
std::unique_ptr<Scene> wavesConical();
std::unique_ptr<Scene> rotatingMobiusStrip();
*/

/// Returns Klein Drive's scene grouping.
std::unique_ptr<SceneCollection>
kdSceneCollection()
{
  // We can't use an initializer list with unqiue_ptrs, so we push back vectors.
  // Fun.

  std::vector<std::unique_ptr<Scene>> scenes;

  // Standard Surfaces
  // -----------------
  scenes.push_back(stdBumps());

  scenes.push_back(stdMobiusStrip());

  scenes.push_back(stdTorus(true));  // Inside.
  scenes.push_back(stdTorus(false)); // Outside.
  scenes.push_back(stdHornTorus());  // Inside.

  scenes.push_back(stdEnneperSurface());

  scenes.push_back(stdRomanSurface());
  scenes.push_back(stdCrossCap());
  scenes.push_back(stdBoysSurface());

  scenes.push_back(stdKleinBottle());

  scenes.push_back(stdSphere(true));  // Inside.
  scenes.push_back(stdSphere(false)); // Inside.

  scenes.push_back(stdParaboloid(true));
  scenes.push_back(stdParaboloid(false));
  scenes.push_back(stdSaddle());
  scenes.push_back(stdHyperboloidSingleSheet(true));// Inside
  scenes.push_back(stdHyperboloidSingleSheet(false)); // Outside
  scenes.push_back(stdHyperboloidDoubleSheet(true)); // Inside
  scenes.push_back(stdHyperboloidDoubleSheet(false)); // Outside
  scenes.push_back(stdCone(true)); 
  scenes.push_back(stdCone(false)); 

  /*
    // Spheres

    // Projective Planes

    // Klein Bottle

    // Minimal Surfaces
    //scenes.push_back( stdCatenoid()               ); // Boring!
    //scenes.push_back( stdHelicoid()               ); // Boring!
    //scenes.push_back( stdCatalansMinimalSurface() ); // Too Weird!

    // Classical surfaces
    //scenes.push_back( stdDinisSurface()           );

    // Designer Surfaces
    scenes.push_back( designerMobiusStrip()     );
    scenes.push_back( designerToroidalTwill()   );
    scenes.push_back( designerPolarSphere()     );
    scenes.push_back( designerCartesianSphere() );

    // Waves
    scenes.push_back( wavesSquare()         );
    scenes.push_back( wavesSpherical()      );
    scenes.push_back( wavesConical()        );
    scenes.push_back( rotatingMobiusStrip() );
    */
  return std::make_unique<SceneCollection>(scenes);
}

} // namespace scenes

} // namespace kleindrive
