//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdBoysSurface.cpp
//
//  BoysSurface implementation file. 
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementaiton of the StdBoySurface
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdBoysSurface()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  const int M = 8;
  const int Rows = 6 * M;
  const int Cols = 6 * M+1;

  // Domain
  phys_t uMin = 0;
  phys_t uMax = PI;
  phys_t vMin = 0;
  phys_t vMax = PI;

  phys_t halfTileWidth = 0; //(uMax - uMin)/Rows/2;

  const Domain domain(
    uMin - halfTileWidth,           // Min u value.
    uMax - halfTileWidth,           // Max u value.
    Gluing::WrongWay,               // Gluing in u direction.
    vMin - halfTileWidth,           // Min v value.
    vMax - halfTileWidth,           // Max v value.
    Gluing::MinAndMaxCrushedToPoint // Gluing in v Direction // FIXME
                                    // FIXME Actually it's crushed to a point!!!
  );

  // Tiling
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (j == Cols / 2)
                          return colors::WarmRed;
                        if (j == 8 || j == Cols - 1 - 8)
                          return colors::HardRed;
                        if (j == 16 || j == Cols - 1 - 16)
                          return colors::HardRed;

                        if (j % 2 == 0)
                          return colors::White;
                        return colors::Invisible;

                        /*

                                                if (i == 0 || i == Rows / 2)
                                                  return colors::WarmRed;

                                                if (j == 0 || j == Cols - 1 || j
                           == Cols / 2) return colors::HardRed;

                                                if (i == Rows / 4 || i == 3 *
                           Rows / 4 || j == Cols / 4 || j == 3 * Cols / 4)
                                                  return colors::Red;

                                                if (i % 2 == 0)
                                                  return colors::White;

                                                return colors::Invisible;
                        */
                      });

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (cos(u) * cos(v) * sin(v) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       2 * cos(v) * sin(u) * sin(v) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
       cos(u) * sin(u) * pow(sin(v), 2) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) +
       (-pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(sin(v), 2) -
        pow(sin(u), 2) * pow(sin(v), 2)) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) * (cos(u) * cos(v) * sin(v) *
                    (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
                  cos(u) * sin(u) * pow(sin(v), 2) *
                    (-(pow(cos(u), 2) * pow(sin(v), 2)) +
                     pow(sin(u), 2) * pow(sin(v), 2)) +
                  (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) *
                    (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
                     pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
       (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
        8.);
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (-2 * pow(cos(u), 2) * cos(v) * sin(u) * pow(sin(v), 3) +
       4 * cos(u) * cos(v) * pow(sin(u), 2) * pow(sin(v), 3) +
       4 * pow(cos(u), 2) * pow(sin(u), 2) * pow(sin(v), 4) -
       cos(v) * sin(u) * sin(v) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       2 * cos(u) * cos(v) * sin(v) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
       pow(cos(u), 2) * pow(sin(v), 2) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       pow(sin(u), 2) * pow(sin(v), 2) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       6 * cos(u) * sin(u) * pow(sin(v), 2) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) * (2 * pow(cos(u), 2) * cos(v) * sin(u) * pow(sin(v), 3) +
                  4 * pow(cos(u), 2) * pow(sin(u), 2) * pow(sin(v), 4) -
                  cos(v) * sin(u) * sin(v) *
                    (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
                  pow(cos(u), 2) * pow(sin(v), 2) *
                    (-(pow(cos(u), 2) * pow(sin(v), 2)) +
                     pow(sin(u), 2) * pow(sin(v), 2)) -
                  pow(sin(u), 2) * pow(sin(v), 2) *
                    (-(pow(cos(u), 2) * pow(sin(v), 2)) +
                     pow(sin(u), 2) * pow(sin(v), 2)) +
                  2 * cos(u) * sin(u) * pow(sin(v), 2) *
                    (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
                     pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
       (-4 * cos(u) * sin(v) * (-cos(v) + cos(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        4 * sin(u) * sin(v) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (cos(u) * sin(v) + sin(u) * sin(v)) +
        3 * (cos(u) * sin(v) - sin(u) * sin(v)) *
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * sin(v) - sin(u) * sin(v)) *
         (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
          8.);
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (cos(u) * cos(v) * sin(v) *
         (2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
       2 * cos(v) * sin(u) * sin(v) *
         (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       cos(u) * sin(u) * pow(sin(v), 2) *
         (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       cos(u) * pow(cos(v), 2) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) -
       cos(u) * pow(sin(v), 2) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
        2 * cos(v) * pow(sin(u), 2) * sin(v)) *
         (-pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(sin(v), 2) -
          pow(sin(u), 2) * pow(sin(v), 2)) +
       2 * pow(cos(v), 2) * sin(u) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) -
       2 * sin(u) * pow(sin(v), 2) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
       2 * cos(u) * cos(v) * sin(u) * sin(v) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) +
       (2 * cos(v) * sin(v) + 4 * pow(cos(u), 2) * cos(v) * sin(v) -
        2 * cos(v) * pow(sin(u), 2) * sin(v)) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) *
       (cos(u) * cos(v) * sin(v) *
          (-2 * cos(v) * sin(v) - 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
        cos(u) * sin(u) * pow(sin(v), 2) *
          (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) +
        cos(u) * pow(cos(v), 2) *
          (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) -
        cos(u) * pow(sin(v), 2) *
          (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
        (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
         2 * cos(v) * pow(sin(u), 2) * sin(v)) *
          (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
        2 * cos(u) * cos(v) * sin(u) * sin(v) *
          (-(pow(cos(u), 2) * pow(sin(v), 2)) +
           pow(sin(u), 2) * pow(sin(v), 2)) +
        (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) *
          (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
           pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
       (4 * (-(cos(u) * cos(v)) + cos(v) * sin(u)) *
          (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) +
        4 * (-(cos(v) * sin(u)) - sin(v)) * (-cos(v) + cos(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        4 * (cos(u) * cos(v) + sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        3 * (cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
         (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
          8.);
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (-2 * pow(cos(u), 3) * cos(v) * pow(sin(v), 3) +
       12 * pow(cos(u), 2) * cos(v) * sin(u) * pow(sin(v), 3) +
       6 * cos(u) * cos(v) * pow(sin(u), 2) * pow(sin(v), 3) -
       4 * cos(v) * pow(sin(u), 3) * pow(sin(v), 3) +
       12 * pow(cos(u), 3) * sin(u) * pow(sin(v), 4) -
       12 * cos(u) * pow(sin(u), 3) * pow(sin(v), 4) -
       cos(u) * cos(v) * sin(v) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) -
       2 * cos(v) * sin(u) * sin(v) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) -
       4 * cos(u) * sin(u) * pow(sin(v), 2) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       6 * pow(cos(u), 2) * pow(sin(v), 2) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2)) +
       6 * pow(sin(u), 2) * pow(sin(v), 2) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) * (2 * pow(cos(u), 3) * cos(v) * pow(sin(v), 3) -
                  6 * cos(u) * cos(v) * pow(sin(u), 2) * pow(sin(v), 3) +
                  12 * pow(cos(u), 3) * sin(u) * pow(sin(v), 4) -
                  12 * cos(u) * pow(sin(u), 3) * pow(sin(v), 4) -
                  cos(u) * cos(v) * sin(v) *
                    (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) -
                  4 * cos(u) * sin(u) * pow(sin(v), 2) *
                    (-(pow(cos(u), 2) * pow(sin(v), 2)) +
                     pow(sin(u), 2) * pow(sin(v), 2)) +
                  2 * pow(cos(u), 2) * pow(sin(v), 2) *
                    (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
                     pow(sin(u), 2) * pow(sin(v), 2)) -
                  2 * pow(sin(u), 2) * pow(sin(v), 2) *
                    (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
                     pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
       (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (cos(u) * sin(v) - sin(u) * sin(v)) +
        8 * cos(u) * sin(u) * pow(sin(v), 2) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        4 * sin(u) * sin(v) * (-cos(v) + cos(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        4 * cos(u) * sin(v) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        8 * cos(u) * sin(v) * (-cos(v) + cos(u) * sin(v)) *
          (cos(u) * sin(v) + sin(u) * sin(v)) -
        8 * sin(u) * sin(v) * (cos(v) - sin(u) * sin(v)) *
          (cos(u) * sin(v) + sin(u) * sin(v)) +
        6 * pow(cos(u) * sin(v) - sin(u) * sin(v), 2) *
          (cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) +
        3 * (-(cos(u) * sin(v)) - sin(u) * sin(v)) *
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * sin(v) - sin(u) * sin(v)) *
         (-4 * cos(u) * sin(v) * (-cos(v) + cos(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
          4 * sin(u) * sin(v) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (cos(u) * sin(v) + sin(u) * sin(v)) +
          3 * (cos(u) * sin(v) - sin(u) * sin(v)) *
            pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          4. +
        ((-(cos(u) * sin(v)) - sin(u) * sin(v)) *
         (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
          8.);
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (-6 * pow(cos(u), 2) * pow(cos(v), 2) * sin(u) * pow(sin(v), 2) +
       12 * cos(u) * pow(cos(v), 2) * pow(sin(u), 2) * pow(sin(v), 2) +
       16 * pow(cos(u), 2) * cos(v) * pow(sin(u), 2) * pow(sin(v), 3) +
       2 * pow(cos(u), 2) * sin(u) * pow(sin(v), 4) -
       4 * cos(u) * pow(sin(u), 2) * pow(sin(v), 4) -
       cos(v) * sin(u) * sin(v) *
         (2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
       2 * cos(u) * cos(v) * sin(v) *
         (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       pow(cos(u), 2) * pow(sin(v), 2) *
         (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) -
       pow(sin(u), 2) * pow(sin(v), 2) *
         (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) -
       6 * cos(u) * sin(u) * pow(sin(v), 2) *
         (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) -
       pow(cos(v), 2) * sin(u) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       sin(u) * pow(sin(v), 2) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       2 * cos(u) * pow(cos(v), 2) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) -
       2 * cos(u) * pow(sin(v), 2) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
       2 * pow(cos(u), 2) * cos(v) * sin(v) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       2 * cos(v) * pow(sin(u), 2) * sin(v) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       12 * cos(u) * cos(v) * sin(u) * sin(v) *
         (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
          pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) *
       (6 * pow(cos(u), 2) * pow(cos(v), 2) * sin(u) * pow(sin(v), 2) +
        16 * pow(cos(u), 2) * cos(v) * pow(sin(u), 2) * pow(sin(v), 3) -
        2 * pow(cos(u), 2) * sin(u) * pow(sin(v), 4) -
        cos(v) * sin(u) * sin(v) *
          (-2 * cos(v) * sin(v) - 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
        pow(cos(u), 2) * pow(sin(v), 2) *
          (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) -
        pow(sin(u), 2) * pow(sin(v), 2) *
          (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) +
        2 * cos(u) * sin(u) * pow(sin(v), 2) *
          (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) -
        pow(cos(v), 2) * sin(u) *
          (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
        sin(u) * pow(sin(v), 2) *
          (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
        2 * pow(cos(u), 2) * cos(v) * sin(v) *
          (-(pow(cos(u), 2) * pow(sin(v), 2)) +
           pow(sin(u), 2) * pow(sin(v), 2)) -
        2 * cos(v) * pow(sin(u), 2) * sin(v) *
          (-(pow(cos(u), 2) * pow(sin(v), 2)) +
           pow(sin(u), 2) * pow(sin(v), 2)) +
        4 * cos(u) * cos(v) * sin(u) * sin(v) *
          (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
           pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
       (-4 * cos(u) * (-(cos(u) * cos(v)) + cos(v) * sin(u)) * sin(v) *
          (-cos(v) + cos(u) * sin(v)) -
        4 * sin(u) * (-(cos(u) * cos(v)) + cos(v) * sin(u)) * sin(v) *
          (cos(v) - sin(u) * sin(v)) +
        4 * (cos(u) * cos(v) + cos(v) * sin(u)) * (-cos(v) + cos(u) * sin(v)) *
          (cos(v) - sin(u) * sin(v)) -
        4 * sin(u) * (-(cos(v) * sin(u)) - sin(v)) * sin(v) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        4 * cos(u) * sin(v) * (cos(u) * cos(v) + sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        4 * cos(u) * cos(v) * (-cos(v) + cos(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
        4 * cos(v) * sin(u) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        4 * (-(cos(v) * sin(u)) - sin(v)) * (-cos(v) + cos(u) * sin(v)) *
          (cos(u) * sin(v) + sin(u) * sin(v)) +
        4 * (cos(u) * cos(v) + sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (cos(u) * sin(v) + sin(u) * sin(v)) +
        6 * (cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
          (cos(u) * sin(v) - sin(u) * sin(v)) *
          (cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) +
        3 * (cos(u) * cos(v) - cos(v) * sin(u)) *
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * sin(v) - sin(u) * sin(v)) *
         (4 * (-(cos(u) * cos(v)) + cos(v) * sin(u)) *
            (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) +
          4 * (-(cos(v) * sin(u)) - sin(v)) * (-cos(v) + cos(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          4 * (cos(u) * cos(v) + sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          3 * (cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
            pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
         (-4 * cos(u) * sin(v) * (-cos(v) + cos(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) -
          4 * sin(u) * sin(v) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (cos(u) * sin(v) + sin(u) * sin(v)) +
          3 * (cos(u) * sin(v) - sin(u) * sin(v)) *
            pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((cos(u) * cos(v) - cos(v) * sin(u)) *
         (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
          8.);
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (2 * cos(u) * pow(cos(v), 2) *
         (2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v)) -
       2 * cos(u) * pow(sin(v), 2) *
         (2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
       4 * pow(cos(v), 2) * sin(u) *
         (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) -
       4 * sin(u) * pow(sin(v), 2) *
         (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       4 * cos(u) * cos(v) * sin(u) * sin(v) *
         (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       2 *
         (2 * cos(v) * sin(v) + 4 * pow(cos(u), 2) * cos(v) * sin(v) -
          2 * cos(v) * pow(sin(u), 2) * sin(v)) *
         (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
          2 * cos(v) * pow(sin(u), 2) * sin(v)) +
       cos(u) * cos(v) * sin(v) *
         (2 * pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(cos(v), 2) -
          2 * pow(sin(v), 2) - 2 * pow(cos(u), 2) * pow(sin(v), 2)) -
       4 * cos(u) * cos(v) * sin(v) *
         (-pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2)) +
       2 * cos(v) * sin(u) * sin(v) *
         (2 * pow(cos(v), 2) + 2 * pow(cos(v), 2) * pow(sin(u), 2) -
          2 * pow(sin(v), 2) - 2 * pow(sin(u), 2) * pow(sin(v), 2)) +
       cos(u) * sin(u) * pow(sin(v), 2) *
         (-2 * pow(cos(u), 2) * pow(cos(v), 2) +
          2 * pow(cos(v), 2) * pow(sin(u), 2) +
          2 * pow(cos(u), 2) * pow(sin(v), 2) -
          2 * pow(sin(u), 2) * pow(sin(v), 2)) +
       (-2 * pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(cos(v), 2) +
        2 * pow(cos(v), 2) * pow(sin(u), 2) + 2 * pow(sin(v), 2) -
        2 * pow(cos(u), 2) * pow(sin(v), 2) -
        2 * pow(sin(u), 2) * pow(sin(v), 2)) *
         (-pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(sin(v), 2) -
          pow(sin(u), 2) * pow(sin(v), 2)) -
       8 * cos(v) * sin(u) * sin(v) *
         (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
       2 * cos(u) * pow(cos(v), 2) * sin(u) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) -
       2 * cos(u) * sin(u) * pow(sin(v), 2) *
         (-(pow(cos(u), 2) * pow(sin(v), 2)) +
          pow(sin(u), 2) * pow(sin(v), 2)) +
       (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
        pow(sin(u), 2) * pow(sin(v), 2)) *
         (2 * pow(cos(v), 2) + 4 * pow(cos(u), 2) * pow(cos(v), 2) -
          2 * pow(cos(v), 2) * pow(sin(u), 2) - 2 * pow(sin(v), 2) -
          4 * pow(cos(u), 2) * pow(sin(v), 2) +
          2 * pow(sin(u), 2) * pow(sin(v), 2))) /
        2.,
      (sqrt(3) *
       (2 * cos(u) * pow(cos(v), 2) *
          (-2 * cos(v) * sin(v) - 2 * pow(cos(u), 2) * cos(v) * sin(v)) -
        2 * cos(u) * pow(sin(v), 2) *
          (-2 * cos(v) * sin(v) - 2 * pow(cos(u), 2) * cos(v) * sin(v)) +
        4 * cos(u) * cos(v) * sin(u) * sin(v) *
          (-2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) +
        2 * (2 * cos(v) * sin(v) + 2 * cos(v) * pow(sin(u), 2) * sin(v)) *
          (-2 * cos(v) * sin(v) + 2 * pow(cos(u), 2) * cos(v) * sin(v) +
           2 * cos(v) * pow(sin(u), 2) * sin(v)) -
        4 * cos(u) * cos(v) * sin(v) *
          (pow(cos(v), 2) - pow(cos(u), 2) * pow(sin(v), 2)) +
        cos(u) * cos(v) * sin(v) *
          (-2 * pow(cos(v), 2) - 2 * pow(cos(u), 2) * pow(cos(v), 2) +
           2 * pow(sin(v), 2) + 2 * pow(cos(u), 2) * pow(sin(v), 2)) +
        cos(u) * sin(u) * pow(sin(v), 2) *
          (-2 * pow(cos(u), 2) * pow(cos(v), 2) +
           2 * pow(cos(v), 2) * pow(sin(u), 2) +
           2 * pow(cos(u), 2) * pow(sin(v), 2) -
           2 * pow(sin(u), 2) * pow(sin(v), 2)) +
        (-2 * pow(cos(v), 2) + 2 * pow(cos(u), 2) * pow(cos(v), 2) +
         2 * pow(cos(v), 2) * pow(sin(u), 2) + 2 * pow(sin(v), 2) -
         2 * pow(cos(u), 2) * pow(sin(v), 2) -
         2 * pow(sin(u), 2) * pow(sin(v), 2)) *
          (-pow(cos(v), 2) + pow(sin(u), 2) * pow(sin(v), 2)) +
        2 * cos(u) * pow(cos(v), 2) * sin(u) *
          (-(pow(cos(u), 2) * pow(sin(v), 2)) +
           pow(sin(u), 2) * pow(sin(v), 2)) -
        2 * cos(u) * sin(u) * pow(sin(v), 2) *
          (-(pow(cos(u), 2) * pow(sin(v), 2)) +
           pow(sin(u), 2) * pow(sin(v), 2)) +
        (2 * pow(cos(v), 2) + 2 * pow(cos(v), 2) * pow(sin(u), 2) -
         2 * pow(sin(v), 2) - 2 * pow(sin(u), 2) * pow(sin(v), 2)) *
          (pow(cos(v), 2) + pow(cos(u), 2) * pow(sin(v), 2) +
           pow(sin(u), 2) * pow(sin(v), 2)))) /
        2.,
      ((cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
       (4 * (-(cos(u) * cos(v)) + cos(v) * sin(u)) *
          (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) +
        4 * (-(cos(v) * sin(u)) - sin(v)) * (-cos(v) + cos(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        4 * (cos(u) * cos(v) + sin(v)) * (cos(v) - sin(u) * sin(v)) *
          (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
        3 * (cos(u) * cos(v) + cos(v) * sin(u) - sin(v)) *
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          4. +
        ((cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) *
         (8 * (-(cos(u) * cos(v)) + cos(v) * sin(u)) *
            (-(cos(v) * sin(u)) - sin(v)) * (-cos(v) + cos(u) * sin(v)) +
          8 * (-(cos(u) * cos(v)) + cos(v) * sin(u)) *
            (cos(u) * cos(v) + sin(v)) * (cos(v) - sin(u) * sin(v)) +
          4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (cos(u) * sin(v) - sin(u) * sin(v)) +
          8 * (-(cos(v) * sin(u)) - sin(v)) * (cos(u) * cos(v) + sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          4 * (cos(v) - cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          4 * (-cos(v) + cos(u) * sin(v)) * (-cos(v) + sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          6 * pow(cos(u) * cos(v) + cos(v) * sin(u) - sin(v), 2) *
            (cos(v) + cos(u) * sin(v) + sin(u) * sin(v)) +
          3 * (-cos(v) - cos(u) * sin(v) - sin(u) * sin(v)) *
            pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 2))) /
          8. +
        ((-cos(v) - cos(u) * sin(v) - sin(u) * sin(v)) *
         (4 * (-cos(v) + cos(u) * sin(v)) * (cos(v) - sin(u) * sin(v)) *
            (-(cos(u) * sin(v)) + sin(u) * sin(v)) +
          pow(cos(v) + cos(u) * sin(v) + sin(u) * sin(v), 3))) /
          8.);
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              1.2 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        0.4,              // Normal forward velocity.
0.04 // Min distance from edge of domain.
      }),
      0.01,           // Aperture radius.
      0.04 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  state.label = "Boy Surface";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
