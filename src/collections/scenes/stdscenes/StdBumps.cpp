//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdBumps.cpp
//
//  StdOscillatingBumps implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdOscillatingBumps.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

namespace {

const phys_t EE = exp(1);
}

std::unique_ptr<Scene>
stdBumps()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Carful not to let the car go to the origin--the metric is degenerate!
  const Domain domain(-3,               // Min u value.
                      3,                // Max u value.
                      Gluing::NotGlued, // Gluing in u direction.
                      -3,               // Min v value.
                      3,                // Max v value.
                      Gluing::NotGlued  // Gluing in v Direction
  );

  // Tiling

  const int width = 35;

  const Tiling tiling(width, // Number of tiles in u direction.
                      width, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (i == 0 || j == 0 || i == width - 1 ||
                            j == width - 1)
                          return colors::HardRed;
                        if (i == width / 2 || j == width / 2)
                          return colors::WarmRed;
                        return colors::White;
                      } // Tile coloring function.
  );

  // Parameterization

  // Constants controlling the surface.
  const phys_t a = 5;   // Scales amplitude.
  const phys_t b = 0.5; // Scales speed of waving.

  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(u, v, a * pow(EE, -pow(u, 2) - pow(v, 2)) * u * sin(b * t));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(1,
                0,
                a * pow(EE, -pow(u, 2) - pow(v, 2)) * sin(b * t) -
                  2 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * pow(u, 2) *
                    sin(b * t));
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(
      0, 1, -2 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * u * v * sin(b * t));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(0,
                0,
                -6 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * u * sin(b * t) +
                  4 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * pow(u, 3) *
                    sin(b * t));
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(0,
                0,
                -2 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * v * sin(b * t) +
                  4 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * pow(u, 2) * v *
                    sin(b * t));
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t = PI / 2;
    return Vec3(0,
                0,
                -2 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * u * sin(b * t) +
                  4 * a * pow(EE, -pow(u, 2) - pow(v, 2)) * u * pow(v, 2) *
                    sin(b * t));
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator

  AffineFrame affineFrame;

  affineFrame.position << -1, -2, 3;
  affineFrame.forward = Vec3(1, 1, -0.1).normalized();
  affineFrame.right = Vec3(1, -1, 0).normalized();
  affineFrame.down = affineFrame.forward.cross(affineFrame.right);
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<LookoutCameraOperator>(
      Camera(1,              // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-0,            // Min zoom power.
              0,             // Max zoom power.
              0              // Default zoom power.
              )),
      affineFrame, // Default camera position
      -PI / 5,     // Min horizontal deflection
      PI / 4,      // Max horizontal deflection
      -PI / 2,     // Min vertical deflection.
      -PI / 12     // Max vertical deflection.
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(-1, -2), // Initial position.
        PI / 3 + 0.1, // Initial heading.
        0.7,          // Normal forward velocity.
        0.2 // Min distance from edge of domain.
      }),
      0.05,           // Aperture radius.
      0.2 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;

  state.label = "Bumps";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
