//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdCrossCap.cpp
//
//  StdCrossCap function implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementaiton of the StdCrossCap function.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <ctgmath>
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {

namespace scenes {

using namespace cameraoperators;

namespace {

phys_t
sech(phys_t angle)
{
  return 1 / cosh(angle);
}

} // namespace

std::unique_ptr<Scene>
stdCrossCap()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  const int M = 6;
  const int Rows = 6 * M;
  const int Cols = 6 * M+1;

  // Domain
  phys_t uMin = 0;
  phys_t uMax = 2 * PI;
  phys_t vMin = -PI;
  phys_t vMax = PI;

  phys_t halfTileWidth = 0;//(uMax - uMin)/Rows/2;

  const Domain domain(uMin - halfTileWidth,       // Min u value.
                      uMax - halfTileWidth,       // Max u value.
                      Gluing::WrongWay,           // Gluing in u direction.
                      vMin - halfTileWidth,       // Min v value.
                      vMax - halfTileWidth,       // Max v value.
                      Gluing::MinAndMaxCrushedToPoint// Gluing in v Direction
  );

  // Tiling
  const Tiling tiling(
    Rows, // Number of tiles in u direction.
    Cols, // Number of tiles in v direction.
    [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
      if (j == Cols/2)
        return colors::WarmRed;
      if( j == 10 || j == Rows -10)
        return colors::HardRed;

      if(j<3 || j > Cols-1 -3)
        return colors::White;
      if(j%2==0)
        return colors::White;
      return colors::Invisible;

      if (i == 0 || i == Rows - 1 || i == Rows / 2 || i == Rows / 2 - 1)

        return colors::WarmRed;
      if (j == 0 || j == Cols - 1 || j == Cols / 2 || j == Cols / 2 - 1)

        return colors::HardRed;

      if (i == Rows / 4 || i == Rows / 4 - 1 || i == 3 * Rows / 4 ||
          i == 3 * Rows / 4 - 1 || j == Cols / 4 || j == Cols / 4 - 1 ||
          j == 3 * Cols / 4 || j == 3 * Cols / 4 - 1)
        return colors::Red;

      return colors::White;
    } // Tile coloring function
  );

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      cos(u) * (1 + cos(v)), (1 + cos(v)) * sin(u), sin(v) * tanh(PI - u));
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-((1 + cos(v)) * sin(u)),
                cos(u) * (1 + cos(v)),
                -(pow(sech(PI - u), 2) * sin(v)));
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-(cos(u) * sin(v)), -(sin(u) * sin(v)), cos(v) * tanh(PI - u));
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-(cos(u) * (1 + cos(v))),
                -((1 + cos(v)) * sin(u)),
                -2 * pow(sech(PI - u), 2) * sin(v) * tanh(PI - u));
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      sin(u) * sin(v), -(cos(u) * sin(v)), -(cos(v) * pow(sech(PI - u), 2)));
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -(cos(u) * cos(v)), -(cos(v) * sin(u)), -(sin(v) * tanh(PI - u)));
  };

  // This parameterization is not periodic in u, so we manually have to program
  // in it's periodicity.

  F2Vec3 g, g_u, g_v, g_uu, g_uv, g_vv;

  auto orientedQuotient = [=](phys_t& u, phys_t& v[[maybe_unused]]) {
    int quotient;
    u = std::remquo(u, (2 * PI), &quotient);
    // remquo seems to give us a remained with abs value less than PI/2.
    // Obnoxious.
    if (u < 0) {
      u += 2 * PI;
      quotient -= 1;
    }
    // Modulo is annoying for negative numbers.  Luckily multiplication by
    // negative 1 is a homomorphism mod 2 :)
    quotient = std::abs(quotient);
    const bool flippedOrientation = (quotient % 2 == 1);
    return flippedOrientation;
  };

  g = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v))
      return f(u, -v);
    return f(u, v);
  };

  g_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v))
      return f_u(u, -v);
    return f_u(u, v);
  };

  g_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v)) {
      Vec3 r = -f_v(u, -v);
      return r;
    }
    return f_v(u, v);
  };

  g_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v))
      return f_uu(u, -v);
    return f_uu(u, v);
  };

  g_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v)) {
      Vec3 r = -f_uv(u, -v);
      return r;
    }
    return f_uv(u, v);
  };

  g_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (orientedQuotient(u, v))
      return f_vv(u, -v);
    return f_vv(u, v);
  };

  const Param param(g, g_u, g_v, g_uu, g_uv, g_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  /*
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              1 // Distance from origin
    );
  */

  AffineFrame affineFrame;
  affineFrame.position <<  1, -0.1, 0.1;
  affineFrame.forward  << -1,  0,   0;
  affineFrame.right    <<  0, -1,   0;
  affineFrame.down     <<  0,  0,   1;

  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<LookoutCameraOperator>(Camera(0.2, // Aperture radius.
                                                   ZoomPowerSlider // Zooming.
                                                   (-3, // Min zoom power.
                                                    3,  // Max zoom power.
                                                    0   // Default zoom power.
                                                    )),
                                            affineFrame, // Affine frame.
                                            -PI / 4,     // Max left deflection.
                                            PI / 4, // Max right defleciton.
                                            -PI / 4,    // Max down deflection.
                                            PI / 4  // Max up deflection.
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(-3.277, -1.03), // Initial position.
        -1.5,                // Initial heading.
        0.76,                   // Normal forward velocity.
        0.3                  // Min distance from edge of domain.
      }),
      0.05,           // Aperture radius.
      0.3 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  state.label = "Cross-Cap";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
