//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdsurfaces/StdEnneperSurface.cpp
//
//  StdEnneperSurface implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file  This file contains the implementaiton of the StdEnneperSurface
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {

namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdEnneperSurface()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  const int M = 4;
  const int Rows = 4 * M + 1;
  const int Cols = 6 * 4 * M;

  // Domain

  // Metric is degenerate at origin, and noticably weird nearby, too, so we
  // don't go all the way there.
  const phys_t epsilon = 0.1;

  const phys_t uMin = 0 + epsilon;
  const phys_t uMax = 1.65;
  const phys_t vMin = 0;
  const phys_t vMax = 2 * PI;

  const phys_t halfVTileWidth = (vMax - vMin)/Cols/2;

  const Domain domain(uMin,                  // Min u value.
                      uMax,                  // Max u value.
                      Gluing::NotGlued,      // Gluing in u direction.
                      vMin - halfVTileWidth, // Min v value.
                      vMax - halfVTileWidth, // Max v value.
                      Gluing::RightWay       // Gluing in v Direction
  );

  // Tiling
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (j % (6 * M) == 0)
                          return colors::HardRed;

                        if (i % 4 == 0)
                          return colors::WarmRed;
                        if (i % 4 == 2)
                          return colors::White;
                        return colors::Invisible;
                      } // Tile Coloring Function

  );

  // Parameterization
  // ----------------

  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (u * cos(v) *
       (1 - (pow(u, 2) * pow(cos(v), 2)) / 3. + pow(u, 2) * pow(sin(v), 2))) /
        3.,
      -(u * sin(v) *
        (1 + pow(u, 2) * pow(cos(v), 2) - (pow(u, 2) * pow(sin(v), 2)) / 3.)) /
        3.,
      (pow(u, 2) * pow(cos(v), 2) - pow(u, 2) * pow(sin(v), 2)) / 3.);
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (u * cos(v) * ((-2 * u * pow(cos(v), 2)) / 3. + 2 * u * pow(sin(v), 2))) /
          3. +
        (cos(v) *
         (1 - (pow(u, 2) * pow(cos(v), 2)) / 3. + pow(u, 2) * pow(sin(v), 2))) /
          3.,
      -(u * sin(v) * (2 * u * pow(cos(v), 2) - (2 * u * pow(sin(v), 2)) / 3.)) /
          3. -
        (sin(v) *
         (1 + pow(u, 2) * pow(cos(v), 2) - (pow(u, 2) * pow(sin(v), 2)) / 3.)) /
          3.,
      (2 * u * pow(cos(v), 2) - 2 * u * pow(sin(v), 2)) / 3.);
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (8 * pow(u, 3) * pow(cos(v), 2) * sin(v)) / 9. -
        (u * sin(v) *
         (1 - (pow(u, 2) * pow(cos(v), 2)) / 3. + pow(u, 2) * pow(sin(v), 2))) /
          3.,
      (8 * pow(u, 3) * cos(v) * pow(sin(v), 2)) / 9. -
        (u * cos(v) *
         (1 + pow(u, 2) * pow(cos(v), 2) - (pow(u, 2) * pow(sin(v), 2)) / 3.)) /
          3.,
      (-4 * pow(u, 2) * cos(v) * sin(v)) / 3.);
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (u * cos(v) * ((-2 * pow(cos(v), 2)) / 3. + 2 * pow(sin(v), 2))) / 3. +
        (2 * cos(v) *
         ((-2 * u * pow(cos(v), 2)) / 3. + 2 * u * pow(sin(v), 2))) /
          3.,
      -(u * sin(v) * (2 * pow(cos(v), 2) - (2 * pow(sin(v), 2)) / 3.)) / 3. -
        (2 * sin(v) *
         (2 * u * pow(cos(v), 2) - (2 * u * pow(sin(v), 2)) / 3.)) /
          3.,
      (2 * pow(cos(v), 2) - 2 * pow(sin(v), 2)) / 3.);
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3((8 * pow(u, 2) * pow(cos(v), 2) * sin(v)) / 3. -
                  (u * sin(v) *
                   ((-2 * u * pow(cos(v), 2)) / 3. + 2 * u * pow(sin(v), 2))) /
                    3. -
                  (sin(v) * (1 - (pow(u, 2) * pow(cos(v), 2)) / 3. +
                             pow(u, 2) * pow(sin(v), 2))) /
                    3.,
                (8 * pow(u, 2) * cos(v) * pow(sin(v), 2)) / 3. -
                  (u * cos(v) *
                   (2 * u * pow(cos(v), 2) - (2 * u * pow(sin(v), 2)) / 3.)) /
                    3. -
                  (cos(v) * (1 + pow(u, 2) * pow(cos(v), 2) -
                             (pow(u, 2) * pow(sin(v), 2)) / 3.)) /
                    3.,
                (-8 * u * cos(v) * sin(v)) / 3.);
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (8 * pow(u, 3) * pow(cos(v), 3)) / 9. -
        (8 * pow(u, 3) * cos(v) * pow(sin(v), 2)) / 3. -
        (u * cos(v) *
         (1 - (pow(u, 2) * pow(cos(v), 2)) / 3. + pow(u, 2) * pow(sin(v), 2))) /
          3.,
      (8 * pow(u, 3) * pow(cos(v), 2) * sin(v)) / 3. -
        (8 * pow(u, 3) * pow(sin(v), 3)) / 9. +
        (u * sin(v) *
         (1 + pow(u, 2) * pow(cos(v), 2) - (pow(u, 2) * pow(sin(v), 2)) / 3.)) /
          3.,
      (-4 * pow(u, 2) * pow(cos(v), 2)) / 3. +
        (4 * pow(u, 2) * pow(sin(v), 2)) / 3.);
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      1   // Default zoom power.
                                                      )),
                                              1.4 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        0.4,              // Normal forward velocity.
        0.08              // Min distance from edge.
      }),
      0.01,           // Aperture radius.
      0.08 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  state.label = "Enneper Surface";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
