//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscene/StdHyperboloidDoubleSheet.cpp
//
//  StdHyperboloidsingleSheet implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdHyperboloidDoubleSheet.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdHyperboloidDoubleSheet(const bool isCarInside)
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Carful not to let the car go to the origin--the metric is degenerate!
  const Domain domain =
    (isCarInside) ? Domain(0.0001,                    // Min v value.
                           2.5,                       // Max v value.
                           Gluing::MinCrushedToPoint, // Gluing in v Direction
                           0,                         // Min u value.
                           2 * PI,                    // Max u value.
                           Gluing::RightWay           // Gluing in u direction.
                           )
                  : Domain(0,                        // Min u value.
                           2 * PI,                   // Max u value.
                           Gluing::RightWay,         // Gluing in u direction.
                           0.0001,                   // Min v value.
                           2.5,                      // Max v value.
                           Gluing::MinCrushedToPoint // Gluing in v Direction
                    );

  // Tiling
  const int rows = (isCarInside) ? 4 * 4 + 1 : 4 * 8;
  const int cols = (isCarInside) ? 4 * 8 : 4 * 4 + 1;
  const Tiling tiling(rows, // Number of tiles in u direction.
                      cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (!isCarInside)
                          std::swap(i, j);
                        if (i % 4 == 0)
                          return colors::WarmRed;
                        if (j % 4 == 0)
                          return colors::HardRed;
                        return colors::White;
                      });
  // Parameterization

  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(cos(u) * sinh(v), sin(u) * sinh(v), cosh(v) - cosh(3)/2);
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(-(sin(u) * sinh(v)), cos(u) * sinh(v), 0);
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(cos(u) * cosh(v), cosh(v) * sin(u), sinh(v));
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(-(cos(u) * sinh(v)), -(sin(u) * sinh(v)), 0);
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(-(cosh(v) * sin(u)), cos(u) * cosh(v), 0);
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(cos(u) * sinh(v), sin(u) * sinh(v), cosh(v));
  };

  const Param param = (isCarInside)
              ? Param(f, f_v, f_u, f_vv, f_uv, f_uu)
              : Param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              12 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(1, 1), // Initial position.
        -2.6,       // Initial heading.
        1.2,          // Normal forward velocity.
        0.02         // Min distance from edge of domain.
      }),
      0.05, // Aperture radius.
      0.2   // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;

  state.label =
    (isCarInside) ? "Hyperboloid II (Inside)" : "Hyperboloid II (Outside)";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
