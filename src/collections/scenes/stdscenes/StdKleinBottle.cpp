//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdScenes.KleinBottle.cpp
//
//  Klein Bottle implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the Klein Bottle.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdKleinBottle()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const Domain domain(
    0,  // Min u value.
    PI, // Max u value.
    // This klein bottle is not glued in the standard manner.
    // Hopefully the rounding error defect doesn't show itself.
    Gluing::PassThroughWrongWay, // Gluing in u direction.
    0,                           // Min v value.
    2 * PI,                      // Max v value.
    Gluing::RightWay             // Gluing in v Direction
  );

  // Tiling
  // ------

  const int M = 5;
  const int R = 7;
  const int C = 3;

  const int Rows = R * M * 2;
  const int Cols = C * M * 2;

  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        const int I = i % R;
                        if (I == 0)
                          return colors::WarmRed;
                        const int J1 = j % (C);
                        const int J2 = (j - Cols / 2 + 1) % (C);
                        if (J1 == 0 || J2 == 0)
                          return colors::White;

                        return colors::HardRed;
                      } // Tile coloring function
  );

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (2 * cos(u) *
       (3 * cos(v) - 30 * sin(u) + 90 * pow(cos(u), 4) * sin(u) -
        60 * pow(cos(u), 6) * sin(u) + 5 * cos(u) * cos(v) * sin(u))) /
        15.,
      (sin(u) * (3 * cos(v) - 3 * pow(cos(u), 2) * cos(v) -
                 48 * pow(cos(u), 4) * cos(v) + 48 * pow(cos(u), 6) * cos(v) -
                 60 * sin(u) + 5 * cos(u) * cos(v) * sin(u) -
                 5 * pow(cos(u), 3) * cos(v) * sin(u) -
                 80 * pow(cos(u), 5) * cos(v) * sin(u) +
                 80 * pow(cos(u), 7) * cos(v) * sin(u))) /
          15. +
        2,
      (2 * (3 + 5 * cos(u) * sin(u)) * sin(v)) / 15.);
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (-2 * sin(u) *
       (3 * cos(v) - 30 * sin(u) + 90 * pow(cos(u), 4) * sin(u) -
        60 * pow(cos(u), 6) * sin(u) + 5 * cos(u) * cos(v) * sin(u))) /
          15. +
        (2 * cos(u) *
         (-30 * cos(u) + 90 * pow(cos(u), 5) - 60 * pow(cos(u), 7) +
          5 * pow(cos(u), 2) * cos(v) - 360 * pow(cos(u), 3) * pow(sin(u), 2) +
          360 * pow(cos(u), 5) * pow(sin(u), 2) -
          5 * cos(v) * pow(sin(u), 2))) /
          15.,
      (cos(u) * (3 * cos(v) - 3 * pow(cos(u), 2) * cos(v) -
                 48 * pow(cos(u), 4) * cos(v) + 48 * pow(cos(u), 6) * cos(v) -
                 60 * sin(u) + 5 * cos(u) * cos(v) * sin(u) -
                 5 * pow(cos(u), 3) * cos(v) * sin(u) -
                 80 * pow(cos(u), 5) * cos(v) * sin(u) +
                 80 * pow(cos(u), 7) * cos(v) * sin(u))) /
          15. +
        (sin(u) *
         (-60 * cos(u) + 5 * pow(cos(u), 2) * cos(v) -
          5 * pow(cos(u), 4) * cos(v) - 80 * pow(cos(u), 6) * cos(v) +
          80 * pow(cos(u), 8) * cos(v) + 6 * cos(u) * cos(v) * sin(u) +
          192 * pow(cos(u), 3) * cos(v) * sin(u) -
          288 * pow(cos(u), 5) * cos(v) * sin(u) - 5 * cos(v) * pow(sin(u), 2) +
          15 * pow(cos(u), 2) * cos(v) * pow(sin(u), 2) +
          400 * pow(cos(u), 4) * cos(v) * pow(sin(u), 2) -
          560 * pow(cos(u), 6) * cos(v) * pow(sin(u), 2))) /
          15.,
      (2 * (5 * pow(cos(u), 2) - 5 * pow(sin(u), 2)) * sin(v)) / 15.);
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (2 * cos(u) * (-3 * sin(v) - 5 * cos(u) * sin(u) * sin(v))) / 15.,
      (sin(u) *
       (-3 * sin(v) + 3 * pow(cos(u), 2) * sin(v) +
        48 * pow(cos(u), 4) * sin(v) - 48 * pow(cos(u), 6) * sin(v) -
        5 * cos(u) * sin(u) * sin(v) + 5 * pow(cos(u), 3) * sin(u) * sin(v) +
        80 * pow(cos(u), 5) * sin(u) * sin(v) -
        80 * pow(cos(u), 7) * sin(u) * sin(v))) /
        15.,
      (2 * cos(v) * (3 + 5 * cos(u) * sin(u))) / 15.);
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (-2 * cos(u) *
       (3 * cos(v) - 30 * sin(u) + 90 * pow(cos(u), 4) * sin(u) -
        60 * pow(cos(u), 6) * sin(u) + 5 * cos(u) * cos(v) * sin(u))) /
          15. -
        (4 * sin(u) *
         (-30 * cos(u) + 90 * pow(cos(u), 5) - 60 * pow(cos(u), 7) +
          5 * pow(cos(u), 2) * cos(v) - 360 * pow(cos(u), 3) * pow(sin(u), 2) +
          360 * pow(cos(u), 5) * pow(sin(u), 2) -
          5 * cos(v) * pow(sin(u), 2))) /
          15. +
        (2 * cos(u) *
         (30 * sin(u) - 1170 * pow(cos(u), 4) * sin(u) +
          1140 * pow(cos(u), 6) * sin(u) - 20 * cos(u) * cos(v) * sin(u) +
          1080 * pow(cos(u), 2) * pow(sin(u), 3) -
          1800 * pow(cos(u), 4) * pow(sin(u), 3))) /
          15.,
      -(sin(u) * (3 * cos(v) - 3 * pow(cos(u), 2) * cos(v) -
                  48 * pow(cos(u), 4) * cos(v) + 48 * pow(cos(u), 6) * cos(v) -
                  60 * sin(u) + 5 * cos(u) * cos(v) * sin(u) -
                  5 * pow(cos(u), 3) * cos(v) * sin(u) -
                  80 * pow(cos(u), 5) * cos(v) * sin(u) +
                  80 * pow(cos(u), 7) * cos(v) * sin(u))) /
          15. +
        (2 * cos(u) *
         (-60 * cos(u) + 5 * pow(cos(u), 2) * cos(v) -
          5 * pow(cos(u), 4) * cos(v) - 80 * pow(cos(u), 6) * cos(v) +
          80 * pow(cos(u), 8) * cos(v) + 6 * cos(u) * cos(v) * sin(u) +
          192 * pow(cos(u), 3) * cos(v) * sin(u) -
          288 * pow(cos(u), 5) * cos(v) * sin(u) - 5 * cos(v) * pow(sin(u), 2) +
          15 * pow(cos(u), 2) * cos(v) * pow(sin(u), 2) +
          400 * pow(cos(u), 4) * cos(v) * pow(sin(u), 2) -
          560 * pow(cos(u), 6) * cos(v) * pow(sin(u), 2))) /
          15. +
        (sin(u) * (6 * pow(cos(u), 2) * cos(v) + 192 * pow(cos(u), 4) * cos(v) -
                   288 * pow(cos(u), 6) * cos(v) + 60 * sin(u) -
                   20 * cos(u) * cos(v) * sin(u) +
                   50 * pow(cos(u), 3) * cos(v) * sin(u) +
                   1280 * pow(cos(u), 5) * cos(v) * sin(u) -
                   1760 * pow(cos(u), 7) * cos(v) * sin(u) -
                   6 * cos(v) * pow(sin(u), 2) -
                   576 * pow(cos(u), 2) * cos(v) * pow(sin(u), 2) +
                   1440 * pow(cos(u), 4) * cos(v) * pow(sin(u), 2) -
                   30 * cos(u) * cos(v) * pow(sin(u), 3) -
                   1600 * pow(cos(u), 3) * cos(v) * pow(sin(u), 3) +
                   3360 * pow(cos(u), 5) * cos(v) * pow(sin(u), 3))) /
          15.,
      (-8 * cos(u) * sin(u) * sin(v)) / 3.);
  };

  const F2Vec3 f_uv = [=](phys_t u, phys_t v) {
    return Vec3(
      (-2 * sin(u) * (-3 * sin(v) - 5 * cos(u) * sin(u) * sin(v))) / 15. +
        (2 * cos(u) *
         (-5 * pow(cos(u), 2) * sin(v) + 5 * pow(sin(u), 2) * sin(v))) /
          15.,
      (cos(u) *
       (-3 * sin(v) + 3 * pow(cos(u), 2) * sin(v) +
        48 * pow(cos(u), 4) * sin(v) - 48 * pow(cos(u), 6) * sin(v) -
        5 * cos(u) * sin(u) * sin(v) + 5 * pow(cos(u), 3) * sin(u) * sin(v) +
        80 * pow(cos(u), 5) * sin(u) * sin(v) -
        80 * pow(cos(u), 7) * sin(u) * sin(v))) /
          15. +
        (sin(u) *
         (-5 * pow(cos(u), 2) * sin(v) + 5 * pow(cos(u), 4) * sin(v) +
          80 * pow(cos(u), 6) * sin(v) - 80 * pow(cos(u), 8) * sin(v) -
          6 * cos(u) * sin(u) * sin(v) -
          192 * pow(cos(u), 3) * sin(u) * sin(v) +
          288 * pow(cos(u), 5) * sin(u) * sin(v) + 5 * pow(sin(u), 2) * sin(v) -
          15 * pow(cos(u), 2) * pow(sin(u), 2) * sin(v) -
          400 * pow(cos(u), 4) * pow(sin(u), 2) * sin(v) +
          560 * pow(cos(u), 6) * pow(sin(u), 2) * sin(v))) /
          15.,
      (2 * cos(v) * (5 * pow(cos(u), 2) - 5 * pow(sin(u), 2))) / 15.);
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      (2 * cos(u) * (-3 * cos(v) - 5 * cos(u) * cos(v) * sin(u))) / 15.,
      (sin(u) *
       (-3 * cos(v) + 3 * pow(cos(u), 2) * cos(v) +
        48 * pow(cos(u), 4) * cos(v) - 48 * pow(cos(u), 6) * cos(v) -
        5 * cos(u) * cos(v) * sin(u) + 5 * pow(cos(u), 3) * cos(v) * sin(u) +
        80 * pow(cos(u), 5) * cos(v) * sin(u) -
        80 * pow(cos(u), 7) * cos(v) * sin(u))) /
        15.,
      (-2 * (3 + 5 * cos(u) * sin(u)) * sin(v)) / 15.);
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      .5   // Default zoom power.
                                                      )),
                                              4 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(Car(Car::Specs{
                                          Vec2(-.5, 18), // Initial position.
                                          1.3,           // Initial heading.
                                          0.5, // Normal forward velocity
                                          0.08 // Min distance from camera.
                                        }),
                                        0.01, // Aperture radius.
                                        0.08 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.label = "Klein Bottle";
  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;
  state.curvatureColorExaggeration = 1;

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
