//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdMobiusStrip.cpp
//
//  The StdMobiusBand implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdMobiusBand
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdMobiusStrip()
{
  const phys_t PI = static_cast<phys_t>(4.*atan(1.));

  // Domain
  const Domain domain(-1.6,             // Min u value.
                      1.6,              // Max u value.
                      Gluing::NotGlued, // Gluing in u direction.
                      0,                // Min v value.
                      2 * PI,           // Max v value.
                      Gluing::WrongWay  // Gluing in v Direction
  );

  // Tiling
  const int Rows = 6 * 3 + 1;
  const int Cols = 12 * 3;

  const auto font = fonts::surfaceSymbols();
  const KDString kdMessage(font, "M");

  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (kdMessage.characteristicFunction(
                              99 - i, j)) // 99 is due to the text height.
                          return colors::HardRed;

                        if (i == 0 || i == Rows / 2 || i == Rows - 1)
                          return colors::WarmRed;

                        return colors::White;
                      } // Tile coloring function.
  );

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]])
  {
    return Vec3(
      u * cos(v/2) * cos(v) + cos(v),
      u * cos(v/2) * sin(v) + sin(v),
      u * sin(v/2)
    );
  };

  const F2Vec3 f_u = [=](phys_t u [[maybe_unused]], phys_t v [[maybe_unused]])
  {
    return Vec3(
      cos(v/2) * cos(v),
      cos(v/2) * sin(v),
      sin(v/2)
    );
  };
  
  const F2Vec3 f_v = [=](phys_t u [[maybe_unused]], phys_t v [[maybe_unused]])
  {
    return Vec3(
      -u * sin(v/2)/2 * cos(v) - u * cos(v/2) * sin(v) - sin(v),
      -u * sin(v/2)/2 * sin(v) + u * cos(v/2) * cos(v) + cos(v),
       u * cos(v/2)/2
    );
  };
 
  const F2Vec3 f_uu = [=](phys_t u [[maybe_unused]], phys_t v [[maybe_unused]])
  {
    return Vec3(0, 0, 0);
  };
  
  const F2Vec3 f_uv = [=](phys_t u [[maybe_unused]], phys_t v [[maybe_unused]])
  {
    return Vec3(
      -sin(v/2)/2 * cos(v) - cos(v/2) * sin(v),
      -sin(v/2)/2 * sin(v) + cos(v/2) * cos(v),
      cos(v/2)/2
    );
  };
  
  
  
  const F2Vec3 f_vv = [=](phys_t u [[maybe_unused]], phys_t v [[maybe_unused]])
  {
    return Vec3(
      -u * cos(v/2)/2/2 * cos(v) + u * sin(v/2)/2 * sin(v) - cos(v) +
       u * sin(v/2)/2   * sin(v) - u * cos(v/2)   * cos(v),
      -u * cos(v/2)/2/2 * sin(v) - u * sin(v/2)/2 * cos(v) - sin(v) +
      -u * sin(v/2)/2   * cos(v) - u * cos(v/2)   * sin(v),
       u * cos(v/2)/2
    );
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(
      Camera(2.5,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0.25           // Default zoom power.
              )),
      2.5 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(Car(Car::Specs{
                                          Vec2(0.04, 1.09), // Initial position.
                                          3.1,              // Initial heading.
                                          0.8, // Normal forward velocity.
                                          0.2  // Min distance from edge.
                                        }),
                                        0.05, // Aperture radius.
                                        0.2   // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  state.label = "Mobius Strip";

  SceneProperties sceneProperties(state);

  // Scene

  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
