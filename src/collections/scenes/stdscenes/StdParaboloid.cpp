//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdParaboloid.cpp
//
//  The StdParaboloi implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdParaboloid.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdParaboloid(const bool isCarInside)
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Carful not to let the car go to the origin--the metric is degenerate!
  const Domain domain =
    (isCarInside) ? Domain(0,                        // Min v value.
                           2 * PI,                   // Max v value.
                           Gluing::RightWay,         // Gluing in v Direction
                           0.0001,                   // Min u value.
                           2,                      // Max u value.
                           Gluing::MinCrushedToPoint // Gluing in u direction.
                           )
                  : Domain(0.0001,                    // Min u value.
                           2,                       // Max u value.
                           Gluing::MinCrushedToPoint, // Gluing in u direction.
                           0,                         // Min v value.
                           2 * PI,                    // Max v value.
                           Gluing::RightWay           // Gluing in v Direction
                    );

  // Tiling

  int rows = 4 * 8 + 1;
  int cols = 4 * 8;
  if (isCarInside)
    std::swap(rows, cols);
  const Tiling tiling(rows, // Number of tiles in u direction.
                      cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (isCarInside)
                          std::swap(i, j);
                        if (i % 4 == 0)
                          return colors::WarmRed;
                        if (j % 4 == 0)
                          return colors::HardRed;
                        return colors::White;
                      } // Tile coloring function
  );

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    const phys_t A =
      (isCarInside) ? pow(domain.vMax(), 2) / 2 : pow(domain.uMax(), 2) / 2;
    return Vec3(pow(u, 2) * pow(cos(v), 2) + pow(u, 2) * pow(sin(v), 2) - A,
                u * sin(v),
                u * cos(v));
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(
      2 * u * pow(cos(v), 2) + 2 * u * pow(sin(v), 2), sin(v), cos(v));
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(0, u * cos(v), -(u * sin(v)));
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(2 * pow(cos(v), 2) + 2 * pow(sin(v), 2), 0, 0);
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(0, cos(v), -sin(v));
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (isCarInside)
      std::swap(u, v);
    return Vec3(0, -(u * sin(v)), -(u * cos(v)));
  };

  const Param param = (isCarInside) ? Param(f, f_v, f_u, f_vv, f_uv, f_uu)
                                    : Param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              4.5 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(Car(Car::Specs{
                                          Vec2(1.5, 4.86), // Initial position.
                                          2.79,            // Initial heading.
                                          0.7,  // Normal forward velocity.
                                          0.04 // Min distance from edge.
                                        }),
                                        0.05, // Aperture radius.
                                        0.4   // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;

  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;
  state.curvatureColorExaggeration = 8;
  state.label = (isCarInside) ? "Paraboloid (Inside)" : "Paraboloid (Outside)";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
