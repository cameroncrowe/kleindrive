//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdRomanSurface.cpp
//
//  StdRomanSurface implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementaiton of the StdRomanSurface
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdRomanSurface()
{
  const phys_t PI = static_cast<phys_t>(4.*atan(1.));

  const int Rows = 36;
  const int Cols = 36;

  // Domain
  phys_t uMin = -PI/2;
  phys_t uMax = PI/2;
  phys_t vMin = -PI/2;
  phys_t vMax = PI/2;

  phys_t halfTileWidth = 0; //(uMax - uMin)/Rows/2;

  const Domain domain(uMin - halfTileWidth,       // Min u value.
                      uMax - halfTileWidth,       // Max u value.
                      Gluing::WrongWay,           // Gluing in u direction.
                      vMin - halfTileWidth,       // Min v value.
                      vMax - halfTileWidth,       // Max v value.
                      Gluing::MinAndMaxCrushedToPoint// Gluing in v Direction
  );

  // Tiling
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        // One row of WarmRed.
                        if (i == 22)
                          return colors::WarmRed;
                        // One column of HardRed.
                        if (j == 22 || j == 35 - 22)
                          return colors::HardRed;

                        // White end cap.
                        if (j == 0 || j == 35)
                          return colors::White;

                        // White strips.
                        if (i % 2 == 0)
                          return colors::White;
                        return colors::Invisible;
                      } // Tile coloring function.
  );

  // Parameterization

  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(cos(u) * cos(v) * sin(v),
                cos(v) * sin(u) * sin(v),
                cos(u) * pow(cos(v), 2) * sin(u));
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-(cos(v) * sin(u) * sin(v)),
                cos(u) * cos(v) * sin(v),
                pow(cos(u), 2) * pow(cos(v), 2) -
                  pow(cos(v), 2) * pow(sin(u), 2));
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(cos(u) * pow(cos(v), 2) - cos(u) * pow(sin(v), 2),
                pow(cos(v), 2) * sin(u) - sin(u) * pow(sin(v), 2),
                -2 * cos(u) * cos(v) * sin(u) * sin(v));
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-(cos(u) * cos(v) * sin(v)),
                -(cos(v) * sin(u) * sin(v)),
                -4 * cos(u) * pow(cos(v), 2) * sin(u));
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-(pow(cos(v), 2) * sin(u)) + sin(u) * pow(sin(v), 2),
                cos(u) * pow(cos(v), 2) - cos(u) * pow(sin(v), 2),
                -2 * pow(cos(u), 2) * cos(v) * sin(v) +
                  2 * cos(v) * pow(sin(u), 2) * sin(v));
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(-4 * cos(u) * cos(v) * sin(v),
                -4 * cos(v) * sin(u) * sin(v),
                -2 * cos(u) * pow(cos(v), 2) * sin(u) +
                  2 * cos(u) * sin(u) * pow(sin(v), 2));
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(0.6, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              0.6 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        0.24,              // Normal forward velocity.
        0.05 // Min distance from edge
      }),
      0.02,           // Aperture radius.
      0.05 // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  state.label = "Roman Surface";

  SceneProperties sceneProperties(state);
  
  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
