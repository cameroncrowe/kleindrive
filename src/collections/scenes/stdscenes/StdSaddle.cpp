//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdSaddle.cpp
//
//  StdSaddle implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdSaddle.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdSaddle()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Carful not to let the car go to the origin--the metric is degenerate!
  const Domain domain(0.0001,           // Min u value.
                      1.5,                // Max u value.
                      Gluing::MinCrushedToPoint, // Gluing in u direction.
                      0,                // Min v value.
                      2 * PI,           // Max v value.
                      Gluing::RightWay  // Gluing in v Direction
  );

  // Tiling
  const int rows = 6 * 4 + 1;
  const int cols = 6 * 8;
  const Tiling tiling(rows, // Number of tiles in u direction.
                      cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (i % (rows / 4) == 0)
                          return colors::WarmRed;
                        if (j % (cols / 8) == 0)
                          return colors::HardRed;
                        return colors::White;
                      });
  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      pow(u,2)*pow(cos(v),2) - pow(u,2)*pow(sin(v),2),
      u*sin(v),
      u*cos(v)
    );
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      2*u*pow(cos(v),2) - 2*u*pow(sin(v),2),
      sin(v),
      cos(v)
    );
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -4*pow(u,2)*cos(v)*sin(v),
      u*cos(v),
      -(u*sin(v))
    );
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      2*pow(cos(v),2) - 2*pow(sin(v),2),
      0,
      0
    );
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -8*u*cos(v)*sin(v),
      cos(v),
      -sin(v)
    );
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -4*pow(u,2)*pow(cos(v),2) + 4*pow(u,2)*pow(sin(v),2),
      -(u*sin(v)),
      -(u*cos(v))
    );
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              4 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(1, -1.52), // Initial position.
        -2.6,           // Initial heading.
        0.6,            // Normal forward velocity.
        0.07             // Min distance from edge of domain.
      }),
      0.05, // Aperture radius.
      0.2   // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;

  state.label = "Hyperbolic Paraboloid";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
