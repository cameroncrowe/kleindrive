//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/stdscenes/StdSphere.cpp
//
//  StrSphere implementation file. 
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of StdSphere
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdSphere(bool isCarInside)
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const phys_t epsilon = 0.0001;

  phys_t uMin = 0;
  phys_t uMax = 2 * PI;
  Gluing uDirection = Gluing::RightWay;

  phys_t vMin = 0 + epsilon;
  phys_t vMax = PI - epsilon;
  Gluing vDirection = Gluing::MinAndMaxCrushedToPoint;

  if (!isCarInside) {
    std::swap(uMin, vMin);
    std::swap(uMax, vMax);
    std::swap(uDirection, vDirection);
  }

  const Domain domain(uMin, uMax, uDirection, vMin, vMax, vDirection);

  // Tiling
  const int M = 4;

  int Rows = 8 * M;     // Covers range 0 to 2PI
  int Cols = 1 + 4 * M; // Covers range 0 to PI

  if (!isCarInside)
    std::swap(Rows, Cols);
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (!isCarInside) {
                          std::swap(i, j);
                        }
                        // End caps.
                        if (j % M == 0)
                          return colors::WarmRed;
                        if (i %M == 0)
                          return colors::HardRed;

                        return colors::White;
                      } // Tile coloring function.
  );

  // Parameterization
  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(cos(u) * sin(v), sin(u) * sin(v), cos(v));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(sin(u) * sin(v)), cos(u) * sin(v), 0);
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(cos(u) * cos(v), cos(v) * sin(u), -sin(v));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(cos(u) * sin(v)), -(sin(u) * sin(v)), 0);
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(cos(v) * sin(u)), cos(u) * cos(v), 0);
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(cos(u) * sin(v)), -(sin(u) * sin(v)), -cos(v));
  };

  const Param param = (isCarInside) ? Param(f, f_u, f_v, f_uu, f_uv, f_vv)
                                    : Param(f, f_v, f_u, f_vv, f_uv, f_uu);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator;

  if (isCarInside) {
    AffineFrame affineFrame;
    affineFrame.position << 0, 0, 0;
    affineFrame.down << 1, 0, 0;
    affineFrame.right << 0, 1, 0;
    affineFrame.forward << 0, 0, 1;

    previewCameraOperator =
      std::make_unique<LookoutCameraOperator>(Camera(0.1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              affineFrame,
                                              -PI / 2, // Max left deflection.
                                              PI / 2,  // Max right deflection.
                                              -PI / 2, // Max down deflection.
                                              PI / 2); // Max Right deflection.
  } else {
    previewCameraOperator = std::make_unique<SphericalCameraOperator>(
      Camera(0.1,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0              // Default zoom power.
              )),
      2);
  }

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(Car(Car::Specs{
                                          Vec2(0.04, 1.09), // Initial position.
                                          3.1,              // Initial heading.
                                          1,  // Normal forward velocity.
                                          0.2 // Min distance from edge.
                                        }),
                                        0.1, // Aperture radius.
                                        0.4  // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 1;
  if (isCarInside) {
    state.label = "Sphere (Inside)";
  } else {
    state.label = "Sphere (Outside)";
  }

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
