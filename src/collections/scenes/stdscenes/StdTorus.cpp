//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/StdTorus.cpp
//
//  StdTorus implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdTorus.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdTorus(bool isCarInside)
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // We (might) assume
  //
  //   1.  P and Q are relatively prime.
  //   2.  Q > P
  //   3.  P, Q and M are positive.
  //   4.  M not too small.

  const int P = 3;
  const int Q = 5;
  const int M = 2; // Refinement

  int Rows = P * M * 4;
  int Cols = Q * M * 4;

  if (!isCarInside)
    std::swap(Rows, Cols);

  // Domain
  phys_t halfRowOffset = 2 * PI / Rows / 2;

  phys_t uMin = 0 - halfRowOffset;
  phys_t uMax = 2 * PI - halfRowOffset;
  phys_t vMin = 0;
  phys_t vMax = 2 * PI;

  if (!isCarInside) {
    std::swap(uMin, vMin);
    std::swap(uMax, vMax);
  }

  const Domain domain(uMin,             // Min u value.
                      uMax,             // Max u value.
                      Gluing::RightWay, // Gluing in u direction.
                      vMin,             // Min v value.
                      vMax,             // Max v value.
                      Gluing::RightWay  // Gluing in v Direction
  );

  // Tiling

  // We we going to put a message here... but it's hard to read without having
  // a bazillion tiles, so it's commented out.

  // const std::string message = "Torus";
  // const auto font = fonts::code8x8ishFont();
  // const KDString kdMessage(font, message);
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        // const int x = (Rows  + i)%Rows;
                        // const int y = (Cols + j)%Cols;
                        // const int halfFontHeight = 4;
                        // if(kdMessage.characteristicFunction(x,y-halfFontHeight))
                        //  return colors::Red;
                        int r = Rows / 4;
                        int c = Cols / 4;
                        if (!isCarInside) {
                          std::swap(i, j);
                          std::swap(r, c);
                        }
                        if (i % (2 * r) == 0 || j % (2 * c) == 0)
                          return colors::WarmRed;
                        if (i % (2 * r) == r || j % (2 * c) == c)
                          return colors::HardRed;
                        return colors::White;
                      } // Tile coloring function.
  );

  // Parameterization
  const phys_t A = 1;
  const phys_t B = static_cast<phys_t>(Q) / P;
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(
      A * sin(u),
      (B + A * cos(u)) * cos(v),
      (B + A * cos(u)) * sin(v)
    );
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(
       A * cos(u),
      -A * sin(u) * cos(v),
      -A * sin(u) * sin(v)
    );
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u,v);
    return Vec3(
      0,
     -(B + A * cos(u)) * sin(v),
      (B + A * cos(u)) * cos(v)

    );
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u,v);
    return Vec3(
      -A * sin(u),
      -A * cos(u) * cos(v),
      -A * cos(u) * sin(v)

    );
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u,v);
    return Vec3(
       0,
       A * sin(u) * sin(v),
      -A * sin(u) * cos(v)

    );
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u,v);
    return Vec3(
      0,
     -(B + A * cos(u)) * cos(v),
     -(B + A * cos(u)) * sin(v)
		    
    );
  };

  const Param param = (isCarInside) ? Param(f, f_u, f_v, f_uu, f_uv, f_vv)
                                    : Param(f, f_v, f_u, f_vv, f_uv, f_uu);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator;
  if (isCarInside) {
    AffineFrame affineFrame;

    const phys_t u = 0;
    const phys_t v = 0;
    const phys_t t = 0;

    affineFrame.position = param.f(u, v, t) + B * param.f_n(u, v, t);

    affineFrame.down = -param.f_u(u, v, t).normalized();
    affineFrame.right = param.f_v(u, v, t).normalized();
    ;
    affineFrame.forward = affineFrame.right.cross(affineFrame.down);

    // Try to keep the torus in the screen, but away from the menu.
    const phys_t minHorizontalDeflection = -PI / 2;
    const phys_t maxHorizontalDeflection = PI / 2;
    const phys_t minVerticalDeflection = -PI / 3;
    const phys_t maxVerticalDeflection = PI / 3;

    previewCameraOperator =
      std::make_unique<LookoutCameraOperator>(Camera(B / 8, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      -1  // Default zoom power.
                                                      )),
                                              affineFrame, // default frame,
                                              minHorizontalDeflection,
                                              maxHorizontalDeflection,
                                              minVerticalDeflection,
                                              maxVerticalDeflection);
  } else {
    previewCameraOperator = std::make_unique<SphericalCameraOperator>(
      Camera((A + B) * 1.2,  // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0.75           // Default zoom power.
              )),
      (A + B) * 1.2 // Distance from origin
    );
  }

  // Drive Camera Operator
  // =====================

  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(-1.327, 1.02), // Initial position.
        3.09,               // Initial heading.
        1,                  // Normal forward velocity
        0.2                 // Min distance from edge.
      }),
      0.01, // Aperture radius.
      0.4   // Camera height.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  if (isCarInside) {
    state.label = "Torus (Inside)";
  } else {
    state.label = "Torus (Outside)";
  }

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
