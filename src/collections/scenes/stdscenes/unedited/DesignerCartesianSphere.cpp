//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/DesignerCartesianSphere.cpp
//
//  DesignerCartesianSphere implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of CartesianSphere
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

namespace {

double
sec(double angle)
{
  return 1./cos(angle);
}

}

std::unique_ptr<Scene>
designerCartesianSphere()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Metric is degenerate at edges, which are all crushed to a point.  End far
  // enough away the physics doesn't get messed up, but near enough we can't see
  // it.

  const phys_t epsilon = 0.00001;

  const Domain domain(-PI / 2 + epsilon, // Min u value.
                      PI / 2 - epsilon,  // Max u value.
                      Gluing::MinAndMaxCrushedToPoint,  // Gluing in u direction.
                      -PI / 2 + epsilon, // Min v value.
                      PI / 2 - epsilon,  // Max v value.
                      Gluing::MinAndMaxCrushedToPoint// Gluing in v Direction
  );

  // Tiling
  int N = 4 * 6 + 1;
  const Tiling tiling(N, // Number of tiles in u direction.
                      N, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (i == 0 || i == N - 1 || j == 0 || j == N - 1)
                          return colors::WarmRed;
                        if (i % 4 == 0 && j % 4 != 2)
                          return colors::WarmRed;
                        if (i % 4 == 2 && j % 4 != 0)
                          return colors::WarmPurple;
                        if (j % 4 == 0)
                          return colors::HardRed;
                        if (j % 4 == 2)
                          return colors::HardPurple;
                        return colors::Invisible;
                      });

  // Parameterization
  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    return Vec3((2 * tan(u)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
                (2 * tan(v)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
                (-1 + pow(tan(u), 2) + pow(tan(v), 2)) /
                  (1 + pow(tan(u), 2) + pow(tan(v), 2)));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    return Vec3(
      (-4 * pow(sec(u), 2) * pow(tan(u), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(u), 2)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
      (-4 * pow(sec(u), 2) * tan(u) * tan(v)) /
        pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (-2 * pow(sec(u), 2) * tan(u) * (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(u), 2) * tan(u)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)));
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    return Vec3(
      (-4 * pow(sec(v), 2) * tan(u) * tan(v)) /
        pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (-4 * pow(sec(v), 2) * pow(tan(v), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(v), 2)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
      (-2 * pow(sec(v), 2) * tan(v) * (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(v), 2) * tan(v)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(
      (16 * pow(sec(u), 4) * pow(tan(u), 3)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (12 * pow(sec(u), 4) * tan(u)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (8 * pow(sec(u), 2) * pow(tan(u), 3)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (4 * pow(sec(u), 2) * tan(u)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
      (16 * pow(sec(u), 4) * pow(tan(u), 2) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (4 * pow(sec(u), 4) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (8 * pow(sec(u), 2) * pow(tan(u), 2) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (8 * pow(sec(u), 4) * pow(tan(u), 2) *
       (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (8 * pow(sec(u), 4) * pow(tan(u), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (2 * pow(sec(u), 4) * (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (4 * pow(sec(u), 2) * pow(tan(u), 2) *
         (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(u), 4)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)) +
        (4 * pow(sec(u), 2) * pow(tan(u), 2)) /
          (1 + pow(tan(u), 2) + pow(tan(v), 2)));
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(
      (16 * pow(sec(u), 2) * pow(sec(v), 2) * pow(tan(u), 2) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (4 * pow(sec(u), 2) * pow(sec(v), 2) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (16 * pow(sec(u), 2) * pow(sec(v), 2) * tan(u) * pow(tan(v), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (4 * pow(sec(u), 2) * pow(sec(v), 2) * tan(u)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (8 * pow(sec(u), 2) * pow(sec(v), 2) * tan(u) * tan(v) *
       (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (8 * pow(sec(u), 2) * pow(sec(v), 2) * tan(u) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2));
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(
      (16 * pow(sec(v), 4) * tan(u) * pow(tan(v), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (4 * pow(sec(v), 4) * tan(u)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (8 * pow(sec(v), 2) * tan(u) * pow(tan(v), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2),
      (16 * pow(sec(v), 4) * pow(tan(v), 3)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (12 * pow(sec(v), 4) * tan(v)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (8 * pow(sec(v), 2) * pow(tan(v), 3)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (4 * pow(sec(v), 2) * tan(v)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)),
      (8 * pow(sec(v), 4) * pow(tan(v), 2) *
       (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 3) -
        (8 * pow(sec(v), 4) * pow(tan(v), 2)) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (2 * pow(sec(v), 4) * (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) -
        (4 * pow(sec(v), 2) * pow(tan(v), 2) *
         (-1 + pow(tan(u), 2) + pow(tan(v), 2))) /
          pow(1 + pow(tan(u), 2) + pow(tan(v), 2), 2) +
        (2 * pow(sec(v), 4)) / (1 + pow(tan(u), 2) + pow(tan(v), 2)) +
        (4 * pow(sec(v), 2) * pow(tan(v), 2)) /
          (1 + pow(tan(u), 2) + pow(tan(v), 2)));
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  AffineFrame affineFrame;
  affineFrame.position << 0, 0, 0;
  affineFrame.down     << 1, 0, 0;
  affineFrame.right    << 0, 1, 0;
  affineFrame.forward  << 0, 0, 1;

  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<LookoutCameraOperator>(Camera(0.1, // Aperture radius.
                                                   ZoomPowerSlider // Zooming.
                                                   (-0, // Min zoom power.
                                                    0,  // Max zoom power.
                                                    0   // Default zoom power.
                                                    )),
                                            affineFrame,
                                            -PI / 2, // Max left defleciton.
                                            PI / 2,  // Max right deflection
                                            -PI / 2, // Max downward defleciton.
                                            PI / 2); // Max upward deflection.

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        1,                // Max forward velocity.
        2,                // Max angular velocity.
        1,                // Max strafing velocity.
        5,                // Forward spring constant.
        5,                // Angular spring constant,
        5,                // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.1,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0              // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 2,       // Max vertical beck deflection.
              -PI / 4 / 8   // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.4, // Camera height.
      0.2, // Camera back from front of car.
      0.2  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 1;
  state.label = "Cartesian Sphere";

  SceneProperties sceneProperties(state);


  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
