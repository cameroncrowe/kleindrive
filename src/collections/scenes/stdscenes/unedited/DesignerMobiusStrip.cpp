//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/DesignerMobiusStrip.cpp
//
//  DesignerMobiusStrip implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the DesignerMobiusStrip.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
designerMobiusStrip()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const Domain domain(-1.6,             // Min u value.
                      1.6,              // Max u value.
                      Gluing::NotGlued, // Gluing in u direction.
                      0,                // Min v value.
                      2 * PI,           // Max v value.
                      Gluing::WrongWay  // Gluing in v Direction
  );

  // Tiling
  const Tiling tiling(6 * 3 + 1, // Number of tiles in u direction.
                      12 * 3,    // Number of tiles in v direction.
                      [](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        return (i % 2) ? colors::Purple : colors::White;
                      });

  // Parameterization
  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      u * cos(v/2) * cos(v) + cos(v),
      u * cos(v/2) * sin(v) + sin(v),
      u * sin(v/2)
    );
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      cos(v/2) * cos(v),
      cos(v/2) * sin(v),
      sin(v/2)
    );
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -u * sin(v/2)/2 * cos(v) - u * cos(v/2) * sin(v) - sin(v),
      -u * sin(v/2)/2 * sin(v) + u * cos(v/2) * cos(v) + cos(v),
       u * cos(v/2)/2
    );
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(0, 0, 0);
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -sin(v/2)/2 * cos(v) - cos(v/2) * sin(v),
      -sin(v/2)/2 * sin(v) + cos(v/2) * cos(v),
      cos(v/2)/2
    );
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
      -u * cos(v/2)/2/2 * cos(v) + u * sin(v/2)/2 * sin(v) - cos(v) +
       u * sin(v/2)/2   * sin(v) - u * cos(v/2)   * cos(v),
      -u * cos(v/2)/2/2 * sin(v) - u * sin(v/2)/2 * cos(v) - sin(v) +
      -u * sin(v/2)/2   * cos(v) - u * cos(v/2)   * sin(v),
       u * cos(v/2)/2
    );
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              4 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        1,                // Max forward velocity.
        2,                // Max angular velocity.
        1,                // Max strafing velocity.
        5,                // Forward spring constant.
        5,                // Angular spring constant,
        5,                // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.05,           // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              -1             // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              -PI / 4 / 8   // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.4, // Camera height.
      0.4, // Camera back from front of car.
      0.2  // Max speed that neck stays turned.
    );

  // Scene Properties

  // We add a bit of bit of text so we know we're using this as a filler
  // surface.
  static int i = 0;
  std::ostringstream convert;
  convert << i;

  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  if (i == 0) {
    state.label = "Mobius Strip";
  } else {
    state.label = "Dummy Surface " + convert.str();
  }
  ++i;

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
