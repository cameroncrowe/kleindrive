//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/DesignerPolarSphere.cpp
//
//  DesignerPolarSphere implementation file. 
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of DesignerPolarSphere
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
designerPolarSphere()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const Domain domain(0,                // Min u value.
                      2 * PI,           // Max u value.
                      Gluing::RightWay, // Gluing in u direction.
                      -0 + 0.0001,      // Min v value.
                      PI - 0.0001,      // Max v value.
                      Gluing::MinAndMaxCrushedToPoint// Gluing in v Direction
  );

  // Tiling
  int N = 4 * 6 + 1;
  const Tiling tiling(32, // Number of tiles in u direction.
                      25, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (i == 0 || i == N - 1 || j == 0 || j == N - 1)
                          return colors::WarmRed;
                        if (i % 4 == 0 && j % 4 != 2)
                          return colors::WarmRed;
                        if (i % 4 == 2 && j % 4 != 0)
                          return colors::WarmPurple;
                        if (j % 4 == 0)
                          return colors::HardRed;
                        if (j % 4 == 2)
                          return colors::HardPurple;
                        return colors::Invisible;
                      } // Tile coloring Function.
  );

  // Parameterization

  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    return Vec3(cos(u) * sin(v), sin(u) * sin(v), cos(v));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    return Vec3(-(sin(u) * sin(v)), cos(u) * sin(v), 0);
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    return Vec3(cos(u) * cos(v), cos(v) * sin(u), -sin(v));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(-(cos(u) * sin(v)), -(sin(u) * sin(v)), 0);
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(-(cos(v) * sin(u)), cos(u) * cos(v), 0);
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    return Vec3(-(cos(u) * sin(v)), -(sin(u) * sin(v)), -cos(v));
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  /*
  AffineFrame affineFrame;
  affineFrame.position << 0, 0, 0;
  affineFrame.down << 1, 0, 0;
  affineFrame.right << 0, 1, 0;
  affineFrame.forward << 0, 0, 1;

  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<LookoutCameraOperator>(Camera(0.1, // Aperture radius.
                                                   ZoomPowerSlider // Zooming.
                                                   (-0, // Min zoom power.
                                                    0,  // Max zoom power.
                                                    0   // Default zoom power.
                                                    )),
                                            affineFrame,
                                            -PI / 2, // Max left deflection.
                                            PI / 2,  // Max right deflection.
                                            -PI / 2, // Max down deflection.
                                            PI / 2   // Max up deflection.
    );
  */
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(0.1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              2);
  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        1,                // Max forward velocity.
        2,                // Max angular velocity.
        1,                // Max strafing velocity.
        5,                // Forward spring constant.
        5,                // Angular spring constant,
        5,                // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.1,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0              // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              -PI / 4 / 8   // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.4, // Camera height.
      0.2, // Camera back from front of car.
      0.2  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 1;
  state.label = "Polar Sphere";

  SceneProperties sceneProperties(state);

  // Scene

  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
