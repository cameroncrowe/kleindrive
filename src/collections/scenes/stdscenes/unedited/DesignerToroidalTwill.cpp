//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/DesignerToroidalTwill.cpp
//
//  Torus implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the DesignerToroidalTwill.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/LookoutCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
designerToroidalTwill()
{
  bool isCarInside = true;

  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // We assume
  //
  //   1.  P and Q are relatively prime.
  //   2.  Q > P
  //   3.  P, Q and M are positive.
  //   4.  M not too small.

  const int P = 2;
  const int Q = 3;
  const int M = 18; // Refinement

  // Domain
  const Domain domain(0,                // Min u value.
                      2 * PI,           // Max u value.
                      Gluing::RightWay, // Gluing in u direction.
                      0,                // Min v value.
                      2 * PI,           // Max v value.
                      Gluing::RightWay  // Gluing in v Direction
  );

  // Tiling
  int Rows = P * M;
  // int Cols = Q * M;

  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Rows, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (!isCarInside)
                          std::swap(i, j);

                        if (i % 6 == 0 && j % 6 != 4)
                          return colors::HardPurple;
                        if (i % 6 == 2 && j % 6 != 2)
                          return colors::HardRed;
                        if (i % 6 == 4 && j % 6 != 0)
                          return colors::HardWhite;

                        if (j % 6 == 4)
                          return colors::WarmWhite;
                        if (j % 6 == 2)
                          return colors::WarmRed;
                        if (j % 6 == 0)
                          return colors::WarmPurple;

                        return colors::Invisible;
                      } // Tile coloring function
  );

  // Parameterization
  const phys_t A = 1;
  const phys_t B = static_cast<phys_t>(Q) / P;

  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);

    return Vec3(A * sin(u - v),
                (B + A * cos(u - v)) * cos(u + v),
                (B + A * cos(u - v)) * sin(u + v));
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(
      A * cos(u - v),
      -(A * cos(u + v) * sin(u - v)) - (B + A * cos(u - v)) * sin(u + v),
      (B + A * cos(u - v)) * cos(u + v) - A * sin(u - v) * sin(u + v));
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(A * cos(u - v)),
                A * cos(u + v) * sin(u - v) - (B + A * cos(u - v)) * sin(u + v),
                (B + A * cos(u - v)) * cos(u + v) + A * sin(u - v) * sin(u + v)

    );
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(A * sin(u - v)),
                -(A * cos(u - v) * cos(u + v)) -
                  (B + A * cos(u - v)) * cos(u + v) +
                  2 * A * sin(u - v) * sin(u + v),
                -2 * A * cos(u + v) * sin(u - v) - A * cos(u - v) * sin(u + v) -
                  (B + A * cos(u - v)) * sin(u + v)

    );
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(A * sin(u - v),
                A * cos(u - v) * cos(u + v) - (B + A * cos(u - v)) * cos(u + v),
                A * cos(u - v) * sin(u + v) - (B + A * cos(u - v)) * sin(u + v)

    );
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    if (!isCarInside)
      std::swap(u, v);
    return Vec3(-(A * sin(u - v)),
                -(A * cos(u - v) * cos(u + v)) -
                  (B + A * cos(u - v)) * cos(u + v) -
                  2 * A * sin(u - v) * sin(u + v),
                2 * A * cos(u + v) * sin(u - v) - A * cos(u - v) * sin(u + v) -
                  (B + A * cos(u - v)) * sin(u + v)

    );
  };

  const Param param = (isCarInside) ? Param(f, f_u, f_v, f_uu, f_uv, f_vv)
                                    : Param(f, f_v, f_u, f_vv, f_uv, f_uu);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator

  // We short circuit this one... we want the camera outside, but we might
  // change our mind.
  std::unique_ptr<CameraOperator> previewCameraOperator;
  if (false && isCarInside) {
    AffineFrame affineFrame;

    const phys_t u = 0;
    const phys_t v = 0;
    const phys_t t = 0;

    affineFrame.position = param.f(u, v, t) + 0.1 * param.f_n(u, v, t);

    affineFrame.down = -param.f_u(u, v, t).normalized();
    affineFrame.right = param.f_v(u, v, t).normalized();
    ;
    affineFrame.forward = affineFrame.right.cross(affineFrame.down);

    // Try to keep the torus in the screen, but away from the menu.
    const phys_t minHorizontalDeflection = -PI / 4;
    const phys_t maxHorizontalDeflection = -PI / 12;
    const phys_t minVerticalDeflection = -PI / 4;
    const phys_t maxVerticalDeflection = PI / 4;

    previewCameraOperator =
      std::make_unique<LookoutCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              affineFrame, // default frame,
                                              minHorizontalDeflection,
                                              maxHorizontalDeflection,
                                              minVerticalDeflection,
                                              maxVerticalDeflection);
  } else {
    previewCameraOperator = std::make_unique<SphericalCameraOperator>(
      Camera(1,              // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-0,            // Min zoom power.
              0,             // Max zoom power.
              0              // Default zoom power.
              )),
      4 // Distance from origin
    );
  }

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        1,                // Max forward velocity.
        2,                // Max angular velocity.
        1,                // Max strafing velocity.
        5,                // Forward spring constant.
        5,                // Angular spring constant,
        5,                // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.01,           // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              -1             // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              (isCarInside) ? PI / 4 / 12
                            : -PI / 4 // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.4, // Camera height.
      0.2, // Camera back from front of car.
      0.5  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  if (isCarInside) {
    state.label = "Trefoil Twill"; // Inside";
  } else {
    state.label = "Trefoil Twill"; // Outside";
  }

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
