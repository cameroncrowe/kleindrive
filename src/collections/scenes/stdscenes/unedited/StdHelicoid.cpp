//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/StdHelicoid.cpp
//
//  StdHelicoid implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of the StdHelicoid.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
stdHelicoid()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain

  // Carful not to let the car go to the origin--the metric is degenerate!
  const Domain domain(0,           // Min u value.
                      2,                // Max u value.
                      Gluing::NotGlued, // Gluing in u direction.
                      2*PI,                // Min v value.
                      -2*PI,           // Max v value.
                      Gluing::NotGlued// Gluing in v Direction
  );

  // Tiling
  const Tiling tiling(16, // Number of tiles in u direction.
                      28, // Number of tiles in v direction.
                      [](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (j % 7 == 0)
                          return colors::WarmRed;
                        if (j % 2 == 0)
                          return colors::HardRed;
                        return colors::White;
                      });
  // Parameterization




 const phys_t a = 1; // Controls the period.




  const F2Vec3 f = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
u*cos(a*v),u*sin(a*v),v
    );
  };

  const F2Vec3 f_u = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
cos(a*v),sin(a*v),0
    );
  };

  const F2Vec3 f_v = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
-(a*u*sin(a*v)),a*u*cos(a*v),1
    );
  };

  const F2Vec3 f_uu = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
0,0,0
    );
  };

  const F2Vec3 f_uv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
-(a*sin(a*v)),a*cos(a*v),0
    );
  };

  const F2Vec3 f_vv = [=](phys_t u[[maybe_unused]], phys_t v[[maybe_unused]]) {
    return Vec3(
-(pow(a,2)*u*cos(a*v)),-(pow(a,2)*u*sin(a*v)),0
    );
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              2 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(1, -1.52), // Initial position.
        -2.6,           // Initial heading.
        0.5,            // Max forward velocity.
        2,              // Max angular velocity.
        0.5,            // Max strafing velocity.
        5,              // Forward spring constant.
        5,              // Angular spring constant,
        5,              // Strafing spring constant.
        0.001           // Time step.
      }),
      Camera(0.05,           // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              -1             // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              -PI / 4 / 6   // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.2, // Camera height.
      0.2, // Camera back from front of car.
      0.1  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 2;
  // state.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;

  state.label = "Helicoid";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
