//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/WavesConical.cpp
//
//  WavesConical implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of WavesConical.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
wavesConical()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const Domain domain(0,                // Min v value.
                      2 * PI,           // Max v value.
                      Gluing::RightWay, // Gluing in v Direction
                      5,                // Min u value.
                      50,               // Max u value.
                      Gluing::NotGlued  // Gluing in u direction.
  );

  // Tiling
  const int Rows = 3 * 16;
  const int Cols = 4 * 7 + 1;
  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (i % (Rows/3) == 0)
                          return colors::HardRed;
                        if (j % 4 == 0)
                          return colors::WarmRed;
                        if (j % 4 == 2)
                          return colors::White;
                        return colors::Invisible;
                      } // Tile coloring function.
  );

  // Parameterization
  const phys_t speed = 0.4;

  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3(25 - u / 2. + (4 * sin(t + 3 * v)) / (2 + sin(t + 3 * v)),
                u * cos(v),
                u * sin(v));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3(-0.5, cos(v), sin(v));
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3((-12 * cos(t + 3 * v) * sin(t + 3 * v)) /
                    pow(2 + sin(t + 3 * v), 2) +
                  (12 * cos(t + 3 * v)) / (2 + sin(t + 3 * v)),
                -(u * sin(v)),
                u * cos(v));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3(0, 0, 0);
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3(0, -sin(v), cos(v));
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    std::swap(u, v);
    t *= speed;
    return Vec3((72 * pow(cos(t + 3 * v), 2) * sin(t + 3 * v)) /
                    pow(2 + sin(t + 3 * v), 3) -
                  (72 * pow(cos(t + 3 * v), 2)) / pow(2 + sin(t + 3 * v), 2) +
                  (36 * pow(sin(t + 3 * v), 2)) / pow(2 + sin(t + 3 * v), 2) -
                  (36 * sin(t + 3 * v)) / (2 + sin(t + 3 * v)),
                -(u * cos(v)),
                -(u * sin(v)));
  };

  const Param param(f, f_v, f_u, f_vv, f_uv, f_uu);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-0, // Min zoom power.
                                                      0,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              100 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(27.1, 30.4), // Initial position.
        -1.57,            // Initial heading.
        8,                // Max forward velocity.
        2,                // Max angular velocity.
        8,                // Max strafing velocity.
        5,                // Forward spring constant.
        5,                // Angular spring constant,
        5,                // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.2,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0              // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              +PI / 4 / 12  // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      2, // Camera height.
      2, // Camera back from front of car.
      1  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 1;
  state.label = "Conical Waves";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
