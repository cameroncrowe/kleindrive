//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/scenes/WavesSpherical.cpp
//
//  WavesSpherical implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains implementaiton of WavesSpherical.
///
//===----------------------------------------------------------------------===//

#include "src/collections/cameraoperators/CarCameraOperator.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <sstream>
#include <string>

namespace kleindrive {
namespace scenes {

using namespace cameraoperators;

std::unique_ptr<Scene>
wavesSpherical()
{
  const phys_t PI = static_cast<phys_t>(4. * atan(1.));

  // Domain
  const Domain domain(0,                              // Min u value.
                      2 * PI,                         // Max u value.
                      Gluing::RightWay,               // Gluing in u direction.
                      0,                              // Min v value.
                      PI,                             // Max v value.
                      Gluing::MinAndMaxCrushedToPoint // Gluing in v Direction
  );

  // Tiling
  const int Rows = 68;
  const int Cols = 2 * 15 + 1;

  const auto font = fonts::surfaceSymbols();
  const KDString kdMessage(font, "E"); // E is for earth.

  const Tiling tiling(Rows, // Number of tiles in u direction.
                      Cols, // Number of tiles in v direction.
                      [=](int i[[maybe_unused]], int j[[maybe_unused]]) {
                        if (j < 2 || j > Cols - 1 - 2)
                          return colors::WarmRed;
                        if (j == Cols / 2)
                          return colors::WarmRed;
                        if (kdMessage.characteristicFunction(99 - j, i + 1))
                          // 99 is due to the text height.
                          return colors::HardRed;
                        return colors::White;
                      } // Tile coloring function
  );

  // Parameterization
  const phys_t speed = 1;

  int M = 7;
  int N = 7;
  phys_t a = 0.1;

  const F2p1Vec3 f = [=](phys_t u[[maybe_unused]],
                         phys_t v[[maybe_unused]],
                         phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(sin(u) * sin(v) *
                  (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
                cos(u) * sin(v) *
                  (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
                cos(v) *
                  (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)));
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(
      a * sin(t) * N * cos(N * u) * sin(u) * pow(sin(v), 3) * sin(M * v) +
        cos(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      a * sin(t) * N * cos(u) * cos(N * u) * pow(sin(v), 3) * sin(M * v) -
        sin(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      a * sin(t) * N * cos(N * u) * cos(v) * pow(sin(v), 2) * sin(M * v));
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(
      sin(u) * sin(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) +
        cos(v) * sin(u) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      cos(u) * sin(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) +
        cos(u) * cos(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      cos(v) * (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
                2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) -
        sin(v) * (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)));
  };

  const F2p1Vec3 f_uu = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(
      2 * a * sin(t) * N * cos(u) * cos(N * u) * pow(sin(v), 3) * sin(M * v) -
        a * sin(t) * pow(N, 2) * sin(u) * sin(N * u) * pow(sin(v), 3) *
          sin(M * v) -
        sin(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      -2 * a * sin(t) * N * cos(N * u) * sin(u) * pow(sin(v), 3) * sin(M * v) -
        a * sin(t) * pow(N, 2) * cos(u) * sin(N * u) * pow(sin(v), 3) *
          sin(M * v) -
        cos(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      -(a * sin(t) * pow(N, 2) * cos(v) * sin(N * u) * pow(sin(v), 2) *
        sin(M * v)));
  };

  const F2p1Vec3 f_uv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(
      a * sin(t) * M * N * cos(N * u) * cos(M * v) * sin(u) * pow(sin(v), 3) +
        3 * a * sin(t) * N * cos(N * u) * cos(v) * sin(u) * pow(sin(v), 2) *
          sin(M * v) +
        cos(u) * sin(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) +
        cos(u) * cos(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      a * sin(t) * M * N * cos(u) * cos(N * u) * cos(M * v) * pow(sin(v), 3) +
        3 * a * sin(t) * N * cos(u) * cos(N * u) * cos(v) * pow(sin(v), 2) *
          sin(M * v) -
        sin(u) * sin(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) -
        cos(v) * sin(u) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      a * sin(t) * M * N * cos(N * u) * cos(v) * cos(M * v) * pow(sin(v), 2) +
        2 * a * sin(t) * N * cos(N * u) * pow(cos(v), 2) * sin(v) * sin(M * v) -
        a * sin(t) * N * cos(N * u) * pow(sin(v), 3) * sin(M * v));
  };

  const F2p1Vec3 f_vv = [=](phys_t u[[maybe_unused]],
                            phys_t v[[maybe_unused]],
                            phys_t t[[maybe_unused]]) {
    t *= speed;
    return Vec3(
      2 * cos(v) * sin(u) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) -
        sin(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)) +
        sin(u) * sin(v) *
          (4 * a * sin(t) * M * cos(v) * cos(M * v) * sin(N * u) * sin(v) +
           2 * a * sin(t) * pow(cos(v), 2) * sin(N * u) * sin(M * v) -
           2 * a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v) -
           a * sin(t) * pow(M, 2) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      2 * cos(u) * cos(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) -
        cos(u) * sin(v) *
          (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)) +
        cos(u) * sin(v) *
          (4 * a * sin(t) * M * cos(v) * cos(M * v) * sin(N * u) * sin(v) +
           2 * a * sin(t) * pow(cos(v), 2) * sin(N * u) * sin(M * v) -
           2 * a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v) -
           a * sin(t) * pow(M, 2) * sin(N * u) * pow(sin(v), 2) * sin(M * v)),
      -2 * sin(v) *
          (a * sin(t) * M * cos(M * v) * sin(N * u) * pow(sin(v), 2) +
           2 * a * sin(t) * cos(v) * sin(N * u) * sin(v) * sin(M * v)) -
        cos(v) * (1 + a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v)) +
        cos(v) *
          (4 * a * sin(t) * M * cos(v) * cos(M * v) * sin(N * u) * sin(v) +
           2 * a * sin(t) * pow(cos(v), 2) * sin(N * u) * sin(M * v) -
           2 * a * sin(t) * sin(N * u) * pow(sin(v), 2) * sin(M * v) -
           a * sin(t) * pow(M, 2) * sin(N * u) * pow(sin(v), 2) * sin(M * v)));
  };

  const Param param(f, f_u, f_v, f_uu, f_uv, f_vv);

  // TiledSruface
  const TiledSurface tiledSurface(param, domain, tiling);

  // Preview Camera Operator
  std::unique_ptr<CameraOperator> previewCameraOperator =
    std::make_unique<SphericalCameraOperator>(Camera(1, // Aperture radius.
                                                     ZoomPowerSlider // Zooming.
                                                     (-3, // Min zoom power.
                                                      3,  // Max zoom power.
                                                      0   // Default zoom power.
                                                      )),
                                              2 // Distance from origin
    );

  // Drive Camera Operator
  std::unique_ptr<CameraOperator> driveCameraOperator =
    std::make_unique<CarCameraOperator>(
      Car(Car::Specs{
        Vec2(0.04, 1.09), // Initial position.
        3.1,              // Initial heading.
        0.09,             // Max forward velocity.
        0.5,              // Max angular velocity.
        0.09,             // Max strafing velocity.
        1.5,              // Forward spring constant.
        1.5,              // Angular spring constant,
        1.5,              // Strafing spring constant.
        0.001             // Time step.
      }),
      Camera(0.1,            // Aperture radius.
             ZoomPowerSlider // Zooming.
             (-3,            // Min zoom power.
              3,             // Max zoom power.
              0              // Default zoom power.
              ),
             NeckTurnSlider // Vertical neck turning.
             (PI / 3,       // Max vertical beck deflection.
              -PI / 4 / 2   // Default downward neck deflection
              ),
             false // Automatically return neck turn sliders to defaults.
             ),
      0.2, // Camera height.
      0.2, // Camera back from front of car.
      0.2  // Max speed that neck stays turned.
    );

  // Scene Properties
  SceneProperties::State state;
  state.curvatureColorExaggeration = 1;
  state.label = "Ocean World";

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            std::move(previewCameraOperator),
                            std::move(driveCameraOperator),
                            sceneProperties);

  return scene;
}

} // namespace scenes
} // namespace kleindrive
