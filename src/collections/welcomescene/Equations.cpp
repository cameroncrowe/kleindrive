//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/welcomescene/Equations.cpp
//
//  The equations for the Klein Drive welcome scene implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the equations for the  Klein Drive's welcome scene
/// implementation.
///
//===----------------------------------------------------------------------===//

// This first despite the name.
#include "src/collections/welcomescene/KDWelcomeScene.h"

#include "src/collections/Colors.h"
#include "src/collections/cameraoperators/FontReader.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <string>

namespace kleindrive {

namespace scenes {

namespace welcomesceneequations {

Param
computeParam(const phys_t scrollVelocity)
{

  // f is computed as follows:
  //
  // 1) Write the map taking R^2 to the statndard  torus centered at
  // the origin, with fundamental domain a square.
  //
  // 2) take the line going at slope 2:3 from the origin.  This
  //    forms the trefoil knot.
  //
  // 3) Suppose you are at the origin. Draw the vector out
  //    through the point of the  trefoil knot at time t.
  //
  // 4) Take that vector's cross product with the tangent
  //    vector to the point on the trefoil at time t.
  //
  // 5) Thicken the trefoil knot along the vector from 4.
  //
  // The result is a strip that is always facing the origin.
  const F2p1Vec3 f = [scrollVelocity](phys_t u[[maybe_unused]],
                                      phys_t v[[maybe_unused]],
                                      phys_t t[[maybe_unused]]) {
    v -= scrollVelocity * t;
    return Vec3(

      cos(v) * (3 + cos((3 * v) / 2.)) +
        u *
          ((-27 * cos((3 * v) / 2.) * sin(v)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(
                36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(
                36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * cos(v) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(
                36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      (3 + cos((3 * v) / 2.)) * sin(v) +
        u *
          ((27 * cos(v) * cos((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * sin(v) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      3 * sin((3 * v) / 2.) +
        u *
          ((-18 * pow(cos(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (12 * pow(cos(v), 2) * cos((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (2 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (18 * pow(sin(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (12 * cos((3 * v) / 2.) * pow(sin(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (2 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))))

    );
  };

  const F2p1Vec3 f_u = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    v -= scrollVelocity * t;
    return Vec3(

      (-27 * cos((3 * v) / 2.) * sin(v)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * pow(cos((3 * v) / 2.), 2) * sin(v)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (18 * cos(v) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * sin(v) * pow(sin((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))),
      (27 * cos(v) * cos((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (9 * cos(v) * pow(cos((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (18 * sin(v) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (9 * cos(v) * pow(sin((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))),
      (-18 * pow(cos(v), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (12 * pow(cos(v), 2) * cos((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (2 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (18 * pow(sin(v), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (12 * cos((3 * v) / 2.) * pow(sin(v), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (2 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))

    );
  };

  const F2p1Vec3 f_v = [=](phys_t u[[maybe_unused]],
                           phys_t v[[maybe_unused]],
                           phys_t t[[maybe_unused]]) {
    v -= scrollVelocity * t;
    return Vec3(

      -((3 + cos((3 * v) / 2.)) * sin(v)) -
        (3 * cos(v) * sin((3 * v) / 2.)) / 2. +
        u *
          ((27 * cos((3 * v) / 2.) * sin(v) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (27 * cos((3 * v) / 2.) * sin(v) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (45 * sin(v) * sin((3 * v) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (18 * cos(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      cos(v) * (3 + cos((3 * v) / 2.)) - (3 * sin(v) * sin((3 * v) / 2.)) / 2. +
        u *
          ((-27 * cos(v) * cos((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * sin(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (27 * cos(v) * cos((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (45 * cos(v) * sin((3 * v) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (18 * sin(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      (9 * cos((3 * v) / 2.)) / 2. +
        u *
          ((9 * pow(cos(v), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(sin(v), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(cos(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * pow(cos(v), 2) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * pow(sin(v), 2) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))))

    );
  };

  const F2p1Vec3 f_uu = [scrollVelocity](phys_t u[[maybe_unused]],
                                         phys_t v[[maybe_unused]],
                                         phys_t t[[maybe_unused]]) {
    return Vec3(0, 0, 0);
  };

  const F2p1Vec3 f_uv = [scrollVelocity](phys_t u[[maybe_unused]],
                                         phys_t v[[maybe_unused]],
                                         phys_t t[[maybe_unused]]) {
    v -= scrollVelocity * t;
    return Vec3(

      (27 * cos((3 * v) / 2.) * sin(v) *
       (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
        243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
        15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
        36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
        15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (9 * cos(v) * sin((3 * v) / 2.) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (27 * cos((3 * v) / 2.) * sin(v) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * cos(v) * sin((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (45 * sin(v) * sin((3 * v) / 2.)) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (18 * cos(v) * pow(sin((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))),
      (-27 * cos(v) * cos((3 * v) / 2.) *
       (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
        243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
        15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
        36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
        15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (9 * sin(v) * sin((3 * v) / 2.) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) -
        (27 * cos(v) * cos((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * sin(v) * sin((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (2. *
           pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (45 * cos(v) * sin((3 * v) / 2.)) /
          (2. *
           sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
        (18 * sin(v) * pow(sin((3 * v) / 2.), 2)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))),
      (9 * pow(cos(v), 2) *
       (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
        243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
        15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
        36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
        15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (9 * pow(sin(v), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
         (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
          243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
          15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
          15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           pow(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 81 * pow(cos((3 * v) / 2.), 2) +
                 4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
               1.5)) +
        (9 * pow(cos(v), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (9 * pow(sin(v), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
         (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
          27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
          9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
          3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
          (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                 pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                 9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                 pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                 9 * pow(sin((3 * v) / 2.), 2),
               1.5) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (18 * pow(cos(v), 2) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (18 * pow(sin(v), 2) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
        (6 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) /
          (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(sin((3 * v) / 2.), 2)) *
           sqrt(36 * pow(cos(v), 2) + 24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                81 * pow(cos((3 * v) / 2.), 2) +
                4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                36 * pow(sin(v), 2) + 24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))

    );
  };

  const F2p1Vec3 f_vv = [scrollVelocity](phys_t u[[maybe_unused]],
                                         phys_t v[[maybe_unused]],
                                         phys_t t[[maybe_unused]]) {
    v -= scrollVelocity * t;
    return Vec3(

      (-9 * cos(v) * cos((3 * v) / 2.)) / 4. -
        cos(v) * (3 + cos((3 * v) / 2.)) + 3 * sin(v) * sin((3 * v) / 2.) +
        u *
          ((-81 * cos((3 * v) / 2.) * sin(v) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (27 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (27 * cos(v) * sin((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (9 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (27 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (27 * cos((3 * v) / 2.) * sin(v) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * cos(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (45 * sin(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (18 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (27 * cos((3 * v) / 2.) * sin(v) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * sin((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (81 * cos((3 * v) / 2.) * sin(v) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (27 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * cos(v) * sin((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (27 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (45 * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (135 * cos((3 * v) / 2.) * sin(v)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (45 * cos(v) * sin((3 * v) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (60 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * sin(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * cos((3 * v) / 2.) * sin(v) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(cos((3 * v) / 2.), 2) * sin(v) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * sin((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      (-9 * cos((3 * v) / 2.) * sin(v)) / 4. -
        (3 + cos((3 * v) / 2.)) * sin(v) - 3 * cos(v) * sin((3 * v) / 2.) +
        u *
          ((81 * cos(v) * cos((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (27 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (27 * sin(v) * sin((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (9 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (27 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) +
           (27 * cos(v) * cos((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (45 * cos(v) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (18 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (27 * cos(v) * cos((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * sin(v) * sin((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (81 * cos(v) * cos((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * sin(v) * sin((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (4. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (45 * cos(v) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (6 * cos(v) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (18 * sin(v) * pow(sin((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (135 * cos(v) * cos((3 * v) / 2.)) /
             (4. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (45 * sin(v) * sin((3 * v) / 2.)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (60 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (27 * cos(v) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (27 * cos(v) * cos((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * pow(cos((3 * v) / 2.), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * sin(v) * sin((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * cos((3 * v) / 2.) * sin(v) * sin((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos(v) * pow(sin((3 * v) / 2.), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)))),
      (-27 * sin((3 * v) / 2.)) / 4. +
        u *
          ((-27 * pow(cos(v), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (9 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (3 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (27 * pow(sin(v), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (9 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (3 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            pow(-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
                  243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
                  15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
                  15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  2.5)) -
           (9 * pow(cos(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (9 * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.)) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (18 * pow(cos(v), 2) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (18 * pow(sin(v), 2) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.) *
            (-36 * pow(cos(v), 2) * sin((3 * v) / 2.) -
             243 * cos((3 * v) / 2.) * sin((3 * v) / 2.) +
             15 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             36 * pow(sin(v), 2) * sin((3 * v) / 2.) +
             15 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(cos(v), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (9 * pow(sin(v), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) +
           (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            (-54 * pow(cos(v), 2) * cos((3 * v) / 2.) -
             (729 * pow(cos((3 * v) / 2.), 2)) / 2. +
             (45 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             54 * cos((3 * v) / 2.) * pow(sin(v), 2) +
             (45 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. +
             (729 * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. -
             (45 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              pow(36 * pow(cos(v), 2) +
                    24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    81 * pow(cos((3 * v) / 2.), 2) +
                    4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    36 * pow(sin(v), 2) +
                    24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2),
                  1.5)) -
           (27 * pow(cos(v), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (27 * pow(sin(v), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (3 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            pow(-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
                  27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
                  9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
                  3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.),
                2)) /
             (2. *
              pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  2.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (18 * pow(cos(v), 2) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (18 * pow(sin(v), 2) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.) *
            (-9 * pow(cos(v), 2) * sin((3 * v) / 2.) +
             27 * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             3 * pow(cos(v), 2) * cos((3 * v) / 2.) * sin((3 * v) / 2.) -
             9 * pow(sin(v), 2) * sin((3 * v) / 2.) -
             3 * cos((3 * v) / 2.) * pow(sin(v), 2) * sin((3 * v) / 2.))) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * pow(cos(v), 2) * cos((3 * v) / 2.)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) -
           (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) /
             (sqrt(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) + 6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(sin((3 * v) / 2.), 2)) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(cos(v), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * pow(cos(v), 2) * cos((3 * v) / 2.) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (9 * pow(sin(v), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (6 * cos((3 * v) / 2.) * pow(sin(v), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))) +
           (pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) *
            ((-27 * pow(cos(v), 2) * cos((3 * v) / 2.)) / 2. +
             (81 * pow(cos((3 * v) / 2.), 2)) / 2. -
             (9 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2)) / 2. -
             (27 * cos((3 * v) / 2.) * pow(sin(v), 2)) / 2. -
             (9 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2)) / 2. -
             (81 * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2. +
             (9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2)) / 2.)) /
             (pow(9 * pow(cos(v), 2) + 6 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                    pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                    9 * pow(sin(v), 2) +
                    6 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                    pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                    9 * pow(sin((3 * v) / 2.), 2),
                  1.5) *
              sqrt(36 * pow(cos(v), 2) +
                   24 * pow(cos(v), 2) * cos((3 * v) / 2.) +
                   81 * pow(cos((3 * v) / 2.), 2) +
                   4 * pow(cos(v), 2) * pow(cos((3 * v) / 2.), 2) +
                   36 * pow(sin(v), 2) +
                   24 * cos((3 * v) / 2.) * pow(sin(v), 2) +
                   4 * pow(cos((3 * v) / 2.), 2) * pow(sin(v), 2) +
                   9 * pow(cos(v), 2) * pow(sin((3 * v) / 2.), 2) +
                   9 * pow(sin(v), 2) * pow(sin((3 * v) / 2.), 2))))

    );
  };

  return Param(f, f_u, f_v, f_uu, f_uv, f_vv);
};

} // namespace welcomesceneequations

} // namespace scenes

} // namespace kleindrive
