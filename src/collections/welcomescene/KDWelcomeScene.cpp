//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/welcomescene/KDWelcomeScene.h
//
//  Klein Drive welcome scene implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains Klein Drive's welcome scene implementation.
///
//===----------------------------------------------------------------------===//

#include "src/collections/welcomescene/KDWelcomeScene.h"
#include "src/collections/Colors.h"
#include "src/collections/cameraoperators/FontReader.h"
#include "src/collections/cameraoperators/SphericalCameraOperator.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/graphics/Color.h"
#include "src/physics/DampenedSlider.h"
#include "src/scene/Scene.h"
#include "src/string/KDString.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <functional>
#include <memory>
#include <string>

namespace kleindrive {

namespace {

// Pi!
const phys_t PI = 4 * atan(1);

} // namespace:w

// Param computeParam(phys_t scrollVelocity) is provided in Equations.cpp
// because it is about 5000 lines long.

namespace scenes {

std::unique_ptr<Scene>
kdWelcomeScene()
{

  // Domain
  const Domain domain(-0.8,             // Min u value.
                      0.8,              // Max u value.
                      Gluing::NotGlued, // Gluing in u direction.
                      0,                // Min v value.
                      4 * PI,           // Max v value.
                      Gluing::RightWay  // Gluing in v Direction
  );

  // Tiling

  const auto font = fonts::code8x8ishFont();
  const std::string message = "Klein Drive                          "
                              "Copyright 2018                  "
                              "By Cameron Crowe                        !";

  KDString scrollText(font, message);

  const Tiling tiling(2 * 5 + 2, // Number of tiles in u direction.
                      150 * 5,   // Number of tiles in v direction.
                      [=](int i, int j) {
                        return (scrollText.characteristicFunction((i - 2), j) ||
                                i == 0 || i == 11)
                                 ? colors::Red
                                 : colors::Invisible;
                      });

  // Parameterization

  const phys_t scrollVelocity = 1.;

  const Param param = welcomesceneequations::computeParam(scrollVelocity);

  // TiledSruface

  const TiledSurface tiledSurface(param, domain, tiling);

  // FontReader

  // We put the camera right up next to the font, and make the light decay
  // quickly to get some contrast between the foreground and the background.
  std::unique_ptr<CameraOperator> scrollingCameraOperator1 =
    std::make_unique<cameraoperators::FontReader>(
      0.1,                   // Distance from surface.
      scrollVelocity,        // Scroll speed.
      Camera(6,              // Aperture radius.
             ZoomPowerSlider // Zooming.
             (1              // Fixed zoom
              )));

  // Scene Properties
  SceneProperties::State state;
  state.lightDecayRate = 5;
  state.isLocked = true;

  SceneProperties sceneProperties(state);

  // Scene
  std::unique_ptr<Scene> scene =
    std::make_unique<Scene>(tiledSurface,
                            // std::move(sphericalCameraOperator)
                            std::move(scrollingCameraOperator1),
                            sceneProperties);

  return scene;
}

} // namespace scenes

} // namespace kleindrive
