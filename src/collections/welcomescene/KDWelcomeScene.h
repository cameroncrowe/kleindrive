//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/collections/welcomescene/KDWelcomeScene.h
//
//  Klein Drive welcome scene definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains Klein Drive's welcome scene definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDWELCOMESCENE_H
#define KLEINDRIVE_KDWELCOMESCENE_H

#include <memory>
#include "src/scene/Scene.h"

namespace kleindrive {

namespace scenes {

namespace welcomesceneequations {

/// The 5000 lines of equations for the scrolltext are in another file.
Param
computeParam(phys_t scrollVelocity);

}


/// Returns Klein Drive's scene grouping.
std::unique_ptr<Scene>
kdWelcomeScene();

} // namespace scenes

} // namespace kleindrive

#endif // KLEINDRIVE_KDWELCOMESCENE_H
