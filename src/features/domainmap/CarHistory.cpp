//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/features/domainmap/CarHistory.cpp
//
//  CarHistory class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementation of the CarHistory class.
///
//===----------------------------------------------------------------------===//

#include "src/features/domainmap/CarHistory.h"
#include "src/gamestate/GameState.h"
#include "src/gui/Box.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/x.H>
#include <chrono>
#include <cmath>
#include <deque>
#include <iostream>

namespace kleindrive {

namespace {

// Pi!
const phys_t PI = 4 * atan (1);

} // namespace

CarHistory::CarHistory(Box box,
                       const TiledSurface& tiledSurface,
                       phys_t maxSecondsOfHistory)
  : maxSecondsOfHistory_{ maxSecondsOfHistory }
  , box_{ box }
  , history_{}
  , tiledSurface_{ &tiledSurface }
  , uMin_{ tiledSurface.uMin() }
  , uRange_{ tiledSurface.uRange() }
  , uDirection_{ tiledSurface.uDirection() }
  , vMin_{ tiledSurface.vMin() }
  , vRange_{ tiledSurface.vRange() }
  , vDirection_{ tiledSurface.vDirection() }
{ }

void
CarHistory::update(Vec2 position, phys_t heading, phys_t time)
{
  CarOnMap current = computeCarOnMap(position, heading, time);
  history_.push_back(current);

  // Delete history longer than 10 seconds ago... but keep 1.
  while (history_.size() > 1 &&
         time - history_[0].time > maxSecondsOfHistory_) {
    history_.pop_front();
  }
}

CarOnMap
CarHistory::current() const
{
  if (history_.size() > 0)
    return history_[history_.size() - 1];
  return {};
}

const std::deque<CarOnMap>
CarHistory::history() const
{
  return history_;
}

void
CarHistory::reset()
{
  history_.resize(0);
}

namespace {

// Returns true if and only if n is odd.
bool
isOdd(int n)
{
  return 1 == (n % 2 + 2) % 2;
}

}


CarOnMap
CarHistory::computeCarOnMap(Vec2 position, phys_t heading, phys_t time)
{
  // Need to compute the following data for a car on the map:
  //   - x, y
  //   - heading and bearing
  //  - headingScaling, bearingScaling
  //  - uQuotient, vQuotient
  //  - isOrientationFlipped
  //  - time

  // Our goal is to move the car to the fundamental domain, and update it's auxilliary information.

  // We begin with the position = (u, v) and the heading in their initial state,
  // and keep track of whether or not the orientation is reversed relative to
  // the starting position
  const phys_t u0 = position(0);
  const phys_t v0 = position(1);
  const phys_t heading0 = heading;
  const bool isOrientationReversed0 = false; // For clarity only.

  // Think of u as the horizontal direction, and v as the vertical direction.

  // We move them horizontally into the representative of the fundamental domain
  // which lies in the same column as the fundamental domain.
  const phys_t u1 =
    !(uDirection_ == Gluing::RightWay || uDirection_ == Gluing::WrongWay ||
      uDirection_ == Gluing::PassThroughRightWay ||
      uDirection_ == Gluing::PassThroughWrongWay)
      ? u0 // If uDirection is not glued, then we don't touch this.
      : std::fmod(std::fmod(u0 - uMin_, uRange_) + uRange_, uRange_) +
          uMin_; // This gives us a positive modulus between uMin and uMax.
  const int uQuotient = static_cast<int>(
    (u1 - u0) / uRange_ +
    ((u1 > u0) ? 0.1 : -0.1)); // The computation is almost an integer, and
                               // static_cast rounds in toward zero.
  const bool isV0Flipped = (uDirection_ == Gluing::WrongWay ||
                            uDirection_ == Gluing::PassThroughWrongWay) &&
                           isOdd(uQuotient);
  const phys_t v1 = (isV0Flipped) ? (vRange_ - (v0 - vMin_)) + vMin_ : v0;
  const phys_t heading1 = (isV0Flipped) ? -heading0 : heading0;
  const bool isOrientationReversed1 =
    (isOrientationReversed0 && !isV0Flipped) ||
    (!isOrientationReversed0 && isV0Flipped); // XOR

  // (u1, v1) and heading1 have been moved into the same row as the fundamental
  // domain.  Now we move them into the same column, too, ie into the
  // fundamental domain.
  const phys_t v2 =
    !(vDirection_ == Gluing::RightWay || vDirection_ == Gluing::WrongWay ||
      vDirection_ == Gluing::PassThroughRightWay ||
      vDirection_ == Gluing::PassThroughWrongWay)
      ? v1 // If vDirection is not glued, then we don't touch this.
      : std::fmod(std::fmod(v1 - vMin_, vRange_) + vRange_, vRange_) +
          vMin_; // This gives us a positive modulus between vMin and vMax.
  const int vQuotient = static_cast<int>(
    ((v2 - v1) / vRange_ +
     ((v2 > v1) ? 0.1 : -0.1))); // The computation is almost zero, and static
                                 // cast rounds in toward zero.
  const bool isU1Flipped = (vDirection_ == Gluing::WrongWay ||
                            vDirection_ == Gluing::PassThroughWrongWay) &&
                           isOdd(vQuotient);
  const phys_t u2 = (isU1Flipped) ? (uRange_ - (u1 - uMin_)) + uMin_ : u1;
  const phys_t heading2 = (isU1Flipped) ? PI - heading1 : heading1;
  const bool isOrientationReversed2 =
    (isOrientationReversed1 && !isU1Flipped) ||
    (!isOrientationReversed1 && isU1Flipped); 

  // (u2, v2) and heading2 are the position and heading of the car in the
  // fundamental domain. Now we compute the remaining data.

  const Mat22 g =
    tiledSurface_->g(position(0), // Okay to use original position vector here.
                     position(1),
                     time);
  const Mat22 g_inv = g.inverse();

  // We define the bearing to be right relative to the heading, in the pullback
  // metric.
  Mat22 rotation;
        rotation << 0, -1, 
                    1,  0;

  const Vec2 forwardDirection(cos(heading2), sin(heading2));
  const Vec2 rightDirection = g_inv * rotation * forwardDirection;
  const phys_t bearing2 = atan2(rightDirection(1), rightDirection(0));

  // Now we compute the length of the normals in the g metric relative to the normal metric.
  const Vec2 forwardEuclideanNormal(cos(heading2), sin(heading2));
  const Vec2   rightEuclideanNormal(cos(bearing2), sin(bearing2));
  
  const phys_t headingScaling =
    static_cast<phys_t>(1) /
    sqrt(forwardEuclideanNormal.transpose() * g * forwardEuclideanNormal);

  const phys_t bearingScaling =
    static_cast<phys_t>(1) /
    sqrt(rightEuclideanNormal.transpose() * g * rightEuclideanNormal);

  // Compute the screen coordinates of (u2, v2).
  const scrn_t x =
    box_.x + static_cast<scrn_t>((box_.w * (u2 - uMin_)) / uRange_);
  const scrn_t y =
    box_.y + box_.h - static_cast<scrn_t>((box_.h * (v2 - vMin_)) / vRange_);

  // Construc the map object.
  CarOnMap carOnMap;

  carOnMap.x = x;
  carOnMap.y = y;
  carOnMap.heading = heading2;
  carOnMap.bearing = bearing2;
  carOnMap.headingScaling = headingScaling;
  carOnMap.bearingScaling = bearingScaling;
  carOnMap.uQuotient = uQuotient;
  carOnMap.vQuotient = vQuotient;
  carOnMap.isOrientationFlipped =
    isOrientationReversed2; // Oh well, we named then differently.
  carOnMap.time = time;

  return carOnMap;
}

} // namespace kleindrive
