//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/features/domainmap/CarHistory.h - Definition of CarHistory class.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the CarHistory class.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_CARHISTORY_H
#define KLEIN_DRIVE_CARHISTORY_H

#include "src/features/domainmap/CarOnMap.h"
#include "src/gamestate/GameState.h"
#include "src/gui/Box.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/x.H>
#include <cmath>
#include <deque>
#include <iostream>

namespace kleindrive {

/// Represents the past positions of the car, at least for some time.
class CarHistory
{
public:
  /// Construct the car history relative to a map with the given surrounding
  /// box.
  CarHistory(Box box,
             const TiledSurface& tiledSurface,
             phys_t maxSecondsOfHistory);

  /// Update the history with current data.
  void update(Vec2 position, phys_t heading, phys_t time);

  /// Resturns the mapping data for the current point.
  CarOnMap current() const;

  /// returns the entire recent history of the car on the map.
  const std::deque<CarOnMap> history() const;

  /// Resets the history.
  void reset();

private:

  /// Max number of seconds to store the history.
  phys_t maxSecondsOfHistory_;
  
/// The box for map.
  Box box_;

  /// List of mapping data for places the car has been, recently.
  std::deque<CarOnMap> history_;

  /// Turns a map position and heading at a given time into mapping data.
  CarOnMap computeCarOnMap(Vec2 position, phys_t heading, phys_t time);

  /// Reference to the tiled surface.
  const TiledSurface* tiledSurface_;

  // Against our better judgement, we store a copy of some of the surface's data
  // for internal use.
  phys_t uMin_;
  phys_t uRange_;
  Gluing uDirection_;

  phys_t vMin_;
  phys_t vRange_;
  Gluing vDirection_;
};

} // namespace kleindrive

#endif // KLEIN_DRIVE_CARHISTORY_H
