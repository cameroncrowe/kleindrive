//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/features/domainmap/CarOnMap.h - Definition of CarOnMap struct.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the CarOnMap struct.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_CARONMAP_H
#define KLEIN_DRIVE_CARONMAP_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Conatins data needed to plot a car on the map.
struct CarOnMap
{
public:
  /// x coordinate in screen coordinates.
  scrn_t x;

  /// y coordinate in screen coordinates
  scrn_t y;

  /// Forward heading angle
  phys_t heading;

  /// Right bearing angle
  phys_t bearing;

  /// Length of forward normal relative to the Euclidean metric used on the map.
  phys_t headingScaling;

  /// Length of right normal relative to the Euclidean metric uned on the map.
  phys_t bearingScaling;

  /// Is the oreinetation reversed relative to our top down view of the map?
  bool isOrientationFlipped;

  /// The time the car was at this point.
  phys_t time;

  ///  U coordinate of the represtantative of the domain relative to the
  ///  fundamental domain.
  ///
  /// We only assume this is accurate mod 2.
  int uQuotient;

  /// V coordinate of the representative of the domain relative to the
  /// fundamental domain.
  ///
  /// We only assume this is accurate mod 2.
  int vQuotient;
};

} // namespace kleindrive

#endif // KLEIN_DRIVE_CARONMAP_H
