//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/features/domainmap/DomainMap.cpp
//
//  DomainMap class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file  This file contains the definition of the DomainMap class.
///
//===----------------------------------------------------------------------===//

#include "src/features/domainmap/DomainMap.h"
#include "src/gamestate/GameState.h"
#include "src/gui/Box.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/x.H>
#include <cmath>
#include <deque>
#include <iostream>

namespace kleindrive {

namespace {

// Pi!
const phys_t PI = 4 * atan (1);

} // namespace

DomainMap::DomainMap(const TiledSurface& tiledSurface, scrn_t point)
  : tiledSurface_{ &tiledSurface }
  , point_{ point }
  , box_{ computeBox() }
  , baseMapBox_{ computeBaseMapBox() }
  , carHistory_(baseMapBox_, tiledSurface, maxSecondsOfHistory_)
{
  // Due to a "bug" in FLTK's X11 support, we cannot create the Fl_Offscreen
  // buffer until after the main window has loaded itself.  So we do this in the
  // draw command, which should only be called from the main window.
}

// CONSTANTS
// This could be separated out and changed on a surface to basis.
const phys_t DomainMap::maxSecondsOfHistory_ = 20;
const int DomainMap::lineWidth_ = 3;
const int DomainMap::arrowLength_ = 30;
const int DomainMap::arrowHeadLength_ = 6;
const int DomainMap::idArrowHeadLength_ = 12;
const int DomainMap::distanceBetweenIdArrowHeads_ = 6;

Box
DomainMap::computeBox()
{
  Box box;
  // In a little to accommodate the arrow heads.
  box.x = idArrowHeadLength_;
  box.y = idArrowHeadLength_;

  // idLines + tiles + idLines
  box.w = lineWidth_ + point_ * tiledSurface_->numberOfTilesInUDirection() +
          lineWidth_;
  box.h = lineWidth_ + point_ * tiledSurface_->numberOfTilesInVDirection() +
          lineWidth_;

  return box;
}

Box
DomainMap::computeBaseMapBox()
{
  Box box;

  // Same as the dimensions of the box_, but line_width smaller on all sides.
  box.x = idArrowHeadLength_ + lineWidth_;
  box.y = idArrowHeadLength_ + lineWidth_;
  box.w = point_ * tiledSurface_->numberOfTilesInUDirection();
  box.h = point_ * tiledSurface_->numberOfTilesInVDirection();

  return box;
}

void
DomainMap::reset()
{
  carHistory_.reset();
}

void
DomainMap::drawMapBaseLayer()
{
  const int numberOfTilesInUDirection = tiledSurface_->numberOfTilesInUDirection();
  const int numberOfTilesInVDirection = tiledSurface_->numberOfTilesInVDirection();

  fl_begin_offscreen(baseMap_);
  {
    // Draw background.
    setFlColor(colors::Green);
    fl_rectf(0, 0, baseMapBox_.w, baseMapBox_.h);

    // Draw visible tiles.
    for (int I = 0; I < numberOfTilesInUDirection; ++I) {
      for (int J = 0; J < numberOfTilesInVDirection; ++J) {
        const Color color = tiledSurface_->colorOfTile(I, J, 0);

        if (!color.isVisible)
          continue;

        setFlColor(color);

        const scrn_t x = I * point_;
        const scrn_t y = baseMapBox_.h - J * point_ - point_;
        const scrn_t w = point_;
        const scrn_t h = point_;

        fl_rectf(x, y, w, h);
      }
    }
  }
  fl_end_offscreen();
}

void
DomainMap::update(GameState* gameState)
{
  Vec2   position = gameState->car.position;
  phys_t heading  = gameState->car.heading;
  phys_t time     = gameState->time;

  carHistory_.update(position, heading, time);
}

void 
DomainMap::draw()
{
  if (!baseMap_) {
    baseMap_ = fl_create_offscreen(baseMapBox_.w, baseMapBox_.h);
    drawMapBaseLayer();
}
  // Draw map base layer.
  fl_copy_offscreen(baseMapBox_.x,
                    baseMapBox_.y,
                    baseMapBox_.w,
                    baseMapBox_.h,
                    baseMap_,
                    0, // x = 0 of the offscreen buffer.
                    0  // y = 0 of the offscreen buffer.
  );

  // Draw car history, clipped to the box_. (Could also clip this to base
  // map box, if desired).
  fl_push_clip(baseMapBox_.x, baseMapBox_.y, baseMapBox_.w, baseMapBox_.h);
  {
    drawHistory();
  }
  fl_pop_clip();

  // Draw gluing identification lines, NOT clipped.
  drawGluingIdentificationLines();

  // Draw car's current state NOT clipped.
  fl_line_style(FL_SOLID | FL_JOIN_MITER, lineWidth_);
  drawCar();
  fl_line_style(0);
}

void
DomainMap::drawGluingIdentificationArrow(scrn_t x0,
                                         scrn_t y0, // Tail
                                         scrn_t x1,
                                         scrn_t y1, // Head
                                         int numberOfHeads)
{
  static const phys_t headLength = idArrowHeadLength_;
  static const phys_t headAngle = PI / 3;
  static const phys_t distanceBetweenHeads = distanceBetweenIdArrowHeads_;

  //fl_line(x0, y0, x1, y1); // Draw base line.

  const scrn_t dx = x1 - x0;
  const scrn_t dy = y1 - y0;

  const phys_t length = sqrt(dx * dx + dy * dy); // Length
  if (length < 1)
    return;

  const phys_t angle = atan2(dy, dx); // Angle

  const phys_t distanceToHeads =
    length / 2 - numberOfHeads * distanceBetweenHeads / 2;

  // We draw the shafts in the drawGluingIdentificationLines function,
  // It makes the corners nicer.
  //
  //// Draw shaft:
  // fl_line(x0, y0, x1, y1);

  // Draw heads:
  for (int I = 0; I < numberOfHeads; ++I) {
    phys_t distanceToHeadI = distanceToHeads + I * distanceBetweenHeads;

    // Make the head pointing right on the x axis, with
    // tip at distanceToHeadI.

    Vec2 head(distanceToHeadI, 0);
    Vec2 headLeftPoint =
      head + headLength * Vec2(cos(PI - headAngle), sin(PI - headAngle));
    Vec2 headRightPoint =
      head + headLength * Vec2(cos(PI + headAngle), sin(PI + headAngle));

    Aff2 at;

    at.linear() << cos(angle), -sin(angle), // Rotate by angle, above.
      sin(angle), cos(angle);

    at.translation() << x0, y0; // Translate to (x0, y0).

    // Move head by transformation.
    head =           at * head;
    headLeftPoint =  at * headLeftPoint;
    headRightPoint = at * headRightPoint;

    // Draw I-th head:
    fl_line(static_cast<scrn_t>(headRightPoint(0)),
            static_cast<scrn_t>(headRightPoint(1)),
            static_cast<scrn_t>(head(0)),
            static_cast<scrn_t>(head(1)),
            static_cast<scrn_t>(headLeftPoint(0)),
            static_cast<scrn_t>(headLeftPoint(1)));
  }
}

void
DomainMap::drawGluingIdentificationLines()
{
  // Set the color before the line style!
  setFlColor(colors::Blue); 
  fl_line_style(FL_SOLID | FL_JOIN_MITER, lineWidth_);
  int numberOfPreviousIdLinePairs = 0;
  
  const Gluing uDirection = tiledSurface_->uDirection();
  const Gluing vDirection = tiledSurface_->vDirection();

  // If two wides meeting at a corner need GluingID lines, then we draw then all
  // the way to the point, otherwise.  If there is only one, we end it at the
  // edge of the map. It helps to know what edges and corners are "active"

  // I suspect the compiler figures out that these can be set once at
  // construction (since tiledSurface_ is a pointer to a constant surfaces. They
  // could always be computed at construction.

  // An edge is active if and only if it needs an id line.
  const bool isBottomEdgeActive = vDirection == Gluing::RightWay ||
                                  vDirection == Gluing::WrongWay ||
                                  vDirection == Gluing::PassThroughRightWay ||
                                  vDirection == Gluing::PassThroughWrongWay ||
                                  vDirection == Gluing::MinCrushedToPoint ||
                                  vDirection == Gluing::MinAndMaxCrushedToPoint;

  const bool isTopEdgeActive = vDirection == Gluing::RightWay ||
                               vDirection == Gluing::WrongWay ||
                               vDirection == Gluing::PassThroughRightWay ||
                               vDirection == Gluing::PassThroughWrongWay ||
                               vDirection == Gluing::MaxCrushedToPoint ||
                               vDirection == Gluing::MinAndMaxCrushedToPoint;

  const bool isLeftEdgeActive = uDirection == Gluing::RightWay ||
                                uDirection == Gluing::WrongWay ||
                                uDirection == Gluing::PassThroughRightWay ||
                                uDirection == Gluing::PassThroughWrongWay ||
                                uDirection == Gluing::MinCrushedToPoint ||
                                uDirection == Gluing::MinAndMaxCrushedToPoint;

  const bool isRightEdgeActive = uDirection == Gluing::RightWay ||
                                 uDirection == Gluing::WrongWay ||
                                 uDirection == Gluing::PassThroughRightWay ||
                                 uDirection == Gluing::PassThroughWrongWay ||
                                 uDirection == Gluing::MaxCrushedToPoint ||
                                 uDirection == Gluing::MinAndMaxCrushedToPoint;

  // A corner is active if and only if both edges coming into it are active.
  const bool isTopLeftCornerActive = isTopEdgeActive && isLeftEdgeActive;
  const bool isTopRightCornerActive = isTopEdgeActive && isRightEdgeActive;
  const bool isBottomLeftCornerActive = isBottomEdgeActive && isLeftEdgeActive;
  const bool isBottomRightCornerActive = isBottomEdgeActive && isRightEdgeActive;

  // Edges
  // -----

  if (isTopEdgeActive) {
    fl_rectf(box_.x + lineWidth_,
             box_.y,
             box_.w - 2 * lineWidth_,
             lineWidth_); // Top edge.
  }
  if (isBottomEdgeActive) {
    fl_rectf(box_.x + lineWidth_,
             box_.y + box_.h - lineWidth_,
             box_.w - 2 * lineWidth_,
             lineWidth_); // Bottom edge.
  }
  if (isLeftEdgeActive) {
    fl_rectf(box_.x,
             box_.y + lineWidth_,
             lineWidth_,
             box_.h - 2 * lineWidth_); // Left.
  }
  if (isRightEdgeActive) {
    fl_rectf(box_.x + box_.w - lineWidth_,
             box_.y + lineWidth_,
             lineWidth_,
             box_.h - 2 * lineWidth_); // Right.
  }

  // Corners
  // -------

  if (isTopLeftCornerActive) {
    fl_rectf(box_.x,
             box_.y,
             lineWidth_,
             lineWidth_); // Top left corner
  }
  if (isTopRightCornerActive) {
    fl_rectf(box_.x + box_.w - lineWidth_,
             box_.y,
             lineWidth_,
             lineWidth_); // Top right corner
  }
  if (isBottomLeftCornerActive) {
    fl_rectf(box_.x,
             box_.y + box_.h - lineWidth_,
             lineWidth_,
             lineWidth_); // Bottom left corner
  }
  if (isBottomRightCornerActive) {
    fl_rectf(box_.x + box_.w - lineWidth_,
             box_.y + box_.h - lineWidth_,
             lineWidth_,
             lineWidth_); // Bottom left corner
  }

  // Arrows
  // ------
  
  // Horizontal Id line arrows.
  switch (vDirection) {
    case Gluing::RightWay:
      [[fallthrough]];
    case Gluing::PassThroughRightWay: {
      // Right on top.
      drawGluingIdentificationArrow(box_.x,
                                    box_.y + lineWidth_ / 2,
                                    box_.x + box_.w,
                                    box_.y + lineWidth_ / 2,
                                    numberOfPreviousIdLinePairs + 1);

      // Right on bottom.
      drawGluingIdentificationArrow(box_.x,
                                    box_.y + box_.h - lineWidth_ / 2,
                                    box_.x + box_.w,
                                    box_.y + box_.h - lineWidth_ / 2,
                                    numberOfPreviousIdLinePairs + 1);
    }
      ++numberOfPreviousIdLinePairs;
      break;
    case Gluing::WrongWay:
      [[fallthrough]];
    case Gluing::PassThroughWrongWay: {
      // Left on top.
      drawGluingIdentificationArrow(box_.x + box_.w,
                                    box_.y + lineWidth_ / 2,
                                    box_.x,
                                    box_.y + lineWidth_ / 2,
                                    numberOfPreviousIdLinePairs + 1);
      // Right on bottom.
      drawGluingIdentificationArrow(box_.x,
                                    box_.y + box_.h - lineWidth_ / 2,
                                    box_.x + box_.w,
                                    box_.y + box_.h - lineWidth_ / 2,
                                    numberOfPreviousIdLinePairs + 1);
    }
      ++numberOfPreviousIdLinePairs;
      break;
    default:
      // Draw nothing... for now.
      break;
    }

    // Vertical ID lines arrows.
    switch (uDirection) {
      case Gluing::RightWay:
        [[fallthrough]];
      case Gluing::PassThroughRightWay: {
        // Up on left.
        drawGluingIdentificationArrow(box_.x + lineWidth_ / 2,
                                      box_.y + box_.h,
                                      box_.x,
                                      box_.y + lineWidth_ / 2,
                                      numberOfPreviousIdLinePairs + 1);
        // Up on right.
        drawGluingIdentificationArrow(box_.x + box_.w - lineWidth_ / 2,
                                      box_.y + box_.h,
                                      box_.x + box_.w - lineWidth_ / 2,
                                      box_.y,
                                      numberOfPreviousIdLinePairs + 1);

      }
      //++numberOfPreviousIdLinePairs;
      break;
      case Gluing::WrongWay:
        [[fallthrough]];
      case Gluing::PassThroughWrongWay: {
        // Down on left.
        drawGluingIdentificationArrow(box_.x + lineWidth_ / 2,
                                      box_.y,
                                      box_.x + lineWidth_ / 2,
                                      box_.y + box_.h,
                                      numberOfPreviousIdLinePairs + 1);
        // Up on right.
        drawGluingIdentificationArrow(box_.x + box_.w - lineWidth_ / 2,
                                      box_.y + box_.h,
                                      box_.x + box_.w - lineWidth_ / 2,
                                      box_.y,
                                      numberOfPreviousIdLinePairs + 1);

      }
      //++numberOfPreviousIdLinePairs;
      break;
      default:
        // Draw nothing... for now.
        break;
    }
}

void
DomainMap::drawHistory()
{
  const auto history = carHistory_.history();
  using sz_t = decltype(history)::size_type;

  const int sizeOfHistory = static_cast<int>(history.size());
  const Gluing uDirection = tiledSurface_->uDirection();
  const Gluing vDirection = tiledSurface_->vDirection();

  // Don't do anything if it's just the current location in history.
  if (sizeOfHistory < 2)
    return;

  // Set the color before the line style.
  setFlColor(colors::Red);
  fl_line_style(FL_SOLID | FL_CAP_ROUND, lineWidth_);

  CarOnMap previous = history[0];
  // Draw a line from the previous to the next point on the map.  If we pass
  // over a border, then draw a piece of the line segment on each side of the
  // border, in the appropriate position.  TODO: There should really be a class
  // that returns the car's data relative to a given representative of the
  // fundamental domain.
  for (int I = 1; I < sizeOfHistory; ++I) {
    const CarOnMap current = history[static_cast<sz_t>(I)];

    const bool changedRepresentatives =
      current.uQuotient != previous.uQuotient ||
      current.vQuotient != previous.vQuotient;

    if (!changedRepresentatives) {
      // I we haven't left the current representative, just connect the previous
      // point to the new point.
      fl_line(previous.x, previous.y, current.x, current.y);
    } else {
      // We've left the current representative.

      // We draw a bit of line in the OLD (previous) representative,
      // and a bit of line in the NEW (current) representative.

      // Their coordinates are shifted versions of:
      const scrn_t xPreviousOld = previous.x;
      const scrn_t yPreviousOld = previous.y;
            scrn_t xCurrentOld  = current.x;
            scrn_t yCurrentOld  = current.y;

            scrn_t xPreviousNew = previous.x;
            scrn_t yPreviousNew = previous.y;
      const scrn_t xCurrentNew = current.x;
      const scrn_t yCurrentNew = current.y;

      // We now maneuver them into their correct positions.
      //
      // WARNING: Weird things can happen to the quotient when you cross
      // a boundary (Like the OTHER variable might go up 2 times the range.
      // Or 60.
      //
      // HOWEVER: We assume everything is okay MOD 2.
      //
      // This does NOT even include the sign.
      const int uDifferenceMod2 =
        ((current.uQuotient - previous.uQuotient) % 2 + 2) % 2;

      const int vDifferenceMod2 =
        ((current.vQuotient - previous.vQuotient) % 2 + 2) % 2;

      // It is possible to fool the algorithm below if you go really far or a
      // really small distance AND go through an edge.  There is (are?),
      // unfortunately, and easy way to do this. For example if the surface is
      // connected the wrong way in both edges, then going out a corner puts you
      // roughly back in the same spot, and you've gone through two edges.  This
      // would resulut in ugly lines across the diagonal.  In this case, we
      // simply don't draw the line
      // We work out all the possible cases.
      const bool isDiagonalOfProjectivePlaneABAB =
        (uDifferenceMod2 == 1 && vDifferenceMod2 == 1 &&
         (uDirection == Gluing::WrongWay ||
          uDirection == Gluing::PassThroughWrongWay) &&
         (vDirection == Gluing::WrongWay ||
          vDirection == Gluing::PassThroughWrongWay));

      const bool isProblematicLine = isDiagonalOfProjectivePlaneABAB; // || others.

      if (isProblematicLine) {
        previous = current;
        continue;
      }

      if (vDifferenceMod2 != 0) {
        // We're either wrapped the right way or the wrong way in the v
        // direction.  So we need to move up or down by box_.h.

        const int vSign = (previous.y > current.y)
                            ? 1  // Moved up through the top
                            : -1 // Move down through the bottom
          ;

        yCurrentOld += vSign * baseMapBox_.h * vDifferenceMod2;
        yPreviousNew -= vSign * baseMapBox_.h * vDifferenceMod2;

        if (vDirection == Gluing::WrongWay ||
            vDirection == Gluing::PassThroughWrongWay) {
          // If we're also glued the wrong way, then we need to flip the x
          // coordinate.
          xCurrentOld =
            (baseMapBox_.w - (xCurrentOld - baseMapBox_.x)) + baseMapBox_.x;
          xPreviousNew =
            (baseMapBox_.w - (xPreviousNew - baseMapBox_.x)) + baseMapBox_.x;
          ;
        }
      }
      if (uDifferenceMod2 != 0) {
        // We're either wrapped the right way or the wrong way in the u
        // direction.  So we need to move left or right by box_.w.

        const int uSign = (previous.x > current.x)
                            ? 1  // Moved right through the right side.
                            : -1 // Move left throught the left side
          ;

        xCurrentOld += uSign * baseMapBox_.w * uDifferenceMod2;
        xPreviousNew -= uSign * baseMapBox_.w * uDifferenceMod2;

        if (uDirection == Gluing::WrongWay ||
            uDirection == Gluing::PassThroughWrongWay) {
          // If we're also glued the wrong way, then we need to flip the y
          // coordinate.
          yCurrentOld =  (baseMapBox_.h - (yCurrentOld - baseMapBox_.y) + baseMapBox_.y);
          yPreviousNew = (baseMapBox_.h - (yPreviousNew- baseMapBox_.y) + baseMapBox_.y);
        }
      }

      // Draw  line betwene the two in the two different representatives.
      // We clip, so that the excess is not seen.
      // In the new representative
      fl_line(xPreviousNew, yPreviousNew, xCurrentNew, yCurrentNew);
      // In the previous representative.
      fl_line(xPreviousOld, yPreviousOld, xCurrentOld, yCurrentOld);
    }
    previous = current;
  }

  // Reset the line style.
  fl_line_style(0);
}

namespace {

// Is close to the identity when scaling is near the identity.
//
// But bounds the scaling smoothly off between 1/maxfactor and maxfactor.
//
phys_t
scaleNavigationVector(phys_t scaling)
{
  static const phys_t maxFactor = 2;

  return pow(maxFactor, ((2. / PI * atan(log(scaling) / log(maxFactor)))));
}

} // namespace

void
DomainMap::drawCar()
{
  const int lengthOfForwardVector = arrowLength_;

  const CarOnMap current = carHistory_.current();

  // Forward arrow.
  setFlColor(colors::Red);

  const phys_t headingScaling = scaleNavigationVector(current.headingScaling);
  const phys_t bearingScaling = scaleNavigationVector(current.bearingScaling);
 
  // Set the color before the line style.
  setFlColor(colors::Red);
  fl_line_style(FL_SOLID | FL_JOIN_MITER, lineWidth_);
  drawArrow(current.x,
            current.y,
            current.heading,
            headingScaling * lengthOfForwardVector);

  // Right arrow.
  // FIXME--For some reason right is actually left??
  
  // Set the color before the line style.
  setFlColor(colors::Black);
  fl_line_style(FL_SOLID | FL_JOIN_MITER, lineWidth_);
  drawArrow(current.x,
            current.y,
            current.bearing + PI + ((current.isOrientationFlipped) ? PI : 0),
            bearingScaling * lengthOfForwardVector);

  // Set the color before the line style.
  setFlColor(
    colors::Blue); // This should probably be inside the arrow, since it more or
                   // less needs the line style, but also needs the color to be
                   // set before setting the line style.  How annoying.
  fl_line_style(FL_SOLID | FL_JOIN_MITER, lineWidth_);
  // Circle has some bug in it.
  fl_line_style(0);
  fl_begin_polygon();
{
  fl_arc(static_cast<double>(current.x) ,static_cast<double>(current.y),4.,0.,360.);
 }
 fl_end_polygon();
  //fl_line_style(0);
  //fl_circle(current.x, current.y, 4);
}

void
DomainMap::drawArrow(scrn_t x, scrn_t y, phys_t angle, phys_t length)
{
  const phys_t headLength = arrowHeadLength_;
  const phys_t headAngle = PI / 6;

  const Vec2 position(x, y);

  Vec2 tail(0, 0);
  Vec2 head(length, 0);
  Vec2 headLeftPoint =
    head + headLength * Vec2(cos(PI - headAngle), sin(PI - headAngle));
  Vec2 headRightPoint =
    head + headLength * Vec2(cos(PI + headAngle), sin(PI + headAngle));

  Aff2 at;

  at.linear() << cos(angle), sin(angle), -sin(angle), cos(angle);

  at.translation() = position;

  tail =           at * tail;
  head =           at * head;
  headLeftPoint =  at * headLeftPoint;
  headRightPoint = at * headRightPoint;

  // Shaft:
  fl_line(static_cast<scrn_t>(tail(0)),
          static_cast<scrn_t>(tail(1)),
          static_cast<scrn_t>(head(0)),
          static_cast<scrn_t>(head(1)));

  // Arrow head:
  fl_line(static_cast<scrn_t>(headRightPoint(0)),
          static_cast<scrn_t>(headRightPoint(1)),
          static_cast<scrn_t>(head(0)),
          static_cast<scrn_t>(head(1)),
          static_cast<scrn_t>(headLeftPoint(0)),
          static_cast<scrn_t>(headLeftPoint(1)));
}

} // namespace kleindrive
