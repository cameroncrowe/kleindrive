//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/graphics/DomainMap.h - DomainMap class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the DomainMap class.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_DOMAINMAP_H
#define KLEIN_DRIVE_DOMAINMAP_H

#include "src/features/domainmap/CarHistory.h"
#include "src/gamestate/GameState.h"
#include "src/gui/Box.h"
#include "src/surface/TiledSurface.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/x.H>
#include <cmath>
#include <vector>

namespace kleindrive {

/// Represents a map of the domain in a surface, and one's position in it.
class DomainMap
{
public:
  /// Uses the surface to cosntruct a map and plot car position data.
  DomainMap(const TiledSurface& tiledSurface, scrn_t point);

  /// Resets the car history on the map.
  void reset();

  /// Draws the map with the recent history of the car along with it's current
  /// position.
  void draw();

  /// Updates the car's position on the map.
  void update(GameState* gameState);

private:
  /// \name Constants 
  /// @{

  /// How long do we store the history.
  static const phys_t maxSecondsOfHistory_;
  
  /// How wide is each line.
  static const int lineWidth_;

  /// How long are the car's arrows?
  static const int arrowLength_;

  /// How long are the heads on the car's arrow?
  static const int arrowHeadLength_;

  /// How long are the heads on the ID line arrows?
  static const int idArrowHeadLength_;

  /// How far apart are the ID lines' arrows?
  static const int distanceBetweenIdArrowHeads_;
  
  /// @}

  /// Computes the box required to draw the map.
  Box computeBox();

  /// Compute the box for just the base map.
  Box computeBaseMapBox();

  /// Draws a map of the surface to an offscreen buffer.  This may be why the
  /// program is so slow to start?
  void drawMapBaseLayer();

  /// Draws the identification lines on the map, indicating if and how surface
  /// is glued (or crushed?)
  void drawGluingIdentificationLines();

  /// Actually draws ID lines on the map.
  void drawGluingIdentificationArrow(scrn_t x0,
                                     scrn_t y0,
                                     scrn_t x1,
                                     scrn_t y1,
                                     int numberOfHeads);

  /// Draws the cars recent history on the map.
  void drawHistory();

  /// Draws the car's current position, heading and bearing on the map.
  void drawCar();

  /// Draws an arrow.
  void drawArrow(scrn_t x, scrn_t y, phys_t angle, phys_t length);

  /// Reference to the tiled surface.
  const TiledSurface* tiledSurface_;

  /// Nunber of pixels used for each tile.  Need a better name.
  scrn_t point_;

  /// Total box used for map and id lines.  The car arrows and ID line arrow
  /// heads spill of of this.
  Box box_;

  /// The box used for the map only.  The car history is cropped to this... at
  /// time of this comment's writing.
  Box baseMapBox_;

  /// Offscreen buffer for the base map.
  Fl_Offscreen baseMap_{0};

  /// The history of the car. 
  CarHistory carHistory_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_DOMAINMAP_H
