//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/frontend/kleinDrive.h - kleinDrive function implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the kleinDrive function implementation.
///
//===----------------------------------------------------------------------===//

#include "src/frontend/KleinDrive.h"
#include "src/collections/scenes/KDSceneCollection.h"
#include "src/gui/WindowWrapper.h"
#include "src/scenecollection/SceneCollection.h"

namespace kleindrive {

int
kleinDrive()
{
  std::unique_ptr<SceneCollection> kdSceneCollection =
    scenes::kdSceneCollection();
  WindowWrapper windowWrapper(kdSceneCollection);
  return windowWrapper.run();
}

int
kleinDrive(std::unique_ptr<SceneCollection>& sceneCollection)
{
  WindowWrapper windowWrapper(sceneCollection);
  return windowWrapper.run();
}

} // namespace kleindrive
