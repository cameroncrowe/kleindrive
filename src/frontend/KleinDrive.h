//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/frontend/KleinDrive.h - kleinDrive function definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the kleinDrive function definition.
///
//===----------------------------------------------------------------------===//
#ifndef KLEIN_DRIVE_KLEIN_DRIVE_H
#define KLEIN_DRIVE_KLEIN_DRIVE_H

#include "src/collections/scenes/KDSceneCollection.h"
#include "src/gui/WindowWrapper.h"
#include "src/scenecollection/SceneCollection.h"

namespace kleindrive {

/// Launch Klein DRive with the builtin scenes.
int
kleinDrive();

/// Launches Klein Drive with a custom collection of scenes.
int
kleinDrive(const SceneCollection& sceneCollection);

} // namespace kleindrive

#endif // KLEIN_DRIVE_KLEINDRIVE_H
