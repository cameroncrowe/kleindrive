//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/AnimatedBoxState.h - The AnimatedBoxState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the AnimatedBoxState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_ANIMATEDBOXSTATE_H
#define KLEINDRIVE_ANIMATEDBOXSTATE_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains state information to be used by an AnimatedBox.
struct AnimatedBoxState final
{
public:
  /// Top left corner x coordinate.
  scrn_t x{ 0 };

  /// Top left corner y coordinate.
  scrn_t y{ 0 };

  /// Width.
  scrn_t w{ 0 };

  /// Height.
  scrn_t h{ 0 };
};

} // namespace kleindrive

#endif // KLEINDRIVE_ANIMATEDBOXSTATE_H
