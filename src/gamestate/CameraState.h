//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/CameraState.h - The CameraState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CameraState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CAMERASTATE_H
#define KLEINDRIVE_CAMERASTATE_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains state information used by the Camera.
struct CameraState final
{
public:
  /// Is the camera zooming out.
  bool isZoomingOut{ false };

  /// Is the camera zooming in.
  bool isZoomingIn{ false };

  /// Is the camera moving toward its default zoom
  bool isResettingZoom{ false };

  /// Is the camera turning up.
  bool isTurningHeadUp{ false };

  /// Is the camera turning down.
  bool isTurningHeadDown{ false };

  /// Is the camera moving toward the default up/down position.
  bool isResettingNeckTurn{ false };

  /// Is the head temporarily turned by horizontalTurn or verticalTurn.
  bool isHeadTemporarilyTurned{ false };

  /// How much to horizontally turn if isHeadTemporarilyTurned.
  phys_t horizontalTurn{ 0 };

  /// How much to vertically turn if isHeadTemporarilyTurned.
  phys_t verticalTurn{ 0 };
};

} // namespace kleindrive

#endif // KLEINDRIVE_CAMERASTATE_H
