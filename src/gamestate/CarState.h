//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/CarState.h - The CarState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the CarState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CARSTATE_H
#define KLEINDRIVE_CARSTATE_H

#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains game state information used by the Car.
struct CarState final
{
public:
  /// Is the car strafing
  bool isStrafing{ false };

  /// Are the breaks on?
  bool isBraking{ false };

  /// Is turning off--ie are we confining ourselves to a geodesic.
  bool isGeodesic{ false };

  /// Are we going forward (like with an arrow key).
  bool isGoingForward{ false };
 
  /// Are we going backward (like with an arrow key).
  bool isGoingBackward{ false };

  /// Are we sliding left.
  bool isSlidingLeft{ false };

  /// Are we sliding right.
  bool isSlidingRight{ false };

  /// The Current position of the car.
  Vec2 position{ 0, 0 };

  /// THe current heading of the car.
  phys_t heading{ 0 };
  
  // Are we going the max speed.
  bool isGoingFast{false};
};

} // namespace kleindrive

#endif // KLEINDRIVE_CARSTATATE_H
