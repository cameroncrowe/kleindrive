//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/GameState.h - The GameState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the GameState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_GAMESTATE_H
#define KLEINDRIVE_GAMESTATE_H

#include "src/gamestate/AnimatedBoxState.h"
#include "src/gamestate/CameraState.h"
#include "src/gamestate/CarState.h"
#include "src/gamestate/MouseState.h"
#include "src/gamestate/SceneState.h"
#include "src/gamestate/WindowState.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains all information relevant to the state of the simulation that is
/// needed by somethign other than the gui, alone.
struct GameState final
{
public:
  /// The current time.
  phys_t time{ 0 };

  /// The mouse state.
  MouseState mouse;

  /// The window state.
  WindowState window;

  /// The animated box state.
  AnimatedBoxState animatedBox;

  /// The car's state.
  CarState car;

  /// The Camera's state.
  CameraState camera;

  /// The Scene's state.
  SceneState scene;
};

} // namespace kleindrive

#endif // KLEINDRIVE_GAMESTATE_H
