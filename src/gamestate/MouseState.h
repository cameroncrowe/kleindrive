//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/MouseState.h - The MouseState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MouseState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MOUSESTATE_H
#define KLEINDRIVE_MOUSESTATE_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains the mouse state.
struct MouseState final
{
public:
  /// Mouse x coordinate relative to the screen.
  scrn_t x{ 0 };

  /// Mouse y coordinate relative to the screen.
  scrn_t y{ 0 };
};

} // namespace kleindrive

#endif // KLEINDRIVE_MOUSESTATE_H
