//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/state/SceneState.h - The SceneState class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SceneState class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_SCENESTATE_H
#define KLEINDRIVE_SCENESTATE_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains state information used by the Scene.
struct SceneState final
{
public:
  /// Are we rotating between drawing methods.
  bool isRotatingDrawingMethod{ false };

  /// Are we trying to set the drawing method by number.
  bool isSettingDrawingMethodByNumber{ false };

  // The number we are attempting to set our drawing method to.
  int numberOfDesiredDrawingMethod{ 1 };
};

} // namespace kleindrive

#endif // KLEINDRIVE_SCENESTATE_H
