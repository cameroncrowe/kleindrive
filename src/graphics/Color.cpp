//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/graphics/Color.cpp - Color implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Color implementation.
///
//===----------------------------------------------------------------------===//

#include "src/graphics/Color.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>

namespace kleindrive {

void
setFlColor(Color color)
{
  fl_color(static_cast<rgb_channel_t>(color.R * MaxRGBChannelValue),
           static_cast<rgb_channel_t>(color.G * MaxRGBChannelValue),
           static_cast<rgb_channel_t>(color.B * MaxRGBChannelValue));
}

Color
scaleColor(Color color, phys_t coeff)
{
  color.R *= coeff;
  color.G *= coeff;
  color.B *= coeff;
  return color;
}

Color
affineCombination(Color color1, Color color2, phys_t weight)
{
  Color color;
  const phys_t antiWeight = 1 - weight;

  color.R = color1.R * weight + antiWeight * color2.R;
  color.G = color1.G * weight + antiWeight * color2.G;
  color.B = color1.B * weight + antiWeight * color2.B;

  return color;
}

} // namespace kleindrive
