//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/graphics/Color.h - Color struct definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Color struct definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_COLOR_H
#define KLEIN_DRIVE_COLOR_H

#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>

namespace kleindrive {

/// Represents a color.
struct Color final
{
public:
  /// \name Color channels.
  /// @{

  /// Red color channel, assumed to be a value of type phys_t in [0,1].
  phys_t R;

  /// Green color channel, assumed to be a value of type phys_t in [0,1].
  phys_t G;

  /// Blue color channel, assumed to be a value of type phys_t in [0,1].
  phys_t B;

  ///
  bool isVisible{ true };

  /// @}
};

/// Converts Color to Fl_Color and sets that as FLTK's current color.
void
setFlColor(Color color);

/// Scales the color's rgb channels by the coeff.. Coeff should be in the
/// interval [0,1].
Color
scaleColor(Color color, phys_t coeff);

/// Affinely combines the color channels of the two.  Can be thought of as
/// returning a color between the two input colors. Weight should be in the
/// interval [0,1].  The closer the weight is to zero, the closer the output to
/// colro1 than color2.  The closer the weight is to 1, the closer the output is
/// to color.2 than color1.
Color
affineCombination(Color color1, Color color2, phys_t weight);

} // namespace kleindrive

#endif // KLEINDRIVE_COLOR_H
