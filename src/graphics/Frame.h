//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/graphics/Frame.h - Definition of Frame type.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the Frame type.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_FRAME_H
#define KLEIN_DRIVE_FRAME_H

#include "src/features/domainmap/DomainMap.h"
#include "src/graphics/Tile.h"
#include "src/scene/SceneProperties.h"
#include <vector>

namespace kleindrive {

/// Represents a Frame ready to be drawn on the screen.
struct Frame
{
  /// The frames tiles.
  std::vector<Tile> tiles;

  /// The scene's properties.
  SceneProperties::State scenePropertiesState;

  /// DomainMap
  DomainMap* domainMap;
};

} // namespace kleindrive

#endif // KLEINDRIVE_FRAME_H
