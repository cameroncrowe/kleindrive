//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/graphics/Tile.h - Definition of Tile structure.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of a Tile structure definition.  The
/// Tile structure contains all the data needed to sort the tiles and draw them
/// on the screen.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_TILE_H
#define KLEIN_DRIVE_TILE_H

#include "src/graphics/Color.h"
#include "src/types/ValueTypes.h"
#include <array>

namespace kleindrive {

/// Contains the type of the tile needed for drawing the tile to the screen.
enum class TileType
{
  /// A central tile is a tile that is NOT on the uMax edge, or vMax edge, and
  /// one that is not clipped.
  Central,

  /// On the uMax side of the domain.
  RightEdge,

  /// On the vMax side of the domain.
  TopEdge,

  /// In the corner of the domain at uMax and vMax
  TopRightCorner,

  /// A tile that has been clipped.
  Clipped,
};

/// Represents a painted tile, ready to be drawn.
struct Tile final
{
public:
  /// \name Drawing data.
  /// @{

  /// Vertices of the corners in screen coordinates.
  /// These are stored as
  /// > row0, col0, row1, col1, row2, col2, row3, col3 ...
  /// where
  /// > rowi, coli
  /// is the row and column of the i-th corner.
  std::array<scrn_t, 20> corners;

  /// If the tile is not clipped, then use four vertices.  Otherwise,
  /// use more or less.
  int numberOfCorners{ 4 };

  /// The tile's color.
  Color color;

  /// Special drawing parameters
  TileType type{ TileType::Central };

  /// @}
  /// \name Sorting data.
  /// @{

  /// The distance from the (center of the) tile to the camera.
  phys_t distance;

  /// @}
};

} // namespace kleindrive

#endif // KLEINDRIVE_TILE_H
