//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/AboutBox.cpp - The AboutBox class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the AboutBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/AboutBox.h"
#include "src/gui/StaticInformationBox.h"
#include <string>

namespace kleindrive {

const std::string AboutBox::title_ = "About";

const std::string AboutBox::text_ =
  "\tKlein Drive is Copyright 2018 by Cameron Crowe."
  "\n "
  "\n\tEmail: <cameron.crowe1@gmail.com>"
  "\n\tGit:  &!<http://www.gitlab.com/cameroncrowe>"
  "\n "
  "\n\tThanks: Tapti Palit, Nusrat Alam, Lara Letaw, and Edward Bryden." 
  "\n "
  "\n\tKlein Drive is free software: you can redistribute it and/or modify it "
  "under the terms of the GNU General Public License as published by the Free "
  "Software Foundation, either version 3 of the License, or (at your option) "
  "any later version."
  "\n "
  "\n\tKlein Drive is distributed in the hope that it will be useful, but "
  "WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY "
  "or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License "
  "for more details."
  "\n "
  "\n\tYou should have received a copy of the GNU General Public License along "
  "with Klein Drive.  If not, see <http://www.gnu.org/licenses/>";

AboutBox::AboutBox(Box box, GuiController* guiController)
  : StaticInformationBox(box, guiController, title_, text_)
{}

} // namespace Klein
