//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/AboutBox.h - The AboutBox class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the AboutBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_ABOUTBOX_H
#define KLEINDRIVE_ABOUTBOX_H

#include "src/gui/StaticInformationBox.h"
#include <string>

namespace kleindrive {

/// Displays author, license and program information.
class AboutBox final : public StaticInformationBox
{
public:
  /// Constructs an About page within the given box.
  AboutBox(Box box, GuiController* guiController);

private:
  /// The heading.
  static const std::string title_;

  /// The about text.
  static const std::string text_;
};

} // namespace Klein

#endif // KLEINDRIVE_ABOUTBOX_H
