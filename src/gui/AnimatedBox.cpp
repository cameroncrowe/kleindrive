//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/AnimatedBox.cpp - The AnimatedBox class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the AnimatedBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/AnimatedBox.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/LabelBox.h"
#include "src/scene/Scene.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <chrono>
#include <future>
#include <iostream>
#include <memory>

namespace kleindrive {

AnimatedBox::AnimatedBox(Box box, GuiController* guiController)
  : Fl_Widget(box.x, box.y, box.w, box.h)
  , box_{ box }
  , guiController_{ guiController }
{
  guiController->animatedBox(this);

  const phys_t partOfScreen = 0.05;
  const scrn_t height = std::max(16, static_cast<scrn_t>(partOfScreen * box.h));

  const scrn_t x = box.x;
  const scrn_t y = box.y + box.h - 2 * height;
  const scrn_t w = box.w;
  const scrn_t h = box.y + box.h - y - height;

  Box clickToContinueBox = { x, y, w, h };
  static const auto font = fonts::code8x8ishFont(1);
  static const int point = std::max(static_cast<int>(h / 8), 2);
  static const KDLabelAlignment alignment = KDLabelAlignment::Center;
  static const KDLabelFace face = KDLabelFace::Wide;

  const std::string message = "Click to Continue";

  clickToContinue_ =
    LabelBox(clickToContinueBox, message, font, point, alignment, face);
};

void
AnimatedBox::invalidateCurrentFrame()
{
  isCurrentFrameValid_ = false;
};

void
AnimatedBox::pause()
{
  // Stop animation.
  isPaused_ = true;
}

void
AnimatedBox::unpause()
{
  isPaused_ = false;
  // Restart the animation
  redraw();
};

phys_t
AnimatedBox::computeGameTime()
{
  // Game time is this objects total time in existence relative to the computer
  // clock.
  using Time = std::chrono::steady_clock::time_point;
  using Duration = std::chrono::duration<phys_t>;
  using Clock = std::chrono::steady_clock;

  static const Time beginComputer = Clock::now();
  const Time currentComputer = Clock::now();
  const Duration elapsed = currentComputer - beginComputer;

  const phys_t gameTime = elapsed.count();

  return gameTime;
}

void
AnimatedBox::stepTime()
{
  // Make sure we don't draw too many frames per second.
  const phys_t currentTime = computeGameTime();
  const phys_t minFrameDuration =
    static_cast<phys_t>(1) / static_cast<phys_t>(maxFramesPerSecond_);

  static phys_t currentFrameTime = 0;
  const phys_t lastFrameTime = currentFrameTime;
  currentFrameTime = currentTime;

  const phys_t lastFrameDuration = currentFrameTime - lastFrameTime;

  const phys_t durationToWait =
    std::max(static_cast<phys_t>(0), minFrameDuration - lastFrameDuration);

  const int msToWait = static_cast<int>(1000 * durationToWait);
  std::this_thread::sleep_for(std::chrono::milliseconds(msToWait));

  /*{
    // Compute frame rate and wait time.
    //  std::cout << "To fast: Waited for ms: " << msToWait << "\n";
    static int frameNumber = 0;
    ++frameNumber;
    const int everyNthFrame = 100;
    static phys_t previousNthFrameTime = currentTime;
    if (frameNumber % everyNthFrame == 0) {
      const phys_t meanDuration =
        (currentTime - previousNthFrameTime) / everyNthFrame;
      ;
      std::cout << "Average frames per second during past " << everyNthFrame
                << " frames: " << 1. / meanDuration << "\n";
      previousNthFrameTime = currentTime;
    }
  }
*/
}

void
AnimatedBox::draw()
{
  if (isPaused_)
    return;

  // These are defined by the user:
  Scene* scene = getScene();
  GameState* gameState = getGameState();

  gameState->animatedBox.x = x();
  gameState->animatedBox.y = y();
  gameState->animatedBox.w = w();
  gameState->animatedBox.h = h();

  // If the current frame is unusabeble draw it.
  if (!isCurrentFrameValid_) {
    stepTime();
    gameState->time = computeGameTime();
    currentFrame_ = scene->render(gameState);
    isCurrentFrameValid_ = true;
  }

  // Draw the current frame while computing the next frame.

  stepTime();
  gameState->time = computeGameTime();
  futureNextFrame_ =
    std::async(std::launch::async, std::bind(&Scene::render, scene, gameState));

  fl_push_clip(box_.x, box_.y, box_.w, box_.h);
  {
    drawCurrentFrame();
    if (guiController_->windowMode() == WindowMode::Menu &&
        guiController_->menuMode() == MenuMode::Preview) {
      setFlColor(colors::Blue);
      drawClickToContinue();
    }
  }
  fl_pop_clip();

  currentFrame_ = futureNextFrame_.get();

  // Schedule a redraw.
  Fl::add_timeout(0, AnimatedBox::redrawCallback, this);
}

void
AnimatedBox::drawClickToContinue()
{
  clickToContinue_.draw();

/* Use FLTK's fonts:
  char text[100] = "Click to Continue";
  fl_font(1, textBox_.h);
  fl_draw(text,
          textBox_.x,
          textBox_.y,
          textBox_.w,
          textBox_.h,
          FL_ALIGN_CENTER,
          nullptr,
          0);
*/
}

void
AnimatedBox::drawCurrentFrame()
{
  { // Draw background.
    const Color backgroundColor =
      currentFrame_.scenePropertiesState.backgroundColor;

    setFlColor(backgroundColor);

    scrn_t x = this->x(), // Upper left corner of window.
      y = this->y(),
           w = this->w(), // Width and height of window.
      h = this->h();

    fl_rectf(x, y, w, h); // Draw filled rectangle.u
  }

  { // Draw tiles.
    for (const auto& tile : currentFrame_.tiles) {

      setFlColor(tile.color);
      // We assume the compiler will optimize this away:
      if (currentFrame_.scenePropertiesState.tileDrawingMethod ==
            TileDrawingMethod::Solid ||
          currentFrame_.scenePropertiesState.tileDrawingMethod ==
            TileDrawingMethod::GaussianCurvature) {
        fl_begin_complex_polygon();
        {
          using sz_t = decltype(tile.corners)::size_type;
          for (sz_t I = 0; I < static_cast<sz_t>(tile.numberOfCorners); ++I) {
            const scrn_t row = tile.corners[2 * I + 0];
            const scrn_t col = tile.corners[2 * I + 1];
            fl_vertex(row, col);
          }
          fl_end_complex_polygon();
        }
      } else {
        // We'rein Grid mode.

        // Draw the bottom and left edges of all unclipped tiles.  Also draw the
        // top and right edges of tiles on the top and right edges of the
        // tomain.

        // Vertices start at bottom left corner, and proceed anti-clockwise.
        // Bottom edge.

        switch (tile.type) {
          case TileType::Central:
            fl_line(tile.corners[0],
                    tile.corners[1],
                    tile.corners[2],
                    tile.corners[3]); // Bottom
            fl_line(tile.corners[6],
                    tile.corners[7],
                    tile.corners[0],
                    tile.corners[1]); // Left edge
            break;

          case TileType::RightEdge:
            fl_line(tile.corners[0],
                    tile.corners[1],
                    tile.corners[2],
                    tile.corners[3]); // Bottom
            fl_line(tile.corners[6],
                    tile.corners[7],
                    tile.corners[0],
                    tile.corners[1]); // Left edge
            fl_line(tile.corners[2],
                    tile.corners[3],
                    tile.corners[4],
                    tile.corners[5]); // Right
            break;

          case TileType::TopEdge:
            fl_line(tile.corners[0],
                    tile.corners[1],
                    tile.corners[2],
                    tile.corners[3]); // Bottom
            fl_line(tile.corners[6],
                    tile.corners[7],
                    tile.corners[0],
                    tile.corners[1]); // Left
            fl_line(tile.corners[4],
                    tile.corners[5],
                    tile.corners[6],
                    tile.corners[7]); // Top
            break;

          case TileType::TopRightCorner:
            fl_line(tile.corners[0],
                    tile.corners[1],
                    tile.corners[2],
                    tile.corners[3]); // Bottom.
            fl_line(tile.corners[6],
                    tile.corners[7],
                    tile.corners[0],
                    tile.corners[1]); // Left.
            fl_line(tile.corners[2],
                    tile.corners[3],
                    tile.corners[4],
                    tile.corners[5]); // Top.
            fl_line(tile.corners[4],
                    tile.corners[5],
                    tile.corners[6],
                    tile.corners[7]); // Right.
            break;
          case TileType::Clipped:
            // We're on a cropped tile. Just draw all the edges and hope for the
            // best. -- TODO: figure out which edges are against the window and
            // don't draw it.
            fl_begin_loop();
            {
              using sz_t = decltype(tile.corners)::size_type;
              for (sz_t I = 0; I < static_cast<sz_t>(tile.numberOfCorners);
                   ++I) {
                const scrn_t row = tile.corners[2 * I + 0];
                const scrn_t col = tile.corners[2 * I + 1];
                fl_vertex(row, col);
              }
            }
            fl_end_line();
            break;
        }
      }
    }
  }

  // Draw domain map, if available.
  {
    if (currentFrame_.domainMap) {
      currentFrame_.domainMap->draw();
    }
  }
}

void
AnimatedBox::redrawCallback(void* arg)
{
  AnimatedBox* animatedBox = static_cast<AnimatedBox*>(arg);
  animatedBox->redraw();
}

} // namespace kleindrive
