//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/AnimatedBox.h - AnimatedBox class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the AnimatedBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_ANIMATEDBOX_H
#define KLEINDRIVE_ANIMATEDBOX_H

#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/LabelBox.h"
#include "src/scene/Scene.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <chrono>
#include <future>

namespace kleindrive {

/// Animates a scene.
class AnimatedBox : public Fl_Widget
{
public:
  AnimatedBox(Box box, GuiController* guiController);

  /// Call this if the frame waiting to be drawn is invalid.
  ///
  /// Lest a flash of an old scene show on the window.
  void invalidateCurrentFrame();

  /// Stops animation and turns the CameraOperators off.
  ///
  /// This shoud be called any time there is a delay in drawing the next frame,
  /// or if the frame should be paused.
  void pause();

  /// Restarts animation (and by effect the CameraOperators).
  void unpause();

  /// The fl_timeout_handler that redraws the window.
  static void redrawCallback(void* arg);

protected:
  /// The child class is required to produce a scene on command.
  virtual Scene* getScene() = 0;

  /// The child class is required to produce a GameState on command to feed to
  /// the scene.
  virtual GameState* getGameState() = 0;

  /// Box
  Box box_;
  
  /// The address of the GuiController.
  GuiController* guiController_;

private:
  /// Draws the current frame to the enclosing box and schedules another redraw
  /// if unpaused.
  void draw() final override;

  /// Returns the game time.
  phys_t computeGameTime();

  /// Returns the time since the last frame.
  void stepTime();

  /// Updates the game state if the frame had to wait to keep the frame rate
  /// down.
  void UpdateGameState();

  /// Paints the current frame to the enclosing box.
  void drawCurrentFrame();

  /// Draw "Click to Continue" if we're previewing a scene.
  void drawClickToContinue();

  /// The box used to show text over the animation (eg. click to continue).
  LabelBox clickToContinue_;
  
  // Fl_OffScreen ClickToContinueBuffer_;

  /// Current frame.
  Frame currentFrame_;

  /// Next frame.
  std::future<Frame> futureNextFrame_;

  // Is the current frame useable.
  bool isCurrentFrameValid_{ false };

  /// Is the animation running.
  bool isPaused_{ false };

  /// Rendering properties and methods:
  phys_t maxFramesPerSecond_{ 60 };
};

} // namespace kleindrive

#endif // KLEINDRIVE_ANIMATEDBOX_H
