//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/Box.h - Box class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Box class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_BOX_H
#define KLEINDRIVE_BOX_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Contains the upper left corner, and side lengths of a box.
struct Box final
{
public:
  scrn_t x, y, w, h;
};

} // namespace Klein

#endif // KLEINDRIVE_BOX_H
