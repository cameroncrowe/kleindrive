//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/DriveScreen.cpp - The DriveScreen class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the DriveScreen class implementation.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/gui/AnimatedBox.h"
#include "src/gui/DriveScreen.h"

namespace kleindrive {

DriveScreen::DriveScreen(Box box, GuiController* guiController)
  : AnimatedBox(box, guiController)
{
  guiController->driveScreen(this);
}

// Retrieves the drive screen from the GuiController.
Scene*
DriveScreen::getScene()
{
  return guiController_->driveScene();
};

// Retreives the GameState from the GuiController.
GameState*
DriveScreen::getGameState()
{
  return guiController_->gameState();
}

// Lets the GuiController handle everthing that the parent class doesn't want.
int
DriveScreen::handle(int event)
{
  // Let chilren handle anything they like.
  if (AnimatedBox::handle(event))
    return 1;

  // Handle the rest.
  return 0;
}

} // namespace Kleindrive
