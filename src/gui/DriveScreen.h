//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/DriveScreen.h - The DriveScreen class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the DriveScreen class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_DRIVESCREEN_H
#define KLEINDRIVE_DRIVESCREEN_H

#include "src/gui/AnimatedBox.h"

namespace kleindrive {

/// Screen for the driving animation.
class DriveScreen final : public AnimatedBox
{
public:
  /// Construct a driving animation inside the given box.
  DriveScreen(Box box, GuiController* guiController);

private:

  /// Returns the drive scene.
  virtual Scene* getScene() final override;

  /// Returns the game state.
  virtual GameState* getGameState() final override;

  /// Handles diving events.
  virtual int handle(int event) final override;
};

} // namespace Kleindrive

#endif // KLEINDRIVE_DRIVESCREEN
