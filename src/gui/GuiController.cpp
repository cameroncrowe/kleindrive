//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/GuiController.h - GuiController class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the GuiController class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/collections/welcomescene/KDWelcomeScene.h"
#include "src/gui/AnimatedBox.h"
#include "src/gui/DriveScreen.h"
#include "src/gui/KDWindow.h"
#include "src/gui/MenuContextBox.h"
#include "src/gui/PreviewBox.h"
#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <memory>
#include <vector>

namespace kleindrive {

GuiController::GuiController(std::unique_ptr<SceneCollection>& sceneCollection)
  : sceneCollection_{ std::move(sceneCollection) }
  , welcomeScene_{ scenes::kdWelcomeScene() }
  , window_{ nullptr }
  , previewBox_{ nullptr }
  , driveScreen_{ nullptr }
  , menuContextBox_{ nullptr }
  , menuBox_{ nullptr }
{}

// Methods that record the address of an object
// --------------------------------------------

void
GuiController::window(KDWindow* window)
{
  window_ = window;
}

void
GuiController::animatedBox(AnimatedBox* animatedBox)
{
  animatedBoxes_.push_back(animatedBox);
}

void
GuiController::previewBox(PreviewBox* previewBox)
{
  previewBox_ = previewBox;
}

void
GuiController::driveScreen(DriveScreen* driveScreen)
{
  driveScreen_ = driveScreen;
}

void
GuiController::menuContextBox(MenuContextBox* menuContextBox)
{
  menuContextBox_ = menuContextBox;
};

void
GuiController::menuBox(MenuBox* menuBox)
{
  menuBox_ = menuBox;
}

// Methods that reveal a state stored in the GuiController.
// --------------------------------------------------------

WindowMode
GuiController::windowMode()
{
  return windowMode_;
}

MenuMode
GuiController::menuMode()
{
  return menuMode_;
}

int
GuiController::sceneNumber()
{
  return sceneCollection_->currentSceneNumber();
}

std::string
GuiController::sceneName(int sceneNumber)
{
  return sceneCollection_->sceneName(sceneNumber);
};

GameState*
GuiController::gameState()
{
  gameState_.window.w = window_->w();
  gameState_.window.h = window_->h();

  return &gameState_;
}

Scene*
GuiController::driveScene()
{
  return sceneCollection_->driveScene();
}

Scene*
GuiController::previewScene()
{
  return sceneCollection_->previewScene();
}

Scene*
GuiController::welcomeScene()
{
  return welcomeScene_.get();
}

int
GuiController::numberOfScenes()
{
  return sceneCollection_->numberOfScenes();
}

// Methods that manipulate gui objects.
// ------------------------------------

void
GuiController::changeMenuMode(MenuMode newMode, int newSceneNumber)
{
  switch (newMode) {
    case MenuMode::Welcome:
      if (menuMode_ == newMode)
        return;
      menuMode_ = newMode;
      break;

    case MenuMode::About:
      if (menuMode_ == newMode)
        return;
      menuMode_ = newMode;
      break;

    case MenuMode::Info:
      if (menuMode_ == newMode)
        return;
      menuMode_ = newMode;
      break;

    case MenuMode::Preview:
      if (menuMode_ == newMode &&
          newSceneNumber == sceneCollection_->currentSceneNumber())
        return;
      if (!sceneCollection_->changeScene(newSceneNumber))
        return;
      // If here, then the mode changed or the number changed.
      // Overkill, but simple.
      previewBox_->invalidateCurrentFrame();
      driveScreen_->invalidateCurrentFrame();
      menuMode_ = newMode;
      sceneCollection_->resetSceneProperties();
      break;
  }
  menuContextBox_
    ->changeMode(); // ChangeMode schedules a redraw of the relevant area.
};

void
GuiController::changeWindowMode(WindowMode newMode)
{
  windowMode_ = newMode;

  // This also schedules a redraw.
  window_->changeMode();

  sceneCollection_->reset();

  // When we change states, we stop receiving keyup events, thus we lose the
  // game state.
  //  gameState_ = GameState();

  previewBox_->invalidateCurrentFrame();
  driveScreen_->invalidateCurrentFrame();
}

// Methods that handle external events.
// ------------------------------------

void
GuiController::lostFocus()
{
  sceneCollection_->turnOff();
  welcomeScene_->turnOff();
  for (const auto animatedBox : animatedBoxes_)
    animatedBox->pause();
}

void
GuiController::gainedFocus()
{
  for (const auto animatedBox : animatedBoxes_)
    animatedBox->unpause();

  // For some reason the menu box redraws or shows late, so we schedule it to
  // redraw. This may have a more obvious underlying cause.  Better to fix it
  // there, but for now.
  menuBox_->redraw();
}

void
GuiController::moveMouse(scrn_t x, scrn_t y)
{
  gameState_.mouse.x = x;
  gameState_.mouse.y = y;
};

void
GuiController::quit(int errors)
{
  exit(errors);
}

int
GuiController::handle(int event)
{
  // Used to prevent a click putting the window back into focus from clicking
  // a button.
  static bool isInClickFocus = true;

  switch (event) {
    case FL_FOCUS:
      gainedFocus();
      return 1;

    case FL_UNFOCUS:
      lostFocus();
      std::cout << "Taking out of focus";
      isInClickFocus = false;
      return 1;

    case FL_KEYDOWN: {
      int keyUsedByGameState = changeGameStateKeyIsDown(Fl::event_key(), true);
      int keyUsedByGuiController = 0;
      switch (Fl::event_key()) {

        case FL_Enter:
        case FL_KP_Enter:
        case 32: // Space bar.
          if (windowMode_ == WindowMode::Menu) {
            if (menuMode_ == MenuMode::Preview) {
              changeWindowMode(WindowMode::Drive);
              keyUsedByGuiController = 1;
              break;
            }
          }
          keyUsedByGuiController = 0;
          break;
          /*

                    case 'q':
                    case 'Q':
                      if(windowMode_ == WindowMode::Drive)
                      {
                        changeWindowMode(WindowMode::Menu);
                        keyUsedByGuiController = 1;
                        break;
                      }
                      keyUsedByGuiController = 0;
                      break;

                    case 'e':
                    case 'E':
                      if(windowMode_ == WindowMode::Menu)
                      {
                        if(menuMode_ == MenuMode::Preview)
                        {
                          changeWindowMode(WindowMode::Drive);
                          keyUsedByGuiController = 1;
                          break;
                        }
                      }
                      keyUsedByGuiController = 0;
                      break;
          */

        case 'w': // Up keys
        case 'W':
        case 'p':
        case 'P':
        case FL_Up:
          if (windowMode_ == WindowMode::Menu) {
            menuBox_->decrementMenuSelection();
            keyUsedByGuiController = 1;
            break;
          }
          break;

        case 's': // Down keys.
        case 'S':
        case ';':
        case ':':
        case FL_Down:
          if (windowMode_ == WindowMode::Menu) {
            menuBox_->incrementMenuSelection();
            keyUsedByGuiController = 1;
            break;
          }
          break;

        case 'a': // Left keys
        case 'A':
        case 'l':
        case 'L':
        case FL_Left:
          if (windowMode_ == WindowMode::Menu) {
            menuBox_->pageDown();
            keyUsedByGuiController = 1;
          }
          break;

        case 'd': // Right keys
        case 'D':
        case '"':
        case '\'':
        case FL_Right:
          if (windowMode_ == WindowMode::Menu) {
            menuBox_->pageUp();
            keyUsedByGuiController = 1;
          }
          break;

        // Number keys control the drawing method.
        case '1':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 1;
          keyUsedByGuiController = 1;
          break;

        case '2':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 2;
          keyUsedByGuiController = 1;
          break;

        case '3':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 3;
          keyUsedByGuiController = 1;
          break;

        case '4':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 4;
          keyUsedByGuiController = 1;
          break;

        case '5':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 5;
          keyUsedByGuiController = 1;
          break;

        case '6':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 6;
          keyUsedByGuiController = 1;
          break;

        case '7':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 7;
          keyUsedByGuiController = 1;
          break;

        case '8':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 8;
          keyUsedByGuiController = 1;
          break;

        case '9':
          gameState_.scene.isSettingDrawingMethodByNumber = true;
          gameState_.scene.numberOfDesiredDrawingMethod = 9;
          keyUsedByGuiController = 1;
          break;
      }
      return ((keyUsedByGameState) || (keyUsedByGuiController)) ? 1 : 0;
    }
    case FL_KEYUP:
      return changeGameStateKeyIsDown(Fl::event_key(), false);

    case FL_SHORTCUT:
      switch (Fl::event_key()) {
        case FL_Escape:
          if (windowMode_ == WindowMode::Drive) {
            changeWindowMode(WindowMode::Menu);
          }
          if (windowMode_ == WindowMode::Menu) {
            // key is repeated too fast for this to be useful.
            // quit();
          }
          return 1;

        default:
          return 0;
      }

    case FL_MOVE:
      // Listen to the mouse, but let the window handle it.
      moveMouse(Fl::event_x(), Fl::event_y());
      return window_->letChildrenHandle(event);

    case FL_PUSH:
      if (isInClickFocus) {
        if (window_->letChildrenHandle(event))
          return 1;
      } else {
        isInClickFocus = true;
        return 1;
      }

      //if (windowMode_ == WindowMode::Drive) {
      //  gameState_.scene.isRotatingDrawingMethod = true;
      //  return 1;
      //}
      return 0;

    default:
      // Everything else goes to the window.
      return window_->letChildrenHandle(event);
  }
}

int
GuiController::changeGameStateKeyIsDown(int key, bool isDown)
{
  // Already handled escape key in Window class.
  switch (key) {
    case 'a': // Left handed.
    case 'A':
    case 'l': // Right handded.
    case 'L':
    case FL_Left: // Alternate.
      gameState_.car.isSlidingLeft = isDown;
      return 1;

    case 'd': // Left handed.
    case 'D':
    case '\'': // Right handed.
    case '"':
    case FL_Right: // Alternate.
      gameState_.car.isSlidingRight = isDown;
      return 1;

    case 'w': // Left handed.
    case 'W':
    case 'p': // Right handed.
    case 'P':
    case FL_Up: // Alternate.
      gameState_.car.isGoingForward = isDown;
      if (isDown)
        return 1;

    case 's': // Left handed.
    case 'S':
    case ':': // Right handed.
    case ';':
    case FL_Down: // Alternate
      gameState_.car.isGoingBackward= isDown;
      return 1;

    case '+':
    case '=':
      gameState_.camera.isZoomingIn = isDown;
      return 1;

    case '-':
    case '_':
      gameState_.camera.isZoomingOut = isDown;
      return 1;

    case '0':
    case ')':
      gameState_.camera.isResettingZoom = isDown;
      return 1;

    case FL_Shift_L: // Left handed.
    case FL_Shift_R: // Right handed.
      gameState_.car.isGeodesic= isDown;
      return 1;

    case FL_Meta_L: // Left handed.
    case FL_Meta_R: // Right handed.
      gameState_.car.isGoingFast = !isDown;
      return 1;

    case 32: // Space bar; both left and right handed.
      gameState_.car.isBraking = isDown;
      return 1;

    default:
      return 0;
  }
}

} // namespace Klein
