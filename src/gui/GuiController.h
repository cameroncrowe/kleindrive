//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/GuiController.h - GuiController class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the GuiController class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_GUICONTROLLER_H
#define KLEINDRIVE_GUICONTROLLER_H

#include "src/scene/Scene.h"
#include "src/scenecollection/SceneCollection.h"
#include <memory>
#include <vector>

namespace kleindrive {

/// The states for at the level of the gui's Window class.
enum class WindowMode
{
  Menu,
  Drive
};

/// The states at the level of the gui's MenuScreen class.
enum class MenuMode
{
  Welcome,
  Info,
  About,
  Preview
};

// These classes pass their addresses to the GuiController so they can be
// reached.
class AnimatedBox;
class DriveScreen;
class MenuContextBox;
class PreviewBox;
class MenuBox;
class KDWindow;

/// The central controller for gui events and scenecollection events.
class GuiController final
{
public:
  /// \name Constructor.
  /// @{

  /// Constructs the guicontroller with the given SceneCollection.
  explicit GuiController(std::unique_ptr<SceneCollection>& sceneCollection);

  /// @}

  //////////////////////////////////////////////////////////////////////////////

  /// \name Methods for passing gui object addresses into the GuiController.
  /// @{

  /// Method for the gui's DriveScreen to record its address in the
  /// GuiController.
  void driveScreen(DriveScreen* driveScreen);

  /// Method for the gui's MenuBox to record its address in the GuiController.
  void menuBox(MenuBox* menuBox);

  /// Method for the gui's MenuContextBox to record its address in the
  /// GuiController.
  void menuContextBox(MenuContextBox* menuContextBox);

  /// Method for the gui's PreviewBox to record its address in the
  /// GuiController.
  void previewBox(PreviewBox* previewBox);

  /// Method for the gui's Window to record its address in the GuiController.
  void window(KDWindow* window);

  /// Method for the gui's AnimatedBox to record its address in the
  /// GuiController.
  void animatedBox(AnimatedBox* animatedBox);

  /// @}

  //////////////////////////////////////////////////////////////////////////////

  /// \name Accessors for gui objects.
  /// @{

  /// Returns the current WindowMode.
  WindowMode windowMode();

  /// Returns the current MenuMode,
  MenuMode menuMode();

  /// Returns the current scene nunber.
  int sceneNumber();

  /// Returns the total number of scenes.
  int numberOfScenes();

  /// Returns the name of a scene by its identifying number.
  std::string sceneName(int sceneNumber);

  /// Returns the address of the GameState.
  GameState* gameState();

  /// Returns the Scene used for the driving animation.
  Scene* driveScene();

  /// Returns the Scene used for the preview animation.
  Scene* previewScene();

  /// Returns the Scene used for the logo animatin in the WelcomeBox.
  Scene* welcomeScene();

  /// @}

  //////////////////////////////////////////////////////////////////////////////

  /// \name  Methods for requesting a change of mode.
  /// @{

  /// Requests a change of menu mode.
  ///
  /// For the preview mode, the number of the desired scene may also be passed.
  void changeMenuMode(MenuMode newMode, int newSceneNumber = 0);

  /// Requests a chance of Windowmode.
  void changeWindowMode(WindowMode newMode);

  /// @}

  //////////////////////////////////////////////////////////////////////////////

  /// \name Centralized event handler.
  /// @{

  /// Handles certain events.
  int handle(int event);

  /// @}

private:
  /// Processes the KEYUP and KEYDOWN events related to the GameState
  int changeGameStateKeyIsDown(int key, bool isDown);

  /// To be called when the gui loses focus.
  void lostFocus();

  /// To be called when the gui regains focus.
  void gainedFocus();

  /// Move the mouse in the game state.
  void moveMouse(scrn_t x, scrn_t y);

  /// This is called for exit, rather than exit(0), in case we want to do
  /// something more elaborate in the future.
  [[noreturn]] void quit(int errors = 0);

  /// The gui's scene colleciton.
  std::unique_ptr<SceneCollection> sceneCollection_;

  /// The gui's welcome scene (logo).
  std::unique_ptr<Scene> welcomeScene_;

  /// The game state stores the state of objects not related to the gui.
  GameState gameState_;

  /// The gui's states.
  WindowMode windowMode_{ WindowMode::Menu };
  MenuMode menuMode_{ MenuMode::Welcome };

  // Addresses of objects the GuiController needs to get in contact with.
  /// Address of the window.
  KDWindow* window_;

  /// Address of the preview box.
  PreviewBox* previewBox_;

  /// Address of the driving screen.
  DriveScreen* driveScreen_;

  /// Address of the menu context box.
  MenuContextBox* menuContextBox_;

  /// Address of the menubox.
  MenuBox* menuBox_;

  // A list of addresses of all the  animated boxes for passing turn off events.
  std::vector<AnimatedBox*> animatedBoxes_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_GUICONTROLLER
