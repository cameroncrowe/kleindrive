//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/InfoBox.cpp - InfoBox class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Caperon Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the InfoBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/InfoBox.h"
#include "src/gui/StaticInformationBox.h"
#include <string>

namespace kleindrive {

const std::string InfoBox::title_ = "Instructions";

// &h = shift
// &m = meta


const std::string InfoBox::text_ =

  "\tWelcome to the alpha release of Klein Drive! You are about to embarque on "
  "a journey around the Great Classic Surfaces&l in your new electric "
  "Kleinmobile. Here's how to drive:"
  "\n "
  "\n1. Put one hand on the &o.  &_ turns the car, and &\\ looks up and down."
  "\n "
  "\n2. Put another hand on your favorite set of 'arrow keys'. &| moves forward "
  "and reverse, and and &- strafes left or right:"
  "\n "
  "\n\t& &W& \t& &P& \t& &^&\a"
  "\n\t&A&S&D\t&L&;&'\t&<&v&>"
  "\n "
  "\n3. You can also use these tertiary controls:"
  "\n\t&*&h follow a geodesic."
  "\n\t&*&m slow down."
  "\n\t&*&s speed up."
  "\n\t&*&1, &2, &3 "/*and &b*/"switch the display mode."
  "\n\t&*&i zoom in. &j zoom out. &0 reset zoom."
  "\n "
  "\n4. &x turns off the car, and returns to the menu.";

InfoBox::InfoBox(Box box, GuiController* guiController)
  : StaticInformationBox(box, guiController, title_, text_)
{}

} // namespace Klein
