//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/KDWindow.cpp - KDWindow class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDWindow class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/KDWindow.h"
#include "src/gui/Box.h"
#include "src/gui/DriveScreen.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuScreen.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <memory>

namespace kleindrive {

KDWindow::KDWindow(Box box, char* label, GuiController* guiController)
  : Fl_Double_Window(box.x, box.y, box.w, box.h, label)
  , guiController_{ guiController }
  , driveScreen_{ std::make_unique<DriveScreen>(box, guiController) }
  , menuScreen_{ std::make_unique<MenuScreen>(box, guiController) }
{
  guiController->window(this);
  driveScreen_->hide();
  end();
}

void
KDWindow::changeMode()
{
  driveScreen_->hide();
  menuScreen_->hide();

  switch (guiController_->windowMode()) {
    case WindowMode::Drive:
      driveScreen_->show();
      return;

    case WindowMode::Menu:
      menuScreen_->show();
  }
}

int
KDWindow::handle(int event)
{
  return guiController_->handle(event);
}

int
KDWindow::letChildrenHandle(int event)
{
  return Fl_Group::handle(event);
}

} // namespace Klein
