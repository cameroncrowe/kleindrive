//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/KDWindow.h - KDWindow class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDWindow class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDWINDOW_H
#define KLEINDRIVE_KDWINDOW_H

#include "src/gui/Box.h"
#include "src/gui/DriveScreen.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuScreen.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <memory>

namespace kleindrive {

/// The double buffered window for all the gui objects.
class KDWindow final : public Fl_Double_Window
{
public:
  /// Construct a window within the given box.
  KDWindow(Box box, char* label, GuiController* guiController);

  /// Changes the mode between the menu and driving screens.
  void changeMode();

  /// Passes an event to the window's children.
  int letChildrenHandle(int event);

private:
  // Carefully strips out the escape key, and listens to the
  // focus/unfocus events to quell or restart animation, and
  // listents to the mouse.  Everything else is passed on.
  virtual int handle(int event) final override;

  /// Address of the GuiController.
  GuiController* guiController_;

  /// Drive screen.
  std::unique_ptr<DriveScreen> driveScreen_;

  /// Menu screen.
  std::unique_ptr<MenuScreen> menuScreen_;

};

} // namespace Klein

#endif // KLEINDRIVE_KDWINDOW_H
