//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/LabelBox.h
//
//  The LabelBox template class file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the LabelBox class impementation.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_LABELBOX_H
#define KLEINDRIVE_LABELBOX_H

#include "src/collections/Colors.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/string/KDFont.h"
#include "src/string/KDString.h"
#include "src/string/KDTextBlock.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <string>

namespace kleindrive {

/// The label alignment options for a label.
enum class KDLabelAlignment
{
  Left,
  Center
};

/// The font face options for a label.
enum class KDLabelFace
{
  Normal,
  Wide,
  Tall,
  Bold,
};

/// A lable box displays an aliased font in a box.
class LabelBox
{
public:
  /// The constructor must know the number of rows used in the font.
  ///
  /// The is an artifact of the problem that the fonts were not designed to be
  /// used to display text on a screen, but on a surface.
  template<int NUMBEROFROWS>
  inline LabelBox(Box box,
                  const std::string& label,
                  KDFont<NUMBEROFROWS> font,
                  int point,
                  KDLabelAlignment alignment = KDLabelAlignment::Left,
                  KDLabelFace face = KDLabelFace::Normal);

  /// Default constructor.
  inline LabelBox();

  /// Draws the label to the bounding box.
  inline void draw();

private:
  // Data for the enclosing box.
  Box box_;

  /// Number of rows needed by the text.
  int numberOfRows_;

  /// Number of columns needed by the text.
  int numberOfColumns_;

  // Font data.
  KDString labelString_;
  int point_;

  // We shut up a false positive warning about this not being used--it is!
  /// Label alignment
  [[maybe_unused]] KDLabelAlignment alignment_;

  /// Type face.
  KDLabelFace face_;

  // Vertical offset for centering the text.
  int verticalOffset_{ 0 };

  // Horizontal offset for centering the text.
  int horizontalOffset_{ 0 };
};

template<int NUMBEROFROWS>
inline LabelBox::LabelBox(Box box,
                          const std::string& label,
                          KDFont<NUMBEROFROWS> font,
                          int point,
                          KDLabelAlignment alignment,
                          KDLabelFace face)
  : box_{ box }
  , numberOfRows_{ box.h / point }
  , numberOfColumns_{ box.w / point }
  , labelString_{ font, label }
  , point_{ point }
  , alignment_{ alignment }
  , face_{ face }
{
  const int horizontalExtra = box.w - labelString_.numberOfColumns() * point;
  const int verticalExtra = box.h - labelString_.numberOfRows() * point;

  horizontalOffset_ = horizontalExtra / 2;
  verticalOffset_ = verticalExtra / 2;
}

inline LabelBox::LabelBox()
  : box_{ 0, 0, 0, 0 }
  , numberOfRows_{ 0 }
  , numberOfColumns_{ 0 }
  , labelString_{ KDFont<1>(1), "" }
  , point_{ 0 }
  , alignment_{ KDLabelAlignment::Left }
  , face_{ KDLabelFace::Normal }
{}

inline void
LabelBox::draw()
{
  for (int R = 0; R < numberOfRows_; ++R)
    for (int C = 0; C < numberOfColumns_; ++C)
      if (labelString_.characteristicFunction(R, C) == 1) {
        const int x = box_.x + point_ * C + horizontalOffset_;
        const int y = box_.y + point_ * R + verticalOffset_;
        switch (face_) {
          case KDLabelFace::Normal:
            fl_rectf(x, y, point_, point_);
            break;
          case KDLabelFace::Tall:
            fl_rectf(x, y, point_, 2 * point_);
            break;
          case KDLabelFace::Wide:
            fl_rectf(x, y, 2 * point_, point_);
            break;
          case KDLabelFace::Bold:
            fl_rectf(x, y, 2 * point_, 2 * point_);
            break;
        }
      }
}

} // namespace kleindrive

#endif // KLEINDRIVE_LABELBOX_H
