//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuBox.h - The MenuBox class implementation file. 
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/MenuBox.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuItem.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <iostream>
#include <vector>

namespace kleindrive {

MenuBox::MenuBox(Box box, GuiController* guiController)
  : Fl_Group(box.x, box.y, box.w, box.h)
  , guiController_{ guiController }
{

  guiController_->menuBox(this);

  const int totalItems = numberOfPreviewItems_ + numberOfMainMenuItems_ +
                         numberOfArrowButtons_ + numberOfBlanks_;

  // Currently:
  //
  // [Blank 0.]
  // Welcome.
  // Instruction.
  // ABout.
  // [Blank 1.]
  // GO!
  // PageLeft.
  // PreviewItems 0
  // :
  // PreviewItem N-1
  // PageDown/
  // [Blank 2.]

  // MENUITEM 0.
  blank0_ = std::make_unique<MenuItem>(computeMenuItemBox(box, 0, totalItems));

  // MENUITEM 1.
  // Welcome
  {
    const LabelFunction labelFunction = []() {
      return "Klein Drive";
    }; // &~ is the version character.

    const IsShownFunction isShownFunction = []() { return true; };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::Welcome);
    };

    const ClickFunction singleClickFunction = [&]() {
      guiController_->changeMenuMode(MenuMode::Welcome);
    };

    welcome_ =
      std::make_unique<MenuItem>(computeMenuItemBox(box, 1, totalItems),
                                 labelFunction,
                                 isShownFunction,
                                 isClickableFunction,
                                 isSelectedFunction,
                                 singleClickFunction,
                                 singleClickFunction,
                                 KDMenuItemAlign::Center,
                                 KDMenuItemFontFace::Wide);
  }

  // MENUITEM 2.
  // Info.
  {
    const LabelFunction labelFunction = []() { return "Instructions"; };

    const IsShownFunction isShownFunction = []() { return true; };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::Info);
    };

    const ClickFunction singleClickFunction = [&]() {
      guiController_->changeMenuMode(MenuMode::Info);
    };

    info_ = std::make_unique<MenuItem>(computeMenuItemBox(box, 2, totalItems),
                                       labelFunction,
                                       isShownFunction,
                                       isClickableFunction,
                                       isSelectedFunction,
                                       singleClickFunction,
                                       singleClickFunction,
                                       KDMenuItemAlign::Center,
                                       KDMenuItemFontFace::Wide);
  }

  // MENUITEM 3.
  // About
  {
    const LabelFunction labelFunction = []() { return "About"; };

    const IsShownFunction isShownFunction = []() { return true; };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::About);
    };

    const ClickFunction singleClickFunction = [&]() {
      guiController_->changeMenuMode(MenuMode::About);
    };

    about_ = std::make_unique<MenuItem>(computeMenuItemBox(box, 3, totalItems),
                                        labelFunction,
                                        isShownFunction,
                                        isClickableFunction,
                                        isSelectedFunction,
                                        singleClickFunction,
                                        singleClickFunction,
                                        KDMenuItemAlign::Center,
                                        KDMenuItemFontFace::Wide);
  }

  // MENUITEM 4.
  // Blank 1.
  blank1_ = std::make_unique<MenuItem>(computeMenuItemBox(box, 4, totalItems));

  // MENUITEM 5.
  // Go Button.
  {
    const LabelFunction labelFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::Preview)
               ? "Go!"
               : "Select:";
    };

    const IsShownFunction isShownFunction = []() { return true; };

    const IsClickableFunction isClickableFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::Preview);
    };

    const IsSelectedFunction isSelectedFunction = [&]() {
      return (guiController_->menuMode() == MenuMode::Preview);
    };

    const ClickFunction singleClickFunction = [&]() {
      if (guiController_->menuMode() == MenuMode::Preview)
        guiController_->changeWindowMode(WindowMode::Drive);
    };

    go_ = std::make_unique<MenuItem>(computeMenuItemBox(box, 5, totalItems),
                                     labelFunction,
                                     isShownFunction,
                                     isClickableFunction,
                                     isSelectedFunction,
                                     singleClickFunction,
                                     singleClickFunction,
                                     KDMenuItemAlign::Center);
  }

  // MENUITEM 6.
  // Page up.
  {
    const LabelFunction labelFunction = []() {
      return "&{";
    }; // Super wide up arrow.

    const IsShownFunction isShownFunction = []() {
      return true;
    }; // return (0 <= this->pageNumber_ - 1); };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = []() { return false; };

    const ClickFunction singleClickFunction = [&]() { pageDown(); };

    pageLeft_ =
      std::make_unique<MenuItem>(computeMenuItemBox(box, 6, totalItems),
                                 labelFunction,
                                 isShownFunction,
                                 isClickableFunction,
                                 isSelectedFunction,
                                 singleClickFunction,
                                 singleClickFunction,
                                 KDMenuItemAlign::Center);
  }

  // MENUITEM TOTALITEMS-3
  // Page down menu item.
  {
    //  &} is a super wide down arrow.
    const LabelFunction labelFunction = []() { return "&}"; };

    const IsShownFunction isShownFunction = [&]() {
      /*
      static const int numberOfScenes = guiController_->numberOfScenes();

      static const int numberOfPages = (numberOfScenes +
                       numberOfPreviewItems_ - 1) /
                       numberOfPreviewItems_;

      return pageNumber_ + 1 < numberOfPages;
       */
      return true;
    };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = []() { return false; };

    const ClickFunction singleClickFunction = [&]() { pageUp(); };

    pageRight_ = std::make_unique<MenuItem>(
      computeMenuItemBox(box, totalItems - 3, totalItems),
      labelFunction,
      isShownFunction,
      isClickableFunction,
      isSelectedFunction,
      singleClickFunction,
      singleClickFunction,
      KDMenuItemAlign::Center);
  }

  // MENUITEM TOTALITEMS-2.
  // Exit.
  {
    const LabelFunction labelFunction = []() { return "Exit"; };
    const IsShownFunction isShownFunction = []() { return true; };
    const IsClickableFunction isClickableFunction = []() { return true; };
    const IsSelectedFunction isSelectedFunction = []() { return false; };
    const ClickFunction singleClickFunction = []() { exit(0); };

    exit_ = std::make_unique<MenuItem>(
      computeMenuItemBox(box, totalItems - 2, totalItems),
      labelFunction,
      isShownFunction,
      isClickableFunction,
      isSelectedFunction,
      singleClickFunction,
      singleClickFunction,
      KDMenuItemAlign::Center);
  }

  // MENUITEM TOTALITEMS-1.
  // Blank 2.
  blank2_ = std::make_unique<MenuItem>(
    computeMenuItemBox(box, totalItems - 1, totalItems));

  // PREVIEW MENU
  //
  using sz_t = decltype(previews_)::size_type;
  previews_.reserve(static_cast<sz_t>(numberOfPreviewItems_));

  for (int I = 0; I < numberOfPreviewItems_; ++I) {
    const LabelFunction labelFunction = [I, this]() {
      return this->guiController_->sceneName(I + this->numberOfPreviewItems_ *
                                                   this->pageNumber_);
    };

    const IsShownFunction isShownFunction = [I, this]() {
      const int sceneNumber =
        I + this->numberOfPreviewItems_ * this->pageNumber_;
      return (0 <= sceneNumber &&
              sceneNumber < this->guiController_->numberOfScenes());
    };

    const IsClickableFunction isClickableFunction = []() { return true; };

    const IsSelectedFunction isSelectedFunction = [I, this]() {
      const int sceneNumber =
        I + this->numberOfPreviewItems_ * this->pageNumber_;

      return (this->guiController_->menuMode() == MenuMode::Preview) &&
             (this->guiController_->sceneNumber() == sceneNumber);
    };

    const ClickFunction singleClickFunction = [I, this]() {
      const int sceneNumber =
        I + this->numberOfPreviewItems_ * this->pageNumber_;

      if (0 <= sceneNumber &&
          sceneNumber < this->guiController_->numberOfScenes()) {
        this->guiController_->changeMenuMode(MenuMode::Preview, sceneNumber);
      };
    };

    const ClickFunction doubleClickFunction = [I, this]() {
      const int sceneNumber = I + numberOfPreviewItems_ * pageNumber_;
      if (0 <= sceneNumber && sceneNumber < guiController_->numberOfScenes()) {
        this->guiController_->changeMenuMode(MenuMode::Preview, sceneNumber);
        this->guiController_->changeWindowMode(WindowMode::Drive);
      };
    };

    previews_.push_back(std::make_unique<MenuItem>(
      computeMenuItemBox(box, numberOfMainMenuItems_ + 2 + I, totalItems),
      labelFunction,
      isShownFunction,
      isClickableFunction,
      isSelectedFunction,
      singleClickFunction,
      doubleClickFunction,
      KDMenuItemAlign::Center));
  } // End of previewMenuItems.

  end();
} // MenuBox::MenuBox(...)

void
MenuBox::pageUp()
{
  static const int numberOfScenes = guiController_->numberOfScenes();

  // The ceiling:
  static const int numberOfPages =
    (numberOfScenes + numberOfPreviewItems_ - 1) / numberOfPreviewItems_;
  ++pageNumber_;
  pageNumber_ %= numberOfPages;

  redraw();
  /*
  if( numberOfPages)
  {
    ++pageNumber_;
    this->redraw();
  };
  */
};

void
MenuBox::pageDown()
{
  const int numberOfScenes = guiController_->numberOfScenes();

  // The ceiling:
  const int numberOfPages =
    (numberOfScenes + numberOfPreviewItems_ - 1) / numberOfPreviewItems_;

  pageNumber_ += numberOfPages - 1;
  pageNumber_ %= numberOfPages;
  redraw();

  /*
  if(0 <= pageNumber_ - 1)
  {
    --pageNumber_;
    this->redraw();
  };
  */
};

void
MenuBox::setCurrentPage()
{
  const int currentScene = guiController_->sceneNumber();

  pageNumber_ = currentScene / numberOfPreviewItems_;
}

void
MenuBox::incrementMenuSelection()
{
  // If not in preview mode, set preview mode and to current scene and page.
  if (guiController_->menuMode() != MenuMode::Preview) {
    guiController_->changeMenuMode(MenuMode::Preview,
                                   guiController_->sceneNumber());
    setCurrentPage();
    return;
  }

  const int currentSceneNumber = guiController_->sceneNumber();
  const int numberOfScenes = guiController_->numberOfScenes();
  if (currentSceneNumber < numberOfScenes - 1) {
    guiController_->changeMenuMode(MenuMode::Preview, currentSceneNumber + 1);
  } else {
    guiController_->changeMenuMode(MenuMode::Preview, 0);
  }
  setCurrentPage();
}

void
MenuBox::decrementMenuSelection()
{
  // If not in preview mode, set preview mode and to current scene and page.
  if (guiController_->menuMode() != MenuMode::Preview) {
    guiController_->changeMenuMode(MenuMode::Preview,
                                   guiController_->sceneNumber());
    setCurrentPage();
    return;
  }

  const int currentSceneNumber = guiController_->sceneNumber();
  const int numberOfScenes = guiController_->numberOfScenes();
  if (currentSceneNumber >= 1) {
    guiController_->changeMenuMode(MenuMode::Preview, currentSceneNumber - 1);
  } else {
    guiController_->changeMenuMode(MenuMode::Preview, numberOfScenes - 1);
  }
  setCurrentPage();
}

std::vector<scrn_t>
MenuBox::computeVertices(Box box, int numberOfMenuItems)
{
  std::vector<scrn_t> vertices;
  // Just to make sure the first vertex lines up with the top, exactly
  vertices.push_back(box.y);

  for (int I = 1; I < numberOfMenuItems; ++I) {
    // Spread the rounding error out!
    double multiplier = static_cast<double>(I) / numberOfMenuItems;
    scrn_t vertex = static_cast<scrn_t>(multiplier * box.h + box.x);
    vertices.push_back(vertex);
  }

  // Just to be sure the last vertex lines up exactly with the bottom of the
  // box.
  vertices.push_back(box.y + box.h);

  return vertices;
};

Box
MenuBox::computeMenuItemBox(Box box, int i, int total)
{
  static const int numberOfMenuItems = total;

  static std::vector<scrn_t> vertices = computeVertices(box, numberOfMenuItems);

  using sz_t = decltype(vertices)::size_type;

  Box menuItemBox = {
    box.x,
    vertices[static_cast<sz_t>(i)],
    box.w,
    vertices[static_cast<sz_t>(i + 1)] -
      vertices[static_cast<sz_t>(i)] // These vary slightly with rounding.
  };

  return menuItemBox;
}

} // namespace Klein
