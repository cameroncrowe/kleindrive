//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuBox.h - class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MENUBOX_H
#define KLEINDRIVE_MENUBOX_H

#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuItem.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <memory>
#include <vector>

namespace kleindrive {

/// The box with the menu butons.
class MenuBox final : public Fl_Group
{
public:
  /// Constructs the menu box within the given box.
  MenuBox(Box box, GuiController* guiController);

  /// Requests a left page turn.
  void pageDown();

  /// Requests a right page turn.
  void pageUp();

  /// Requests incrementation of the menu selection.
  void incrementMenuSelection();

  /// Requests decrementation of the menu selection.
  void decrementMenuSelection();

private:

  // Counts of menu buttons.

  /// Number of preview items
  static const int numberOfPreviewItems_{ 12 };
 
  /// NumberOf Main Menu Items.
  static const int numberOfMainMenuItems_{ 5 };

  /// Number of blank menu items.
  static const int numberOfBlanks_{ 3 };

  /// Number of arrow buttons.
  static const int numberOfArrowButtons_{ 2 };

  /// Computes the position of the boxes for menu buttons.
  std::vector<scrn_t> computeVertices(Box box, int numberOfMenuItems);

  /// Returns the menu box for the n-th menu item.
  Box computeMenuItemBox(Box box, int i, int total);

  /// Sets the current page number to be the page number of the current scene.
  void setCurrentPage();

  /// Address of the GuiController.
  GuiController* guiController_;

  /// Current page number.
  int pageNumber_{ 0 };

  // Main menu items.
  /// Welcome button
  std::unique_ptr<MenuItem> welcome_;

  /// Info button
  std::unique_ptr<MenuItem> info_;

  /// About button
  std::unique_ptr<MenuItem> about_;

  /// Go! button.
  std::unique_ptr<MenuItem> go_;

  /// Exit button.
  std::unique_ptr<MenuItem> exit_;

  // Blank butons--a quick and dirty solution.
  std::unique_ptr<MenuItem> blank0_;
  std::unique_ptr<MenuItem> blank1_;
  std::unique_ptr<MenuItem> blank2_;

  /// The scene preview buttons.
  std::vector<std::unique_ptr<MenuItem>> previews_;

  /// The page left and right buttons.
  std::unique_ptr<MenuItem> pageLeft_;
  std::unique_ptr<MenuItem> pageRight_;
};

} // namespace Klein

#endif // KLEINDRIVE_MENUBOX_H
