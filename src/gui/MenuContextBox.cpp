//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuContextBox.cpp - MenuContextBox class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the MenuContextBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/MenuContextBox.h"
#include "src/gui/AboutBox.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/InfoBox.h"
#include "src/gui/PreviewBox.h"
#include "src/gui/WelcomeBox.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <memory>

namespace kleindrive {

MenuContextBox::MenuContextBox(Box box, GuiController* guiController)
  : Fl_Group(box.x, box.y, box.w, box.h)
  , guiController_{ guiController }
  , welcomeBox_{ std::make_unique<WelcomeBox>(box, guiController) }
  , aboutBox_{ std::make_unique<AboutBox>(box, guiController) }
  , infoBox_{ std::make_unique<InfoBox>(box, guiController) }
  , previewBox_{ std::make_unique<PreviewBox>(box, guiController) }
{
  end();

  aboutBox_->hide();
  infoBox_->hide();
  previewBox_->hide();

  guiController_->menuContextBox(this);
}

void
MenuContextBox::changeMode()
{
  welcomeBox_->hide();
  aboutBox_->hide();
  infoBox_->hide();
  previewBox_->hide();

  switch (guiController_->menuMode()) {
    case MenuMode::Welcome:
      welcomeBox_->show();
      return;

    case MenuMode::Info:
      infoBox_->show();
      return;

    case MenuMode::About:
      aboutBox_->show();
      return;

    case MenuMode::Preview:
      previewBox_->show();
      return;
  }
}

} // namespace Klein
