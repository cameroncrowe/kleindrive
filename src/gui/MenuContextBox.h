//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuContextBox.h - MenuContextBox class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the MenuContextBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MENUCONTEXTBOX_H
#define KLEINDRIVE_MENUCONTEXTBOX_H

#include "src/gui/AboutBox.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/InfoBox.h"
#include "src/gui/PreviewBox.h"
#include "src/gui/WelcomeBox.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <memory>

namespace kleindrive {

/// Displays a box based on the MenuMode.
class MenuContextBox final : public Fl_Group
{
public:
  /// Constructs the menu contexdt box within the given box.
  MenuContextBox(Box box, GuiController* guiController);

  /// Request a change of mode.
  void changeMode();

private:
  /// Address of the GuiController.
  GuiController* guiController_;

  /// Welcome box.
  std::unique_ptr<WelcomeBox> welcomeBox_;

  /// About box.
  std::unique_ptr<AboutBox> aboutBox_;
  
  /// Istructions box.
  std::unique_ptr<InfoBox> infoBox_;

  /// Scene preview box.
  std::unique_ptr<PreviewBox> previewBox_;
};

} // namespace Klein

#endif // KLEINDRIVE_MENUCONTEXTBOX_H
