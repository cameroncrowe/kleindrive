//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuItem.cpp - MenuItem class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuItem class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/MenuItem.h"
#include "src/collections/Colors.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/graphics/Color.h"
#include "src/gui/Box.h"
#include "src/string/KDString.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

namespace kleindrive {

MenuItem::MenuItem(Box box)
  : Fl_Widget(box.x, box.y, box.w, box.h)
  , box_{ box }
  , labelFunction_{ []() { return ""; } }
  , isShownFunction_{ []() { return false; } }
  , isClickableFunction_{ []() { return false; } }
  , isSelectedFunction_{ []() { return false; } }
  , singleClickFunction_{ []() { return; } }
  , doubleClickFunction_{ []() { return; } }
  , cols_{ std::max((rows_ * w()) / h(), 1) }
  , alignment_{ KDMenuItemAlign::Center }
  , face_{ KDMenuItemFontFace::Normal }
{
  computeLabelVertices();
}

MenuItem::MenuItem(Box box,
                   LabelFunction labelFunction,
                   IsShownFunction isShownFunction,
                   IsClickableFunction isClickableFunction,
                   IsSelectedFunction isSelectedFunction,
                   ClickFunction singleClickFunction,
                   ClickFunction doubleClickFunction,
                   KDMenuItemAlign alignment,
                   KDMenuItemFontFace face)
  : Fl_Widget(box.x, box.y, box.w, box.h)
  , box_{ box }
  , labelFunction_{ labelFunction }
  , isShownFunction_{ isShownFunction }
  , isClickableFunction_{ isClickableFunction }
  , isSelectedFunction_{ isSelectedFunction }
  , singleClickFunction_{ singleClickFunction }
  , doubleClickFunction_{ doubleClickFunction }
  , cols_{ std::max((rows_ * w()) / h(), 1) }
  , alignment_{ alignment }
  , face_{ face }
{
  computeLabelVertices();
}

int
MenuItem::handle(int event)
{
  switch (event) {
    case FL_ENTER:
      isMouseOver_ = true;
      redraw();
      return 1;

    case FL_LEAVE:
      isMouseOver_ = false;
      redraw();
      return 1;

    case FL_PUSH:
      isBeingPressed_ = true;
      xMousePush_ = Fl::event_x(); // MousePush position.
      yMousePush_ = Fl::event_y();
      redraw();
      return 1;

    case FL_DRAG:
      // Update MousePush position, isMouseOver_, and redraw if isMouseOver
      // changes.
      {
        xMousePush_ = Fl::event_x();
        yMousePush_ = Fl::event_y();

        const int x = xMousePush_;
        const int y = yMousePush_;

        bool wasMouseOver = isMouseOver_;

        isMouseOver_ = (box_.x <= x) && (x < box_.x + box_.w) &&
                       (box_.y <= y) && (y < box_.y + box_.h);

        if (wasMouseOver != isMouseOver_) {
          redraw();
        }
      }
      return 1;

    case FL_RELEASE:
      isBeingPressed_ = false;
      if (isMouseOver_) {

        if (isSelectedFunction_() || Fl::event_clicks()) {
          doubleClickFunction_();
        }
        singleClickFunction_();
        // isSelected_ = true;
        redraw();
      }
      return 1;
    default:
      return 0;
  }
}

void
MenuItem::draw()
{
  // ----------------
  // Draw background.
  // ----------------

  setFlColor(colors::Green);
  fl_rectf(x(), y(), w(), h());

  // -------------
  // Empty button.
  // -------------
  if (!isShownFunction_()) {
    return;
  };

  // -----------------------------------
  // Shown, but doesn't respond to mouse
  // -----------------------------------
  if (isShownFunction_() && !isClickableFunction_()) {
    setFlColor(colors::White);
    drawLabel();
    return;
  }

  // Can't trust isMouseOver For drawing, but can't just check, either.
  if (isMouseOver_) {
    // Double check.  There is a betetr way to do this, perhaps.
    const int x = Fl::event_x();
    const int y = Fl::event_y();

    isMouseOver_ = (box_.x <= x) && (x < box_.x + box_.w) && (box_.y <= y) &&
                   (y < box_.y + box_.h);
  }

  // --------------------------------------------
  // Mouse over and being pressed and is selected.
  // ---------------------------------------------
  if (isMouseOver_ && isBeingPressed_) {
    setFlColor(colors::Purple);
    drawLabel();

    return;
  }
  // ----------------------------
  // Mouse over and being pressed
  // ----------------------------
  if (isMouseOver_ && isBeingPressed_) {
    setFlColor(colors::Yellow);
    drawLabel();

    return;
  }

  // -------------------------------------
  // Mouse hovering and button is selected.
  // -------------------------------------
  if (isMouseOver_ && isSelectedFunction_()) {
    setFlColor(colors::Yellow);
    drawLabel();

    return;
  }

  // --------------------------------
  // Mouse hovering and not selected.
  // --------------------------------

  if (isMouseOver_) {

    static const Color color = colors::HardRed;
    setFlColor(color);
    drawLabel();
    return;
  }

  // ---------------------------------
  // Mouse Pressed and Is Selectected.
  // ---------------------------------
  if (isSelectedFunction_() && isBeingPressed_) {
    setFlColor(colors::WarmWhite);
    drawLabel();

    return;
  };

  // ----------------
  // Selected button.
  // ----------------
  if (isSelectedFunction_()) {
    setFlColor(colors::Green);
    fl_rectf(x(), y(), w(), h());

    setFlColor(colors::Red);
    drawLabel();
    return;
  }

  // ------------------
  // Unselected Button.
  // ------------------
  setFlColor(colors::Green);
  fl_rectf(x(), y(), w(), h());

  setFlColor(colors::White);
  drawLabel();
};

// Compute the vertices of dividing the segment [a,b] into n pieces
std::vector<scrn_t>
MenuItem::divideEvenly(scrn_t a, scrn_t b, int n)
{
  std::vector<scrn_t> vertices;

  // Make sure the first vertex lines up perfectly.
  vertices.push_back(a);

  // Spread the rounding error out!
  for (int i = 1; i < n; ++i) {
    const double multiplier = static_cast<double>(i) / n;
    const scrn_t vertex = static_cast<scrn_t>(multiplier * (b - a) + a);
    vertices.push_back(vertex);
  }
  // Make sure the last vertex lines up perfectly.
  vertices.push_back(b);
  return vertices;
}

void
MenuItem::computeLabelVertices()
{
  // TODO Move some of this data into the class definition, or to be fed into
  // the constructor.

  const int extraRows =
    (face_ == KDMenuItemFontFace::Bold || face_ == KDMenuItemFontFace::Tall)
      ? 1
      : 0;

  const int extraCols =
    (face_ == KDMenuItemFontFace::Bold || face_ == KDMenuItemFontFace::Wide)
      ? 1
      : 0;

  rows_ += extraRows;
  cols_ += extraCols;

  horizontalVertices_ = divideEvenly(x(), x() + w(), cols_);
  verticalVertices_ = divideEvenly(y(), y() + h(), rows_);
}

void
MenuItem::drawLabel()
{
  // We draw the font so it expands to the cell's height, but doesn't draw
  // nicely ont he pixel boundaries.  This makes it weird.  BUT if every box
  // is the same  size, thye're all the same flavor of weird. Voiala.

  const int kerning =
    (face_ == KDMenuItemFontFace::Bold || face_ == KDMenuItemFontFace::Wide)
      ? 2
      : 1;
  auto font = fonts::code8x8ishFontSpecial(kerning);
  std::string label = labelFunction_();
  KDString kdLabel(font, label);

  // Compute horizontal offset.
  scrn_t horizontalOffset;
  {
    const double averageWidthOfCol = static_cast<double>(w()) / cols_;
    const double horizontalExtra =
      w() - kdLabel.numberOfColumns() * averageWidthOfCol;

    horizontalOffset = 0;

    if (alignment_ == KDMenuItemAlign::Center)
      horizontalOffset = static_cast<scrn_t>(horizontalExtra / 2);
  }

  // Compute vertical Offset.
  scrn_t verticalOffset;
  {
    const double averageHeightOfCol = static_cast<double>(h()) / rows_;
    scrn_t verticalExtra =
      h() - static_cast<scrn_t>(kdLabel.numberOfRows() * averageHeightOfCol);
    verticalOffset = verticalExtra / 2;
  }

  using sz_t = std::vector<scrn_t>::size_type;

  // This should probably be scrn_t, but I'm lazy.
  const int horizontalPoint =
    (face_ == KDMenuItemFontFace::Bold || face_ == KDMenuItemFontFace::Wide)
      ? 2
      : 1;

  const int verticalPoint =
    (face_ == KDMenuItemFontFace::Bold || face_ == KDMenuItemFontFace::Tall)
      ? 2
      : 1;

  for (int col = 0; col < cols_; ++col) {
    for (int row = 0; row < rows_; ++row) {
      if (kdLabel.characteristicFunction(row, col)) {
        const scrn_t x =
          horizontalVertices_[static_cast<sz_t>(col)] + horizontalOffset;
        const scrn_t y =
          verticalVertices_[static_cast<sz_t>(row)] + verticalOffset;
        const scrn_t w =
          horizontalVertices_[static_cast<sz_t>(col + horizontalPoint)] -
          horizontalVertices_[static_cast<sz_t>(col)];
        const scrn_t h =
          verticalVertices_[static_cast<sz_t>(row + verticalPoint)] -
          verticalVertices_[static_cast<sz_t>(row)];
        fl_rectf(x, y, w, h);
      }
    }
  }
}

} // namespace Klein
