//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuItem.h - MenuItem class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuItem class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MENUITEM_H
#define KLEINDRIVE_MENUITEM_H

#include "src/graphics/Color.h"
#include "src/gui/Box.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <functional>
#include <string>
#include <vector>

namespace kleindrive {

/// Type of a function providing a label.
using LabelFunction = std::function<std::string(void)>;

/// Type of a function telling wether or not a button is to be shown.
using IsShownFunction = std::function<bool(void)>;

/// Type of a funciton telling wether or not a button can be clicked.
using IsClickableFunction = std::function<bool(void)>;

/// Type of a funtion tellin wether or not a button is selected.
using IsSelectedFunction = std::function<bool(void)>;

/// Type of a function performing a task upon being clicked.
using ClickFunction = std::function<void(void)>;

/// Alignment options for a MenuItem
enum class KDMenuItemAlign
{
  Left,
  Center
};

/// Font faces for a MenuItem
enum class KDMenuItemFontFace
{
  Normal,
  Wide,
  Tall,
  Bold
};

/// MenuItems are buttons with a "Pro-Aliased" font.
///
/// They are drawn on boxes which are rounded to pixel boundaries to make
/// a slightly weird font.
class MenuItem final : public Fl_Widget
{
public:
  /// \name Constructors.
  /// @{

  MenuItem(Box box,
           LabelFunction labelFunction,
           IsShownFunction isShownFunction,
           IsClickableFunction isClickableFunction,
           IsSelectedFunction isSelectedFunction,
           ClickFunction singleClickFunction,
           ClickFunction doubleClickFunction,
           KDMenuItemAlign alignment,
           KDMenuItemFontFace face_ = KDMenuItemFontFace::Normal);

  /// Constructs a blank box.
  explicit MenuItem(Box box);
  /// @}

private:
  /// Handles menu item events.
  virtual int handle(int event) final override;

  /// Draws the menu item in its box.
  virtual void draw() final override;

  /// Draws the label text to the box.
  void drawLabel();

  /// button box.
  Box box_;

  // Functions used to control button drawing.

  /// Labelling function.
  LabelFunction labelFunction_;
  /// Function for deciding if the button text is shown.
  IsShownFunction isShownFunction_;

  /// Function that decides is the button can be clicked.
  IsClickableFunction isClickableFunction_;

  /// Funtion that decides if the button is currently selected.
  IsSelectedFunction isSelectedFunction_;

  /// Function executed when the button is single clicked havin been inactive.
  ClickFunction singleClickFunction_;

  /// Function that is executed when button is double clicked, or active and
  /// single clicked.
  ClickFunction doubleClickFunction_;

  // Data for event handling.
  int xMousePush_{ 0 };
  int yMousePush_{ 0 };

  /// The number of rows to start.  The actual number may increase by one or two to accommodate fonts.
  static const int baseRows_{24};

  /// Rows given for text.
  ///
  /// This may be increased a little to draw bold and other fonts, but only by one or two.
  int rows_ {baseRows_};

  /// Cols given for the text.
  int cols_; // Computed in constructor.

  bool isMouseOver_{ false };
  bool isBeingPressed_{ false };

  /// Inactive color
  Color inactiveColor_;

  /// Active color.
  Color activeColor_;

  // Font data.
  KDMenuItemAlign alignment_;
  KDMenuItemFontFace face_;

  // Compute the lattice of vertices for drawing the label.
  //
  // These have to be recomputed if the font face changes!
  void computeLabelVertices();

  // Evenly spreads lattice vertices over the interval [a,b], breaking it into n
  // subintervals.
  std::vector<scrn_t> divideEvenly(scrn_t a, scrn_t b, int n);

  /// Horizontal lattice points used for text pixels.
  std::vector<scrn_t> horizontalVertices_;

  /// Vertical lattice points used for text pixels.
  std::vector<scrn_t> verticalVertices_;
};

} // namespace Klein

#endif // KLEINDRIVE_MENUITEM_H
