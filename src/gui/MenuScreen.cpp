//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuScreen.cpp - The MenuScreen class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuScreen class implementation file.
///
//===----------------------------------------------------------------------===//

#include "src/gui/MenuScreen.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuBox.h"
#include "src/gui/MenuContextBox.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/fl_draw.H>
#include <memory>

namespace kleindrive {

const double MenuScreen::menuWidthFactor_ = 0.25;

MenuScreen::MenuScreen(Box box, GuiController* guiController)
  : Fl_Group(box.x, box.y, box.w, box.h)
  , guiController_{ guiController }
{
  /*


    IF YOU WANT TO MAKE THE MENU BOX WIDER TO ALLOW FOR LONGER WORDS, SIMPLY ADD
    TO THE NUMBER OF ROWS IN MENUBOX.H

    UNFORTUNATELY, IT'S COUNTERINTUITIVE.  WOULD BE BETTER IF THE MENUITEMS
    TRACKED THE LONGEST WORD AND EXPANDED OR CONTRACTED AS NEEDED, BUT IT'S TIME
    TO WRAP THINGS UP, FOR NOW


  */
  const scrn_t menuWidth = static_cast<scrn_t>(box.w * menuWidthFactor_);

  const Box menuBoxBox = { box.x, box.y, menuWidth, box.h };
  const Box menuContextBoxBox = {
    box.x + menuWidth, box.y, box.w - menuWidth, box.h
  };

  menuContextBox_ =
    std::make_unique<MenuContextBox>(menuContextBoxBox, guiController);
  menuBox_ = std::make_unique<MenuBox>(menuBoxBox, guiController);

  end();
}

int
MenuScreen::handle(int event)
{

  // Pass events to children.
  if (Fl_Group::handle(event))
    return 1;

  // Handle the rest.
  switch (event) {
    case FL_PUSH:
      if (guiController_->menuMode() == MenuMode::Preview) {
        guiController_->changeWindowMode(WindowMode::Drive);
        return 1;
      }
      return 0;
    default:
      return 0;
  }
}

} // namespace Klein
