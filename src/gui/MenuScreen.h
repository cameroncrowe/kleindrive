//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/MenuScreen.h - The MenuScreen class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the MenuScreen class definition file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_MENUSCREEN_H
#define KLEINDRIVE_MENUSCREEN_H

#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/MenuBox.h"
#include "src/gui/MenuContextBox.h"
#include "src/types/ValueTypes.h"
#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/fl_draw.H>
#include <memory>

namespace kleindrive {

/// Displays a menu and the menu context.
class MenuScreen final : public Fl_Group
{
public:
  /// Constructs the menu screen within the given box.
  MenuScreen(Box box, GuiController* guiController);

private:
  /// Part of the screen devoted to the menu.
  static const double menuWidthFactor_;
  
  /// Handles events related tot he menu screen.
  virtual int handle(int event) final override;

  /// Address of the GuiController.
  GuiController* guiController_;

  /// The menu box.
  std::unique_ptr<MenuBox> menuBox_;

  /// The context of the menu box (About, Instructions, Preview, Welcome)
  std::unique_ptr<MenuContextBox> menuContextBox_;

};

} // namespace Klein

#endif // KLEINDRIVE_MENUSCREEN_H
