//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/PreviewBox.cpp - PreviewBox class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the PreviewBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/PreviewBox.h"
#include "src/gui/AnimatedBox.h"

namespace kleindrive {

PreviewBox::PreviewBox(Box box, GuiController* guiController)
  : AnimatedBox(box, guiController)
{
  guiController->previewBox(this);
}

Scene*
PreviewBox::getScene()
{
  return guiController_->previewScene();
}

GameState*
PreviewBox::getGameState()
{
  return guiController_->gameState();
}

} // namespace Klein
