The structure is as follows:

WindowWrapper
  GuiController
    SceneCollection 
    GameState
  Window
    DriveScreen : AnimatedBox
    MenuScreen
      MenuBox
        MenuItem
      MenuContextBox
        WelcomeBox : AnimatedBox        (Logo)
        InfoBox : StaticInformationBox  (Instructions)
        AboutBox : StaticInformationBox

StaticInformationBox
  LabelBox
  TextBox

GuiController is injected into window down and passed down the tree.

Events local to a gui object are handled locally.

Events not local to a gui object are handled in the guiController, along with the GameState and commands to the SceneCollection.



