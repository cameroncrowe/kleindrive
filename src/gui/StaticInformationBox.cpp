//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/StaticInformationBox.cpp
//
//  The StaticInformationbox class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the StaticInformationBox class impementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/StaticInformationBox.h"
#include "src/collections/Colors.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/LabelBox.h"
#include "src/gui/TextBox.h"
#include "src/string/KDString.h"
#include "src/string/KDTextBlock.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <string>

namespace kleindrive {

// Infinity.
int StaticInformationBox::maxPoint_ = 10000;

StaticInformationBox::StaticInformationBox(Box box,
                                           GuiController* guiController,
                                           std::string title,
                                           std::string text)
  : Fl_Widget(box.x, box.y, box.w, box.h)
  , guiController_{ guiController }
  , box_{ box }
{
  const scrn_t titleHeight = static_cast<scrn_t>(box.h * splitFactor_);
  const scrn_t margin = static_cast<scrn_t>(box.h * marginFactor_);

  const Box titleBox = { margin, margin, box.w - 2 * margin, titleHeight };

  // Generate title box = labelBox_;
  {
    const int kerning = 2;
    static const auto font = fonts::code8x8ishFont(kerning);
    const int point = std::max(1, titleBox.h / font.numberOfRows());
    const KDLabelAlignment alignment = KDLabelAlignment::Center;
    const KDLabelFace face = KDLabelFace::Wide;

    labelBox_ = LabelBox(titleBox, title, font, point, alignment, face);
  }

  // Generate information for making text box.
  {
    infoBox_ = { margin,
                 margin + titleHeight + margin,
                 box.w - 2 * margin,
                 box.h - titleHeight - 3 * margin };

    text_ = text;

    // We reduce the maxPoint to a font size that will fit the entire text in
    // the box, but not a point below 1.
    {
      int P;
      for (P = 1; generateTextBox(P).textHeight() <= infoBox_.h &&
                  //	!generateTextBox(P).hasWordThatIsTooWide() &&
                  P <= maxPoint_;
           ++P)
        ; // Do nothing.
      if (P == 1) {
        // std::cout << "Window too small to show all text in smallest size
        // font.\n";
      }
      maxPoint_ = (P == 1) ? 1 : P - 1;
    }
  }
}

TextBox
StaticInformationBox::generateTextBox(int point)
{
  static const int kerning = 1;
  static const auto font = fonts::code8x8ishFontSpecial(kerning);
  static const int leading = 3;

  return TextBox(infoBox_, text_, font, point, leading);
}

void
StaticInformationBox::drawToOffscreenBuffer()
{
  TextBox textBox = generateTextBox(maxPoint_);

 offScreenBuffer_ = fl_create_offscreen(w(), h());
 fl_begin_offscreen(offScreenBuffer_);
  {
    setFlColor(colors::Green);

    fl_rectf(0, 0, box_.w, box_.h);
    fl_rectf(box_.x, box_.y, box_.w, box_.h);

    setFlColor(colors::DarkRed);
    textBox.draw();
    labelBox_.draw();
  }
 fl_end_offscreen();
}

void
StaticInformationBox::draw()
{
  // In order not to segfault sporadically, we have to:
  //   A) wait until the main window is drawn to get an offscreen buffer on X11.
  //   B) Initialize the offscreen buffer to zero.
  if (!offScreenBuffer_) {
    drawToOffscreenBuffer();
  }
 fl_copy_offscreen(box_.x, box_.y, box_.w, box_.h, offScreenBuffer_, 0, 0);
}

} // namespace kleindrive
