//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/StaticInformationBox.h - The StaticInformationBox class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the StaticInformationBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_STATICINFORMATIONBOX_H
#define KLEINDRIVE_STATICINFORMATIONBOX_H

#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/LabelBox.h"
#include "src/gui/TextBox.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <string>

namespace kleindrive {

/// Displays Static information in the largest font that works on all such
/// pages.
class StaticInformationBox : public Fl_Widget
{
public:
  /// Constructs a box of static information within the given box, with the
  /// given title and text.  All such boxes will have the same font size, which
  /// is the maximal.  If the smallest size is too small, it uses the smalllest
  /// size anyway, and lets the text hang off the bottom.
  StaticInformationBox(Box box,
                       GuiController* guiController,
                       std::string title,
                       std::string text);

  /// Draws to the bounding box.
  virtual void draw() final override;

private:
  /// Largest point text that fits... so far.  Starts at infinity.
  static int maxPoint_;

  /// Part of screen that is title.
  static constexpr double splitFactor_{0.05};

  /// Part of box that is margin.
  static constexpr double marginFactor_{0.02};

  /// draws the text to an offscreen buffer.
  void drawToOffscreenBuffer();

  /// Generate the TextBox with the given size text.
  TextBox generateTextBox(int point);

  /// This is used, but the compiler thinks it isn't.
  [[maybe_unused]] GuiController* guiController_;

  /// The box.
  Box box_;

  /// LabelBox.
  LabelBox labelBox_;

  /// Text to display.
  std::string text_;
  
  /// Box to display the text.
  Box infoBox_;

  // There is some problem with drawing offfscreen.
  // In X11, we can only do it once the main window has a context.  In the
  // commented code, we would have only drawn to an offscreen buffer when called
  // by the Static box's draw function, which is presumably only called once the
  // main class has context. On OSX, we still can't call it.  Strange.
  /// Cache for the page, since it is slow to draw.
  Fl_Offscreen offScreenBuffer_{0};
};

} // namespace Klein

#endif // KLEINDRIVE_STATICINFORMATIONBOX_H
