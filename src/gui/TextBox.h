//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/TextBox.h
//
//  The TextBox template class definition (and implementation).
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the TextBox tempate class definition (and
/// implementation).
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_TEXTBOX_H
#define KLEINDRIVE_TEXTBOX_H

#include "src/collections/Colors.h"
#include "src/collections/fonts/KleinDriveFonts.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"
#include "src/gui/StaticInformationBox.h"
#include "src/string/KDFont.h"
#include "src/string/KDString.h"
#include "src/string/KDTextBlock.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <string>

namespace kleindrive {

/// This is essentially a KDTextBox, but wrapped with the ability to draw
/// itself.
class TextBox
{
public:
  /// \n Constructors.
  /// @{

  /// Constructor needs to know the number of rows--it wasn't designed for this.
  template<int NUMBEROFROWS>
  inline TextBox(Box box,
                 const std::string& text,
                 KDFont<NUMBEROFROWS> font,
                 int point,
                 int leading);

  /// Default constructor
  inline TextBox();

  /// @}

  /// Returns the actual height of the text in scrn_t.
  inline int textHeight() const;

  inline bool hasWordThatIsTooWide() const;

  /// Draws the text box.  This is SLOW, so cache it offscreen.
  inline void draw();

private:
  /// The box for the text.
  Box box_;

  /// Number of rows needed for the text.
  int numberOfRows_;

  /// Number of columns needed for the text.
  int numberOfColumns_;

  /// The KDTextBlock that formats the string into rows.
  KDTextBlock textBlock_;

  /// The size of the text.  Each row and column gets point_ pixels.
  int point_;
};

template<int NUMBEROFROWS>
inline TextBox::TextBox(Box box,
                        const std::string& text,
                        KDFont<NUMBEROFROWS> font,
                        int point,
                        int leading)
  : box_{ box }
  , numberOfRows_{ box.h / std::max(1, point) }
  , numberOfColumns_{ box.w / std::max(1, point) }
  , textBlock_{ KDTextBlock(font, numberOfColumns_, leading, text) }
  , point_{ point }
{}

inline TextBox::TextBox()
  : box_{ 0, 0, 0, 0 }
  , numberOfRows_{ 0 }
  , numberOfColumns_{ 0 }
  , textBlock_{ KDFont<1>(1), 1, 0, "" }
  , point_{ 0 }
{}

inline int
TextBox::textHeight() const
{
  return point_ * textBlock_.numberOfRows();
}

inline bool
TextBox::hasWordThatIsTooWide() const
{
  return textBlock_.hasWordThatIsTooWide();
}

inline void
TextBox::draw()
{
  for (int R = 0; R < numberOfRows_; ++R)
    for (int C = 0; C < numberOfColumns_; ++C)
      if (textBlock_.characteristicFunction(R, C) == 1) {
        const int x = box_.x + point_ * C;
        const int y = box_.y + point_ * R;

        fl_rectf(x, y, point_, point_);
      }
}

} // namespace kleindrive

#endif // KLEINDRIVE_TEXTBOX_H
