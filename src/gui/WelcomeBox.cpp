//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/WelcomeBox.cpp - WelcomeBox class implementation file..
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the WelcomeBox class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/gui/WelcomeBox.h"
#include "src/gui/AnimatedBox.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"

namespace kleindrive {

WelcomeBox::WelcomeBox(Box box, GuiController* guiController)
  : AnimatedBox(box, guiController){};

Scene*
WelcomeBox::getScene()
{
  return guiController_->welcomeScene();
}

GameState*
WelcomeBox::getGameState()
{
  return guiController_->gameState();
}

} // namespace Klein
