//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/WelcomeBox.h - WelcomeBox class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the WelcomeBox class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_WELCOMEBOX_H
#define KLEINDRIVE_WELCOMEBOX_H

#include "src/gui/AnimatedBox.h"
#include "src/gui/Box.h"
#include "src/gui/GuiController.h"

namespace kleindrive {

/// Shows a trefloil knot with logo, credit and copyright information.
class WelcomeBox : public AnimatedBox
{
public:
  /// Constructs the welcome scene box.
  WelcomeBox(Box box, GuiController* guiController);

private:
  /// Returns the welcome scene.
  virtual Scene* getScene() final override;

  /// Returns the game state.
  virtual GameState* getGameState() final override;
};

} // namespace Klein

#endif // KLEINDRIVE_WELCOMEBOX_H
