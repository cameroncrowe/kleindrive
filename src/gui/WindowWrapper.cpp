//==-------------------------------------------------------------*- C++ -*-===//
//
//  windowwrapper/WindowWrapper.cpp - WindowWrapper class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the WindowWrapper class implementation file.
///
//===----------------------------------------------------------------------===//

#include "src/gui/WindowWrapper.h"
#include "src/gui/Box.h"
#include "src/gui/KDWindow.h"
#include "src/scenecollection/SceneCollection.h"
#include <memory>

namespace kleindrive {

WindowWrapper::WindowWrapper(std::unique_ptr<SceneCollection>& sceneCollection)
{
  // We want to build the window full screen, but it may not work.  So we
  // we build a window the size of the screen, and then attempt to make it
  // fullscreen.

  Box box;

  // Sets x, y, w, and h to max window size.
  Fl::screen_xywh(box.x, box.y, box.w, box.h);

  // box.x = 0; box.y=0; box.w = 800, box.h = 600;

  guiController_ = std::make_unique<GuiController>(sceneCollection);

  char label[80] = "Klein Drive Alpha";

  window_ = std::make_unique<KDWindow>(box, label, guiController_.get());

  // We attempt to make it fullscreen, but this may fail, leaving us with a
  // window of maxiumus size, x,y,w,h.
  window_->fullscreen();

  // Now we tune its settings.

  Fl::visual(FL_DOUBLE | FL_RGB); // Removes dithering?  TODO: Test this out.
  Fl::scheme("plastic");
  Fl::visible_focus(0); // Set the window to focus when FLTK runs.
  window_->show();      // Set the window to be shown when FLTK runs.
}

void
WindowWrapper::setInitialState()
{
  Fl::get_mouse(guiController_->gameState()->mouse.x,
                guiController_->gameState()->mouse.y);
}

int
WindowWrapper::run()
{
  setInitialState();
  return Fl::run();
}

} // namespace kleindrive
