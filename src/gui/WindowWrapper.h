//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/gui/WindowWrapper.h - WindowWrapper class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the WindowWrapper class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_WINDOWWRAPPER_H
#define KLEINDRIVE_WINDOWWRAPPER_H

#include "src/gui/KDWindow.h"
#include "src/scenecollection/SceneCollection.h"
#include <memory>

namespace kleindrive {

/// Creates and manaages the lifetime of a window.
class WindowWrapper
{
public:
  /// Construct a window of maximal size from a given Scene.
  explicit WindowWrapper(std::unique_ptr<SceneCollection>& sceneCollection);

  /// Launches the window, returns the number of errors.
  int run();

private:
  /// The owner of the GuiController.
  std::unique_ptr<GuiController> guiController_;

  /// The owner of the window.
  std::unique_ptr<KDWindow> window_;

  /// Dials in the initial State.
  void setInitialState();
};

} // namespace kleindrive

#endif // KLEINDRIVE_WINDOWWRAPPER_H
