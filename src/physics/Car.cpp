//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/physics/Car.h - Car class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Car class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/physics/Car.h"
#include "src/gamestate/GameState.h"
#include "src/surface/TiledSurface.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <iostream> 

namespace kleindrive {

Car::Car(const Car::Specs& specs)
  : specs_{ specs }
{
  setInitialConditions();
}

void
Car::turnOff()
{
  isTurnedOff_ = true;
}

void
Car::reset()
{
  setInitialConditions();
  turnOff();
}

void
Car::setInitialConditions(phys_t time)
{
  // Set initial state.
  state_.position = specs_.initialPosition;
  state_.heading = specs_.initialHeading;

  state_.forwardVelocity = 0;
  state_.angularVelocity = 0;
  state_.strafeVelocity = 0;

  // These are meaningless, but we set them to reasonable values just in case
  // something weird happens.
  state_.targetForwardVelocity = 0;
  state_.targetAngularVelocity = 0;
  state_.targetStrafeVelocity = 0;

  state_.time = time;
}

void
Car::updateCarState(GameState* gameState)
{
  // If we haven't updated in an entire second, then probably there is a
  // problem, and we should turn the car off.  This avoids having to wait around
  // while the car updates itself a billion times.
  if (gameState->time - state_.time > 1) {
    turnOff();
  }

  // If car is turned off, it has been idle, so need to reset the time and
  // iterate.
  if (isTurnedOff_) {
    isTurnedOff_ = false;
    state_.time = gameState->time;
  }

  // Convert mouse coordinates into a point
  //
  //   x in [-1,1]  for turning, and
  // First we performa an affine change:

  // Mouse position relative to the animated box. Note this could hang way out
  // over the edges of the box.
  const phys_t X =
    static_cast<phys_t>(gameState->mouse.x - gameState->animatedBox.x);

  const phys_t W = static_cast<phys_t>(gameState->animatedBox.w);

  static const phys_t x_max = 1;
  static const phys_t x_min = -1;
  static const phys_t x_range = x_max - x_min;

  const phys_t xRaw = -(x_range * X / W + x_min);

  // Massage that affine change to something that feels good.
  const phys_t xMassaged = xRaw * pow(std::abs(xRaw), 1);

  // Chop small values to zero.
  const phys_t xChopped = (std::abs(xMassaged) > 0.02) ? xMassaged : 0;

  const phys_t angularVelocityScaling = xChopped;

  const phys_t speedMultiplierBase = 2;

  const phys_t speedMultiplierPower =
    (gameState->car.isBraking ? 0 : -1) + (gameState->car.isGoingFast ? 1 : 0);

  const phys_t speedMultiplier = pow(speedMultiplierBase, speedMultiplierPower);

  const phys_t forwardVelocityScaling =
    ((gameState->car.isGoingForward) ? speedMultiplier : 0) +
    ((gameState->car.isGoingBackward) ? -speedMultiplier : 0);

  const phys_t strafeVelocityScaling =
    ((gameState->car.isSlidingLeft) ? speedMultiplier : 0) +
    ((gameState->car.isSlidingRight) ? -speedMultiplier : 0);

  // Set the input forward, angular and strafe velocities.
  state_.targetForwardVelocity =
    forwardVelocityScaling * specs_.normalForwardVelocity;

  state_.targetStrafeVelocity =
    strafeVelocityScaling * specs_.normalForwardVelocity;

  state_.targetAngularVelocity = angularVelocityScaling * maxAngularVelocity_;

  // If we're breaking, don't move (just look around?)
  // if (gameState->car.isBraking) {
  //  state_.targetForwardVelocity = 0;
  //  state_.targetStrafeVelocity = 0;
  //}
  // If we're geodesic, don't turn, and don't turn.
  if (gameState->car.isGeodesic) {
    state_.targetAngularVelocity = 0;
  }
}

// Drive the car forward in small steps until it lags behind less than the
// timeStep.  Then step a ghost car forward for remaining lag time and return
// its state.
Car::State
Car::driveCar(const TiledSurface& tiledSurface, GameState* gameState)
{
  // Update the car state.
  updateCarState(gameState);

  // The lag is how far behind the game time the car is.
  phys_t lag = gameState->time - state_.time;

  // Move the car forward by timeStep_ until we would be driving into
  // the fugure.
  while (lag > timeStep_) {
    state_ = stepCar(tiledSurface, timeStep_);
    lag -= timeStep_;
  }

  // Drive the a ghostCar forward for the remaining tiny lag time to get the
  // approximate position of the car at the desired time. Do NOT update the
  // car itself.
  return stepCar(tiledSurface, lag);
}

// Step a car forward for given time.  Because this is called so often, it
// should presumably be inlined.
inline Car::State
Car::stepCar(const TiledSurface& tiledSurface, phys_t timeStep)
{
  // We update the car into a new state.
  Car::State newState = state_;

  // Accellerate to target velocities proportionally to distance from target,
  // that is:
  //
  //   Y' = - Const * (Y-Y_target)
  //
  // You are free to change this to a critically damped spring if you
  // want.  I like the feel of this non-Hookian spring.
  {
    newState.forwardVelocity +=
      forwardSpringConstant_ * timeStep *
      (newState.targetForwardVelocity - newState.forwardVelocity);

    newState.angularVelocity +=
      angularSpringConstant_ * timeStep *
      (newState.targetAngularVelocity - newState.angularVelocity);

    newState.strafeVelocity +=
      angularSpringConstant_ * timeStep *
      (newState.targetStrafeVelocity - newState.strafeVelocity);
  }

  // Compute the differential geometry at our current time/position (in the
  // domain).
  const phys_t u = newState.position(0);
  const phys_t v = newState.position(1);
  const phys_t t = newState.time;

  const Mat22 C_u =
    tiledSurface.C_u(u, v, t); // TODO check if these are supposed to
  const Mat22 C_v = tiledSurface.C_v(u, v, t); // be negative or positive.

  const Mat22 g = tiledSurface.g(u, v, t);
  const Mat22 g_inv = g.inverse();

  // Construct a 2d ON frame in the local metric for our car, and get the
  // rotated forward normal.
  //
  // Recall that the norm is given by the pullback inner product:
  //
  //   <u|v> = u.transpose() * g * v
  //
  // so
  //
  //   Norm(u) = sqrt(<u|u>)
  //
  const Vec2 forward(cos(newState.heading), sin(newState.heading));

  const Vec2 forwardNormal = forward / sqrt(forward.transpose() * g * forward);

  // Compute the right normal vector in the pullback metric.
  Mat22 rotation;
  rotation << 0, -1, 1, 0;

  const Vec2 right = g_inv * rotation * forwardNormal;
  const Vec2 rightNormal = right / sqrt(right.transpose() * g * right);

  // Steer the car a bit.
  const phys_t angle = newState.angularVelocity * timeStep;

  Mat22 steeringRot;
  steeringRot << cos(angle), -sin(angle), sin(angle), cos(angle);

  Mat22 ONBasis;
  ONBasis.col(0) = forwardNormal;
  ONBasis.col(1) = rightNormal;

  const Mat22 RotONBasis = ONBasis * steeringRot;

  const Vec2 rotatedForwardNormal = RotONBasis.col(0);
  const Vec2 rotatedRightNormal = RotONBasis.col(1);

  // The (rotated) velocity is the (rotated) forwardVelocity combined with the
  // (rotated) strafing velocity.  Move the car a little along the velocity.

  const Vec2 rotatedForwardVelocity =
    rotatedForwardNormal * newState.forwardVelocity;
  const Vec2 rotatedStrafeVelocity =
    rotatedRightNormal * newState.strafeVelocity;

  const Vec2 rotatedVelocity = rotatedForwardVelocity + rotatedStrafeVelocity;

  newState.position += timeStep * rotatedVelocity;

  // Tranport the forward normal along the velocity, and compute the new
  // heading.
  const Vec2 dRotatedForwardNormalByDt(
    -rotatedVelocity.transpose() * C_u * rotatedForwardNormal,
    -rotatedVelocity.transpose() * C_v * rotatedForwardNormal);

  const Vec2 newForwardNormal =
    rotatedForwardNormal + timeStep * dRotatedForwardNormalByDt;

  newState.heading = atan2(newForwardNormal(1), newForwardNormal(0));

  // Make sure the car doesn't go out of bounds.  It's okay to go over if
  // opposite edges are glued.

  // We assume the functions are periodic in both directions.

  const Gluing uDirection = tiledSurface.uDirection();
  const Gluing vDirection = tiledSurface.vDirection();

  const phys_t uMin = tiledSurface.uMin();
  const phys_t uMax = tiledSurface.uMax();

  const phys_t vMin = tiledSurface.vMin();
  const phys_t vMax = tiledSurface.vMax();
  
  const phys_t minDist = specs_.minDistanceFromEdge;

  // Stay minDist away from unglued edges.
  // Can go right up to glued uedge.
  // Can go over glued edges.
  switch (uDirection) {
    case Gluing::NotGlued:
      if (newState.position(0) < uMin + minDist)
        newState.position(0) = uMin + minDist;
      if (newState.position(0) > uMax - minDist)
        newState.position(0) = uMax - minDist;
      break;
    case Gluing::MinCrushedToPoint:
      if (newState.position(0) < uMin)
        newState.position(0) = uMin;
      if (newState.position(0) > uMax - minDist)
        newState.position(0) = uMax - minDist;
      break;
    case Gluing::MaxCrushedToPoint:
      if (newState.position(0) < uMin + minDist)
        newState.position(0) = uMin + minDist;
      if (newState.position(0) > uMax)
        newState.position(0) = uMax;
      break;
    case Gluing::MinAndMaxCrushedToPoint:
      if (newState.position(0) < uMin)
        newState.position(0) = uMin;
      if (newState.position(0) > uMax)
        newState.position(0) = uMax;
      break;
    default: // Glued
      break;
  }

  switch (vDirection) {
    case Gluing::NotGlued:
      if (newState.position(1) < vMin + minDist)
        newState.position(1) = vMin + minDist;
      if (newState.position(1) > vMax - minDist)
        newState.position(1) = vMax - minDist;
      break;
    case Gluing::MinCrushedToPoint:
      if (newState.position(1) < vMin)
        newState.position(1) = vMin;
      if (newState.position(1) > vMax - minDist)
        newState.position(1) = vMax - minDist;
      break;
    case Gluing::MaxCrushedToPoint:
      if (newState.position(1) < vMin + minDist)
        newState.position(1) = vMin + minDist;
      if (newState.position(1) > vMax)
        newState.position(1) = vMax;
      break;
    case Gluing::MinAndMaxCrushedToPoint:
      if (newState.position(1) < vMin)
        newState.position(1) = vMin;
      if (newState.position(1) > vMax)
        newState.position(1) = vMax;
      break;
    default: // Glued
      break;
  }

  // If we go too near a singularity, or something else goes awry, we may get
  // launched to infinity.  We previously tried just putting us back to the old
  // state--that froze us in position.  Now we just correct the errant data, and
  // not the whole state.
  if (std::isnan(newState.position(0)))
    newState.position(0) = state_.position(0);
  if (std::isnan(newState.position(1)))
    newState.position(1) = state_.position(0);
  if (std::isnan(newState.heading))
    newState.heading = state_.heading;
  if (std::isnan(newState.forwardVelocity))
    newState.forwardVelocity = state_.forwardVelocity;
  if (std::isnan(newState.strafeVelocity))
    newState.strafeVelocity = state_.strafeVelocity;
  if (std::isnan(newState.angularVelocity))
    newState.angularVelocity = state_.angularVelocity;

  // Update the time
  newState.time += timeStep;

  return newState;
}

} // namespace kleindrive
