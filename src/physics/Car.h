//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/physics/Car.h - Car class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains Car class definitions
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_CAR_H
#define KLEINDRIVE_CAR_H

#include "src/gamestate/GameState.h"
#include "src/surface/TiledSurface.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Represents a car
class Car
{
public:
  /// Container for the state of a car.
  struct State
  {
  public:
    /// Position in domain.
    Vec2 position;

    /// Heading in domain, measured clockwise from the u axis toward the v axis.
    /// This is the usual angle in the Euclidean metric.
    phys_t heading;

    /// Forward velocity along in the direction of the heading, but using the
    /// pullback metric.
    phys_t forwardVelocity;

    /// Angular velocity of the heading.
    phys_t angularVelocity;

    /// Strafing velcocity measured right of forward in the pullback metric.
    /// Both its direction and magnitude rely on the pullback metric.
    phys_t strafeVelocity;

    /// Target forward velocity.
    phys_t targetForwardVelocity;

    /// Target angular velocity.
    phys_t targetAngularVelocity;

    /// Target strafing velcocity.
    phys_t targetStrafeVelocity;

    /// Internal car time is decoupled from the game time to obtain better
    /// physics.
    phys_t time;
  };

  /// Container for the constants defining a car.
  struct Specs
  {
  public:
    /// Initial position in the domain.
    Vec2 initialPosition;

    /// Initial heading, measured in the Euclidean metric.
    phys_t initialHeading;

    /// Normal forward velocity as measured in the pullback metric.
    phys_t normalForwardVelocity;

    /// Min distance from edge of surface.
    phys_t minDistanceFromEdge;
  };

public:
  /// Construct a car according to given specs, with the given initial state.
  explicit Car(const Car::Specs& specs);

  /// This resets the current time of the car to the present time on next call
  /// to driveCar.
  void turnOff();

  /// Turn the car on at given time, if it's been inactive.
  void reset();

  /// Move the car according to the system state, and return the state at the
  /// game time.
  Car::State driveCar(const TiledSurface& tiledSurface, GameState* gameState);

private:
    /// Spring constant for forward acceleration toward target forward velocity.
    static constexpr phys_t forwardSpringConstant_ = 5;

    /// Spring constant for angular acceleration toward target angualr velocity.
    static constexpr phys_t angularSpringConstant_ = 5;

    /// Spring constant for strafing acceleration toward target strafe velocity.
    static constexpr phys_t strafeSpringConstant_ = 5;
    
    /// Maximal angular speed measured in the pullback metric.
    static constexpr phys_t maxAngularVelocity_ = 2;

    /// How far to step the car forward in computations.  Car is stepped
    /// forward, but internally lags behind the game time.
    static constexpr phys_t timeStep_ = 0.001;

    /// Sets the initil sttate based on the speccs.
    void setInitialConditions(phys_t time = 0);

    /// Update the car State accoding to the game state.
    void updateCarState(GameState* gameState);

    /// Step the car forward for duration timeStep.
    inline Car::State stepCar(const TiledSurface& tiledSurface,
                              phys_t timeStep);

    /// the car's specs.
    Specs specs_;

    /// The car's current state.
    State state_;

    /// Is teh car off.
    bool isTurnedOff_{ true };
};

} // namespace kleindrive

#endif // KLEINDRIVE_CAR_H
