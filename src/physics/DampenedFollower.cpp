//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/physics/DampenedFollower.cpp
//
//  DampenedFollower class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the DampenedFollower definition.
///
//===----------------------------------------------------------------------===//

#include "src/physics/DampenedFollower.h"
#include "src/types/ValueTypes.h"
#include <algorithm>

namespace kleindrive {

DampenedFollower::DampenedFollower(phys_t defaultXTarget,
                                   DampenedFollower::Specs specs)
  : specs_{ specs }
  , initialState_{ defaultXTarget, defaultXTarget, 0 }
  , state_{ initialState_ }
{
  setInitialState();
}

void
DampenedFollower::setInitialState()
{
  state_ = initialState_;
}

void
DampenedFollower::reset()
{
  setInitialState();
  turnOff();
}

void
DampenedFollower::turnOff()
{
  isTurnedOff_ = true;
}

// Moves the slider by the SlidingState until given time, returning the
// position// at given time.
phys_t
DampenedFollower::moveFollower(phys_t xTarget, phys_t time)
{
  // If we haven't updated the slider in more than a second, then there is a
  // problem, and we turn the slider off.  This avoids having to sit around
  // while the slider updates a billion times.
  if (time - state_.time > 1) {
    turnOff();
  }

  state_.xTarget = xTarget;

  // If turned off, then it has been idling.  Need to turn it back on, and reset
  // the time.
  if (isTurnedOff_) {
    state_.time = time;
    isTurnedOff_ = false;
    // Don't bother iterating--just return the current state.
    return state_.xPosition;
  }

  // How far do we need to advance in time.
  phys_t lag = time - state_.time;

  // Advance the slider in time by small timesteps until only a little lag
  // is left.
  while (lag > specs_.timeStep) {
    state_ = stepFollower(specs_.timeStep);
    lag -= specs_.timeStep;
  }

  // now step a ghost slider forward for the remaining lag time and return
  // the position of the ghost slider. This way we always iterate the slider
  // by a fixed timestep, but absorb the errant small timestep in a frame.
  return stepFollower(lag).xPosition;
}

// Move the slider in given direction for given timeStep.
// This might be called many times each frame, so it should probably be
// inlined.
inline DampenedFollower::State
DampenedFollower::stepFollower(phys_t timeStep)
{
  DampenedFollower::State newState = state_;
  
  // Dampened non-hookian spring.  You can change this to the
  // critically-damped or overdamped spring if you like.
  newState.xPosition -=
    timeStep * specs_.springConstant * (newState.xPosition - newState.xTarget);

  // Update time
  newState.time += timeStep;

  return newState;
}

} // namespace kleindrive
