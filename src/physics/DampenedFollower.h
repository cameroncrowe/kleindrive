//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/physics/DampenedFollower.h - DampenedFollower class definition file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the DampenedFollowed definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_DAMPENEDFOLLOWER_H
#define KLEINDRIVE_DAMPENEDFOLLOWER_H

#include "src/types/ValueTypes.h"
#include <algorithm>

namespace kleindrive {

/// Represents a dampened follower that smoothly tracks a target value.
class DampenedFollower
{
public:
  /// Container for the data determining a dampened slider.
  struct Specs
  {
  public:
    /// How fast the value of the slider springs toward it's target position.
    phys_t springConstant;

    /// How far foward the slider iterates as it steps toward current time.
    phys_t timeStep;
  };
public:
  /// Construc a dampened slider to specs, and set the intial state accordingly.
  DampenedFollower(phys_t defaultXTarget_, DampenedFollower::Specs specs);

  /// Resets the slider to it's initial state.
  void reset();

  /// Flags a time reset next time slider is updated.
  ///
  /// This should be called if the slider has been inactive for more than a
  /// single frame.
  void turnOff();

  /// Moves the slider by the SlidingState until given time, returning the
  /// position at given time.
  phys_t moveFollower(phys_t defaultXTarget, phys_t time);

private:
  // Container for the data dermining the state of a dampened slider.
  struct State
  {
  public:
    /// Target value for the slider.
    phys_t xTarget;

    /// Current position of the slider.
    phys_t xPosition;

    /// Internal time of the slider is slightly out of synch with the game time.
    phys_t time;
  };

  /// Move the slider in given direction for given timeStep.
  /// This might be called many times each frame, so it should probably be
  /// inlined.
  inline DampenedFollower::State stepFollower(phys_t timeStep);

  /// Sets the initial state.
  void setInitialState();
  
  /// Specs of the slider.
  DampenedFollower::Specs specs_;

  /// State of the slider.
  DampenedFollower::State initialState_;

  /// State of the slider.
  DampenedFollower::State state_;


  /// Is the slider active.
  bool isTurnedOff_{ true };
};

} // namespace kleindrive

#endif // KLEINDRIVE_DAMPENEDFOLLOWER_H
