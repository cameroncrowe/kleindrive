//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/physics/DampenedSlider.cpp - DampenedSlider class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the DampenedSlider definition.
///
//===----------------------------------------------------------------------===//

#include "src/physics/DampenedSlider.h"
#include "src/types/ValueTypes.h"
#include <algorithm>

namespace kleindrive {

DampenedSlider::DampenedSlider(DampenedSlider::Specs specs)
  : specs_{ specs }
{
  setInitialState();
}

void
DampenedSlider::setInitialState()
{
  // Set initialstate.
  state_.xTarget = specs_.xDefault;
  state_.xPosition = specs_.xDefault;
  state_.time = 0; // In case someone forgets to set this.
}

void
DampenedSlider::reset()
{
  setInitialState();
  turnOff();
}

void
DampenedSlider::turnOff()
{
  isTurnedOff_ = true;
}

// Moves the slider by the SlidingState until given time, returning the
// position// at given time.
phys_t
DampenedSlider::moveSlider(Sliding sliding, phys_t time)
{
  // If we haven't updated the slider in more than a second, then there is a
  // problem, and we turn the slider off.  This avoids having to sit around
  // while the slider updates a billion times.
  if (time - state_.time > 1) {
    turnOff();
  }

  // If turned off, then it has been idling.  Need to turn it back on, and reset
  // the time.
  if (isTurnedOff_) {
    state_.time = time;
    isTurnedOff_ = false;
    // Don't bother iterating--just return the current state.
    return state_.xPosition;
  }

  // If reset, then we need to reset the target and change the slider
  // motion to still
  if (sliding == Sliding::Reset) {
    sliding = Sliding::Reset;
    state_.xTarget = specs_.xDefault;
  }

  // How far do we need to advance in time.
  phys_t lag = time - state_.time;

  // Advance the slider in time by small timesteps until only a little lag
  // is left.
  while (lag > specs_.timeStep) {
    state_ = stepSlider(sliding, specs_.timeStep);
    lag -= specs_.timeStep;
  }

  // now step a ghost slider forward for the remaining lag time and return
  // the position of the ghost slider. This way we always iterate the slider
  // by a fixed timestep, but absorb the errant small timestep in a frame.
  return stepSlider(sliding, lag).xPosition;
}

// Move the slider in given direction for given timeStep.
// This might be called many times each frame, so it should probably be
// inlined.
inline DampenedSlider::State
DampenedSlider::stepSlider(Sliding sliding, phys_t timeStep)
{
  DampenedSlider::State newState = state_;

  if (sliding == Sliding::Up) {
    newState.xTarget =
      std::min(specs_.xMax, newState.xTarget + timeStep * specs_.slideSpeed);
  }
  if (sliding == Sliding::Down) {
    newState.xTarget =
      std::max(specs_.xMin, newState.xTarget - timeStep * specs_.slideSpeed);
  }
  // if Sliding::Still, don't change the target.
  // Should never be Sliding::Reset, but won't cause harm.

  // Dampened non-hookian spring.  You can change this to the
  // critically-damped or overdamped spring if you like.
  newState.xPosition -=
    timeStep * specs_.springConstant * (newState.xPosition - newState.xTarget);

  // Update time
  newState.time += timeStep;

  return newState;
}

} // namespace kleindrive
