//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scene/RenderingCache.cpp - RenderingCache struct implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the RenderingCache struct implementation.
///
//===----------------------------------------------------------------------===//

#include "src/scene/RenderingCache.h"
#include "src/camera/Camera.h"
#include "src/graphics/Tile.h"
#include "src/surface/TiledSurface.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <array>
#include <vector>

namespace kleindrive {

// Initializes the rendering cache based on number of vertices and tiles.
RenderingCache::RenderingCache(const TiledSurface& tiledSurface)
{
  { // Vertex data.
    const int numberOfVertices = tiledSurface.numberOfVertices();
    {
      using sz_t = decltype(vertices)::size_type;
      vertices.resize(static_cast<sz_t>(numberOfVertices));
    }
  }

  { // Tile data.
    const int numberOfTiles = tiledSurface.numberOfTiles();

    {
      using sz_t = decltype(normals)::size_type;
      normals.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(centers)::size_type;
      centers.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(distances)::size_type;
      distances.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(toFPVectors)::size_type;
      toFPVectors.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(colors)::size_type;
      colors.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(radiuses)::size_type;
      radiuses.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(isVisibles)::size_type;
      isVisibles.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(needsAperturePlaneClippings)::size_type;
      needsAperturePlaneClippings.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(corners)::size_type;
      corners.resize(static_cast<sz_t>(numberOfTiles));
    }
    {
      using sz_t = decltype(tileTypes)::size_type;
      tileTypes.resize(static_cast<sz_t>(numberOfTiles), TileType::Central);
    }
  }
}

} // namespace kleindrive
