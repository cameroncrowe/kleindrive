//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scene/RenderingCache.h - RenderingCache struct definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the RenderingCache struct definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_RENDERINGCACHE_H
#define KLEIN_DRIVE_RENDERINGCACHE_H

#include "src/camera/Camera.h"
#include "src/camera/CameraShotData.h"
#include "src/gamestate/GameState.h"
#include "src/graphics/Tile.h"
#include "src/surface/TiledSurface.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <array>
#include <vector>

namespace kleindrive {

/// Provides storage for the rendering of a frame.
struct RenderingCache
{
public:
  /// Initializes the rendering cache.  This simply resizes each list of data to
  /// the appropriate size, filling them with dummy information.  They should never be resized back to zero, except for the tiles, which is resized to zero every frame.
  explicit RenderingCache(const TiledSurface& tiledSurface);
  
  /// Camera shot data. 
  CameraShotData cameraShotData;

  /// The game State. 
  GameState gameState;

  /// Vertices of tiles.  Do not resize.
  EigenAlignedStdVector<Vec3> vertices;

  /// Normal of surface at center of tile.  Do not resize.
  EigenAlignedStdVector<Vec3> normals;

  /// Center of tile.  Do not resize.
  EigenAlignedStdVector<Vec3> centers;

  /// Vectors from tile center to focal point.  Do not resize.
  EigenAlignedStdVector<Vec3> toFPVectors;

  /// Distance from tile center to focal point.  Do not resize.
  std::vector<phys_t> distances;

  /// Colors of tiles.  Do not resize.
  std::vector<Color> colors;

  /// Radiuses of tiles.  Do not resize.
  std::vector<phys_t> radiuses;

  /// IsVisible flag for each tile.  Do not resize.
  std::vector<bool> isVisibles;

  /// Whether or not a tile needs to be clipped at the aperture plane.  Do not
  /// resize.
  std::vector<bool> needsAperturePlaneClippings;

  /// Corners of each tile before clipping.  Do not resize.
  std::vector<std::array<Vec3, 4>> corners;

  /// Type of each tile.  Do not resize.
  std::vector<TileType> tileTypes;

  /// Tiles as graphical objects.  DO resize back to zero after every frame.
  std::vector<Tile> tiles;

};

} // namespace kleindrive

#endif // KLEINDRIVE_RENDERINGCACHE_H
