//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scene/Scene.cpp - Scene class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Scene class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/scene/Scene.h"
#include "src/camera/Camera.h"
#include "src/camera/CameraShotData.h"
#include "src/cameraoperator/CameraOperator.h"
#include "src/features/domainmap/DomainMap.h"
#include "src/gamestate/GameState.h"
#include "src/graphics/Frame.h"
#include "src/graphics/Tile.h"
#include "src/scene/RenderingCache.h"
#include "src/scene/SceneProperties.h"
#include "src/surface/TiledSurface.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"
#include <array>
#include <memory>
#include <vector>

namespace kleindrive {

Scene::Scene(const TiledSurface& tiledSurface,
             std::unique_ptr<CameraOperator> previewCameraOperator,
             std::unique_ptr<CameraOperator> driveCameraOperator,
             const SceneProperties& sceneProperties)
  : tiledSurface_{ tiledSurface }
  , domainMap_(tiledSurface_, 5)
  , sceneProperties_{ sceneProperties }
  , previewCameraOperator_{ std::shared_ptr<CameraOperator>(
      previewCameraOperator.release()) }
  , driveCameraOperator_{ std::shared_ptr<CameraOperator>(
      driveCameraOperator.release()) }
  , cache_{ tiledSurface }
{}

Scene::Scene(const TiledSurface& tiledSurface,
             std::unique_ptr<CameraOperator> cameraOperator,
             const SceneProperties& sceneProperties)
  : tiledSurface_{ tiledSurface }
  , domainMap_(tiledSurface_, 5)
  , sceneProperties_{ sceneProperties }
  , previewCameraOperator_{ std::shared_ptr<CameraOperator>(
      cameraOperator.release()) }
  , driveCameraOperator_{ previewCameraOperator_ }
  , cache_{ tiledSurface }
{}

void
Scene::reset()
{
  // NOT sceneProperties_.reset(); -- this is done in a different function.
  previewCameraOperator_->reset();
  driveCameraOperator_->reset();
  domainMap_.reset();
  turnOff();
}

void
Scene::resetSceneProperties()
{
  sceneProperties_.reset();
}

void
Scene::turnOff()
{
  previewCameraOperator_->turnOff();
  driveCameraOperator_->turnOff();
}

Scene*
Scene::preview()
{
  // TODO This needs a better solution.
  // Ie, one that will work if several different views are running
  // simulatneously. This should be put into the direct cameras function.
  mode_ = Mode::Preview;
  return this;
}

Scene*
Scene::drive()
{
  // TODO This needs a better solution.
  mode_ = Mode::Drive;
  return this;
}

std::string
Scene::label()
{
  return sceneProperties_.state().label;
}

CameraShotData
Scene::directCameras(const TiledSurface& tiledSurface, GameState* gameState)
{
  if (mode_ == Mode::Preview)
    return previewCameraOperator_->pointCamera(tiledSurface, gameState);
  return driveCameraOperator_->pointCamera(tiledSurface, gameState);
}

Frame
Scene::render(GameState* gameState)
{
  // Let the SceneProperties process itself
  sceneProperties_.state(gameState);

  // Let the camera process the game state and return data
  // to shoot with the camera.
  CameraShotData cameraShotData = directCameras(tiledSurface_, gameState);

  // Process the scene.
  cache_.gameState = *gameState;
  cache_.cameraShotData = cameraShotData;

  populateRenderingCache();
  processRenderingCache();

  // Build the frame.
  Frame frame;
  frame.tiles = cache_.tiles;

  // Draw the domain map if we're driving.
  if (mode_ == Scene::Mode::Drive) {
    domainMap_.update(gameState);
    frame.domainMap = &domainMap_;
  } else {
    frame.domainMap = nullptr;
  }

  frame.scenePropertiesState = sceneProperties_.state();

  return frame;
}

void
Scene::populateRenderingCache()
{
  { // Populate vertex information

    const int numberOfVerticesInUDirection =
      tiledSurface_.numberOfVerticesInUDirection();
    const int numberOfVerticesInVDirection =
      tiledSurface_.numberOfVerticesInVDirection();

    phys_t time = cache_.gameState.time;

    // Iterate over vertices, first in u direction, then in v direction.
    // We assume the cache has the correct size, already.
    for (int j = 0; j < numberOfVerticesInVDirection; ++j) {
      for (int i = 0; i < numberOfVerticesInUDirection; ++i) {
        using sz_t = decltype(RenderingCache::vertices)::size_type;
        const int index = tiledSurface_.indexOfVertex(i, j);
        cache_.vertices[static_cast<sz_t>(index)] =
          tiledSurface_.vertex(i, j, time);
      }
    }
  }

  { // Populate tile information

    const int numberOfTilesInUDirection =
      tiledSurface_.numberOfTilesInUDirection();
    const int numberOfTilesInVDirection =
      tiledSurface_.numberOfTilesInVDirection();

    const Vec3 focalPoint = cache_.cameraShotData.focalPoint;

    const phys_t time = cache_.gameState.time;

    std::function<Color(int, int, phys_t)> colorFunction;

    switch (sceneProperties_.state().tileDrawingMethod) {
      case TileDrawingMethod::Solid: {
        const auto i = std::placeholders::_1;
        const auto j = std::placeholders::_2;
        const auto t = std::placeholders::_3;
        colorFunction =
          std::bind(&TiledSurface::colorOfTile, &tiledSurface_, i, j, t);
      } break;

      case TileDrawingMethod::Grid: {
        const Color gridColor = sceneProperties_.state().gridColor;
        colorFunction = [=](int, int, phys_t) { return gridColor; };
      } break;

      case TileDrawingMethod::GaussianCurvature: {
        const auto i = std::placeholders::_1;
        const auto j = std::placeholders::_2;
        const auto t = std::placeholders::_3;

        const phys_t curvatureColorExaggeration =
          sceneProperties_.state().curvatureColorExaggeration;

        colorFunction = std::bind(&TiledSurface::colorOfTileByGaussianCurvature,
                                  &tiledSurface_,
                                  curvatureColorExaggeration,
                                  i,
                                  j,
                                  t);
      } break;
    }

    // Iterate over tiles, first in u direction, then in v direction.
    // We assume the cache has the correct size, already.
    for (int j = 0; j < numberOfTilesInVDirection; ++j) {
      for (int i = 0; i < numberOfTilesInUDirection; ++i) {
        const Color color_ij = colorFunction(i, j, time);

        const phys_t radius_ij = tiledSurface_.radiusOfTile(i, j, time);
        const Vec3 center_ij = tiledSurface_.centerOfTile(i, j, time);
        const Vec3 normal_ij = tiledSurface_.normalToTile(i, j, time);

        const Vec3 toFPVector_ij = focalPoint - center_ij;
        const phys_t distance_ij = toFPVector_ij.norm();

        const int index = tiledSurface_.indexOfTile(i, j);
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.centers[static_cast<sz_t>(index)] = center_ij;
        }
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.normals[static_cast<sz_t>(index)] = normal_ij;
        }
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.distances[static_cast<sz_t>(index)] = distance_ij;
        }
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.toFPVectors[static_cast<sz_t>(index)] = toFPVector_ij;
        }
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.radiuses[static_cast<sz_t>(index)] = radius_ij;
        }
        {
          using sz_t = decltype(RenderingCache::centers)::size_type;
          cache_.colors[static_cast<sz_t>(index)] = color_ij;
        }
      }
    }
    // If we are in grid mode, we need to know to draw the bottom and right
    // edges as well.
    if (sceneProperties_.state().tileDrawingMethod == TileDrawingMethod::Grid) {
      using sz_t = decltype(RenderingCache::tileTypes)::size_type;

      if (tiledSurface_.vDirection() == Gluing::NotGlued) {
        for (int i = 0; i < numberOfTilesInUDirection; ++i) {
          const sz_t index = static_cast<sz_t>(
            tiledSurface_.indexOfTile(i, numberOfTilesInVDirection - 1));
          cache_.tileTypes[index] = TileType::TopEdge;
        }
      }

      if (tiledSurface_.uDirection() == Gluing::NotGlued) {
        for (int j = 0; j < numberOfTilesInVDirection; ++j) {
          const sz_t index = static_cast<sz_t>(
            tiledSurface_.indexOfTile(numberOfTilesInUDirection - 1, j));
          cache_.tileTypes[index] = TileType::RightEdge;
        }
      }

      if (tiledSurface_.uDirection() == Gluing::NotGlued &&
          tiledSurface_.vDirection() == Gluing::NotGlued) {
        const sz_t index = static_cast<sz_t>(tiledSurface_.indexOfTile(
          numberOfTilesInUDirection - 1, numberOfTilesInVDirection - 1));
        cache_.tileTypes[index] = TileType::TopRightCorner;
      }
    }
  }
} // populateRenderingCache()

void
Scene::processRenderingCache()
{
  // This is a minimalistic graphics rendering algorithm.
  //
  // It is broken into block scopes {...} to give a general sense of how it
  // structured, but it is left as one whole so that it can be more easily
  // streamlined in the future.
  //
  // At present it is not a bottleneck, so it may stay like this.

  { // Convert coordinates of vertices to a z buffer:
    //   x_world, y_world, z_world ---> x_screen, y_screen, z_camera

    // First we map the world coordinates to:
    //   ----> x_screen * z_camera, y_screen * z_camera, z_camera
    // with an affine transformation:
    const Aff3 at = cache_.cameraShotData.worldToAffineScreen;

    // Second we divide the out the extra z_camera coordinate:
    //   ----> x_screen, y_screen, z_camera

    const int numberOfVertices = tiledSurface_.numberOfVertices();

    for (int I = 0; I < numberOfVertices; ++I) {
      using sz_t = decltype(RenderingCache::vertices)::size_type;
      sz_t i = static_cast<sz_t>(I);

      // First transformation:
      cache_.vertices[i] = at * cache_.vertices[i];
      /*
       // FIXME TODO Might need to check div/0
       // Second transformation:
       cache_.vertices[i](0) = cache_.vertices[i](0)/cache_.vertices[i](2);
       cache_.vertices[i](1) = cache_.vertices[i](1)/cache_.vertices[i](2);
       */
    }
  }

  // Remove the rounding error artifact that happens when we glue edges of the
  // domain together.
  {
    const int numberOfVerticesInUDirection =
      tiledSurface_.numberOfVerticesInUDirection();
    const int numberOfVerticesInVDirection =
      tiledSurface_.numberOfVerticesInVDirection();

    // Fix U directional gluing.
    switch (tiledSurface_.uDirection()) {
      case Gluing::RightWay: {
        // Make the V vertices match perfectly on the ends.
        using sz_t = decltype(cache_.vertices)::size_type;

        const int uLeftIndex = 0;
        const int uRightIndex = numberOfVerticesInUDirection - 1;

        for (int J = 0; J < numberOfVerticesInVDirection; ++J) {
          const sz_t indexLeft =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(uLeftIndex, J));
          const sz_t indexRight =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(uRightIndex, J));

          cache_.vertices[indexRight] = cache_.vertices[indexLeft];
        }
      } break;

      case Gluing::WrongWay: {
        // Make the V vertices match perfectly on the ends.
        using sz_t = decltype(cache_.vertices)::size_type;

        const int uLeftIndex = 0;
        const int uRightIndex = numberOfVerticesInUDirection - 1;

        const int vTopIndex = numberOfVerticesInVDirection - 1;

        for (int J = 0; J < numberOfVerticesInVDirection; ++J) {
          const sz_t indexLeft =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(uLeftIndex, J));
          const sz_t indexRight = static_cast<sz_t>(
            tiledSurface_.indexOfVertex(uRightIndex, vTopIndex - J));

          cache_.vertices[indexLeft] = cache_.vertices[indexRight];
        }
      } break;

      default:
        break;
    }

    // Fix V directional gluing.
    switch (tiledSurface_.vDirection()) {
      case Gluing::RightWay: {
        // Make the V vertices match perfectly on the ends.
        using sz_t = decltype(cache_.vertices)::size_type;

        const int vBottomIndex = 0;
        const int vTopIndex = numberOfVerticesInVDirection - 1;

        for (int I = 0; I < numberOfVerticesInUDirection; ++I) {
          const sz_t indexBottom =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(I, vBottomIndex));
          const sz_t indexTop =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(I, vTopIndex));

          cache_.vertices[indexTop] = cache_.vertices[indexBottom];
        }
      } break;
      case Gluing::WrongWay: {
        // Make the V vertices match perfectly on the ends.
        using sz_t = decltype(cache_.vertices)::size_type;

        const int vBottomIndex = 0;
        const int vTopIndex = numberOfVerticesInVDirection - 1;

        const int uRightIndex = numberOfVerticesInUDirection - 1;

        for (int I = 0; I < numberOfVerticesInUDirection; ++I) {
          const sz_t indexBottom =
            static_cast<sz_t>(tiledSurface_.indexOfVertex(I, vBottomIndex));
          const sz_t indexTop = static_cast<sz_t>(
            tiledSurface_.indexOfVertex(uRightIndex - I, vTopIndex));

          cache_.vertices[indexTop] = cache_.vertices[indexBottom];
        }
      } break;

      default:
        break;
    }
  }

  { // We assemble the corners of the tiles.
    const int numberOfTilesInUDirection =
      tiledSurface_.numberOfTilesInUDirection();
    const int numberOfTilesInVDirection =
      tiledSurface_.numberOfTilesInVDirection();

    // Recall to iterate in the u direction first, then the v direction.
    for (int j = 0; j < numberOfTilesInVDirection; ++j) {
      for (int i = 0; i < numberOfTilesInUDirection; ++i) {

        using sz_t = decltype(RenderingCache::corners)::size_type;

        // Vertices start at bottom left, and proceed anti-clockwise.
        const sz_t indexOfVertex_0 =
          static_cast<sz_t>(tiledSurface_.indexOfVertex(i, j));
        const sz_t indexOfVertex_1 =
          static_cast<sz_t>(tiledSurface_.indexOfVertex(i + 1, j));
        const sz_t indexOfVertex_2 =
          static_cast<sz_t>(tiledSurface_.indexOfVertex(i + 1, j + 1));
        const sz_t indexOfVertex_3 =
          static_cast<sz_t>(tiledSurface_.indexOfVertex(i, j + 1));

        const sz_t indexOfTile =
          static_cast<sz_t>(tiledSurface_.indexOfTile(i, j));

        cache_.corners[indexOfTile][0] = cache_.vertices[indexOfVertex_0];
        cache_.corners[indexOfTile][1] = cache_.vertices[indexOfVertex_1];
        cache_.corners[indexOfTile][2] = cache_.vertices[indexOfVertex_2];
        cache_.corners[indexOfTile][3] = cache_.vertices[indexOfVertex_3];
      }
    }
  }

  { // Compute whether or not tiles are visible, or need clipping at
    // the aperture plane.

    // For now we only partially implement this:
    const int numberOfTiles = tiledSurface_.numberOfTiles();

    // For now, we say a tile is visible if any of it's corners are in front of
    // the aperture plane, but not if it's color is set to invisible

    const phys_t f = cache_.cameraShotData.focalLength;
    for (int I = 0; I < numberOfTiles; ++I) {
      using sz_t = decltype(RenderingCache::corners)::size_type;
      sz_t i = static_cast<sz_t>(I);
      const bool isColorVisible = cache_.colors[i].isVisible;
      const auto corners = cache_.corners[i];
      const phys_t z0 = corners[0](2);
      const phys_t z1 = corners[1](2);
      const phys_t z2 = corners[2](2);
      const phys_t z3 = corners[3](2);
      cache_.isVisibles[i] =
        isColorVisible && ((z0 > f) || (z1 > f) || (z2 > f) || (z3 > f));

      // If any of the vertices (of a visible tile) is behind the aperture plane
      // we need to clip it.
      cache_.needsAperturePlaneClippings[i] =
        (z0 < f) || (z1 < f) || (z2 < f) || (z3 < f);
    }
  }

  { // Build a list of visible tiles, and clip them at the aperture plane.
    const int numberOfTiles = tiledSurface_.numberOfTiles();

    const phys_t focalLength = cache_.cameraShotData.focalLength;

    const Color backgroundColor = sceneProperties_.state().backgroundColor;

    cache_.tiles.resize(0);

    for (int I = 0; I < numberOfTiles; ++I) {
      using sz_t = decltype(RenderingCache::isVisibles)::size_type;
      sz_t i = static_cast<sz_t>(I); // FIXME: we're abusing the type below.
      if (!cache_.isVisibles[i])
        continue;

      // We weed out bogus tiles.
      bool isTileBogus = false;

      // COMPUTE THE TILE ATTRIBUTES
      // ===========================

      // Distance to focal point
      // -----------------------

      const phys_t distance = cache_.distances[i];

      // Shading
      // -------

      // Shade by distance
      const phys_t alpha = sceneProperties_.state().lightDecayRate;
      const phys_t lightCoeff = pow(distance / focalLength, -alpha);

      // Shade by angle of normal vector.
      const phys_t beta = sceneProperties_.state().surfaceReflectiveness;
      const phys_t coeffAngle =
        pow(std::abs(cache_.toFPVectors[i].dot(cache_.normals[i]) /
                     cache_.distances[i]),
            beta);
      const phys_t coeff = coeffAngle * lightCoeff;

      const Color baseColor = cache_.colors[i];

      // Fade to background color by distance, and darken by angle.
      const Color shadedColor = {
        coeff * baseColor.R + (1 - lightCoeff) * backgroundColor.R,
        coeff * baseColor.G + (1 - lightCoeff) * backgroundColor.G,
        coeff * baseColor.B + (1 - lightCoeff) * backgroundColor.B
      };

      // Corners
      // --------

      // Given these:
      const std::array<Vec3, 4> cornersWorld = cache_.corners[i];
      const bool needsAperturePlaneClipping =
        cache_.needsAperturePlaneClippings[i];
      TileType tileType =
        (needsAperturePlaneClipping) ? TileType::Clipped : cache_.tileTypes[i];

      // Compute these:
      std::vector<Vec2> cornersScreen; // About to compute.
      cornersScreen.reserve(6);

      if (!needsAperturePlaneClipping) {
        // Don't need to clip to the aperture plane--just make the standard
        // tile. 98% of the time.

        const Vec2 corner0 =
          Vec2(cornersWorld[0](0), cornersWorld[0](1)) / cornersWorld[0](2);
        const Vec2 corner1 =
          Vec2(cornersWorld[1](0), cornersWorld[1](1)) / cornersWorld[1](2);
        const Vec2 corner2 =
          Vec2(cornersWorld[2](0), cornersWorld[2](1)) / cornersWorld[2](2);
        const Vec2 corner3 =
          Vec2(cornersWorld[3](0), cornersWorld[3](1)) / cornersWorld[3](2);

        cornersScreen.push_back(corner0);
        cornersScreen.push_back(corner1);
        cornersScreen.push_back(corner2);
        cornersScreen.push_back(corner3);

      } else {
        // We need to clip.
        // 2% of the time.

        // We label the sides 0 to 4.
        // Side 0 goes from vertex 0 to vertex 1.
        // Side 1 goes from vertex 1 to vertex 2.
        // :
        //
        // We need to clip side i, if it's vertices lie on opposite sides of the
        // apertues plane.
        const phys_t z0 = cornersWorld[0](2);
        const phys_t z1 = cornersWorld[1](2);
        const phys_t z2 = cornersWorld[2](2);
        const phys_t z3 = cornersWorld[3](2);

        const phys_t f = cache_.cameraShotData.focalLength;

        const bool needToClipSide0 = (z0 > f && z1 < f) || (z0 < f && z1 > f);
        const bool needToClipSide1 = (z1 > f && z2 < f) || (z1 < f && z2 > f);
        const bool needToClipSide2 = (z2 > f && z3 < f) || (z2 < f && z3 > f);
        const bool needToClipSide3 = (z3 > f && z0 < f) || (z3 < f && z0 > f);

        std::vector<Vec3> newCornersWorld;
        newCornersWorld.reserve(8);
        if (z0 >= f) {
          newCornersWorld.push_back(cornersWorld[0]);
        }
        if (needToClipSide0) {
          const phys_t affineCoeff = (f - z1) / (z0 - z1);
          const Vec3 newCorner =
            affineCoeff * cornersWorld[0] + (1 - affineCoeff) * cornersWorld[1];
          newCornersWorld.push_back(newCorner);
        }
        if (z1 >= f) {
          newCornersWorld.push_back(cornersWorld[1]);
        }
        if (needToClipSide1) {
          const phys_t affineCoeff = (f - z2) / (z1 - z2);
          const Vec3 newCorner =
            affineCoeff * cornersWorld[1] + (1 - affineCoeff) * cornersWorld[2];
          newCornersWorld.push_back(newCorner);
        }
        if (z2 >= f) {
          newCornersWorld.push_back(cornersWorld[2]);
        }
        if (needToClipSide2) {
          const phys_t affineCoeff = (f - z3) / (z2 - z3);
          const Vec3 newCorner =
            affineCoeff * cornersWorld[2] + (1 - affineCoeff) * cornersWorld[3];
          newCornersWorld.push_back(newCorner);
        }
        if (z3 >= f) {
          newCornersWorld.push_back(cornersWorld[3]);
        }
        if (needToClipSide3) {
          const phys_t affineCoeff = (f - z0) / (z3 - z0);
          const Vec3 newCorner =
            affineCoeff * cornersWorld[3] + (1 - affineCoeff) * cornersWorld[0];
          newCornersWorld.push_back(newCorner);
        }

        // Insert the cornersWorld into the cornersScreen.
        for (const auto cornerWorld : newCornersWorld) {
          const Vec2 cornerScreen =
            Vec2(cornerWorld(0), cornerWorld(1)) / cornerWorld(2);
          cornersScreen.push_back(cornerScreen);
        }
      }

      // cornerScreen contains a list of all the vertices in the screen's plane.
      // Some of these shoot way over the edge of the screen, and can even
      // overflow whatever integer representation is used for the screen (we had
      // problems even with numbers smaller than max integers!)
      //
      // So it is important to clip these polygons to the screen
      //
      // We know that every polygon is a convex 3-gon, 4-gon, 5-gon or 6-gon.
      // Since it's convex, a line can only intersect two edges.  So we can get
      // at most 4 new edges.  So we end up with at most a 10-gon.

      std::array<scrn_t, 20> cornersScreenClipped; // About to compute this.
      cornersScreenClipped.fill(0);
      int numberOfCorners = 0; // About to compute this.
      cornersScreen.reserve(10);
      {
        // Box dimensions -- We enlarge them by 10 pixel to acommodate rounding
        // error.
        const scrn_t x = cache_.gameState.animatedBox.x - 10;
        const scrn_t y = cache_.gameState.animatedBox.y - 10;
        const scrn_t w = cache_.gameState.animatedBox.w + 20;
        const scrn_t h = cache_.gameState.animatedBox.h + 20;

        // Clip corners to left edge.
        {
          // Container to process vertices into.
          std::vector<Vec2> newCornersScreen;
          newCornersScreen.reserve(10); 

          using sz_t = decltype(cornersScreen)::size_type;
          for (sz_t J = 0, K = cornersScreen.size(); J < K; ++J) {
            const Vec2 cornerA = cornersScreen[J];
            const Vec2 cornerB = cornersScreen[(J + 1) % K];

            // Add corner A if it's on the right side of the line
            if (cornerA(0) >= x)
              newCornersScreen.push_back(cornerA);

            /// Add a point between corner A and corner B if the edge crosses
            /// the line
            if ((cornerA(0) < x && cornerB(0) > x) ||
                (cornerA(0) > x && cornerB(0) < x)) {
              tileType = TileType::Clipped;
              const phys_t affineCoeff =
                (x - cornerB(0)) / (cornerA(0) - cornerB(0));
              const Vec2 newCorner =
                affineCoeff * cornerA + (1 - affineCoeff) * cornerB;
              newCornersScreen.push_back(newCorner);
            }
          }

          // Update cornersScreen
          cornersScreen = std::move(newCornersScreen);
        }

        // Clip corners to right edge.
        {
          // Container to process vertices into.
          std::vector<Vec2> newCornersScreen;
          newCornersScreen.reserve(10); 

          using sz_t = decltype(cornersScreen)::size_type;
          for (sz_t J = 0, K = cornersScreen.size(); J < K; ++J) {
            const Vec2 cornerA = cornersScreen[J];
            const Vec2 cornerB = cornersScreen[(J + 1) % K];

            // Add corner A if it's on the right side of the line
            if (cornerA(0) <= x + w)
              newCornersScreen.push_back(cornerA);

            /// Add a point between corner A and corner B if the edge crosses
            /// the line
            if ((cornerA(0) < x + w && cornerB(0) > x + w) ||
                (cornerA(0) > x + w && cornerB(0) < x + w)) {
              tileType = TileType::Clipped;
              const phys_t affineCoeff =
                (x + w - cornerB(0)) / (cornerA(0) - cornerB(0));
              const Vec2 newCorner =
                affineCoeff * cornerA + (1 - affineCoeff) * cornerB;
              newCornersScreen.push_back(newCorner);
            }
          }

          // Update cornersScreen
          cornersScreen = std::move(newCornersScreen);
        }

        // Clip corners to Top edge.
        {
          // Container to process vertices into.
          std::vector<Vec2> newCornersScreen;
          newCornersScreen.reserve(10);

          using sz_t = decltype(cornersScreen)::size_type;
          for (sz_t J = 0, K = cornersScreen.size(); J < K; ++J) {
            const Vec2 cornerA = cornersScreen[J];
            const Vec2 cornerB = cornersScreen[(J + 1) % K];

            // Add corner A if it's on the right side of the line
            if (cornerA(1) >= y)
              newCornersScreen.push_back(cornerA);

            /// Add a point between corner A and corner B if the edge crosses
            /// the line
            if ((cornerA(1) < y && cornerB(1) > y) ||
                (cornerA(1) > y && cornerB(1) < y)) {
              tileType = TileType::Clipped;
              const phys_t affineCoeff =
                (y - cornerB(1)) / (cornerA(1) - cornerB(1));
              const Vec2 newCorner =
                affineCoeff * cornerA + (1 - affineCoeff) * cornerB;
              newCornersScreen.push_back(newCorner);
            }
          }

          // Update cornersScreen
          cornersScreen = std::move(newCornersScreen);
        }

        // Clip corners to Bottom edge.
        {
          // Container to process vertices into.
          std::vector<Vec2> newCornersScreen;
          newCornersScreen.reserve(10);

          using sz_t = decltype(cornersScreen)::size_type;
          for (sz_t J = 0, K = cornersScreen.size(); J < K; ++J) {
            const Vec2 cornerA = cornersScreen[J];
            const Vec2 cornerB = cornersScreen[(J + 1) % K];

            // Add corner A if it's on the right side of the line
            if (cornerA(1) <= y + h)
              newCornersScreen.push_back(cornerA);

            /// Add a point between corner A and corner B if the edge crosses
            /// the line
            if ((cornerA(1) < y + h && cornerB(1) > y + h) ||
                (cornerA(1) > y + h && cornerB(1) < y + h)) {
              tileType = TileType::Clipped;
              const phys_t affineCoeff =
                (y + h - cornerB(1)) / (cornerA(1) - cornerB(1));
              const Vec2 newCorner =
                affineCoeff * cornerA + (1 - affineCoeff) * cornerB;
              newCornersScreen.push_back(newCorner);
            }
          }

          // Update cornersScreen
          cornersScreen = std::move(newCornersScreen);
        }


        numberOfCorners = static_cast<int>(cornersScreen.size());

        // It's possible that the tile is nowhere near the screen in which case
        // we have no vertices.  To be on the safe side, we kick out the tile if
        // it has less than 3 vertices.
        if (numberOfCorners < 3)
          isTileBogus = true;

        using sz_t = decltype(cornersScreenClipped)::size_type;

        for (sz_t J = 0; J < static_cast<sz_t>(numberOfCorners); ++J) {
          cornersScreenClipped[2 * J + 0] =
            static_cast<scrn_t>(cornersScreen[J](0));
          cornersScreenClipped[2 * J + 1] =
            static_cast<scrn_t>(cornersScreen[J](1));
        }
      }

      // CONSTRUCT THE TILE
      // ==================
      if (!isTileBogus) {
        Tile tile{
          cornersScreenClipped, numberOfCorners, shadedColor, tileType, distance
        };
        cache_.tiles.push_back(tile);
      }
    }
  }

  { // Sort the tiles by distance to camera.  Not perfect, but does the job.
    sort(
      cache_.tiles.begin(),
      cache_.tiles.end(),
      [](const Tile& t1, const Tile& t2) { return t1.distance > t2.distance; });
  }
}

} // namespace kleindrive
