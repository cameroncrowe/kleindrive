//==-------------------------------------------------------------*- C++ -*-===//
//
//  srn/scene/Scene.h - Scene class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Scene class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_SCENE_H
#define KLEIN_DRIVE_SCENE_H

#include "src/cameraoperator/CameraOperator.h"
#include "src/features/domainmap/DomainMap.h"
#include "src/gamestate/GameState.h"
#include "src/graphics/Frame.h"
#include "src/scene/RenderingCache.h"
#include "src/scene/SceneProperties.h"
#include "src/surface/TiledSurface.h"
#include <memory>
#include <string>

namespace kleindrive {

/// Represents a scene with a TiledSurface and a Camera Operator.
class Scene final
{
public:
  /// Constructs a Scene with both a preview and drive camera operatr.
  Scene(const TiledSurface& tiledSurface,
        std::unique_ptr<CameraOperator> previewCameraOperator,
        std::unique_ptr<CameraOperator> driveCameraOperator,
        const SceneProperties& sceneProperties = SceneProperties());

  /// Constructs  ascene with only one cameraOperator.
  ///
  /// A request for a preview or drive camera will return the same camera
  /// operator.
  Scene(const TiledSurface& tiledSurface,
        std::unique_ptr<CameraOperator> cameraOperator,
        const SceneProperties& sceneProperties = SceneProperties());

  /// Renders a frame of the scene according to the given game state.
  Frame render(GameState* gameState);

  /// Resets the scene to its initial state, except for its SceneProperties.
  void reset();

  /// Resets the scene's properties to their initial state.
  void resetSceneProperties();

  /// Turn the scene off if it wasn't rendered last frame and the frame was
  /// delayed for some reason (like having lost focus).
  void turnOff();

  /// Returns the scene in preview mode.
  Scene* preview();

  /// Returns the scene in camera mode.
  Scene* drive();

  /// Returns the scene's label.
  std::string label();

  /// Moves the carera in accodrance with it's mode, and the input.
  CameraShotData directCameras(const TiledSurface& tiledSurface,
                               GameState* gameState);

private:
  /// Private mode of scene.  This is somewhat a hack (since it duplicates a
  /// state held elsewhre)... maybe, but made the code easy to adapt to the gui
  /// , and will make it easy to adapt again.
  enum class Mode
  {
    Preview,
    Drive
  };

  /// Populates the rendering cache according to the game state and scene.
  void populateRenderingCache();

  /// Processes the rendering data once populated.
  void processRenderingCache();

  /// We are the  owner of a tiled surface.
  TiledSurface tiledSurface_;

  /// Small map of the domain used in driving mode.
  DomainMap domainMap_;

  /// Properties of the scene.
  SceneProperties sceneProperties_;

  /// Camera operator for preview mode.
  std::shared_ptr<CameraOperator> previewCameraOperator_;

  /// Camera operator for drive mode.... doesn't have to be a car camera.
  std::shared_ptr<CameraOperator> driveCameraOperator_;

  /// Cache for rendering data.
  RenderingCache cache_;

  /// Current mode, Determines what cameraOperator to used when asked to render,
  /// and also whether or not to add the domain map to the frame.
  Mode mode_{ Mode::Preview };

};

} // namespace kleindrive

#endif // KLEINDRIVE_SCENE_H
