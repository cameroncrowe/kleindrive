//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scene/SceneProperties.cpp - SceneProperties class implementation
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SceneProperties class definition.
///
//===----------------------------------------------------------------------===//

#include "src/scene/SceneProperties.h"
#include "src/collections/Colors.h"
#include "src/gamestate/GameState.h"
#include "src/types/ValueTypes.h"
#include <limits>

namespace kleindrive {

SceneProperties::SceneProperties()
  : initialState_()
  , currentState_()
{}

SceneProperties::SceneProperties(const State& initialState)
  : initialState_{ initialState }
  , currentState_{ initialState }
{}

void
SceneProperties::reset()
{
  if (!initialState_.isLocked)
    currentState_ = initialState_;
}

void
SceneProperties::state(GameState* gameState)
{
  // Only make changes if state is not locked.
  if (initialState_.isLocked)
    return;

  // Rotate drawing methods.
  if (gameState->scene.isRotatingDrawingMethod) {
    gameState->scene.isRotatingDrawingMethod = false;

    switch (currentState_.tileDrawingMethod) {
      case TileDrawingMethod::Solid:
        currentState_.tileDrawingMethod = TileDrawingMethod::Grid;
        break;

      case TileDrawingMethod::Grid:
        currentState_.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;
        break;

      case TileDrawingMethod::GaussianCurvature:
        currentState_.tileDrawingMethod = TileDrawingMethod::Solid;
        break;
    }
  }

  // Select a drawing method.
  if (gameState->scene.isSettingDrawingMethodByNumber) {
    gameState->scene.isSettingDrawingMethodByNumber = false;

    switch (gameState->scene.numberOfDesiredDrawingMethod) {
      case 1:
        currentState_.tileDrawingMethod = TileDrawingMethod::Solid;
        break;

      case 2:
        currentState_.tileDrawingMethod = TileDrawingMethod::Grid;
        break;

      case 3:
        currentState_.tileDrawingMethod = TileDrawingMethod::GaussianCurvature;
        break;

      default:
        // Not a valid number.
        break;
    }
  }
}

SceneProperties::State
SceneProperties::state()
{
  return currentState_;
}

} // namespace kleindrive
