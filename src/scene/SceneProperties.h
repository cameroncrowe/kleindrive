//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scene/SceneProperties.h - SceneProperties struct definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SceneProperties struct definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_SCENEPROPERTIES_H
#define KLEIN_DRIVE_SCENEPROPERTIES_H

#include "src/collections/Colors.h"
#include "src/gamestate/GameState.h"
#include "src/types/ValueTypes.h"
#include <string>

namespace kleindrive {

/// Methods for drawing the tiles.
enum class TileDrawingMethod
{
  Solid,
  Grid,
  GaussianCurvature
};

/// Container for scene information.
//
// It's really more of a struct, but it can manipulate the information, too.
class SceneProperties
{
public:
  /// A set of scene properties.
  struct State
  {
  public:
    /// How far can a tile be before we stop drawing it.
    phys_t maxVisibleDistance{ std::numeric_limits<phys_t>::max() };

    /// How fast does lighting fade.
    phys_t lightDecayRate{ 0.001 };

    /// How much does light reflect off a surface.
    phys_t surfaceReflectiveness{ 0.3 };

    /// How much do we exaggerate curvature color.  This should be positive.
    phys_t curvatureColorExaggeration{ 1 };

    /// Drawing method.
    TileDrawingMethod tileDrawingMethod{ TileDrawingMethod::Solid };

    /// Color of the background.
    Color backgroundColor{ colors::Green };
    Color gridColor{ colors::Brown };

    // Name of the surface to be shown on buttons.
    std::string label{ "Unnamed Surface" };

    // Do we wish this information not to change during the game.
    bool isLocked{ false };
  };

  /// \name Constructors
  /// @{

  /// Populates the scene's state with default values.
  SceneProperties();

  /// Populate the scene's state with given values.
  explicit SceneProperties(const State& initialState);

  /// @}

  /// Returns the current state, modified according to the GameState.
  void state(GameState* gameState);

  // Returns the current state.
  State state();

  /// Resets the scene's properties to their initial state, if not locked.
  void reset();

private:
  /// The initial state of the scene properties.
  State initialState_;

  /// The current state of the scene properties.
  State currentState_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_SCENEPROPERTIES_H
