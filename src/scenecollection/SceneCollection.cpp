//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scenecollection/SceneCollection.cpp
//
//  The SceneCollection class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SceneCollection class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/scenecollection/SceneCollection.h"
#include "src/scene/Scene.h"
#include <memory>
#include <vector>

namespace kleindrive {

int
SceneCollection::numberOfScenes()
{
  return static_cast<int>(scenes_.size());
}

Scene*
SceneCollection::previewScene()
{
  using sz_t = decltype(scenes_)::size_type;
  return scenes_[static_cast<sz_t>(currentSceneNumber_)]->preview();
}

Scene*
SceneCollection::driveScene()
{
  using sz_t = decltype(scenes_)::size_type;
  return scenes_[static_cast<sz_t>(currentSceneNumber_)]->drive();
}

bool
SceneCollection::isValidSceneNumber(int sceneNumber)
{
  return (0 <= sceneNumber && sceneNumber < static_cast<int>(scenes_.size()));
}

SceneCollection::SceneCollection(std::vector<std::unique_ptr<Scene>>& sceneList)
  : scenes_{ std::move(sceneList) }
  , currentSceneNumber_{ 0 }
{
  assert(scenes_.size() > 0 && "Where are the scenes?");
  // 0 is always a valid scene number.

  // Everything should start off, but someone might mess this up.
  turnOff();
}

int
SceneCollection::currentSceneNumber()
{
  return currentSceneNumber_;
};

std::string
SceneCollection::sceneName(int sceneNumber)
{
  if (isValidSceneNumber(sceneNumber)) {
    using sz_t = decltype(scenes_)::size_type;
    return scenes_[static_cast<sz_t>(sceneNumber)]->label();
  }
  return "";
}

bool
SceneCollection::changeScene(int newSceneNumber)
{
  if (isValidSceneNumber(newSceneNumber)) {
    currentSceneNumber_ = newSceneNumber;
    // This is overkill, but simple.
    reset();
    resetSceneProperties();
    return true;
  }
  return false;

  // return isValidSceneNumber(newSceneNumber) && (currentSceneNumber_ =
  // newSceneNumber);
}

void
SceneCollection::reset()
{
  // This is overkill, but simple.
  for (auto& scene : scenes_)
    scene->reset();
  turnOff();
}

void
SceneCollection::resetSceneProperties()
{
  // This is overkill, but simple.
  for (auto& scene : scenes_)
    scene->resetSceneProperties();
  turnOff();
}

void
SceneCollection::turnOff()
{
  // This is overkill, but simple.
  for (auto& scene : scenes_)
    scene->turnOff();
}

} // namespace Klein
