//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/scenecollection/SceneCollection.h - SceneCollection class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the SceneCollection class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_SCENECOLLECTION_H
#define KLEINDRIVE_SCENECOLLECTION_H

#include "src/scene/Scene.h"
#include <memory>
#include <vector>

namespace kleindrive {

/// Manages a colleciton of Scenes and their needs.
class SceneCollection
{
public:
  /// Takes ownership of a collection of scenes.
  explicit SceneCollection(std::vector<std::unique_ptr<Scene>>& sceneList);

  /// Reports the current scene number.
  int currentSceneNumber();

  /// Reports the scene name of the scene with the given number.
  std::string sceneName(int sceneNumber);

  /// Changes the scene, and reports back if the scene was actually changed.
  bool changeScene(int newSceneNumber);

  /// Repturns the total number of scenes in the collection.
  int numberOfScenes();

  /// Turns the scenes off.
  ///
  /// Call this if scene won't be rendered immediatedly, and in  in the next
  // /frame
  void turnOff();

  /// Resets the scenes to their initial state, except for scene properties.
  void reset();

  /// Resets the scene properties to their initial states.
  void resetSceneProperties();

  /// Returns the current scene in preview mode.
  Scene* previewScene();

  /// REturns the current scene in drive mode.
  Scene* driveScene();

private:
  /// Returns true if there is a scene of the given number.
  bool isValidSceneNumber(int sceneNunber);

  /// List of scenes in the collection.
  std::vector<std::unique_ptr<Scene>> scenes_;

  /// Currently selected scene.
  int currentSceneNumber_;

};

} // namespace Klein

#endif // KLEINDRIVE_SCENECOLLECTION_H
