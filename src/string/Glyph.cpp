//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/glyph.cpp - The Glyph template class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Glyph template class implementation file.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_GLYPH_TPP
#define KLEINDRIVE_GLYPH_TPP

#include "src/string/Glyph.h"

namespace kleindrive {

template<int NUMBEROFROWS>
inline Glyph<NUMBEROFROWS>::Glyph()
{
  processPicture({});
}

template<int NUMBEROFROWS>
inline Glyph<NUMBEROFROWS>::Glyph(int kerning)
{
  // TODO This should be allowed to be zero.  I don't know why it isn't.
  assert(kerning > 0 && "Kerning must be positive.");
  assert(NUMBEROFROWS > 0 && "NUMBEROFROWS must be positive.");

  using sz_t = std::string::size_type;
  const std::string kerningString(static_cast<sz_t>(kerning), ' ');

  using sz_t = std::vector<std::string>::size_type;
  std::vector<std::string> kerningPicture(static_cast<sz_t>(NUMBEROFROWS),
                                          kerningString);
  processPicture(kerningPicture);
}

template<int NUMBEROFROWS>
inline Glyph<NUMBEROFROWS>::Glyph(std::initializer_list<std::string> picture)
{
  processPicture(std::vector<std::string>(picture));
}

template<int NUMBEROFROWS>
inline int
Glyph<NUMBEROFROWS>::numberOfColumns() const
{
  return numberOfColumns_;
}

template<int NUMBEROFROWS>
inline void
Glyph<NUMBEROFROWS>::processPicture(const std::vector<std::string>& picture)
{
  // We don't aim to be super efficient.

  // A container for our picture as strings.
  std::vector<std::string> stringPicture;

  // Fill stringPicture with up to numberOfColumns_ rows from the
  // initializer list picture, prepadding with empty strings as necessary.
  {
    using sz_t = std::vector<std::string>::size_type;

    const sz_t numberOfUsedRows =
      std::min(static_cast<sz_t>(NUMBEROFROWS), picture.size());
    const sz_t numberOfPaddingRows = NUMBEROFROWS - numberOfUsedRows;

    stringPicture = std::vector<std::string>(numberOfPaddingRows, "");
    auto bi = std::back_inserter(stringPicture);
    copy(picture.begin(),
         picture.begin() + static_cast<std::ptrdiff_t>(numberOfUsedRows),
         bi);
  }

  // Determine the numberOfColumns.
  {
    using sz_t = std::string::size_type;

    sz_t maxNumberOfColumns = 0;

    for (const auto& row : stringPicture)
      maxNumberOfColumns = std::max(maxNumberOfColumns, row.size());

    // This protects us from having to worry about size types.
    static const int wayTooManyColumns = 100000;

    assert(maxNumberOfColumns < static_cast<sz_t>(wayTooManyColumns) &&
           "Seriously, way too many columns!");

    numberOfColumns_ = static_cast<int>(maxNumberOfColumns);
  }

  // Pad the colulmns out to numberOfColumns_.
  for (auto& row : stringPicture) {
    using sz_t = decltype(stringPicture)::value_type::size_type;
    row.resize(static_cast<sz_t>(numberOfColumns_), ' ');
  }

  // Now convert stringPicture to it's characteristic function, in the form
  // of an array.
  {
    using sz_t = typename decltype(picture_)::size_type;
    picture_.reserve(static_cast<sz_t>(numberOfColumns_ * NUMBEROFROWS));
  }
  // Iterate over rows first, then columns, in accordance with index
  // function.
  using sz_t_row = decltype(stringPicture)::size_type;
  using sz_t_col = decltype(stringPicture)::value_type::size_type;

  for (int col = 0; col < numberOfColumns_; ++col) {
    for (int row = 0; row < NUMBEROFROWS; ++row) {
      const char c =
        stringPicture[static_cast<sz_t_row>(row)][static_cast<sz_t_col>(col)];
      const bool isSpace = std::isspace(static_cast<unsigned char>(c));
      picture_.push_back(isSpace ? 0 : 1);
    }
  }

} // processPicture(...)

// Print the Glyph with a box around it so we can see whitespace.
template<int NUMBEROFROWS>
inline void
Glyph<NUMBEROFROWS>::print() const
{
  using sz_t = std::string::size_type;
  const std::string horizontalBar(static_cast<sz_t>(NUMBEROFROWS + 2), '-');

  std::cout << horizontalBar << "\n";
  for (int row = 0; row < NUMBEROFROWS; ++row) {
    std::cout << "|";
    for (int col = 0; col < numberOfColumns_; ++col) {
      std::cout << ((characteristicFunction(row, col) == 1) ? '0' : ' ');
    }
    std::cout << "|\n";
  }
  std::cout << horizontalBar << "\n";
}

} // namespace kleindrive

#endif // KLEINDRIVE_GLYPH_TPP
