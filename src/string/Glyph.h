//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/glyph.h
//
//  The Glyph template class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Glyph template class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_GLYPH_H
#define KLEINDRIVE_GLYPH_H

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

namespace kleindrive {

/// Represents a glyph that takes exactly the given number of rows, and any
/// number of columns.
template<int NUMBEROFROWS>
class Glyph final
{
public:
  /// \name Constructors.
  /// @{

  /// Construct the empty Glyph.
  Glyph();

  /// Construct a whitespace kerning Glyph of given width.
  explicit Glyph(int kerning);

  /// Construct a Glyph from an initializer list.
  explicit Glyph(std::initializer_list<std::string> picture);

  /// @}

  /// Returns the number of columns used in the Glyph.
  int numberOfColumns() const;

  /// The characteristic function of the picture of the glyph.
  ///
  /// Returns 1 if there is a non-whitespace character in the tile at the given
  /// position, and 0- otherwise.
  //
  // We put this here to force it inline.
  inline int characteristicFunction(int row, int col) const
  {
    using sz_t = typename decltype(picture_)::size_type;
    return picture_[static_cast<sz_t>(indexOfTile(row, col))];
  }

  /// Prints the glyph to standard output.
  inline void print() const;

private:
  /// Returns the index of the tile in given row and column.
  int indexOfTile(int row, int col) const { return row + NUMBEROFROWS * col; }

  /// Turn a vector of strings into a vector of ints, ie picture_.
  inline void processPicture(const std::vector<std::string>& picture);

  /// Number of columns used by the Glyph.
  int numberOfColumns_;

  /// Image of picture in 0s an 1s in row major order.
  std::vector<int> picture_;
}; // class Glyph

} // namespace kleindrive

#include "src/string/Glyph.cpp"

#endif // KLEINDRIVE_GLYPH_H
