//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDFont.tpp - The KDFont template class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDFont template class implementation.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDFONT_TPP
#define KLEINDRIVE_KDFONT_TPP

#include "src/string/KDFont.h"

namespace kleindrive {

template<int NUMBEROFROWS>
inline KDFont<NUMBEROFROWS>::KDFont(int kerning, int amperstandOffset)
  : kerningGlyph_(kerning)
  , amperstandOffset_{ amperstandOffset }
  , kerning_{ kerning }
{
  assert(NUMBEROFROWS > 0 && "What kind of font is this with now rows?!");
}

template<int NUMBEROFROWS>
inline void
KDFont<NUMBEROFROWS>::add(int identifier,
                          const std::initializer_list<std::string>& picture)
{
  glyphs_[identifier] = Glyph<NUMBEROFROWS>(picture);
}

template<int NUMBEROFROWS>
inline int
KDFont<NUMBEROFROWS>::amperstandOffset() const
{
  return amperstandOffset_;
}

template<int NUMBEROFROWS>
inline Glyph<NUMBEROFROWS>
KDFont<NUMBEROFROWS>::toGlyph(int identifier) const
{
  return glyphs_[identifier];
}

template<int NUMBEROFROWS>
inline Glyph<NUMBEROFROWS>
KDFont<NUMBEROFROWS>::kerningGlyph() const
{
  return kerningGlyph_;
}

template<int NUMBEROFROWS>
inline int
KDFont<NUMBEROFROWS>::kerning() const
{
  return kerning_;
}

template<int NUMBEROFROWS>
inline int
KDFont<NUMBEROFROWS>::numberOfRows() const
{
  return NUMBEROFROWS;
}

} // namespace kleindrive

#endif // KLEINDRIVE_KDFONT_TPP
