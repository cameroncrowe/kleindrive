//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDFont.h - The KDFont defintiion file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDFont definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDFONT_H
#define KLEINDRIVE_KDFONT_H

#include "src/string/Glyph.h"
#include <cassert>
#include <map>
#include <string>

namespace kleindrive {

/// Represents a font with exactly the given nunber of rows.
template<int NUMBEROFROWS>
class KDFont
{
public:
  /// Constructs a font with given kerning.
  ///
  /// If more characters are needed, they can be given an offset and proceeded
  /// wiht an amperstand in the string.  See KDString.
  explicit KDFont(int kerning, int amperstandOffset = 0);

  /// Add a symbol to the list, with rows in the initializer list.
  void add(int identifier, const std::initializer_list<std::string>& picture);

  /// Returns the offset given by amperstand.
  int amperstandOffset() const;

  /// Returns the glyph with the given identifier.
  Glyph<NUMBEROFROWS> toGlyph(int identifier) const;

  /// Returns a kerning glyph.
  Glyph<NUMBEROFROWS> kerningGlyph() const;

  /// Returns the kerning width.
  int kerning() const;

  /// Returns the number of rows of a string in this font.
  int numberOfRows() const;

private:
  /// Map from integers (characters, or shifted characters) to glyphs.
  mutable std::map<int, Glyph<NUMBEROFROWS>> glyphs_;

  /// A glyph used as whitespace for kerning.
  Glyph<NUMBEROFROWS> kerningGlyph_;

  /// Offset used to get a second characterset.
  int amperstandOffset_;

  /// Width of kerning character, must be positive. TODO: allow 0.
  int kerning_;

};

} // namespace kleindrive

#include "src/string/KDFont.cpp"

#endif // KLEINDRIVE_KDFONT_H
