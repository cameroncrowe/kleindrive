//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDString.tpp - The KDString template class implementation file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDString template class implementation file.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/string/KDString.h"

namespace kleindrive {

inline int
KDString::indexOfTile(int row, int col) const
{
  return row + numberOfRows_ * col;
}

template<int NUMBEROFROWS>
KDString::KDString(const KDFont<NUMBEROFROWS>& font, const std::string& string)
  : numberOfRows_{ NUMBEROFROWS }
  , numberOfColumns_{ 0 }
{
  processString(font, string);
}

inline bool
KDString::hasBell() const
{
  return hasBell_;
}

inline int
KDString::characteristicFunction(int row, int col) const
{
  using sz_t = decltype(picture_)::size_type;

  if ((0 <= row) && (row < numberOfRows_) && (0 <= col) &&
      (col < numberOfColumns_))
    return picture_[static_cast<sz_t>(indexOfTile(row, col))];
  return 0;
}

inline int
KDString::numberOfRows() const
{
  return numberOfRows_;
}

inline int
KDString::numberOfColumns() const
{
  return numberOfColumns_;
}

// This could be done more efficiently, but it is only inefficient for a very
// small time before the animation starts.
template<int NUMBEROFROWS>
inline void
KDString::processString(const KDFont<NUMBEROFROWS>& font,
                        const std::string& string)
{
  // If a string has the bell character, it is to alert us
  // to lead differently.  You can almost certainly ignore this.
  if (std::string::npos != string.find('\a')) {
    hasBell_ = true;
  }
  std::vector<Glyph<NUMBEROFROWS>> glyphs;

  // Convert string to glyphs using font.
  glyphs.reserve(string.length());

  const int amperstandOffset = font.amperstandOffset();
  int offset = 0;

  for (const char c : string) {
    // A spcial character applies an offset to a new charset.  The amperstand
    // is only special if the amperstand offset is nonzero.
    if (amperstandOffset != 0 && c == '&') {
      offset = amperstandOffset;
      continue;
    }
    // If we're not a special character, and we're offset, then we apply the
    // offset to our character, and reset the offset.
    if (offset != 0) {
      glyphs.push_back(font.toGlyph(static_cast<int>(c) + offset));
      offset = 0;
      continue;
    }
    // No offset, not a special symbol
    glyphs.push_back(font.toGlyph(static_cast<int>(c)));
  }

  // Now we can build the picture of the string column by column.  This is
  // somewhat inefficient, but simple, and not overly bad.
  pictureRows_.resize(NUMBEROFROWS);
  // We do
  //
  //   kern, glyph, kern, ..., kern
  //
  const Glyph<NUMBEROFROWS> kerningGlyph = font.kerningGlyph();

  addGlyphToPictureRow(kerningGlyph);
  for (const Glyph<NUMBEROFROWS>& glyph : glyphs) {
    addGlyphToPictureRow(glyph);
    addGlyphToPictureRow(kerningGlyph);
  }

  // Make a picture of the string.  We are assured at least one row.
  // And we are assured that the size of each row is the same.
  {
    numberOfColumns_ = static_cast<int>(pictureRows_[0].size());

    using sz_t = decltype(picture_)::size_type;
    picture_.reserve(static_cast<sz_t>(NUMBEROFROWS * numberOfColumns_));

    for (int col = 0; col < numberOfColumns_; ++col) {
      for (int row = 0; row < NUMBEROFROWS; ++row) {
        using sz_t_row = decltype(pictureRows_)::size_type;
        using sz_t_col = decltype(pictureRows_)::value_type::size_type;
        ;
        // Gross--we have to iterate the wrong way over pictureRows.
        picture_.push_back(
          pictureRows_[static_cast<sz_t_row>(row)][static_cast<sz_t_col>(col)]);
      }
    }
  }
  // Delete the pictureRows, for all practical purposes.
  pictureRows_.resize(0);
}

template<int NUMBEROFROWS>
inline void
KDString::addGlyphToPictureRow(const Glyph<NUMBEROFROWS>& glyph)
{
  assert(NUMBEROFROWS == numberOfRows_ && "Glyph has wrong number of rows");

  const int numberOfColumns = glyph.numberOfColumns();

  using sz_t = decltype(pictureRows_)::size_type;

  for (int row = 0; row < NUMBEROFROWS; ++row) {
    for (int col = 0; col < numberOfColumns; ++col) {
      pictureRows_[static_cast<sz_t>(row)].push_back(
        glyph.characteristicFunction(row, col));
    }
  }
}

// Print the string with a box around it to show whitespace.
inline void
KDString::print() const
{
  using sz_t = std::string::size_type;
  const std::string horizontalBar(static_cast<sz_t>(numberOfColumns_) + 2, '-');
  std::cout << horizontalBar << "\n";
  for (int i = 0; i < numberOfRows_; ++i) {
    std::cout << "|";
    for (int j = 0; j < numberOfColumns_; ++j) {
      std::cout << ((characteristicFunction(i, j) == 1) ? '0' : ' ');
    }
    std::cout << "|\n";
  }
  std::cout << horizontalBar << "\n";
}

} // namespace kleindrive
