//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDString.h - The KDString defintiion file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDString definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDSTRING_H
#define KLEINDRIVE_KDSTRING_H

#include "src/string/Glyph.h"
#include "src/string/KDFont.h"
#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace kleindrive {

/// Represents a string of glyphs.
class KDString
{
public:
  /// Builds a string.
  template<int NUMBEROFROWS>
  KDString(const KDFont<NUMBEROFROWS>& font, const std::string& string);

  /// Returns wether or not the string was fed a bell.
  ///
  /// This is a hack used to signal to the KDTextBlock that it should change
  /// its leading behavior.
  bool hasBell() const;

  /// The tharacteristic function of the picture of the glyph.
  ///
  /// Returns 1 if there is a non-whitespace character in the tile at the given
  /// position, and 0- otherwise.
  inline int characteristicFunction(int row, int col) const;

  /// Returns the number of rows in the string.
  int numberOfRows() const;

  /// Returns the number of columns the string fills up.
  int numberOfColumns() const;

  /// Prints the string to standard output.
  inline void print() const;

private:
  /// Processes the string into a row major picture of 1s and 0s.
  template<int NUMBEROFROWS>
  inline void processString(const KDFont<NUMBEROFROWS>& font,
                            const std::string& string);

  /// Adds a glyph to the picture row.
  template<int NUMBEROFROWS>
  inline void addGlyphToPictureRow(const Glyph<NUMBEROFROWS>& glyph);

  /// The index of a tile with given column and row, in the picture_.  
  int indexOfTile(int row, int col) const;

  /// How many rows is the string.
  int numberOfRows_;

  /// How many columns is the string.
  int numberOfColumns_;

  /// Picture of rows
  std::vector<std::vector<int>>
    pictureRows_; // TODO: this is superfluous -- remove me!

  /// Picture of rows stored in row major order.
  std::vector<int> picture_;

  /// If the string has a bell character--which is of no use to use,
  /// otherwise--then KDTexdtBlock adjusts it's leading width to the kerning
  /// width.  See the nice spacing in the Instructions screen.
  bool hasBell_{ false }; // This is a hack used to change the leading.
};

} // nemespace kleindrive

#include "src/string/KDString.cpp"

#endif // KLEINDRIVE_KDSTRING_H
