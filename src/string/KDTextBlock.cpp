//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDTextBlock.h - The KDTextBlock defintiion file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDTextBlock definition.
///
//===----------------------------------------------------------------------===//

#include "src/string/KDTextBlock.h"
#include "src/string/Glyph.h"
#include "src/string/KDFont.h"
#include "src/string/KDString.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <vector>

namespace kleindrive {

template<int NUMBEROFROWS>
KDTextBlock::KDTextBlock(const KDFont<NUMBEROFROWS>& font,
                         int numberOfColumns,
                         int leading,
                         const std::string& string)
  : numberOfRows_{ 0 }
  , numberOfColumns_{ numberOfColumns }
  , leading_{ leading }
{
  processString(font, string);
}

inline bool
KDTextBlock::hasWordThatIsTooWide() const
{
  return hasWordThatIsTooWide_;
}

/*
inline int
KDTextBlock::indexOfTile(int row, int col) const
{
  return row + numberOfRows_ * col;
}
*/

inline int
KDTextBlock::characteristicFunction(int row, int col) const
{
  using sz_t_row = decltype(pictureRows_)::size_type;
  using sz_t_col = decltype(pictureRows_)::value_type::size_type;

  if ((0 <= row) && (static_cast<sz_t_row>(row) < pictureRows_.size()) &&
      (0 <= col) &&
      (static_cast<sz_t_col>(col) <
       pictureRows_[static_cast<sz_t_row>(row)].size()))
    return pictureRows_[static_cast<sz_t_row>(row)][static_cast<sz_t_col>(col)];
  return 0;
}

inline int
KDTextBlock::numberOfRows() const
{
  return numberOfRows_;
}

inline int
KDTextBlock::numberOfColumns() const
{
  return numberOfColumns_;
}

// This could be done more efficiently, but it is only inefficient for a very
// small time before the animation starts.
template<int NUMBEROFROWS>
void
KDTextBlock::processString(const KDFont<NUMBEROFROWS>& font,
                           const std::string& string)
{
  // Break the string into paragraphs.
  std::vector<std::string> paragraphs;
  {
    std::string paragraph;
    std::istringstream iss(string);
    while (std::getline(iss, paragraph, '\n'))
      paragraphs.push_back(paragraph);
  }

  // Process the paragraphs, adding empty lines betwee.
  {
    using sz_t = decltype(paragraphs)::size_type;

    const sz_t numberOfParagraphs = paragraphs.size();
    if (numberOfParagraphs == 0) {
      return;
    }
    // We're guaranteed at least one paragraph.
    sz_t P = 0;
    processParagraph(font, paragraphs[P]);

    ++P;
    // While paragraphs remain, add a new line and next paragraph.
    while (P < numberOfParagraphs) {
      processParagraph(font, paragraphs[P]);
      ++P;
    }
  }
}

template<int NUMBEROFROWS>
void
KDTextBlock::processParagraph(const KDFont<NUMBEROFROWS>& font,
                              const std::string& paragraph)
{
  // Process the paragraph in to words.
  std::vector<std::string> words;
  {
    /* Adding a tab here is now obscelete!
     *

    // Add tab
    const std::string tab(tabLength_,' ');
    words.push_back(tab);

    *
    */

    // Add words from paragraph.
    std::string word;
    std::istringstream iss(paragraph);
    while (std::getline(iss, word, ' '))
      words.push_back(word);
  }
  // Record the cols of each word
  std::vector<int> wordCols;
  {
    for (const auto w : words) {
      // We're lazy--this can be done faster without actually making the word.
      // What can you do in a NY microsecond?
      // Who even reads these comments?
      // Dear Diary:
      //
      //   It has been far too long since I saw another human being...
      //
      const KDString kdString(font, w);
      const int cols = kdString.numberOfColumns();
      wordCols.push_back(cols);

      // Oh no!  A words was too wide to fit.
      hasWordThatIsTooWide_ = true;

      // At least I don't crash like you:
      // assert( cols < numberOfColumns_ && "Word too long for textbox.");
    }
  }

  // Compute the number of words in each row, and denote the index of the first
  // word in each row.
  std::vector<int> indexOfFirstWords;
  std::vector<int> wordsPerLine;
  {

    const int spaceCols =
      static_cast<int>(KDString(font, " ").numberOfColumns());
    const int numberOfWords = static_cast<int>(words.size());
    for (int W = 0; W < numberOfWords; ++W) {

      // If we're here, there's at least one more word in the queue.
      using sz_t = decltype(wordCols)::size_type;

      int index = W;
      int noWords = 1;
      int cols = wordCols[static_cast<sz_t>(W)];
      // While there are still words left, and the next word fits, add the next
      // word.
      while (W < numberOfWords - 1 &&
             cols + spaceCols + wordCols[static_cast<sz_t>(W + 1)] <=
               numberOfColumns_) {
        cols += spaceCols + wordCols[static_cast<sz_t>(W + 1)];
        ++W;
        ++noWords;
      }
      // Index is the index of the first row.
      // Words is the number of words in this row.
      // NOTE: This always forces at least one word in to the line,
      // even if the word is wider than the number of columns.
      indexOfFirstWords.push_back(index);
      wordsPerLine.push_back(noWords);
    }
  }

  // Now form lines
  std::vector<std::string> lines;
  {
    const std::string space = " ";
    const int numberOfLines = static_cast<int>(wordsPerLine.size());
    for (int L = 0; L < numberOfLines; ++L) {
      using sz_t = decltype(wordsPerLine)::size_type;
      const int numberOfWords = wordsPerLine[static_cast<sz_t>(L)];

      using sz_t = decltype(indexOfFirstWords)::size_type;
      const int index = indexOfFirstWords[static_cast<sz_t>(L)];

      // Every line has at least one word
      using sz_t = decltype(words)::size_type;
      std::string line = words[static_cast<sz_t>(index)];
      // While there are more words in the line, add them.
      for (int W = 1; W < numberOfWords; ++W) {
        using sz_t = decltype(words)::size_type;
        // Add remaining words with spaces.
        line += space;
        line += words[static_cast<sz_t>(index + W)];
      }

      lines.push_back(line);
    }
  }

  // Now for KDString from the lines.
  std::vector<KDString> kdLines;
  for (const std::string line : lines) {
    const KDString kdLine(font, line);
    kdLines.push_back(kdLine);
  }

  // Now compute the pictureRows_;
  {
    using sz_t = std::vector<int>::size_type;
    const std::vector<int> leadingRow(static_cast<sz_t>(numberOfColumns_), 0);
    /*
    // Add leading;
    // If the line has a bell in it, we don't add leading before it.
    if(kdLines.size()>0)
      if(kdLines[0].hasBell())
        for(int row = 0; row < leading_; ++row)
          pictureRows_.push_back(leadingRow);
*/
    // Add lines and leading between
    for (const auto& kdLine : kdLines) {
      // Rows of line
      for (int row = 0; row < NUMBEROFROWS; ++row) {
        std::vector<int> pictureRow;
        for (int col = 0; col < numberOfColumns_; ++col) {
          const int pixel = kdLine.characteristicFunction(row, col);
          pictureRow.push_back(pixel);
        }
        pictureRows_.push_back(pictureRow);
      }

      // Add leading, unless someone tucked a bell in,
      if (kdLine.hasBell()) {
        // The bell means we are supposed to add kerning, in
        // place of leading.
        for (int row = 0; row < font.kerning(); ++row) {
          pictureRows_.push_back(leadingRow);
        }
      } else {
        for (int row = 0; row < leading_; ++row) {
          pictureRows_.push_back(leadingRow);
        }
      }
    }
  }

  numberOfRows_ = static_cast<int>(pictureRows_.size());
  // And that is what we wanted!  Not super efficiently, though :)

} // processString(...)

// Print the string with a box around it to show whitespace.
inline void
KDTextBlock::print() const
{
  using sz_t = std::string::size_type;
  const std::string horizontalBar(static_cast<sz_t>(numberOfColumns_ + 2), '-');
  std::cout << horizontalBar << "\n";
  for (int i = 0; i < numberOfRows_; ++i) {
    std::cout << "|";
    for (int j = 0; j < numberOfColumns_; ++j) {
      std::cout << ((characteristicFunction(i, j) == 1) ? '0' : ' ');
    }
    std::cout << "|\n";
  }
  std::cout << horizontalBar << "\n";
}

} // nemespace kleindrive
