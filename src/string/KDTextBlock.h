//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/string/KDTextBlock.h - The KDTextBlock defintiion file.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the KDTextBlock definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEINDRIVE_KDTEXTBLOCK_H
#define KLEINDRIVE_KDTEXTBLOCK_H

#include "src/string/Glyph.h"
#include "src/string/KDFont.h"
#include "src/string/KDString.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <vector>

namespace kleindrive {

/// Represents a string of glyphs.
class KDTextBlock
{
public:
  /// Constructs a block of text in given font, with given leading, made from
  /// the given string in such a way that it fits into the given number of
  /// columns
  template<int NUMBEROFROWS>
  KDTextBlock(const KDFont<NUMBEROFROWS>& font,
              int numberOfColumns,
              int leading,
              const std::string& string);

  /// The tharacteristic function of the picture of the glyph.
  ///
  /// Returns 1 if there is a non-whitespace character in the tile at the given
  /// position, and 0- otherwise.
  inline int characteristicFunction(int row, int col) const;

  /// Returns the number of rows the text block fills up.
  int numberOfRows() const;

  /// Returns the number of columns the texd block fills up.
  int numberOfColumns() const;

  /// Returns true if the textbox was given a word that is too wide to fit.
  bool hasWordThatIsTooWide() const;

  /// Prints the text block to standar output.
  inline void print() const;

private:
  /// Processes the string.
  template<int NUMBEROFROWS>
  void processString(const KDFont<NUMBEROFROWS>& font,
                     const std::string& string);

  /// Adds a paragraph to the pictureRow.
  template<int NUMBEROFROWS>
  void processParagraph(const KDFont<NUMBEROFROWS>& font,
                        const std::string& string);

  /// How many rows does the block take.
  int numberOfRows_;

  /// How many columns does the block use.
  int numberOfColumns_;

  /// Number of rows used for leading.
  int leading_;

  /// Is there a word that is just too wide for the box? (It will just hang off
  /// the ednd).
  bool hasWordThatIsTooWide_{ false };

  /// Rows of the picture of the block, in zeros and ones.
  std::vector<std::vector<int>> pictureRows_;

  /*
    /// returns the index of a tile.
    int indexOfTile(int row, int col) const;
  */
};

} // nemespace kleindrive

#include "src/string/KDTextBlock.cpp"

#endif // KLEINDRIVE_KDTEXTBLOCK_H
