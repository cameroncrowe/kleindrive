KD Strings
==========

Strings were designed to be drawn on surfaces, not for use in the menu screens.  They are also not designed to be performance critical or very flexible.  They have been pushed way past their design specs to be used in the gui, which required at least one hack--the bell is used to signal a very small carriage return..  

Kerning is awkwardly built into the font, but not leading.  This was only necessary for getting the font in the gui.

I don't recommend pushing them much further without a redesign.

For examples of font definitions, see

  src/collections/fonts

For examples of how to use the fonts for their intended purpose, see

     src/collections/welcomescene/KDWelcomeScene.cpp
    src/collections/cameraoperators/FontReader.h


