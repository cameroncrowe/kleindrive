//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/Domain.h - Definition of Domain class.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the domain class.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_DOMAIN_H
#define KLEIN_DRIVE_DOMAIN_H

#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Different ways of gluing the a pair of opposite edges of a rectangle.
///
/// \param None Do not glue the opposete edges
///
/// \param RightWay Glue the opposing edges as if to make a tube.
///
/// \param WrongWay Glue the opposing edges as if to make a Mobius band.
///
/// \param PassThroughRightWay Don't correct the visual defect that arrises
///        from gluing, but pretend it's glued the RightWay.
///
/// \param PassthroughWrongWay Don't correct the visual defect that arrises
///        from gluing, but pretend it's glued the WrongWay.
///
/// \param MinCrushedToPoint The min edge is quotiented out to a point.
///
/// \param MaxCrushedToPoint The max edge is quotiented out to a point.
///
/// \param MinAndMaxCrushedToPoint The min and max edges are quotiented to a
///        point.
///
///
enum class Gluing
{
  NotGlued,
  RightWay,
  WrongWay,
  PassThroughRightWay,
  PassThroughWrongWay,
  MinCrushedToPoint,
  MaxCrushedToPoint,
  MinAndMaxCrushedToPoint,
};

/// Represents a rectangular domain whose opposite edges can be glued together.
///
/// The domain has coordinates (\param u,\param v) where
/// > uMin <= u <= uMax
/// > vMin <= v <= vMax
class Domain
{

public:
  /// \name Constructors.
  /// @{

  /// Constructs a Domain with given size, and with unglued edges.
  Domain(phys_t uMin, phys_t uMax, phys_t vMin, phys_t vMax)
    : uMin_{ uMin }
    , uMax_{ uMax }
    , uDirection_{ Gluing::NotGlued }
    , vMin_{ vMin }
    , vMax_{ vMax }
    , vDirection_{ Gluing::NotGlued }
  {}

  /// Construts the domain of given size and gluing options.
  Domain(phys_t uMin,
         phys_t uMax,
         Gluing uDirection,
         phys_t vMin,
         phys_t vMax,
         Gluing vDirection)
    : uMin_{ uMin }
    , uMax_{ uMax }
    , uDirection_{ uDirection }
    , vMin_{ vMin }
    , vMax_{ vMax }
    , vDirection_{ vDirection }
  {}

  /// @}

  /// \name Reveals the dimensions, range and gluing options.
  /// @{

  /// Returns the minimum u-value of the domain.
  inline phys_t uMin() const { return uMin_; }

  /// Returns the maximum u-value of the domain.
  inline phys_t uMax() const { return uMax_; }

  /// Returns the range of u-values in the domain.
  inline phys_t uRange() const { return uMax_ - uMin_; }

  /// Returns the manner of gluing in the u direction.
  inline Gluing uDirection() const { return uDirection_; }

  /// Returns the minimum v-value of the domain.
  inline phys_t vMin() const { return vMin_; }

  /// Returns the maximum v-value of the domain.
  inline phys_t vMax() const { return vMax_; }

  /// Returns the range of v-values of the domain.
  inline phys_t vRange() const { return vMax_ - vMin_; }

  /// Returns the manner of gluing in the v range.
  inline Gluing vDirection() const { return vDirection_; }

  /// @}
protected:
  /// Minimal u value in the domain.
  phys_t uMin_;

  /// Max u value in the domain.
  phys_t uMax_;

  /// Gluing in the u direction.
  Gluing uDirection_;

  /// Min v value in the domain.
  phys_t vMin_;

  /// Max v value in the domain.
  phys_t vMax_;

  /// Gluing in the v direction.
  Gluing vDirection_;
};

} // namespace kleindrive

#endif // KLEIN_DRIVE_DOMAIN_H
