//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/Param.cpp - Param class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the implementation of the Param class.
///
//===----------------------------------------------------------------------===//

#include "src/surface/Param.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

// Adds a dummy time variable to f and derivatives.
Param::Param(const F2Vec3& f,
             const F2Vec3& f_u,
             const F2Vec3& f_v,
             const F2Vec3& f_uu,
             const F2Vec3& f_uv,
             const F2Vec3& f_vv)
{
  auto u = std::placeholders::_1;
  auto v = std::placeholders::_2;

  f_    = std::bind(f    ,u ,v); 
  f_u_  = std::bind(f_u  ,u ,v);
  f_v_  = std::bind(f_v  ,u ,v);
  f_uu_ = std::bind(f_uu ,u ,v);
  f_uv_ = std::bind(f_uv ,u ,v);
  f_vv_ = std::bind(f_vv ,u ,v);
}

// Stores the functions just as they are.
Param::Param(const F2p1Vec3& f,
             const F2p1Vec3& f_u,
             const F2p1Vec3& f_v,
             const F2p1Vec3& f_uu,
             const F2p1Vec3& f_uv,
             const F2p1Vec3& f_vv)
  : f_{ f }
  , f_u_{ f_u }
  , f_v_{ f_v }
  , f_uu_{ f_uu }
  , f_uv_{ f_uv }
  , f_vv_{ f_vv }
{ }

Mat22
Param::g(phys_t u, phys_t v, phys_t t) const
{
  Mat32 m;

  m.col(0) = f_u_(u, v, t);
  m.col(1) = f_v_(u, v, t);

  return m.transpose() * m;
};

phys_t
Param::gaussianCurvature(phys_t u, phys_t v, phys_t t) const
{
  Vec3 n = f_n(u, v, t);
  phys_t L = f_uu(u,v,t).dot(n);
  phys_t M = f_uv(u, v, t).dot(n);
  phys_t N = f_vv(u, v, t).dot(n);

  Mat22 SFF;
  SFF << L, M,
         M, N;

  Mat22 g_ = g(u, v, t);

  return SFF.determinant() / g_.determinant();
}

Mat22
Param::g_inv(phys_t u, phys_t v, phys_t t) const
{
  return g(u, v, t).inverse();
};

Mat22
Param::g_u(phys_t u, phys_t v, phys_t t) const
{
  Mat32 m;

  m.col(0) = f_u_(u, v, t);
  m.col(1) = f_v_(u, v, t);

  Mat32 m_u;

  m_u.col(0) = f_uu_(u, v, t);
  m_u.col(1) = f_uv_(u, v, t);

  return m.transpose() * m_u + m_u.transpose() * m;
};

Mat22
Param::g_v(phys_t u, phys_t v, phys_t t) const
{
  Mat32 m;

  m.col(0) = f_u_(u, v, t);
  m.col(1) = f_v_(u, v, t);

  Mat32 m_v;

  m_v.col(0) = f_uv_(u, v, t);
  m_v.col(1) = f_vv_(u, v, t);

  return m.transpose() * m_v + m_v.transpose() * m;
};



// Returns the Christoffel symbols with upper index associated with the u-
// direction,  as a symmetric 2x2 matrix.
// matrix.
Mat22
Param::C_u(phys_t u, phys_t v, phys_t t) const
{
  Mat22 g_ = g(u, v, t);
  Mat22 g_inv = g_.inverse();
  Mat22 g_u_ = g_u(u, v, t);
  Mat22 g_v_ = g_v(u, v, t);

  Mat22 C_u_;

//C_k_(i, j) = 0.5 * (g_inv(k, m) * (g_j_(m, i) + g_i_(m, j) - g_m_(i, j))

  C_u_(0, 0) = 0.5 * (g_inv(0, 0) * (g_u_(0, 0) + g_u_(0, 0) - g_u_(0, 0)) +
                      g_inv(0, 1) * (g_u_(1, 0) + g_u_(1, 0) - g_v_(0, 0)));

  C_u_(0, 1) = 0.5 * (g_inv(0, 0) * (g_v_(0, 0) + g_u_(0, 1) - g_u_(0, 1)) +
                      g_inv(0, 1) * (g_v_(1, 0) + g_u_(1, 1) - g_v_(0, 1)));

  C_u_(1, 0) = C_u_(0, 1);

  C_u_(1, 1) = 0.5 * (g_inv(0, 0) * (g_v_(0, 1) + g_v_(0, 1) - g_u_(1, 1)) +
                      g_inv(0, 1) * (g_v_(1, 1) + g_v_(1, 1) - g_v_(1, 1)));

  return C_u_;
};

// Returns the Christoffel symbols with upper index associated with the v-
// direction, as a symmetric 2x2 matrix.
Mat22
Param::C_v(phys_t u, phys_t v, phys_t t) const
{
  Mat22 g_ = g(u, v, t);
  Mat22 g_inv = g_.inverse();
  Mat22 g_u_ = g_u(u, v, t);
  Mat22 g_v_ = g_v(u, v, t);

  Mat22 C_v_;

//C_k_(i, j) = 0.5 * (g_inv(k, m) * (g_j_(m, i) + g_i_(m, j) - g_m(i, j))

  C_v_(0, 0) = 0.5 * (g_inv(1, 0) * (g_u_(0, 0) + g_u_(0, 0) - g_u_(0, 0)) +
                      g_inv(1, 1) * (g_u_(1, 0) + g_u_(1, 0) - g_v_(0, 0)));

  C_v_(0, 1) = 0.5 * (g_inv(1, 0) * (g_v_(0, 0) + g_u_(0, 1) - g_u_(0, 1)) +
                      g_inv(1, 1) * (g_v_(1, 0) + g_u_(1, 1) - g_v_(0, 1)));

  C_v_(1, 0) = C_v_(0, 1);

  C_v_(1, 1) = 0.5 * (g_inv(1, 0) * (g_v_(0, 1) + g_v_(0, 1) - g_u_(1, 1)) +
                      g_inv(1, 1) * (g_v_(1, 1) + g_v_(1, 1) - g_v_(1, 1)));

  return C_v_;
};

} // namespace kleindrive
