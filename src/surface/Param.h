//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/Param.h - Param class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the definition of the Param class.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_PARAM_H
#define KLEIN_DRIVE_PARAM_H

#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Type of a function mapping two space variables to 3D vectors
using F2Vec3 = std::function<Vec3(phys_t, phys_t)>;

/// Type of a function mapping two space plus one time variable to 3D vectors.
using F2p1Vec3 = std::function<Vec3(phys_t, phys_t, phys_t)>;

/// Represents a time-depentent parameterization of a surface in 3-space.
class Param
{
public:
  /// \name Constructors.
  /// @{

  /// \brief Constructs a Param from data that is independent of time.
  Param(const F2Vec3& f,
        const F2Vec3& f_u,
        const F2Vec3& f_v,
        const F2Vec3& f_uu,
        const F2Vec3& f_uv,
        const F2Vec3& f_vv);

  /// \brief Constructs a Param from data that depends on time.
  Param(const F2p1Vec3& f,
        const F2p1Vec3& f_u,
        const F2p1Vec3& f_v,
        const F2p1Vec3& f_uu,
        const F2p1Vec3& f_uv,
        const F2p1Vec3& f_vv);
  /// @}

  /// \name Defining function and its derivatives.
  /// @{

  /// Returns the parameterization f.
  inline Vec3 f(phys_t u, phys_t v, phys_t t) const { return f_(u, v, t); }

  /// Returns the first derivatives of f in the u direction
  inline Vec3 f_u(phys_t u, phys_t v, phys_t t) const { return f_u_(u, v, t); }

  /// Returns the first derivative of f in the v direction.
  inline Vec3 f_v(phys_t u, phys_t v, phys_t t) const { return f_v_(u, v, t); }

  /// Returns the second order partial derivative in the u direction.
  inline Vec3 f_uu(phys_t u, phys_t v, phys_t t) const
  {
    return f_uu_(u, v, t);
  }

  /// Returns the second order partial derivative in the u and v directions.
  inline Vec3 f_uv(phys_t u, phys_t v, phys_t t) const
  {
    return f_uv_(u, v, t);
  }

  /// Returns the second order partila derivative in the v direction.
  inline Vec3 f_vv(phys_t u, phys_t v, phys_t t) const
  {
    return f_vv_(u, v, t);
  }

  /// @}

  /// \name Differential geometric information.
  /// @{

  /// \brief Returns the normal vector to the surface at f.
  /// The normal vector \param f_n satisfies
  /// > (f_u, f_v, f_n)
  /// is right-handed.
  inline Vec3 f_n(phys_t u, phys_t v, phys_t t) const
  {
    Vec3 r = f_u_(u, v, t).cross(f_v_(u, v, t));
    return r.normalized();
  };

  /// Returns the pullback metric g, via f.
  Mat22 g(phys_t u, phys_t v, phys_t t) const;

  /// Returns the Gaussian Curvature.
  phys_t gaussianCurvature(phys_t u, phys_t v, phys_t t) const;

  /// Returns the inverse of the pullback metric.
  Mat22 g_inv(phys_t u, phys_t v, phys_t t) const;

  /// Returns the first derivative of g in the u direction.
  Mat22 g_u(phys_t u, phys_t v, phys_t t) const;

  /// Returns the first derivative of g in the v direction.
  Mat22 g_v(phys_t u, phys_t v, phys_t t) const;

  /// Returns the Christoffel symbols with upper index u.
  Mat22 C_u(phys_t u, phys_t v, phys_t t) const;

  /// Returns the Christoffel symbols with upper index v.
  Mat22 C_v(phys_t u, phys_t v, phys_t t) const;

  /// @}

private:
  /// Mapping from domain to Euclidean 3-space, and derivatives.
  F2p1Vec3 f_, f_u_, f_v_, f_uu_, f_uv_, f_vv_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_PARAM_H
