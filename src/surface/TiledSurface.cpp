//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/TiledSurface.cpp - TiledSurface class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the TIledSurface class implementation.
///
//===----------------------------------------------------------------------===//

#include "src/surface/TiledSurface.h"
#include "src/surface/Domain.h"
#include "src/surface/Param.h"
#include "src/surface/Tiling.h"

namespace kleindrive {

TiledSurface::TiledSurface(const Param& param,
                           const Domain& domain,
                           const Tiling& tiling)
  : Param(param)
  , Domain(domain)
  , Tiling(tiling)
  , uStep_{ uRange() / numberOfTilesInUDirection() }
  , vStep_{ vRange() / numberOfTilesInVDirection() }
  , halfUStep_{ uStep_ / static_cast<phys_t>(2) }
  , halfVStep_{ vStep_ / static_cast<phys_t>(2) }
{}

Color
TiledSurface::colorOfTileByGaussianCurvature(phys_t curvatureColorExaggeration,
                                             int i,
                                             int j,
                                             phys_t t)
{
  static const Color flat = colors::White;
  static const Color positive = colors::Red;
  static const Color negative = colors::Purple;

  const phys_t curvature = gaussianCurvature(tileCenterU(i), tileCenterV(j), t);

  static const phys_t PI = 4 * atan(1);
  const phys_t strength =
    pow(std::abs(atan(curvatureColorExaggeration * curvature) / (PI / 2)), 0.4);

  const Color color = (curvature > 0)
                        ? (affineCombination(positive, flat, strength))
                        : (affineCombination(negative, flat, strength));
  return color;
}

} // namespace kleindrive
