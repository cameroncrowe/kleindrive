//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/TiledSurface.h - TiledSurface class defintion.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the TiledSurface class definitions.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_TILEDSURFACE_H
#define KLEIN_DRIVE_TILEDSURFACE_H

#include "src/collections/Colors.h"
#include "src/surface/Domain.h"
#include "src/surface/Param.h"
#include "src/surface/Tiling.h"
#include "src/types/LinearAlgebra.h"
#include "src/types/ValueTypes.h"

namespace kleindrive {

/// Represents a parameterized surface on a tiled rectangular domain whose edges
/// can glue.
class TiledSurface final
  : public Param
  , public Domain
  , public Tiling
{
public:
  /// \n Constructor.
  /// @{

  /// Constructs a tiled surface from a Param, Domain and a Tiling.
  TiledSurface(const Param& param, const Domain& domain, const Tiling& tiling);

  /// @}

  /// \name Tile Data
  /// @{

  /// Returns the ij-th vertex  at time t.
  inline Vec3 vertex(int i, int j, phys_t t) const
  {
    return f(vertexU(i), vertexV(j), t);
  }

  /// Returns the vector to the center of the ij-th tile at time t.
  inline Vec3 centerOfTile(int i, int j, phys_t t) const
  {
    return f(tileCenterU(i), tileCenterV(j), t);
  }

  /// Returns the normal to the ij-th tile at time t.
  inline Vec3 normalToTile(int i, int j, phys_t t) const
  {
    return f_n(tileCenterU(i), tileCenterV(j), t);
  }

  // TODO ---  NOT IMPLEMENTED
  /// Returns the radius of the ij-th tile at time t.
  inline phys_t radiusOfTile(int, int, phys_t) const { return 1; }

  /// Returns the color of ij-th tile on the TiledSurface.
  inline Color colorOfTile(int i, int j, phys_t t) const
  {
    return Tiling::colorOfTile(i, j, t, this);
  }

  /// Return a color that depends on the Gaussian curvature at the center of the
  /// tile.
  Color colorOfTileByGaussianCurvature(phys_t curvatureColorExaggeration,
                                       int i,
                                       int j,
                                       phys_t t);

  /// Returns the u coordinate of an ij-th vertex for any j.
  inline phys_t vertexU(int i) const { return uMin_ + (uStep_ * i); }

  /// Returns the v coordinate of an ij-th vertex for any i.
  inline phys_t vertexV(int j) const { return vMin_ + (vStep_ * j); }

  // Returns the u coordinate of the ij-th tile for any j.
  inline phys_t tileCenterU(int i) const
  {
    return uMin_ + halfUStep_ + (uStep_ * i);
  }

  // Returns the v coordinate of the ij-th tile for any i.
  inline phys_t tileCenterV(int j) const
  {
    return vMin_ + halfVStep_ + (vStep_ * j);
  }
  /// @}
private:
  /// Width of a tile in the domain in the u direction, using Euclidean metric.
  phys_t uStep_;

  /// Width of a tile in the domain in the v direction, using Euclidean metric.
  phys_t vStep_;

  /// Half uStep_.
  phys_t halfUStep_;

  // Half vStep_;
  phys_t halfVStep_;

};

} // namespace kleindrive

#endif // KLEINDRIVE_TILEDSURFACE_H
