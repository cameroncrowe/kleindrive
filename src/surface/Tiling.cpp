//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/Tiling.h - Tiling class implementation.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Tiling class implementation.
///
//===----------------------------------------------------------------------===//

// Klein Drive
#include "src/surface/Tiling.h"
#include "src/collections/Colors.h"

namespace kleindrive {

Tiling::Tiling(int numberOfTilesInUDirection,
               int numberOfTilesInVDirection,
               AdvancedTileColoring tileColoring)
  : numberOfTilesInUDirection_{ numberOfTilesInUDirection }
  , numberOfTilesInVDirection_{ numberOfTilesInVDirection }
  , tileColoring_{ tileColoring }
{}

Tiling::Tiling(int numberOfTilesInUDirection,
               int numberOfTilesInVDirection,
               TileColoring tileColoring)
  : numberOfTilesInUDirection_{ numberOfTilesInUDirection }
  , numberOfTilesInVDirection_{ numberOfTilesInVDirection }
{
  auto i = std::placeholders::_1;
  auto j = std::placeholders::_2;

  // Add a dummy variable to the tile coloring.
  tileColoring_ = std::bind(tileColoring, i, j);
}

Tiling::Tiling(int numberOfTilesInUDirection, int numberOfTilesInVDirection)
  : numberOfTilesInUDirection_{ numberOfTilesInUDirection }
  , numberOfTilesInVDirection_{ numberOfTilesInVDirection }
{
  tileColoring_ = [](int m[[maybe_unused]],
                     int n[[maybe_unused]],
                     phys_t t[[maybe_unused]],
                     const TiledSurface* tiledSurface[[maybe_unused]]) {
    if (n % 2)
      return colors::White;
    return colors::Purple;
    ;
  };
}

} // namespace kleindrive
