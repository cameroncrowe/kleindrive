//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/surface/Tiling.h - Tiling class definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains the Tiling class definition.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_TILING
#define KLEIN_DRIVE_TILING

#include "src/graphics/Color.h"
#include <functional>

namespace kleindrive {

// The Tiling needs to know about the TiledSurface.
class TiledSurface;

/// Type of a function mapping tiles in a rectangular lattice to colors.
using TileColoring = std::function<Color(int, int)>;

/// Type of a function mapping tiles in a rectangular lattice at time t, and a
/// tiledSurface to colors.
///
/// A request to this function will be fed the surface the function is a part
/// of.
using AdvancedTileColoring =
  std::function<Color(int, int, phys_t, const TiledSurface*)>;

///  Represents a rectangle of painted tiles.
class Tiling
{
public:
  /// \name
  /// @{

  /// Construct a Tiling based on number of tiles in each direction and an
  /// AdvancedTileColoring function.
  Tiling(int numberOfTilesInUDirection,
         int numberOfTilesInVDirection,
         AdvancedTileColoring tileColoring);

  /// Construct a Tiling based on number of tiles in each direction and a
  /// coloring function.
  Tiling(int numberOfTilesInUDirection,
         int numberOfTilesInVDirection,
         TileColoring tileColoring);

  /// Construct a tiling based on the number of tiles in each direction and
  /// color them using a default coloring function.
  Tiling(int numberOfTilesInUDirection, int numberOfTilesInVDirection);

  /// @}

  /// \name Reveals tile-related information.
  /// @{

  /// Returns the number of tiles in the u direction.
  int numberOfTilesInUDirection() const { return numberOfTilesInUDirection_; }

  /// Returns the number of tiles in the v direction
  int numberOfTilesInVDirection() const { return numberOfTilesInVDirection_; }

  /// Returns the total number of tiles.
  int numberOfTiles() const
  {
    return numberOfTilesInUDirection_ * numberOfTilesInVDirection_;
  }

  /// Returns the linear index of a tile, running over the u direction first,
  /// then the v direction.
  inline int indexOfTile(int i, int j) const
  {
    return i + numberOfTilesInUDirection_ * j;
  }

  /// @}

  /// \name Reveals vertex-related information.
  /// @{

  /// Returns the number of vertices in the u direction.
  int numberOfVerticesInUDirection() const
  {
    return numberOfTilesInUDirection_ + 1;
  }

  /// Returns the number of vertices in the v direction.
  int numberOfVerticesInVDirection() const
  {
    return numberOfTilesInVDirection_ + 1;
  }

  /// Returns the total number of vertices.
  int numberOfVertices() const
  {
    return numberOfVerticesInUDirection() * numberOfVerticesInVDirection();
  }

  /// Returns the linear index of a vertex, running over the u direction first,
  /// then the v direction.
  int indexOfVertex(int i, int j) const
  {
    return i + numberOfVerticesInUDirection() * j;
  }

  /// @}

protected:
  /// Returns the color of ij-th tile on the TiledSurface.
  inline Color colorOfTile(int i,
                           int j,
                           phys_t t,
                           const TiledSurface* tiledSurface) const
  {
    return tileColoring_(i, j, t, tiledSurface);
  }

private:
  /// Number of tiles in u direction.
  int numberOfTilesInUDirection_;

  /// Number of tiles in v direction.
  int numberOfTilesInVDirection_;

  /// A coloring function to rule the world.
  AdvancedTileColoring tileColoring_;
};

} // namespace kleindrive

#endif // KLEINDRIVE_TILING
