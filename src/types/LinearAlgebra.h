//==-------------------------------------------------------------*- C++ -*-===//
//
// src/types/LinearAlgebra.h - Linear algebra type definitions.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains type definitions related to linear algebra.
///
/// STL containers containing elements of these types must use Eigen's custom
/// aligned allocator, e.g.:
/// \code
///     std::vector<Vec3, Eigen::aligned_allocator<Vec3>>;
/// \endcode
///
/// Classes and structures using these types should use Eigen's custom aligned
/// new operator:
/// \code
///     class ... {
///        :
///       public:
/// 	    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
///        :
///     };
/// \endcode
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_LINEARALGEBRA_H
#define KLEIN_DRIVE_LINEARALGEBRA_H


#include "src/types/ValueTypes.h"

// Disable all the warnings from Eigen
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wall"
#pragma clang diagnostic ignored "-Wextra"
#pragma clang diagnostic ignored "-Wpedantic"
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wshadow"

#ifdef Success
#undef Success  //X.h of X11 defines this -- ANNOYING!
#endif

#include <Eigen/Dense>
#include <Eigen/StdVector>

#pragma clang diagnostic pop

#include <vector>
namespace kleindrive {

/// \name Column vectors.
/// @{

/// Type of a 2D column vector, with elements of type phys_t.
using Vec2 = Eigen::Matrix<phys_t, 2, 1>;

/// Type of a 3D column vector, with elements of type phys_t.
using Vec3 = Eigen::Matrix<phys_t, 3, 1>;

/// @}

/// \name Matrices.
/// @{

/// Type of a 2-row  by 2-column  matrix.
using Mat22 = Eigen::Matrix<phys_t, 2, 2>;

/// Type of a 3-row by 2-column matrix.
using Mat32 = Eigen::Matrix<phys_t, 3, 2>;

/// Type of a 3-row by 3-column matrix.
using Mat33 = Eigen::Matrix<phys_t, 3, 3>;

/// @}

/// \name Affine transformation.
/// @{

/// Type of an affine transformation on 2D column vectors.
using Aff2 = Eigen::Transform<phys_t, 2, Eigen::Affine>;

/// Type of an affine transformation on 3D column vectors.
using Aff3 = Eigen::Transform<phys_t, 3, Eigen::Affine>;

/// @}

/// \name STL containers containing linear algebraic objects.
/// @{

/// Type of a std::vector of elements of type T, using Eigen's custom aligned
/// allocator.
///
/// T is expected to be one of the linear algebraic types, Vec2, Vec3, etc.
// TODO Staticly assert that T is of an appropriate type.
template<class T>
using EigenAlignedStdVector = std::vector<T, Eigen::aligned_allocator<T>>;

/// @}

} // namespace kleindrive

#endif // KLEIN_DRIVE_LINEARALGEBRA_H
