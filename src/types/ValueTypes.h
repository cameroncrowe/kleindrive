//==-------------------------------------------------------------*- C++ -*-===//
//
//  src/types/ValueTypes.h - Value types definition.
//
//==-----------------------------------------------------------------------===//
//
// Copyright 2018 by Cameron Crowe.
//
// This file is part of Klein Drive.
//
// Klein Drive is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Klein Drive is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Klein Drive.  If not, see <http://www.gnu.org/licenses/>.
//
//===----------------------------------------------------------------------===//
///
/// \file This file contains value types for the quantities used in Klein Drive.
///
//===----------------------------------------------------------------------===//

#ifndef KLEIN_DRIVE_VALUETYPES_H
#define KLEIN_DRIVE_VALUETYPES_H

#include <limits>

namespace kleindrive {

/// Floating point value type used for physical quantities (e.g. distance).
using phys_t = double;

/// Integeral value type used for screen coordinates.
using scrn_t = int;

/// Integral value type used for RGB channel strengths.
using rgb_channel_t = unsigned char;

/// The maximal value for an rgb collor channel value.
static constexpr rgb_channel_t MaxRGBChannelValue =
  std::numeric_limits<rgb_channel_t>::max();

} // namespace kleindrive

// Due to a bad name of a macro (Success) in X11's X.h file, we must include
// Eigen before X.h gets included.  It is almos certain that if one of our files
// includes X.h, then it first pulls src/types/LinearAlgebra.h or
// src/types/ValueTypes.h, so we make sure that both of these undefine Success.

// A BETTER way would be to completely isolate the two libraries from each
// other...

#include "src/types/LinearAlgebra.h"
#endif // KLEIN_DRIVE_VALUETYPES_H
